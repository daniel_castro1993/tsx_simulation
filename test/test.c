#include <stdlib.h>
#include <stdio.h>
#include "collections.h"

#define ints_size 100

int ints[ints_size];

int print_int_to_buffer(void * elem, char * buffer, size_t buffer_size) {
  int* to_print = (int*) elem;

  int wchars = snprintf(buffer, buffer_size, " %i ", *to_print);
  return wchars;
}

bool print_ints_in_list(int index, void* elem, void* param) {
  int* in_list = (int*) elem;

  printf(" %i ", *in_list);
  return false;
}

bool print_ints_in_btree(void* key, void* value, void* param) {
  int* k_num = (int*) key;

  printf(" %i ", *k_num);
  return false;
}

void destroyer(void* elem) {
  free(elem);
}

int comparator(void* a, void* b) {
  int* v_a = (int*) a;
  int* v_b = (int*) b;

  return *v_a - *v_b;
}

void initInts() {
  int i;

  for (i = 0; i < ints_size; i++) {
    ints[i] = i;
  }
}

void test_llist() {
  COL_llist_t* list = COL_LLIST_new();
  int i;

  COL_LLIST_add(list, &ints[0]);
  COL_LLIST_add(list, &ints[1]);
  COL_LLIST_add(list, &ints[2]);
  COL_LLIST_add(list, &ints[3]);

  // 0, 1, 2, 3
  printf("list: [");
  COL_LLIST_iterate(list, print_ints_in_list, NULL);
  printf("]\n");

  // 3, 2, 1, 0
  printf("list: [");
  COL_LLIST_iterate_r(list, print_ints_in_list, NULL);
  printf("]\n");

  COL_it_t it;

  COL_LLIST_it_new(list, &it);

  // 0, 1, 2, 3
  int* in_list = NULL;
  printf("list: [");
  while (in_list = (int*) COL_LLIST_it_next(&it)) {
    printf(" %i ", *in_list);
  }
  printf("]\n");

  COL_LLIST_it_prev(&it); // not in the end

  // 3, 2, 1, 0
  printf("list: [");
  while (in_list = (int*) COL_LLIST_it_prev(&it)) {
    printf(" %i ", *in_list);
  }
  printf("]\n");

  COL_LLIST_it_next(&it);

  // 0, 1, 2, 3
  printf("list: [");
  while (in_list = (int*) COL_LLIST_it_check(&it)) {
    printf(" %i ", *in_list);
    if (COL_LLIST_it_next(&it) == NULL) break;
  }
  printf("]\n");

  COL_LLIST_it_destroy(&it);

  // 0, 1, 2, 3
  COL_LLIST_it_new(list, &it);
  printf("list: [");
  while (in_list = (int*) COL_LLIST_it_check(&it)) {
    printf(" %i ", *in_list);
    COL_LLIST_it_next(&it);
  }
  printf("]\n");
  COL_LLIST_it_destroy(&it);

  COL_LLIST_push(list, &ints[4]);

  // 4, 0, 1, 2, 3
  printf("list: [");
  COL_LLIST_iterate(list, print_ints_in_list, NULL);
  printf("]\n");

  COL_LLIST_pop(list);

  // 0, 1, 2, 3
  printf("list: [");
  COL_LLIST_iterate(list, print_ints_in_list, NULL);
  printf("]\n");

  // 0
  printf("top: %i \n", *((int*) COL_LLIST_top(list)));

  COL_LLIST_clear(list, NULL);

  COL_LLIST_add_sorted(list, comparator, &ints[10]);
  COL_LLIST_add_sorted(list, comparator, &ints[6]);
  COL_LLIST_add_sorted(list, comparator, &ints[14]);
  COL_LLIST_add_sorted(list, comparator, &ints[5]);
  printf("list: [");
  COL_LLIST_iterate(list, print_ints_in_list, NULL);
  printf("]\n");
  printf("list: [");
  COL_LLIST_iterate_r(list, print_ints_in_list, NULL);
  printf("]\n");
  COL_LLIST_add_sorted(list, comparator, &ints[7]);
  COL_LLIST_add_sorted(list, comparator, &ints[18]);
  COL_LLIST_add_sorted(list, comparator, &ints[12]);
  printf("list: [");
  COL_LLIST_iterate(list, print_ints_in_list, NULL);
  printf("]\n");
  printf("list: [");
  COL_LLIST_iterate_r(list, print_ints_in_list, NULL);
  printf("]\n");

  COL_LLIST_clear(list, NULL);

  COL_LLIST_push(list, &ints[rand() % ints_size]);
  COL_LLIST_pop(list);
  for (i = 0; i < 20; i++) {
    COL_LLIST_push(list, &ints[rand() % ints_size]);
  }
  COL_LLIST_pop(list);
  COL_LLIST_pop(list);
  COL_LLIST_push(list, &ints[rand() % ints_size]);

  printf("list: [");
  COL_LLIST_iterate(list, print_ints_in_list, NULL);
  printf("]\n");

  printf("list: [");
  COL_LLIST_iterate_r(list, print_ints_in_list, NULL);
  printf("]\n");

  COL_LLIST_destroy(list, NULL);
}

void test_btree() {
  // B+ tree
  char buffer[4096];
  COL_btree_t* btree = COL_BTREE_new(4, comparator);
  int i;
  COL_BTREE_set_print(btree, print_int_to_buffer, print_int_to_buffer);

  for (i = 0; i < ints_size / 2; i++) {
    COL_BTREE_insert(btree, &ints[i], &ints[i]);
  }

  for (i = ints_size - 1; i > ints_size / 2 - 1; i--) {
    COL_BTREE_insert(btree, &ints[i], &ints[i]);

    //    if (i == 93 || i == 92) {
    //      printf("\n%s\n", COL_BTREE_print(btree, buffer));
    //    }
  }

  printf("\n%s\n", COL_BTREE_print(btree, buffer, 4096));

  printf("search: %i \n", *((int*) COL_BTREE_search(btree, &ints[3])));
  printf("search: %i \n", *((int*) COL_BTREE_search(btree, &ints[10])));
  printf("search: %i \n", *((int*) COL_BTREE_search(btree, &ints[21])));

  COL_BTREE_remove(btree, &ints[20]);
  COL_BTREE_remove(btree, &ints[20]);
  COL_BTREE_remove(btree, &ints[20]);
  printf("remove 20 - %s\n", COL_BTREE_print(btree, buffer, 4096));
  //  
  COL_BTREE_remove(btree, &ints[19]);
  //      printf("remove 19 - %s\n", COL_BTREE_print(btree, buffer));
  COL_BTREE_remove(btree, &ints[18]);
  //    printf("remove 18 - %s\n", COL_BTREE_print(btree, buffer));
  COL_BTREE_remove(btree, &ints[21]);
  printf("remove 21 - %s\n", COL_BTREE_print(btree, buffer, 4096));
  //    COL_BTREE_remove(btree, &ints[22]);
  //    printf("remove 22 - %s\n", COL_BTREE_print(btree, buffer));
  //    COL_BTREE_remove(btree, &ints[23]);
  //    printf("remove 23 - %s\n", COL_BTREE_print(btree, buffer));
  //  
  //    COL_BTREE_remove(btree, &ints[16]);
  //    printf("remove 16 - %s\n", COL_BTREE_print(btree, buffer));
  //    COL_BTREE_remove(btree, &ints[17]);
  //    printf("remove 17 - %s\n", COL_BTREE_print(btree, buffer));
  //    COL_BTREE_remove(btree, &ints[14]);
  //    printf("remove 14 - %s\n", COL_BTREE_print(btree, buffer));
  //    COL_BTREE_remove(btree, &ints[21]);
  //    printf("remove 21 - %s\n", COL_BTREE_print(btree, buffer));
  //    COL_BTREE_remove(btree, &ints[15]);
  //    printf("remove 15 - %s\n", COL_BTREE_print(btree, buffer));
}

//void test_workload() {
//  UTL_application_t* app = UTL_load_application("example.txt");
//}

int main() {
  initInts();

  test_llist();
  //  test_btree();
  //  test_workload();
}
