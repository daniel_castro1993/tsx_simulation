import numpy as np
import matplotlib.pyplot as plt

from os import listdir
from os.path import isfile, join

import re
import json

import matplotlib as mpl
mpl.rcParams['legend.numpoints'] = 1

data_files = [f for f in listdir('.') if isfile(join('.', f)) and f.startswith('data')]

dic_data = {}

for file_name in data_files:
	with open(file_name) as f:
		m = re.search(r'data.(\d+).thr.(\d+).', file_name)
		
		if m == None:
			continue
			
		data = f.read()
		data = data.split('\n')
			
		print('processing:', file_name)
		
		nb_threads = int(m.group(1))
		lines = int(m.group(2))
		
		if lines not in dic_data:
			dic_data[lines] = {}
		if nb_threads not in dic_data[lines]:
			dic_data[lines][nb_threads] = {
				'time_non_tx':		{'total-avg': 0, 'total-sd': 0, 'per-sample': []},
				'time_commit':		{'total-avg': 0, 'total-sd': 0, 'per-sample': []},
				'time_abort':		{'total-avg': 0, 'total-sd': 0, 'per-sample': []},
				'time_spinning':	{'total-avg': 0, 'total-sd': 0, 'per-sample': []},
				'wait_for_serial':	{'total-avg': 0, 'total-sd': 0, 'per-sample': []},
				'time_serial':		{'total-avg': 0, 'total-sd': 0, 'per-sample': []}
			}
		
		i = 1
		nb_samples = 0
		
		while i + nb_threads < len(data):
			# fetch a sample
			time_non_tx = [float(row.split()[5]) for row in data[i:i+nb_threads]]
			time_commit = [float(row.split()[7]) for row in data[i:i+nb_threads]]
			time_abort = [float(row.split()[6]) for row in data[i:i+nb_threads]]
			time_spinning = [float(row.split()[8]) for row in data[i:i+nb_threads]]
			wait_for_serial = [float(row.split()[9]) for row in data[i:i+nb_threads]]
			time_serial = [float(row.split()[10]) for row in data[i:i+nb_threads]]
			
			dic_data[lines][nb_threads]['time_non_tx']['per-sample'] += [{
				'avg': np.average(time_non_tx),
				'sd': np.std(time_non_tx)
#				'se': np.std(time_non_tx) / np.sqrt(nb_threads)
			}]
			dic_data[lines][nb_threads]['time_commit']['per-sample'] += [{
				'avg': np.average(time_commit),
				'sd': np.std(time_commit)
#				'se': np.std(time_commit) / np.sqrt(nb_threads)
			}]
			dic_data[lines][nb_threads]['time_abort']['per-sample'] += [{
				'avg': np.average(time_abort),
				'sd': np.std(time_abort)
#				'se': np.std(time_abort) / np.sqrt(nb_threads)
			}]
			dic_data[lines][nb_threads]['time_spinning']['per-sample'] += [{
				'avg': np.average(time_spinning),
				'sd': np.std(time_spinning)
#				'se': np.std(time_spinning) / np.sqrt(nb_threads)
			}]
			dic_data[lines][nb_threads]['wait_for_serial']['per-sample'] += [{
				'avg': np.average(wait_for_serial),
				'sd': np.std(wait_for_serial)
#				'se': np.std(wait_for_serial) / np.sqrt(nb_threads)
			}]
			dic_data[lines][nb_threads]['time_serial']['per-sample'] += [{
				'avg': np.average(time_serial),
				'sd': np.std(time_serial)
#				'se': np.std(time_serial) / np.sqrt(nb_threads)
			}]
			
			nb_samples += 1
			i += nb_threads
			
		avg_time_non_tx = [float(s['avg']) for s in dic_data[lines][nb_threads]['time_non_tx']['per-sample']]
		avg_time_commit = [float(s['avg']) for s in dic_data[lines][nb_threads]['time_commit']['per-sample']]
		avg_time_abort = [float(s['avg']) for s in dic_data[lines][nb_threads]['time_abort']['per-sample']]
		avg_time_spinning = [float(s['avg']) for s in dic_data[lines][nb_threads]['time_spinning']['per-sample']]
		avg_wait_for_serial = [float(s['avg']) for s in dic_data[lines][nb_threads]['wait_for_serial']['per-sample']]
		avg_time_serial = [float(s['avg']) for s in dic_data[lines][nb_threads]['time_serial']['per-sample']]
		
		dic_data[lines][nb_threads]['time_non_tx']['total-avg'] = np.average(avg_time_non_tx)
		dic_data[lines][nb_threads]['time_non_tx']['total-sd'] = np.std(avg_time_non_tx)
#		dic_data[prob_abort][nb_threads]['time_non_tx']['total-se'] = np.std(avg_time_non_tx) / np.sqrt(nb_samples)
		
		dic_data[lines][nb_threads]['time_commit']['total-avg'] = np.average(avg_time_commit)
		dic_data[lines][nb_threads]['time_commit']['total-sd'] = np.std(avg_time_commit)
#		dic_data[prob_abort][nb_threads]['time_commit']['total-se'] = np.std(avg_time_commit) / np.sqrt(nb_samples)
		
		dic_data[lines][nb_threads]['time_abort']['total-avg'] = np.average(avg_time_abort)
		dic_data[lines][nb_threads]['time_abort']['total-sd'] = np.std(avg_time_abort)
#		dic_data[prob_abort][nb_threads]['time_abort']['total-se'] = np.std(avg_time_abort) / np.sqrt(nb_samples)
		
		dic_data[lines][nb_threads]['time_spinning']['total-avg'] = np.average(avg_time_spinning)
		dic_data[lines][nb_threads]['time_spinning']['total-sd'] = np.std(avg_time_spinning)
#		dic_data[prob_abort][nb_threads]['time_spinning']['total-se'] = np.std(avg_time_spinning) / np.sqrt(nb_samples)
		
		dic_data[lines][nb_threads]['wait_for_serial']['total-avg'] = np.average(avg_wait_for_serial)
		dic_data[lines][nb_threads]['wait_for_serial']['total-sd'] = np.std(avg_wait_for_serial)
#		dic_data[prob_abort][nb_threads]['wait_for_serial']['total-se'] = np.std(avg_wait_for_serial) / np.sqrt(nb_samples)
		
		dic_data[lines][nb_threads]['time_serial']['total-avg'] = np.average(avg_time_serial)
		dic_data[lines][nb_threads]['time_serial']['total-sd'] = np.std(avg_time_serial)
#		dic_data[prob_abort][nb_threads]['time_serial']['total-se'] = np.std(avg_time_serial) / np.sqrt(nb_samples)

processed_data = open("data_processed.json", "w+")
processed_data.write(json.dumps(dic_data, sort_keys=True, indent=4, separators=(',', ': ')))

pas = set()
thrs = {}

for pa in dic_data:
	pas.add(pa)
	thrs[pa] = set()
	for nb_thr in dic_data[pa]:
		thrs[pa].add(nb_thr)
		

for pa in sorted(pas):
	threads = []
	avg_thr = {
		'non_tx':		[],
		'commit':		[],
		'abort':		[],
		'spinning':		[],
		'wait_serial':	[],
		'serial':		[]
	}
	sd_thr = {
		'non_tx':		[],
		'commit':		[],
		'abort':		[],
		'spinning':		[],
		'wait_serial':	[],
		'serial':		[]
	}
	title = "Prob abort = " + str(pa) + "%; Time Commit=20, Time non-trans=10"
	fig = plt.figure()
	ax1 = fig.add_subplot(111)
	for nb_thr in sorted(thrs[pa]):
		threads += [nb_thr]
		avg_thr['non_tx'] += [float(dic_data[pa][nb_thr]['time_non_tx']['total-avg'])]
		sd_thr['non_tx'] += [float(dic_data[pa][nb_thr]['time_non_tx']['total-sd'])]
		avg_thr['commit'] += [float(dic_data[pa][nb_thr]['time_commit']['total-avg'])]
		sd_thr['commit'] += [float(dic_data[pa][nb_thr]['time_commit']['total-sd'])]
		avg_thr['abort'] += [float(dic_data[pa][nb_thr]['time_abort']['total-avg'])]
		sd_thr['abort'] += [float(dic_data[pa][nb_thr]['time_abort']['total-sd'])]
		avg_thr['spinning'] += [float(dic_data[pa][nb_thr]['time_spinning']['total-avg'])]
		sd_thr['spinning'] += [float(dic_data[pa][nb_thr]['time_spinning']['total-sd'])]
		avg_thr['wait_serial'] += [float(dic_data[pa][nb_thr]['wait_for_serial']['total-avg'])]
		sd_thr['wait_serial'] += [float(dic_data[pa][nb_thr]['wait_for_serial']['total-sd'])]
		avg_thr['serial'] += [float(dic_data[pa][nb_thr]['time_serial']['total-avg'])]
		sd_thr['serial'] += [float(dic_data[pa][nb_thr]['time_serial']['total-sd'])]
		
	
	ax1.set_title(title)  
	ax1.set_xlabel('number of threads')
	ax1.set_ylabel('time')
	
	threads = [t - 0.15 for t in threads]
	x = np.array(threads)
	y = np.array(avg_thr['non_tx'])
	e = np.array(sd_thr['non_tx'])
	plt.errorbar(x, y, e, marker='.', label='non tx')
	
	threads = [t + 0.05 for t in threads]
	x = np.array(threads)
	y = np.array(avg_thr['commit'])
	e = np.array(sd_thr['commit'])
	plt.errorbar(x, y, e, marker='.', label='commit')
	
	threads = [t + 0.05 for t in threads]
	x = np.array(threads)
	y = np.array(avg_thr['abort'])
	e = np.array(sd_thr['abort'])
	plt.errorbar(x, y, e, marker='.', label='abort')
	
	threads = [t + 0.05 for t in threads]
	x = np.array(threads)
	y = np.array(avg_thr['spinning'])
	e = np.array(sd_thr['spinning'])
	plt.errorbar(x, y, e, marker='.', label='spinning')
	
	threads = [t + 0.05 for t in threads]
	x = np.array(threads)
	y = np.array(avg_thr['wait_serial'])
	e = np.array(sd_thr['wait_serial'])
	plt.errorbar(x, y, e, marker='.', label='wait serial')
	
	threads = [t + 0.05 for t in threads]
	x = np.array(threads)
	y = np.array(avg_thr['serial'])
	e = np.array(sd_thr['serial'])
	plt.errorbar(x, y, e, marker='.', label='serial')
	
	plt.subplots_adjust(top=0.8)
	leg = ax1.legend(loc='upper center', bbox_to_anchor=(0.5, 1.3),
          fancybox=True, shadow=True, ncol=3)
	
	fig.savefig('./data_' + str(pa) + 'pa.png')
	plt.close(fig)
	
	plt.clf()
	plt.cla()
			
# first line are the headers and the last is empty
#time = [float(row.split(' ')[0]) for row in data[1:-1]]
#active = [float(row.split(' ')[1]) for row in data[1:-1]]
#txs = [float(row.split(' ')[2]) for row in data[1:-1]]
#non_txs = [float(row.split(' ')[3]) for row in data[1:-1]]
#serial = [float(row.split(' ')[4]) for row in data[1:-1]]
#spinning = [float(row.split(' ')[5]) for row in data[1:-1]]

#fig = plt.figure()

#ax1 = fig.add_subplot(111)

#ax1.set_title("transactional system")    
#ax1.set_xlabel('time')
#ax1.set_ylabel('number of threads')

#ax1.plot(time, txs, c='g', label='active transactions')
#ax1.plot(time, non_txs, c='b', label='active non transaction')
#ax1.plot(time, serial, c='y', label='active serial')
#ax1.plot(time, spinning, c='r', label='spinning threads')

#leg = ax1.legend()

#plt.show()
