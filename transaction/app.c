// System includes
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <ROOT-Sim.h>
#include "stats.h"

// Project includes
#include "app.h"

#define PLOT_FILE_NAME    "data"
#define PLOT_FILE_EXTE    ".txt"
#define EVENT_SIZE        sizeof(event_cnt_t)

#define CRITICAL_VALUE    1.645 // confidence 90%

#define INIT_COMPONENT(_STATE_T, _PID, _EVENT, _NEXT_TICK) ({                 \
  UTL_event_t __event;                                                        \
  _STATE_T * __state = new(_STATE_T, 1);                                      \
  set_zero(__state, _STATE_T, 1);                                             \
  SetState(__state);                                                          \
  UTL_event_init(&__event);                                                   \
  __event.pid = _PID;                                                         \
  __event.event_code = _EVENT;                                                \
  SCHEDULE_EVENT(__event, _NEXT_TICK);                                        \
  __state;                                                                    \
})

// Global variables
FILE * plot_file = NULL;
#ifdef __DEBUG_CACHES__
FILE * caches_file = NULL;
#endif

float def_prob_read = 0;
float def_prob_tran = 1;
float def_C = 1.4;
float def_D = 1000;
float def_L = 20;
float def_budget = 2;
float def_op_cost = 0.005;

float def_lat_pc_r = 0.0001;
float def_lat_pc_w = 0.0002;
float def_lat_sc_r = 0.001;
float def_lat_sc_w = 0.002;
float def_lat_mem_r = 0.01;
float def_lat_mem_w = 0.02;

float def_lat_pc_txbg = 0.0001;
float def_lat_pc_txab = 0.0001;
float def_lat_pc_txcm = 0.0001;

float def_lat_lock = 0.01;
float def_lat_unlock = 0.01;

float def_lat_spin = 0.01;

bool * sample_taken = NULL;
int nb_resets = 0, nb_sample = 0;
bool is_retrying = false, done_sampling = false;

HTM_thr_stats_t * stats = NULL;

void ProcessEvent(unsigned int me, simtime_t now, int event_type,
        event_cnt_t * buffer, unsigned int size, void * state) {

  int i, j, k, nb_thrs;
  simtime_t next_tick = now + DELTA_TIME;

  switch (event_type) {
    case INIT:

      if (n_prc_tot < 6 || (n_prc_tot & 1)) {
        printf(
                "Use a number of LPs > 5!                                 \n"
                "number of LPs must satisfy -> 2*n + 4,                   \n"
                "where n is the number of threads for the simulation.     \n"
                "Given number of LPs: %i                                  \n",
                n_prc_tot);
        exit(EXIT_FAILURE);
      } else {

        if (!global_info.is_init) {
          global_info.is_init = true;

          printf("Given number of LPs: %u; number of threads: %u \n",
                  n_prc_tot, (n_prc_tot - 4) / 2);

          nb_thrs = (n_prc_tot - 4) / 2;

          global_info.prob_read = IsParameterPresent(buffer, "PROB_READ") ?
                  GetParameterDouble(buffer, "PROB_READ")
                  : def_prob_read;

          global_info.prob_tran = IsParameterPresent(buffer, "PROB_TRAN") ?
                  GetParameterDouble(buffer, "PROB_TRAN")
                  : def_prob_tran;

          global_info.C = IsParameterPresent(buffer, "C") ?
                  GetParameterDouble(buffer, "C")
                  : def_C;

          global_info.D = IsParameterPresent(buffer, "D") ?
                  GetParameterInt(buffer, "D")
                  : def_D;

          global_info.L = IsParameterPresent(buffer, "L") ?
                  GetParameterInt(buffer, "L")
                  : def_L;

          global_info.budget = IsParameterPresent(buffer, "BUDGET") ?
                  GetParameterInt(buffer, "BUDGET")
                  : def_budget;

          global_info.op_cost = IsParameterPresent(buffer, "OP_COST") ?
                  GetParameterDouble(buffer, "OP_COST")
                  : def_op_cost;

          global_info.lat_pc_r = IsParameterPresent(buffer, "LAT_PC_R") ?
                  GetParameterDouble(buffer, "LAT_PC_R")
                  : def_lat_pc_r;
          global_info.lat_pc_w = IsParameterPresent(buffer, "LAT_PC_W") ?
                  GetParameterDouble(buffer, "LAT_PC_W")
                  : def_lat_pc_w;

          global_info.lat_sc_r = IsParameterPresent(buffer, "LAT_SC_R") ?
                  GetParameterDouble(buffer, "LAT_SC_R")
                  : def_lat_sc_r;
          global_info.lat_sc_w = IsParameterPresent(buffer, "LAT_SC_W") ?
                  GetParameterDouble(buffer, "LAT_SC_W")
                  : def_lat_sc_w;

          global_info.lat_mem_r = IsParameterPresent(buffer, "LAT_MEM_R") ?
                  GetParameterDouble(buffer, "LAT_MEM_R")
                  : def_lat_mem_r;
          global_info.lat_mem_w = IsParameterPresent(buffer, "LAT_MEM_W") ?
                  GetParameterDouble(buffer, "LAT_MEM_W")
                  : def_lat_mem_w;

          global_info.lat_lock = IsParameterPresent(buffer, "LAT_LOCK") ?
                  GetParameterDouble(buffer, "LAT_LOCK")
                  : def_lat_mem_r;
          global_info.lat_unlock = IsParameterPresent(buffer, "LAT_UNLOCK") ?
                  GetParameterDouble(buffer, "LAT_UNLOCK")
                  : def_lat_mem_w;

          global_info.lat_pc_txbg = IsParameterPresent(buffer, "LAT_BEGIN") ?
                  GetParameterDouble(buffer, "LAT_BEGIN")
                  : def_lat_pc_txbg;
          global_info.lat_pc_txab = IsParameterPresent(buffer, "LAT_ABORT") ?
                  GetParameterDouble(buffer, "LAT_ABORT")
                  : def_lat_pc_txab;
          global_info.lat_pc_txcm = IsParameterPresent(buffer, "LAT_COMMIT") ?
                  GetParameterDouble(buffer, "LAT_COMMIT")
                  : def_lat_pc_txcm;

          global_info.lat_spin = IsParameterPresent(buffer, "LAT_SPIN") ?
                  GetParameterDouble(buffer, "LAT_SPIN")
                  : def_lat_spin;

          global_info.tot_nb_thrs = nb_thrs;
          global_info.tot_nb_pcs = nb_thrs;
          global_info.sc_pid = 2 * nb_thrs;
          global_info.mem_pid = 2 * nb_thrs + 1;
          global_info.cpu_pid = 2 * nb_thrs + 2;
          global_info.lock_pid = 2 * nb_thrs + 3;

          printf(" =================== HTM Simulation =================== \n");
          printf(" > > parameters                                         \n"
                  "          D: %10i                                      \n"
                  "          L: %10i                                      \n"
                  "     BUDGET: %10i                                      \n"
                  "  PROB_READ: %10f                                      \n"
                  "  PROB_TRAN: %10f                                      \n"
                  "    OP_COST: %10f                                      \n"
                  "   LAT_PC_R: %10f                                      \n"
                  "   LAT_PC_W: %10f                                      \n"
                  "   LAT_SC_R: %10f                                      \n"
                  "   LAT_SC_W: %10f                                      \n"
                  "  LAT_MEM_R: %10f                                      \n"
                  "  LAT_MEM_W: %10f                                      \n"
                  "   LAT_LOCK: %10f                                      \n"
                  " LAT_UNLOCK: %10f                                      \n"
                  "  LAT_BEGIN: %10f                                      \n"
                  "  LAT_ABORT: %10f                                      \n"
                  " LAT_COMMIT: %10f                                      \n"
                  "   LAT_SPIN: %10f                                      \n",
                  global_info.D, global_info.L, global_info.budget,
                  global_info.prob_read, global_info.prob_tran,
                  global_info.op_cost, global_info.lat_pc_r,
                  global_info.lat_pc_w, global_info.lat_sc_r,
                  global_info.lat_sc_w, global_info.lat_mem_r,
                  global_info.lat_mem_w,
                  global_info.lat_lock, global_info.lat_unlock,
                  global_info.lat_pc_txbg, global_info.lat_pc_txab,
                  global_info.lat_pc_txcm, global_info.lat_spin);
          printf(" ====================================================== \n");

          global_info.pcs_lines = new(HTM_pc_line_t*, global_info.tot_nb_pcs);

          if (global_info.thrs_pids == NULL) {
            global_info.thrs_pids = new(int, nb_thrs);
            global_info.pcs_pids = new(int, nb_thrs);
            for (i = 0; i < nb_thrs; i++) {
              global_info.thrs_pids[i] = i;
              global_info.pcs_pids[i] = nb_thrs + i;
              INIT_LINES((nb_thrs + i), j, k);
            }
          }
        }
      }

#ifdef __DEBUG_FILE__
      __OPEN_DEBUG_FILE;
#endif

      if (me < global_info.tot_nb_thrs) {
        // init the threads
        INIT_COMPONENT(HTM_thr_state_t, me, THR_INIT, next_tick);

      } else if (me < 2 * global_info.tot_nb_pcs) {
        // init the private caches
        INIT_COMPONENT(HTM_pc_state_t, me, PC_INIT, next_tick);

      } else if (me == global_info.sc_pid) {
        // init the shared cache
        INIT_COMPONENT(HTM_sc_state_t, me, SC_INIT, next_tick);

      } else if (me == global_info.mem_pid) {
        // init the memory
        INIT_COMPONENT(HTM_mem_state_t, me, MEM_INIT, next_tick);

      } else if (me == global_info.cpu_pid) {
        // init the cpu
        INIT_COMPONENT(HTM_cpu_state_t, me, CPU_INIT, next_tick);

      } else {
        // init the lock
        INIT_COMPONENT(HTM_lock_state_t, me, LCK_INIT, next_tick);
      }

      if (plot_file == NULL) {
        char file_name[128];

        sprintf(file_name, "%s-%i_thr_%i_lines%s", PLOT_FILE_NAME, n_prc_tot,
                N_LINES_PCACHE, PLOT_FILE_EXTE);

        plot_file = fopen(file_name, "w+");
        fprintf(plot_file, " SAMPLE THR_ID TIME_SAMPLE FIXED_TC FIXED_NTX "
                "TIME_NON_TRANSAC TIME_ABORT TIME_COMMIT TIME_SPINNING "
                "TIME_SERIAL_WAITING TIME_SERIAL SUM_NOTX_TX\n");
      }
      break;
    default:
      HTM_state_handler(state, me, event_type, now, buffer);
      break;
  }
}

bool OnGVT(unsigned int me, void * state) {

  int i, j, k, p, sum_rel, tot_events;
  HTM_thr_state_t * thr_state;
  bool all_samples_taken;

  if (global_info.is_exit) {
    return true;
  }

  if (me < global_info.tot_nb_thrs) {
    // it is a thread, fetch its data

    thr_state = (HTM_thr_state_t*) state;

    if (stats == NULL) {
      stats = new_init(HTM_thr_stats_t, global_info.tot_nb_thrs);
      for (i = 0; i < global_info.tot_nb_thrs; ++i) {
        stats[i].budget_at_commit = new(int, STATS_SAMPLE);
        stats[i].ops_before_abort = new(int, STATS_SAMPLE);
        BEGIN_FOR_STAT(k);
        stats[i].stat_time[k] = new(simtime_t, STATS_SAMPLE);
        END_FOR_STAT;
      }
    }
    if (sample_taken == NULL) {
      sample_taken = new_init(bool, global_info.tot_nb_thrs);
    }

    if (thr_state->stats.budget_at_commit == NULL
            || thr_state->stats.ops_before_abort == NULL) {

      // still not initialized
      return false;
    }

    memcpy(stats[me].budget_at_commit, thr_state->stats.budget_at_commit,
            STATS_SAMPLE * sizeof (int));
    memcpy(stats[me].ops_before_abort, thr_state->stats.ops_before_abort,
            STATS_SAMPLE * sizeof (int));
    memcpy(stats[me].stats_pointer, thr_state->stats.stats_pointer,
            NB_STATS * sizeof (int));
    memcpy(stats[me].counter, thr_state->stats.counter,
            NB_STATS * sizeof (int));

    sum_rel = 0;
    BEGIN_FOR_STAT(k);
    memcpy(stats[me].stat_time[k], thr_state->stats.stat_time[k],
            STATS_SAMPLE * sizeof (simtime_t));

    if (k == NOTX || k == TXCM || k == TXAB || k == SERL) {
      sum_rel += thr_state->stats.counter[k];
    }
    END_FOR_STAT;

    if ((sum_rel / 4) > SAMPLE_SIZE) {
      // start stopping the simulation
      sample_taken[me] = true;
    }

    all_samples_taken = true;
    for (i = 0; i < global_info.tot_nb_thrs; ++i) {
      all_samples_taken = all_samples_taken && sample_taken[i];
    }

    if (all_samples_taken) {
      // write the stats

      double avg_times[NB_STATS][global_info.tot_nb_thrs];
      double avg_ops_before_abort[global_info.tot_nb_thrs];
      double avg_budget_at_commit[global_info.tot_nb_thrs];
      double tot_avg_times[NB_STATS];

      double sq_sd_times[NB_STATS][global_info.tot_nb_thrs];
      double sq_sd_ops_before_abort[global_info.tot_nb_thrs];
      double sq_sd_budget_at_commit[global_info.tot_nb_thrs];
      double tot_sq_sd_times[NB_STATS];
      int count_stats[NB_STATS];

      double tot_avg_budget_at_commit;
      double tot_avg_ops_before_abort;

      double tot_sq_sd_budget_at_commit;
      double tot_sq_sd_ops_before_abort;

      set_zero(count_stats, int, NB_STATS);

      for (i = 0; i < global_info.tot_nb_thrs; ++i) {

        p = stats[i].counter[TXAB] > stats[i].stats_pointer[TXAB] ?
          STATS_SAMPLE : stats[i].stats_pointer[TXAB];
        avg_ops_before_abort[i] = CALC_AVG(stats[i].ops_before_abort, p);
        sq_sd_ops_before_abort[i] = CALC_SQ_STDDEV(
                stats[i].ops_before_abort, p, avg_ops_before_abort[i]);

        p = stats[i].counter[TXCM] > stats[i].stats_pointer[TXCM] ?
          STATS_SAMPLE : stats[i].stats_pointer[TXCM];
        avg_budget_at_commit[i] = CALC_AVG(stats[i].budget_at_commit, p);
        sq_sd_budget_at_commit[i] = CALC_SQ_STDDEV(
                stats[i].budget_at_commit, p, avg_budget_at_commit[i]);

        BEGIN_FOR_STAT(k);
        p = stats[i].counter[k] > stats[i].stats_pointer[k] ?
          STATS_SAMPLE : stats[i].stats_pointer[k];
        if (stats[i].counter[k] > STATS_SAMPLE) {
          p = STATS_SAMPLE;
        }
        avg_times[k][i] = CALC_AVG(stats[i].stat_time[k], p);
        sq_sd_times[k][i] = CALC_SQ_STDDEV(stats[i].stat_time[k], p,
                avg_times[k][i]);

        count_stats[k] += stats[i].counter[k];

        END_FOR_STAT;
      }

      BEGIN_FOR_STAT(k);
      tot_events += count_stats[k];
      END_FOR_STAT;

      printf("============================================================ \n");

      BEGIN_FOR_STAT(k);
      tot_avg_times[k] = CALC_AVG(avg_times[k], global_info.tot_nb_thrs);
      tot_sq_sd_times[k] = CALC_AVG(sq_sd_times[k], global_info.tot_nb_thrs);

      printf(" %11s (TIME): %9.3e ? %9.3e  (TOTAL EVENTS: %9i / %7.3f%%)\n",
              STAT_STR(k), tot_avg_times[k], sqrt(tot_sq_sd_times[k]),
              count_stats[k], (double) count_stats[k] / (double) tot_events
              * 100.0);

      END_FOR_STAT;

      tot_avg_budget_at_commit = CALC_AVG(avg_budget_at_commit,
              global_info.tot_nb_thrs);
      tot_sq_sd_budget_at_commit = CALC_AVG(sq_sd_budget_at_commit,
              global_info.tot_nb_thrs);

      tot_avg_ops_before_abort = CALC_AVG(avg_ops_before_abort,
              global_info.tot_nb_thrs);
      tot_sq_sd_ops_before_abort = CALC_AVG(sq_sd_ops_before_abort,
              global_info.tot_nb_thrs);

      printf("   BUDGET_AT_COMMIT: %9.3e ? %9.3e \n",
              tot_avg_budget_at_commit,
              sqrt(tot_sq_sd_budget_at_commit));

      printf("   OPS_BEFORE_ABORT: %9.3e ? %9.3e \n",
              tot_avg_ops_before_abort,
              sqrt(tot_sq_sd_ops_before_abort));

      printf("============================================================ \n");


      printf("\nstate distribution\n");
      mod_probFromAcc();
      mod_printTopProbs(stdout, 10);

      global_info.is_exit = true;
      return true;
    }
  }

  return false;
}

