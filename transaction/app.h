#ifndef __APP_H_GUARD
#  define __APP_H_GUARD

#  include <stdbool.h>
#  include "collections.h"
#  include "htm.h"
#  include "utils.h"

#  define SAMPLE_SIZE           2000

// This is the events' payload which is exchanged across LPs

typedef struct _event_content_type {
  int none;
} event_cnt_t;

// LP simulation state

typedef struct _lp_state {
  simtime_t last_ts;
  HTM_thr_state_t state;
} lp_state_t;


#endif // define __APP_H_GUARD
