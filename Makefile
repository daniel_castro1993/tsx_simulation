lib_folder = lib
lock_folder = lock
cache_folder = caches

build:
	cd $(lib_folder) && $(MAKE) build
	cd $(lock_folder) && $(MAKE) build
	cd $(cache_folder) && $(MAKE) build

clean:
	cd $(lib_folder) && $(MAKE) clean
	cd $(lock_folder) && $(MAKE) clean
	cd $(cache_folder) && $(MAKE) clean
