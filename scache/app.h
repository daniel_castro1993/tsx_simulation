#ifndef __APP_H_GUARD
#  define __APP_H_GUARD

#  include <stdbool.h>
#  include "collections.h"
#  include "utils.h"
#  include "htm.h"

typedef enum _EVENTS {
  EVENT_READ = 1,
  EVENT_WRITE,
  EVENT_END
} EVENTS;

typedef struct _event_content_type {
  int addr;
} evt_cnt_t;

typedef struct _lp_state {
  bool is_end, already_locked;
  simtime_t begin, end;
} lp_state_t;

#endif // define __APP_H_GUARD
