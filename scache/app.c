/**
 * Lock simulation.
 * 
 * Simulates multiple processes grabbing a global lock. Some processes will have
 * to wait for the previous one to unlock.
 */

#include "app.h"

#define _INC_I(_I) (_I += 100)

int ended_prcs = 0;

void EVENT_init(unsigned int me, simtime_t now) {

  int i;

  if (n_prc_tot < 3) {
    printf("Use at least 3 LPs\n");
    exit(EXIT_SUCCESS);
  }

#ifdef __DEBUG_FILE__
  __OPEN_DEBUG_FILE;
#endif

  if (!global_info.is_init) {
    global_info.is_init = true;
    global_info.tot_nb_thrs = n_prc_tot - 2;
    global_info.thrs_pids = new(int, global_info.tot_nb_thrs);
    global_info.sc_pid = 0;
    global_info.mem_pid = 1;

    for (i = 0; i < global_info.tot_nb_thrs; i++) {
      global_info.thrs_pids[i] = i + 2;
    }
  }

  if (me == 0) {
    // scache pid
    printf("[TS%4.0f]: init scache at pid: %2u\n", now, me);
    HTM_sc_state_t * state;
    NEW_SC_STATE(state);

    SetState(state);
    ScheduleNewEvent(me, now + DELTA_TIME, SC_INIT, NULL, 0);

  } else if (me == 1) {
    // memory pid
    printf("[TS%4.0f]: init memory at pid: %2u\n", now, me);
    HTM_mem_state_t * state;
    NEW_MEM_STATE(state);

    SetState(state);
    ScheduleNewEvent(me, now + DELTA_TIME, MEM_INIT, NULL, 0);

  } else {
    // thread pid
    printf("[TS%4.0f]: init thread at pid: %2u\n", now, me);

    lp_state_t * state;
    evt_cnt_t buffer;
    int size = sizeof (evt_cnt_t);

    state = new(lp_state_t, 1);

    set_zero(state, lp_state_t, 1);
    set_zero(&buffer, evt_cnt_t, 1);

    SetState(state);

    i = 0;
    
    buffer.addr = 0;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_READ, &buffer, size);

    buffer.addr = 1;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_READ, &buffer, size);

    buffer.addr = 0;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_WRITE, &buffer, size);

    buffer.addr = 2;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_WRITE, &buffer, size);

    buffer.addr = 4;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_READ, &buffer, size);

    buffer.addr = 6;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_READ, &buffer, size);

    buffer.addr = 8;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_READ, &buffer, size);

    buffer.addr = 10;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_READ, &buffer, size);

    buffer.addr = 12;
    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_READ, &buffer, size);

    ScheduleNewEvent(me, me + now + _INC_I(i), EVENT_END, NULL, 0);
  }
}

void schedule_scache_event(int pid, unsigned int event_code, int addr,
    simtime_t now) {

  UTL_event_t next_event;
  HTM_sc_buffer_t sc_buffer;
  UTL_callback_t callback;

  set_zero(&sc_buffer, HTM_sc_buffer_t, 1);

  callback.pid = pid;
  callback.event_code = PC_SCRP;

  sc_buffer.addr = addr;
  sc_buffer.callback = callback;
  sc_buffer.pc_pid = pid;

  UTL_event_init(&next_event);

  next_event.pid = global_info.lock_pid;
  next_event.event_code = event_code;
  next_event.cnt = &sc_buffer;
  next_event.cnt_size = sizeof (HTM_sc_buffer_t);

  SCHEDULE_EVENT(next_event, now + DELTA_TIME);
}

/**
 * Processes a event for a given process.
 * 
 * @param me
 * @param now
 * @param event_type
 * @param event_content
 * @param size
 * @param ptr
 */
void ProcessEvent(unsigned int me, simtime_t now, int event,
    void * buffer, unsigned int size, void * state) {

  bool all_ended, success;
  int i, addr;
  simtime_t lat;
  lp_state_t * lp_state;
  evt_cnt_t * evt_buffer;
  UTL_callback_t callback;
  HTM_pc_buffer_t * pc_buffer;

  switch (event) {
    case INIT:
      EVENT_init(me, now);
      break;
    case EVENT_READ:

      lp_state = (lp_state_t*) state;
      evt_buffer = (evt_cnt_t*) buffer;

      addr = evt_buffer->addr;

      lp_state->begin = now;
      printf("[TS%4.0f]: scache read \n", now);
      schedule_scache_event(me, SC_READ, addr, now);

      break;
    case EVENT_WRITE:

      lp_state = (lp_state_t*) state;
      evt_buffer = (evt_cnt_t*) buffer;

      addr = evt_buffer->addr;

      lp_state->begin = now;
      printf("[TS%4.0f]: scache write \n", now);
      schedule_scache_event(me, SC_WRIT, addr, now);

      break;

    case PC_SCRP:
      // Must implement the PC_SCRP event (this is the response of the scache)

      pc_buffer = (HTM_pc_buffer_t*) buffer;
      lp_state = (lp_state_t*) state;
      lat = now - lp_state->begin;
      
      printf("[TS%4.0f][me%2u]: scache response [lat=%0.0f,acc_lat=%0.0f,"
          "begin=%0.0f]\n", now, me, lat, pc_buffer->acc_lat, lp_state->begin);

      break;
    case EVENT_END:

      lp_state = (lp_state_t*) state;
      lp_state->is_end = true;

      printf("[TS%4.0f]: thread EVENT_END \n", now);

      ScheduleNewEvent(me, now + DELTA_TIME, EVENT_END, NULL, 0);
      break;
    default:
      HTM_state_handler(state, me, event, now, buffer);
      break;
  }

}

/**
 * Checks the simulation.
 * 
 * @param me the process id
 * @param state the current state
 * @return true to terminate the me process
 */
bool OnGVT(unsigned int me, lp_state_t * state) {
  // the lock never ends

  bool thr_ended = false;

  if (me > global_info.mem_pid) { // one of the threads
    if (state->is_end) {
      thr_ended = true;
      printf("process %2u finished \n", me);

      HTM_OnGVT(thr_ended);

      return true;
    }
  } else if (HTM_OnGVT_check_exit()) {
    // the lock is the last
#ifdef __DEBUG_FILE__
    __CLOSE_DEBUG_FILE;
#endif
    return true;
  }

  return false;
}

