#ifndef MC_FORMULAS_H
#define MC_FORMULAS_H

#include "htm_common.h"

#include <vector>
#include <iostream>
#include <functional>

using namespace std;

namespace htm
{

    class mc_formulas
    {
    public:

        int _threads, _budget, _L, _D;
        double _W, _C, _PROB_READ;

        function<double(mc_state&, mc_matrix&) > me;
        function<double(mc_state&, mc_matrix&) > mf;
        function<double(mc_state&, mc_matrix&) > mn;
        // dangerous xacts abort
        function<double(mc_state&, mc_matrix&) > m1;
        // non-dangerous xacts abort
        function<double(mc_state&, mc_matrix&) > m2;
        // xacts commit
        function<double(mc_state&, mc_matrix&) > m3;

        function<double(mc_state&, mc_matrix&) > p1;
        function<double(mc_state&, mc_matrix&) > p2;
        function<double(mc_state&, mc_matrix&) > p3;
        function<double(mc_state&, mc_matrix&) > pe;
        function<double(mc_state&, mc_matrix&) > pt;

        mc_formulas(int threads, int budget, double prob_read,
                    double c, int l, int d);
        mc_formulas(const mc_formulas& orig);
        virtual ~mc_formulas();

        double compute_lambda(mc_state&, mc_matrix&);
        double compute_H_i(mc_state&, mc_matrix&, int nb_inst);
        double compute_P_R_L(mc_state&, mc_matrix&);
        void compute_P_R_L(vector<double>& p_r, mc_state&, mc_matrix&);
        void compute_P_R_L(vector<double>& p_r, mc_state&, mc_matrix&, double);
        double compute_R(mc_state&, mc_matrix&);

        double compute_H_i_d(mc_state&, mc_matrix&, int nb_inst);
        double compute_P_R_L_d(mc_state&, mc_matrix& matrix);
        void compute_P_R_L_d(vector<double>& p_r, mc_state&, mc_matrix&);
        double compute_R_d(mc_state&, mc_matrix&);

        double compute_H_i_n(mc_state&, mc_matrix&, int nb_inst);
        double compute_P_R_L_n(mc_state&, mc_matrix&);
        void compute_P_R_L_n(vector<double>& p_r, mc_state&, mc_matrix&);
        double compute_R_n(mc_state&, mc_matrix&);

        double compute_P_1(mc_state&, mc_matrix&);
        double compute_R_1(mc_state&, mc_matrix& matrix);

        double compute_P_2(mc_state&, mc_matrix&);
        double compute_R_2(mc_state&, mc_matrix&);

        double compute_P_3(mc_state&, mc_matrix&);
        double compute_R_3(mc_state&, mc_matrix&);

        double compute_R_star(mc_matrix& matrix);
        double compute_X(mc_matrix& matrix);
        double compute_P_A(mc_matrix& matrix);

        double getC() const;
        int getL() const;
        double getProbRead() const;
        int getBudget() const;
        int getThreads() const;
    };
}

#endif /* MC_FORMULAS_H */

