
#ifndef MC_TRANSITION_CASE_H
#define MC_TRANSITION_CASE_H

#include "htm_common.h"

#include <list>

using namespace std;

namespace htm
{

	class mc_transition_case
	{
	public:
		mc_transition_case(mc_matrix& matrix, mc_formulas& formulas);
		mc_transition_case(const mc_transition_case& orig);
		virtual ~mc_transition_case();

		virtual double rateCase(mc_state& inState, mc_state& outState);
		virtual void outStatesCase(mc_state& inState, list<mc_state*>& outStates);

	protected:
		mc_matrix& _matrix;
		mc_formulas& _formulas;

		bool isInList(mc_state* state, list<mc_state*>& outStates);
		bool isRateValid(mc_state& inState, mc_state& outState);
		void addToList(mc_state& inState, mc_state& outState, mc_state* state,
			list<mc_state*>& states);
	} ;
}

#endif /* MC_TRANSITION_CASE_H */

