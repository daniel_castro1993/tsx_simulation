#ifndef CASES_H
#define CASES_H

#include "cases/case1.h"
#include "cases/case2.h"
#include "cases/case3.h"
#include "cases/case4.h"
#include "cases/case5.h"
#include "cases/case6.h"
#include "cases/case7.h"
#include "cases/case8.h"
#include "cases/case9.h"
#include "cases/case10.h"
#include "cases/case11.h"
#include "cases/case12.h"

#endif /* CASES_H */

