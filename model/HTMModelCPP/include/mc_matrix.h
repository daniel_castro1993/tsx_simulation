#ifndef MC_MATRIX_H
#define MC_MATRIX_H

#include <omp.h>
#include <utility>
#include <list>
#include <vector>
#include <string>
#include <iostream>

#include <Eigen/SparseQR>

#include "htm_common.h"

#ifndef MDL_PREC
#define MDL_PREC double
#endif
#ifndef __ARMADILLO__
#define MDL_MATX SparseMatrix<MDL_PREC, ColMajor>
#define MDL_VECT Matrix<MDL_PREC, Dynamic, 1>
#else
// TODO: add support to armadillo
#define MDL_MATX SpMat<MDL_PREC>
#define MDL_VECT SpMat<MDL_PREC>
#endif

using namespace Eigen;
using namespace std;

namespace htm
{

	class mc_matrix
	{
	public:
		mc_matrix(mc_formulas& formulas);
		mc_matrix(const mc_matrix& orig);
		virtual ~mc_matrix();

		void setCoeff(int row, int col, MDL_PREC value);
		void insert(int row, int col, MDL_PREC value);
		/**
		 * Sets the diagonal of the matrix as -Sum(row)
		 */
		void setDiag();
		/**
		 * Same as setDiag(). Expects that the diagonal is not set.
		 */
		void insertDiag();
		void resize(int rows, int cols);
		void clear();

		void computeSolution();
		void filterSolution();

		double avg_R();
		double avg_P_R_L();

		void setVecStates(vector<mc_state*>& vecStates);

		vector<pair<mc_state*, double> > getTopStates();

		bool isFirstIteration();

		double prevR(mc_state& st);
		double prevP_R_L(mc_state& st);

		double prevR_d(mc_state& st);
		double prevP_R_L_d(mc_state& st);

		double prevR_n(mc_state& st);
		double prevP_R_L_n(mc_state& st);

		vector<mc_state*> getVecStates() const;

		void printTopStates(ostream& os, int nb_top_states);

		mc_matrix& operator=(const mc_matrix& right);

	private:

		int _rows, _cols;

		mc_formulas& _formulas;
		bool _firstIteration;
		int _iterations;

		MDL_MATX _matrix;
		vector<mc_state*> _vecStates;

		MDL_VECT _solAugmt;

		vector<int> _top_state_idx_aug;
		vector<int> _bot_state_idx_aug;

		vector<double> _top_state_prob_aug;
		vector<double> _bot_state_prob_aug;

		vector<double> _prev_R, _prev_P_R_L;
		vector<double> _prev_R_d, _prev_P_R_L_d;
		vector<double> _prev_R_n, _prev_P_R_L_n;

#ifdef __CMP_SOLVERS__
		MDL_VECT _solRowRep;

		vector<int> _top_state_idx_row;
		vector<int> _bot_state_idx_row;

		vector<double> _top_state_prob_row;
		vector<double> _bot_state_prob_row;
#endif

		void resetVars();

	} ;
}

#endif /* MC_MATRIX_H */

