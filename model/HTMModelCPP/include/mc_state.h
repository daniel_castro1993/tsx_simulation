#ifndef MC_STATE_H
#define MC_STATE_H

#include <iostream>
#include <string>
#include <vector>

using namespace std;

namespace htm
{

	class mc_state
	{
	public:

		mc_state(int threads, int budget);
		mc_state(const mc_state& orig);
		virtual ~mc_state();

		int& operator[](int index) ;
		ostream& printState(ostream& os);

		/**
		 * Shifts all the states to the right (decreases the budget by 1).
		 * 
		 * @return number of transaction in the fall back.
		 */
		int shiftXActsRight();

		/**
		 * Moves a thread in a given budget right (decreases the budget by 1).
		 * 
		 * @param budget_thread
		 * @return the number of threads in the given budget (greater than 0)
		 */
		int xActsAborts(int budget_thread);

		/**
		 * Moves a thread from the given budget to non-transactions.
		 * 
		 * @param budget_from
		 * @return number of threads with the given budget
		 */
		int XActStartsNonTransac(int budget_from);

		/**
		 * Moves a thread from non-transactions to the maximum budget.
		 * 
		 * @return the number of non-transactions
		 */
		int nonTransacStartsXAct();

		/**
		 * Puts all the threads at the given budget. Pass 0 to fall-back path.
		 * 
		 * @param budget the budget to fill (between the maximum budget and 0,
		 * inclusive)
		 */
		void fillBudget(int budget);

		/**
		 * Puts all the threads at non-transaction.
		 */
		void fillNonTransac();

		int getThreadsWithBudget(int budget);
		int getThreadsNonTransac();
		int getThreadsXActs();
		int getThreadsXActsDangerous();
		int getThreadsXActsNonDangerous();
		int getCurrentXActs();
		int getCurrentXActsDangerous();
		int getCurrentXActsNonDangerous();
		bool someXActFallback();
		int getThreadsFallback();

		bool operator==(mc_state& state);
		bool operator>(mc_state& state);
		bool operator<(mc_state& state);

	private:
		int _threads, _budget, _size;
		vector<int> _state;
	} ;

}

#endif /* MC_STATE_H */

