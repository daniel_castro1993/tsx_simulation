#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"

#include <vector>
#include <list>

#ifndef MC_TRANSITION_H
#define MC_TRANSITION_H

namespace htm
{

	class mc_transition
	{
	public:

		mc_transition(mc_matrix& matrix, mc_formulas& formulas);
		virtual ~mc_transition();

		/**
		 * Computes the rate having in account the 12 cases.
		 * 
		 */
		void computeTransitions();


		/**
		 * Computes all the states having in account the 12 cases. The
		 * states are appended in the end of the list.
		 * 
		 * @param startFillXActs true to start in state [T 0 ... 0] false to start
		 * in state [0 ... 0 T]
		 */
		void computeStates(bool startFillXActs);

		void setMatrix(mc_matrix& matrix);
		void setFormulas(mc_formulas& formulas);

		vector<mc_state*>& getVecStates();

	private:

		mc_matrix& _matrix;
		mc_formulas& _formulas;
		list<mc_transition_case*> _transitions;
		list<mc_state*> _states;
		vector<mc_state*> _vecStates;

	} ;
}

#endif /* MC_TRANSITION_H */

