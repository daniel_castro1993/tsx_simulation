#ifndef CASE9_H
#define CASE9_H

#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

namespace htm
{

	class case9 : public mc_transition_case
	{
	public:
		case9(mc_matrix& matrix, mc_formulas& formulas);
		case9(const case9& orig);
		virtual ~case9();

		/**
		 * Case where a thread finishes the fall-back path and starts a new
		 * transactional code block.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_1, t_0, n]
		 * @param outState [t_R + 1, ..., t_i, ..., t_1, t_0 - 1, n]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread finishes the fall-back path and starts a new
		 * transactional code block.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;

	private:

	} ;
}

#endif /* CASE9_H */

