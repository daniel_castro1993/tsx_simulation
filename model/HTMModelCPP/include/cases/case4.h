#ifndef CASE4_H
#define CASE4_H

#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

namespace htm
{

	class case4 : public mc_transition_case
	{
	public:
		case4(mc_matrix& matrix, mc_formulas& formulas);
		case4(const case4& orig);
		virtual ~case4();

		/**
		 * Case where a thread with only one retry left finishes a transactional
		 * code block and starts a new one.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_1, 0, n]
		 * @param outState [t_R + 1, ..., t_i, ..., t_1 - 1, 0, n]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread with only one retry left finishes a transactional
		 * code block and starts a new one.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;

	private:

	} ;
}

#endif /* CASE4_H */

