#ifndef CASE12_H
#define CASE12_H

#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

namespace htm
{

	class case12 : public mc_transition_case
	{
	public:
		case12(mc_matrix& matrix, mc_formulas& formulas);
		case12(const case12& orig);
		virtual ~case12();

		/**
		 * Case where a thread with only one retry left aborts due to a
		 * capacity exception and falls back.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_1, 0, n]
		 * @param outState [0, t_R, ..., t_i, ..., t_2, t_1, n]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread with only one retry left aborts due to a
		 * capacity exception and falls back.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;

	private:

	} ;

}


#endif /* CASE12_H */

