#ifndef CASE1_H
#define CASE1_H

#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

namespace htm
{

	class case1 : public mc_transition_case
	{
	public:
		case1(mc_matrix& matrix, mc_formulas& formulas);
		case1(const case1& orig);
		virtual ~case1();

		/**
		 * Case where a thread finishes a non-transactional code block and starts
		 * a new one.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_0, n]
		 * @param outState [t_R, ..., t_i, ..., t_0, n]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread finishes a non-transactional code block and starts
		 * a new one.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;

	private:

	} ;
}

#endif /* CASE1_H */

