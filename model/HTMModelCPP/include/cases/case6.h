#ifndef CASE6_H
#define CASE6_H

#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

namespace htm
{

	class case6 : public mc_transition_case
	{
	public:
		case6(mc_matrix& matrix, mc_formulas& formulas);
		case6(const case6& orig);
		virtual ~case6();

		/**
		 * Case where a thread with only one retry left finishes a transactional
		 * code block and starts a non-transactional code block.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_1, 0, n]
		 * @param outState [t_R, ..., t_i, ..., t_1 - 1, 0, n + 1]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread with only one retry left finishes a transactional
		 * code block and starts a non-transactional code block.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;
	private:

	} ;
}

#endif /* CASE6_H */

