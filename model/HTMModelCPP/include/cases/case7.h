#ifndef CASE7_H
#define CASE7_H

#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

namespace htm
{

	class case7 : public mc_transition_case
	{
	public:
		case7(mc_matrix& matrix, mc_formulas& formulas);
		case7(const case7& orig);
		virtual ~case7();

		/**
		 * Case where a thread with more than 1 retries left aborts due to a
		 * conflict and decrements its budget.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_1, 0, n]
		 * @param outState [t_R, ..., t_i - 1, t_{i-1} + 1, ..., t_1, 0, n]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread with more than 1 retries left aborts due to a
		 * conflict and decrements its budget.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;

	private:

	} ;

}
#endif /* CASE7_H */

