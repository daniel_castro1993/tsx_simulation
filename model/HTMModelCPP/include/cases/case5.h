#ifndef CASE5_H
#define CASE5_H

#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

namespace htm
{

	class case5 : public mc_transition_case
	{
	public:
		case5(mc_matrix& matrix, mc_formulas& formulas);
		case5(const case5& orig);
		virtual ~case5();

		/**
		 * Case where a thread with more than 1 retries left finishes a
		 * transactional code block and starts a non-transactional code block.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_1, 0, n]
		 * @param outState [t_R, ..., t_i - 1, ..., t_1, 0, n + 1]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread with more than 1 retries left finishes a
		 * transactional code block and starts a non-transactional code block.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;

	private:

	} ;
}

#endif /* CASE5_H */

