#ifndef CASE2_H
#define CASE2_H

#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

namespace htm
{

	class case2 : public mc_transition_case
	{
	public:
		case2(mc_matrix& matrix, mc_formulas& formulas);
		case2(const case2& orig);
		virtual ~case2();

		/**
		 * Case where a thread finishes a non-transactional code block and starts
		 * a transactional code block.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_0, n]
		 * @param outState [t_R + 1, ..., t_i, ..., t_0, n - 1]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread finishes a non-transactional code block and starts
		 * a transactional code block.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;

	private:

	} ;
}

#endif /* CASE2_H */

