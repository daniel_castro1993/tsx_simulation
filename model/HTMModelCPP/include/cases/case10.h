#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"
#include "mc_transition_case.h"
#include <list>

#ifndef CASE10_H
#define CASE10_H

namespace htm
{

	class case10 : public mc_transition_case
	{
	public:
		case10(mc_matrix& matrix, mc_formulas& formulas);
		case10(const case10& orig);
		virtual ~case10();

		/**
		 * Case where a thread finishes the fall-back path and starts a new
		 * non-transactional code block.
		 * 
		 * @param inState [t_R, ..., t_i, ..., t_1, t_0, n]
		 * @param outState [t_R, ..., t_i, ..., t_1, t_0 - 1, n + 1]
		 * @return the rate of the transition
		 */
		double rateCase(mc_state& inState, mc_state& outState) override;

		/**
		 * Case where a thread finishes the fall-back path and starts a new
		 * non-transactional code block.
		 * 
		 * @param inState any valid state
		 * @param outStates list to store the possible output states
		 */
		void outStatesCase(mc_state& inState, list<mc_state*>& outStates) override;

	private:

	} ;
}

#endif /* CASE10_H */

