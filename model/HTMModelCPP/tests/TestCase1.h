#ifndef TESTCASE1_H
#define TESTCASE1_H

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "mc_formulas.h"
#include "mc_matrix.h"
#include "mc_state.h"

class TestCase1 : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE(TestCase1);

	CPPUNIT_TEST(testOutStates);

	CPPUNIT_TEST_SUITE_END();

public:
	TestCase1();
	virtual ~TestCase1();
	void setUp();
	void tearDown();

private:

	htm::mc_formulas _formulas;
	htm::mc_matrix _matrix;

	void testOutStates();

} ;

#endif /* TESTCASE1_H */

