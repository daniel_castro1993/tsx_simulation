#ifndef TESTCASE2_H
#define TESTCASE2_H

#include <cppunit/TestCase.h>
#include <cppunit/TestFixture.h>
#include <cppunit/ui/text/TextTestRunner.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/XmlOutputter.h>
#include <netinet/in.h>

#include "mc_formulas.h"
#include "mc_matrix.h"
#include "mc_state.h"

class TestCase2 : public CPPUNIT_NS::TestFixture
{
	CPPUNIT_TEST_SUITE(TestCase2);

	CPPUNIT_TEST(testOutStates);

	CPPUNIT_TEST_SUITE_END();

public:
	TestCase2();
	virtual ~TestCase2();
	void setUp();
	void tearDown();

private:

	htm::mc_formulas _formulas;
	htm::mc_matrix _matrix;

	void testOutStates();

} ;

#endif /* TESTCASE2_H */

