#include "TestCase2.h"

#include "cases/case1.h"
#include <list>

CPPUNIT_TEST_SUITE_REGISTRATION(TestCase2);

TestCase2::TestCase2()
: _matrix(), _formulas(4, 2, 0.0, 1.0, 10, 5000)
{
}

TestCase2::~TestCase2()
{
}

void TestCase2::setUp()
{
}

void TestCase2::tearDown()
{
}

void TestCase2::testOutStates()
{
	htm::case2 case2_test(_matrix, _formulas);
	list<htm::mc_state*> states;
	int threads = _formulas.GetThreads(), budget = _formulas.GetBudget();

	htm::mc_state inState(threads, budget);
	htm::mc_state expected(threads, budget);
	htm::mc_state* addState;

	inState.fillNonTransac();

	expected.fillNonTransac();
	expected.nonTransacStartsXAct();

	addState = new htm::mc_state(inState);
	states.push_back(addState);

	case2_test.outStatesCase(inState, states);

	CPPUNIT_ASSERT(states.size() == 2);
	CPPUNIT_ASSERT(*(states.pop_back()) == expected);
}

