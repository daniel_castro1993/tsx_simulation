#include "TestCase1.h"

#include "cases/case1.h"
#include <list>

CPPUNIT_TEST_SUITE_REGISTRATION(TestCase1);

TestCase1::TestCase1()
: _matrix(), _formulas(4, 2, 0.0, 1.0, 10, 5000)
{
}

TestCase1::~TestCase1()
{
}

void TestCase1::setUp()
{

}

void TestCase1::tearDown()
{
}

void TestCase1::testOutStates()
{
	htm::case1 case1_test(_matrix, _formulas);
	list<htm::mc_state*> states;
	int threads = _formulas.getThreads(), budget = _formulas.getBudget();

	htm::mc_state inState(threads, budget);
	htm::mc_state* addState;

	inState.fillNonTransac();
	addState = new htm::mc_state(inState);
	states.push_back(addState);

	case1_test.outStatesCase(inState, states);

	CPPUNIT_ASSERT(states.size() == 1);
}

