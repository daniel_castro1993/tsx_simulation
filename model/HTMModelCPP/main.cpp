#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <float.h>
#include <algorithm>
#include <iterator>
#include <vector>

#include "cmath"
#include "mc_formulas.h"
#include "mc_transition.h"

#ifndef NB_THREADS
#define NB_THREADS 4
#endif

#define CONVERGENCE_THRESHOLD 1e-30
#define CONVERGENCE_COUNT 90

FILE * data_file;
FILE * markov_data_file;

typedef struct _convergence_t
{
    double R, P_R, P_state[3];
    htm::mc_state * state[3];
} convergence_t;

#ifndef DATA_FILE
#define DATA_FILE       ({                                                    \
    char __buffer[512];                                                       \
    sprintf(__buffer, "./data/ana-PR_%.2lf-C_%.2lf-L_%i-D_%i-T_%i-B_%i.txt",  \
      _pr, C, L, D, _threads, _budget);                                       \
    mkdir("data", S_IRWXU);                                                   \
    data_file == NULL ? data_file = fopen(__buffer, DATA_FILE_MODE)           \
      : data_file;                                                            \
    data_file;                                                                \
  })
#endif
#ifndef DATA_FILE_MODE
#define DATA_FILE_MODE  "w"
#endif
#ifndef START_DATA_FILE
#define START_DATA_FILE  ({                                                   \
    fseek(DATA_FILE, 0, SEEK_END);                                            \
    if(ftell(DATA_FILE) < 8) {                                                \
      fprintf(DATA_FILE,                                                      \
        "%11s,%11s,%11s,%11s,%11s,%11s,%15s,%15s,%15s,%15s,%15s\n",           \
        "PROB_READ", "D", "L", "C", "THREADS", "BUDGET",                      \
        "ANA_P_R(L)", "ANA_R", "ANA_X", "ANA_R*", "ANA_P_AB");                \
    }                                                                         \
  })
#endif

#ifndef MARKOV_CHAIN_FILE
#define MARKOV_CHAIN_FILE ({                                                  \
    char __buffer[512];                                                       \
    sprintf(__buffer, "./data/mc-ana-PR_%.2lf-C_%.2lf-L_%i-D_%i-T_%i"         \
      "-B_%i.txt", _pr, C, L, D, _threads, _budget);                          \
    mkdir("data", S_IRWXU);                                                   \
    markov_data_file == NULL                                                  \
      ? markov_data_file = fopen(__buffer, MARKOV_CHAIN_FILE_MODE)            \
      : markov_data_file;                                                     \
    markov_data_file;                                                         \
  })
#endif
#ifndef MARKOV_CHAIN_FILE_MODE
#define MARKOV_CHAIN_FILE_MODE  "w"
#endif
#ifndef START_MARKOV_CHAIN_FILE
#define START_MARKOV_CHAIN_FILE  ({                                           \
    fseek(MARKOV_CHAIN_FILE, 0, SEEK_END);                                    \
    if(ftell(MARKOV_CHAIN_FILE) < 8) {                                        \
      fprintf(MARKOV_CHAIN_FILE, "%30s, %15s\n", "STATE", "PROB");            \
    }                                                                         \
  })
#endif

using namespace std;

int _threads = -1;
int _budget = -1;

double _pr = 0; // prob. read

double C = 1.2;
int L = 15;
int D = 1000;

double _pt = 1; // prob. transaction
double _pa = 0.01; // prob. conflict (not used)
double _pe = 0; // prob. capacity exception

double _mt = 1; // transac. rate (not used)
double _ma = 1;
double _mf = 1; // (not used)
double _mn = 1; // (not used)
double _me = 1;

int main(int argc, char** argv)
{

    int i = 1;

#ifdef __XACTS_FULL_LIFE__
    printf("__XACTS_FULL_LIFE__\n");
#elif defined(__XACTS_HALF_LIFE__)
    printf("__XACTS_HALF_LIFE__\n");
#else
    printf("__XACTS_FULL_LIFE__ and __XACTS_HALF_LIFE__ disabled\n");
#endif

    while (i < argc) {

        if (strcmp(argv[i], "C") == 0) {
            C = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "L") == 0) {
            L = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "D") == 0) {
            D = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "PT") == 0) {
            _pt = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "PA") == 0) {
            _pa = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "PE") == 0) {
            _pe = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "PR") == 0) { // prob. read
            _pr = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "PW") == 0) { // prob. write
            _pr = 1.0 - atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "MT") == 0) {
            _mt = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "MA") == 0) {
            _ma = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "MF") == 0) {
            _mf = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "MN") == 0) {
            _mn = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "ME") == 0) {
            _me = atof(argv[i + 1]);
        }
        else if (strcmp(argv[i], "BUDGET") == 0) {
            _budget = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "THREADS") == 0) {
            _threads = atoi(argv[i + 1]);
        }
        i += 2;
    }

    if (_budget == -1 || _threads == -1) {
        std::cerr << "Call this program as follows: \n"
                << "model THREADS <nb_threads> BUDGET <nb_retries> \n";
        return 1;
    }

    //  Eigen::initParallel();
    //
    //  omp_set_num_threads(NB_THREADS);
    //  Eigen::setNbThreads(NB_THREADS);

    cout << "======================= Model Parameters =======================\n";
    cout << " threads: " << _threads << "\n";
    cout << "  budget: " << _budget << "\n";
    cout << "       D: " << D << "\n";
    cout << "       C: " << C << "\n";
    cout << "       L: " << L << "\n";
    cout << "      PT: " << _pt << "\n";
    cout << "      PW: " << 1.0 - _pr << "\n";
    //  cout << "Multithreaded execution with openMP: " << NB_THREADS << " threads\n";
    cout << "================================================================\n";

    htm::mc_formulas formulas(_threads, _budget, _pr, C, L, D);
    htm::mc_matrix matrix(formulas);

    formulas.me = [] (htm::mc_state& st, htm::mc_matrix mat) -> double
    {
        return _me;
    };
    formulas.mf = [] (htm::mc_state& st, htm::mc_matrix mat) -> double
    {
        return 1.0 / C;
    };
    formulas.mn = [] (htm::mc_state& st, htm::mc_matrix mat) -> double
    {
        return 1.0 / C;
    };
    formulas.m1 = [&formulas] (htm::mc_state& st, htm::mc_matrix mat)
            -> double
            {
                double P_1 = formulas.compute_P_1(st, mat);
                double P_2 = formulas.compute_P_2(st, mat);
                double P_3 = formulas.compute_P_3(st, mat);
                double R_1 = formulas.compute_R_1(st, mat);
                double R_2 = formulas.compute_R_2(st, mat);
                double R_3 = formulas.compute_R_3(st, mat);
                return P_1 / (P_1 * R_1 + P_2 * R_2 + R_3 * P_3);
            };
    formulas.m2 = [&formulas] (htm::mc_state& st, htm::mc_matrix mat)
            -> double
            {
                double P_1 = formulas.compute_P_1(st, mat);
                double P_2 = formulas.compute_P_2(st, mat);
                double P_3 = formulas.compute_P_3(st, mat);
                double R_1 = formulas.compute_R_1(st, mat);
                double R_2 = formulas.compute_R_2(st, mat);
                double R_3 = formulas.compute_R_3(st, mat);
                return P_2 / (P_1 * R_1 + P_2 * R_2 + R_3 * P_3);
            };
    formulas.m3 = [&formulas] (htm::mc_state& st, htm::mc_matrix mat)
            -> double
            {
                double P_1 = formulas.compute_P_1(st, mat);
                double P_2 = formulas.compute_P_2(st, mat);
                double P_3 = formulas.compute_P_3(st, mat);
                double R_1 = formulas.compute_R_1(st, mat);
                double R_2 = formulas.compute_R_2(st, mat);
                double R_3 = formulas.compute_R_3(st, mat);
                return P_3 / (P_1 * R_1 + P_2 * R_2 + R_3 * P_3);
            };

    formulas.pe = [] (htm::mc_state& st, htm::mc_matrix mat) -> double
    {
        return _pe;
    };

    formulas.pt = [] (htm::mc_state& st, htm::mc_matrix mat) -> double
    {
        return _pt;
    };

    formulas.p1 = [&formulas] (htm::mc_state& st, htm::mc_matrix & mat)
            -> double
            {
                return formulas.compute_P_1(st, mat);
            };
    formulas.p2 = [&formulas] (htm::mc_state& st, htm::mc_matrix & mat)
            -> double
            {
                return formulas.compute_P_2(st, mat);
            };
    formulas.p3 = [&formulas] (htm::mc_state& st, htm::mc_matrix & mat)
            -> double
            {
                return formulas.compute_P_3(st, mat);
            };

    htm::mc_transition transitions(matrix, formulas);

    transitions.computeStates(true);
    transitions.computeTransitions();

    matrix.computeSolution();
    matrix.filterSolution();

    cout << "Top states: \n";
    matrix.printTopStates(cout, 25);

    double X = formulas.compute_X(matrix);
    double R_star = formulas.compute_R_star(matrix);
    double P_A = formulas.compute_P_A(matrix);

    cout << "\nThroughput: " << X << "\n"
            << "R*: " << R_star << "\n";
    cout << "P_a: " << P_A << " \n";

    //  START_MARKOV_CHAIN_FILE;
    //  model.solToFile(MARKOV_CHAIN_FILE);
    //  fclose(MARKOV_CHAIN_FILE);

    START_DATA_FILE;
    fprintf(DATA_FILE, "%11.2f,%11u,%11u,%11.2f,%11u,%11u,",
            _pr, D, L, C, _threads, _budget);
    fprintf(DATA_FILE, "%15.8e,%15.8e,", matrix.avg_P_R_L(), matrix.avg_R());
    fprintf(DATA_FILE, "%15.8e,%15.8e,%15.8e\n", X, R_star, P_A);
    fclose(DATA_FILE);

    return 0;
}
