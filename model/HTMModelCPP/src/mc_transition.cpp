#include "mc_transition.h"
#include "cases.h"

using namespace htm;

mc_transition::mc_transition(mc_matrix& matrix, mc_formulas& formulas)
: _matrix(matrix), _formulas(formulas), _vecStates(0)
{

	_transitions.push_back(new case1(_matrix, _formulas));
	_transitions.push_back(new case2(_matrix, _formulas));
	_transitions.push_back(new case3(_matrix, _formulas));
	_transitions.push_back(new case4(_matrix, _formulas));
	_transitions.push_back(new case5(_matrix, _formulas));
	_transitions.push_back(new case6(_matrix, _formulas));
	_transitions.push_back(new case7(_matrix, _formulas));
	_transitions.push_back(new case8(_matrix, _formulas));
	_transitions.push_back(new case9(_matrix, _formulas));
	_transitions.push_back(new case10(_matrix, _formulas));
	_transitions.push_back(new case11(_matrix, _formulas));
	_transitions.push_back(new case12(_matrix, _formulas));

}

mc_transition::~mc_transition()
{
	list<mc_transition_case*>::iterator it1;
	list<mc_state*>::iterator it2;

	for (it1 = _transitions.begin(); it1 != _transitions.end(); ++it1) {
		delete *it1;
	}
	for (it2 = _states.begin(); it2 != _states.end(); ++it2) {
		delete *it2; // also deletes the pointer in _vecStates
	}
}

void mc_transition::computeTransitions()
{

	int i, j;

	_matrix.clear();

	for (i = 0; i < _vecStates.size(); ++i) {
		for (j = 0; j < _vecStates.size(); ++j) {
			if (i != j) {
				mc_state inState(*(_vecStates[i]));
				mc_state outState(*(_vecStates[j]));
				list<mc_transition_case*>::iterator it_trans = _transitions.begin();
				double rate = 0;
				while (it_trans != _transitions.end()) {
					rate += (*it_trans)->rateCase(inState, outState);
					++it_trans;
				}
				if (rate != 0) {
					_matrix.insert(i, j, rate);
				}
			}
		}
	}

	_matrix.insertDiag();
}

void mc_transition::computeStates(bool startFillXActs)
{

	list<mc_state*>::iterator it_states;
	int threads = _formulas.getThreads(), budget = _formulas.getBudget();
	mc_state* state = new mc_state(threads, budget);

	if (startFillXActs) {
		state->fillBudget(budget);
	} else {
		state->fillNonTransac();
	}

	if (!_states.empty()) {
		for (it_states = _states.begin(); it_states != _states.end(); ++it_states) {
			delete *it_states;
		}
		_states.clear();
	}

	_states.push_back(state);

	for (it_states = _states.begin(); it_states != _states.end(); ++it_states) {
		mc_state* ptrState = *it_states;
		list<mc_transition_case*>::iterator it_trans = _transitions.begin();
		while (it_trans != _transitions.end()) {
			mc_state inState(*ptrState);
			(*it_trans)->outStatesCase(inState, _states);
			++it_trans;
		}
	}

	// shuffle?

	_vecStates.clear();
	_vecStates.insert(_vecStates.begin(), _states.begin(), _states.end());

	_matrix.setVecStates(_vecStates);
}

void mc_transition::setMatrix(mc_matrix& matrix)
{
	_matrix = matrix;
}

void mc_transition::setFormulas(mc_formulas& formulas)
{
	_formulas = formulas;
}

vector<mc_state*>& mc_transition::getVecStates()
{
	return _vecStates;
}

