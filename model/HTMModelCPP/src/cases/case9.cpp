#include "cases/case9.h"

using namespace htm;

case9::case9(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case9::case9(const case9& orig) : mc_transition_case(orig)
{
}

case9::~case9()
{
}

static bool checkStates(int budget, mc_state& inState, mc_state& outState)
{
	// inState  [t_R    , ..., t_i, ..., t_1, t_0    , n]
	// outState [t_R + 1, ..., t_i, ..., t_1, t_0 - 1, n]
	return (inState.getThreadsFallback()
		== (outState.getThreadsFallback() + 1))
		&& ((inState.getThreadsWithBudget(budget) + 1)
		== outState.getThreadsWithBudget(budget));
}

double case9::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();

	if (checkStates(budget, inState, outState)) {
		res = _formulas.mf(inState, _matrix) * _formulas.pt(inState, _matrix);
	}

	return res;
}

void case9::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state outState(inState);
	mc_state* storeState;
	int budget = _formulas.getBudget();

	if (inState.getThreadsFallback() > 0) {

		outState[budget]--;
		outState[0]++;

		storeState = new mc_state(outState);

		addToList(inState, outState, storeState, outStates);
	}
}

