#include "cases/case4.h"

using namespace htm;

case4::case4(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case4::case4(const case4& orig) : mc_transition_case(orig)
{
}

case4::~case4()
{
}

static bool checkStates(int budget, mc_state& inState, mc_state& outState)
{
	// inState  [t_R    , ..., t_i, ..., t_1    , 0, n]
	// outState [t_R + 1, ..., t_i, ..., t_1 - 1, 0, n]
	int inFullBudget = inState.getThreadsWithBudget(budget);
	int outFullBudget = outState.getThreadsWithBudget(budget);
	int inDang = inState.getThreadsXActsDangerous();
	int outDang = outState.getThreadsXActsDangerous();

	return inFullBudget != outFullBudget
		&& inState.getThreadsFallback() == 0
		&& outState.getThreadsFallback() == 0
		&& ((((inFullBudget + 1) == outFullBudget)
		&& (inDang == (outDang + 1))) || budget == 1);
}

double case4::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();

	if (checkStates(budget, inState, outState)) {
		double nb_thrs = _formulas.getThreads();
		double nonXActs = inState.getThreadsNonTransac();

		double m3 = _formulas.m3(inState, _matrix);
		double pt = _formulas.pt(inState, _matrix);
		res = inState[budget - 1] * m3 * pt / (nb_thrs - nonXActs);
	}

	return res;
}

void case4::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state* storeState;
	int budget = _formulas.getBudget();

	if (!inState.someXActFallback()) {
		mc_state outState(inState);

		if (outState[budget - 1] > 0) {

			outState[budget - 1]--;
			outState[0]++;

			storeState = new mc_state(outState);

			addToList(inState, outState, storeState, outStates);
		}
	}
}
