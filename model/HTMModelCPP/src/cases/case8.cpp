#include "cases/case8.h"

using namespace htm;

case8::case8(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case8::case8(const case8& orig) : mc_transition_case(orig)
{
}

case8::~case8()
{
}

static bool checkStates(int budget, mc_state& inState, mc_state& outState)
{
	// inState  [   t_R, ..., t_i, ..., t_1, 0  , n]
	// outState [0, t_R, ..., t_i, ..., t_2, t_1, n]
	bool res = true;
	int i;

	if (outState[0] != 0) {
		return false;
	}

	for (i = 1; i < budget + 1; ++i) {
		if (inState[i - 1] != outState[i]) {
			res = false;
			break;
		}
	}

	return res
		&& inState.getThreadsNonTransac() == outState.getThreadsNonTransac()
		&& inState.getThreadsFallback() == 0
		&& outState.getThreadsFallback() != 0;
}

double case8::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();

	if (checkStates(budget, inState, outState)) {
		res = _formulas.m1(inState, _matrix);
	}

	return res;
}

void case8::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state* storeState;

	if (!inState.someXActFallback()) {
		mc_state outState(inState);

		outState.shiftXActsRight();

		storeState = new mc_state(outState);

		addToList(inState, outState, storeState, outStates);
	}
}

