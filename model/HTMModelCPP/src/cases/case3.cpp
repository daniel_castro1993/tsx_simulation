#include "cases/case3.h"

using namespace htm;

case3::case3(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case3::case3(const case3& orig) : mc_transition_case(orig)
{
}

case3::~case3()
{
}

static bool checkStates(int budget, mc_state& inState, mc_state& outState)
{
	// inState  [t_R    , ..., t_i    , ..., t_1, 0, n]
	// outState [t_R + 1, ..., t_i - 1, ..., t_1, 0, n]
	int inFullBudget = inState.getThreadsWithBudget(budget);
	int outFullBudget = outState.getThreadsWithBudget(budget);
	int inNonDang = inState.getThreadsXActsNonDangerous() - inFullBudget;
	int outNonDang = outState.getThreadsXActsNonDangerous() - outFullBudget;

	// inState == outState not allowed

	return !(inState == outState)
		&& inState.getThreadsFallback() == 0
		&& outState.getThreadsFallback() == 0
		&& ((inFullBudget + 1) == outFullBudget)
		&& ((inNonDang == (outNonDang + 1)) || budget == 1);
}

double case3::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();
	int i;

	if (checkStates(budget, inState, outState)) {
		double nb_thrs = _formulas.getThreads();
		double nonXActs = inState.getThreadsNonTransac();

		for (i = 1; i < budget - 1; ++i) {
			// find budget such that t_i - 1 -> t_R + 1
			if (inState[i] != outState[i]) {
				double m3 = _formulas.m3(inState, _matrix);
				double pt = _formulas.pt(inState, _matrix);
				res = inState[i] * m3 * pt / (nb_thrs - nonXActs);
				break;
			}
		}
	}

	return res;
}

void case3::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state* storeState;
	int budget = _formulas.getBudget();
	int i;

	if (!inState.someXActFallback()) {

		for (i = 1; i < budget - 1; ++i) {
			mc_state outState(inState);

			if (outState[i] > 0) {

				outState[i]--;
				outState[0]++;

				storeState = new mc_state(outState);

				addToList(inState, outState, storeState, outStates);
			}
		}
	}
}
