#include "cases/case12.h"

using namespace htm;

case12::case12(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case12::case12(const case12& orig) : mc_transition_case(orig)
{
}

case12::~case12()
{
}

static bool checkStates(int budget, mc_state& inState, mc_state& outState)
{
	// inState  [     t_R, ..., t_i, ..., t_1, 0  , n]
	// outState [0  , t_R, ..., t_i, ..., t_2, t_1, n]
	int i;
	bool res = false;

	for (i = 1; i < budget + 1; ++i) {

		if (inState[i - 1] == outState[i]) {

			if (res == true) {
				// happened twice
				res = false;
				break;
			} else {
				res = true;
			}
		}
	}

	return res
		&& inState.getThreadsNonTransac() == outState.getThreadsNonTransac()
		&& inState.getThreadsFallback() == 0
		&& inState.getThreadsXActsDangerous() > 0
		&& outState.getThreadsFallback() > 0
		&& outState.getThreadsWithBudget(budget) == 0;
}

double case12::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();
	int i;

	if (checkStates(budget, inState, outState)) {

		for (i = 1; i < budget - 1; ++i) {
			// find budget such that t_i - 1 -> t_1 + 1
			if (inState[i - 1] != outState[i]) {
				res = inState[i - 1] * _formulas.m1(inState, _matrix)
					* _formulas.pe(inState, _matrix);
				break;
			}
		}
	}

	return res;
}

void case12::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state* storeState;

	if (!inState.someXActFallback()) {
		mc_state outState(inState);

		if (outState.getThreadsXActsDangerous() > 0) {

			outState.shiftXActsRight();

			storeState = new mc_state(outState);

			addToList(inState, outState, storeState, outStates);
		}
	}
}

