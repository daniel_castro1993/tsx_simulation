#include "cases/case11.h"

using namespace htm;

case11::case11(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case11::case11(const case11& orig) : mc_transition_case(orig)
{
}

case11::~case11()
{
}

static bool checkStates(int budget, mc_state& inState, mc_state& outState)
{
	// inState  [     t_R, ..., t_i    , ..., t_1, 0      , n]
	// outState [0  , t_R, ..., t_i - 1, ..., t_2, t_1 + 1, n]
	int i;
	bool res = false;

	for (i = 1; i < budget; ++i) {

		if (inState[i - 1] == (outState[i] + 1)
			&& (inState[budget - 1] + 1) == outState[budget]) {

			if (res == true) {
				// happened twice
				res = false;
				break;
			} else {
				res = true;
			}
		} else if (inState[i - 1] != outState[i]) {
			res = false;
			break;
		}
	}

	return res
		&& inState.getThreadsNonTransac() == outState.getThreadsNonTransac()
		&& inState.getThreadsFallback() == 0
		&& outState.getThreadsFallback() > 0
		&& outState.getThreadsWithBudget(budget) == 0;
}

double case11::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();
	int i;

	if (checkStates(budget, inState, outState)) {

		for (i = 1; i < budget - 1; ++i) {
			// find budget such that t_i - 1 -> t_1 + 1
			if (inState[i - 1] != outState[i]) {
				res = inState[i - 1] * _formulas.m2(inState, _matrix)
					* _formulas.pe(inState, _matrix);
				break;
			}
		}
	}

	return res;
}

void case11::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state* storeState;
	int budget = _formulas.getBudget();
	int i;

	if (!inState.someXActFallback()) {

		for (i = budget - 2; !(i < 0); --i) {
			mc_state outState(inState);

			if (outState[i - 1] > 0) {

				outState[i - 1]--;
				outState[budget]++;

				outState.shiftXActsRight();

				storeState = new mc_state(outState);

				addToList(inState, outState, storeState, outStates);
			}
		}
	}
}

