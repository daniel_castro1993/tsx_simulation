#include "cases/case1.h"

using namespace htm;

case1::case1(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case1::case1(const case1& orig) : mc_transition_case(orig)
{
}

case1::~case1()
{
}

static bool checkStates(mc_state& inState, mc_state& outState)
{
	// inState  [t_R, ..., t_i, ..., t_0, n]
	// outState [t_R, ..., t_i, ..., t_0, n]
	return false; // inState == outState; // we do not want this transition
}

double case1::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;

	if (checkStates(inState, outState)) {
		res = inState.getThreadsNonTransac()
			* _formulas.mn(inState, _matrix)
			* (1.0 - _formulas.pt(inState, _matrix));
	}

	return res;
}

void case1::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{
	// do not add duplicates
}

