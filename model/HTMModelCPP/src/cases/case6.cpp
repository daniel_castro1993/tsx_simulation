#include "cases/case6.h"

using namespace htm;

case6::case6(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case6::case6(const case6& orig) : mc_transition_case(orig)
{
}

case6::~case6()
{
}

static bool checkStates(mc_state& inState, mc_state& outState)
{
	// inState  [t_R, ..., t_i, ..., t_1    , 0, n    ]
	// outState [t_R, ..., t_i, ..., t_1 - 1, 0, n + 1]
	int inDang = inState.getThreadsXActsDangerous();
	int outDang = outState.getThreadsXActsDangerous();
	int inNonTransac = inState.getThreadsNonTransac();
	int outNonTransac = outState.getThreadsNonTransac();

	return inState.getThreadsFallback() == 0
		&& outState.getThreadsFallback() == 0
		&& ((inNonTransac + 1) == outNonTransac)
		&& (inDang == (outDang + 1));
}

double case6::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();

	if (checkStates(inState, outState)) {
		double nb_thrs = _formulas.getThreads();
		double nonXActs = inState.getThreadsNonTransac();

		res = inState[budget - 1] * _formulas.m3(inState, _matrix)
			* (1.0 - _formulas.pt(inState, _matrix))
			/ (nb_thrs - nonXActs);
	}

	return res;
}

void case6::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state* storeState;
	int budget = _formulas.getBudget();

	if (!inState.someXActFallback()) {
		mc_state outState(inState);

		if (outState[budget - 1] > 0) {

			outState[budget - 1]--;
			outState[budget + 1]++;

			storeState = new mc_state(outState);

			addToList(inState, outState, storeState, outStates);
		}
	}
}
