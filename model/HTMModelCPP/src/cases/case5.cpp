#include "cases/case5.h"

using namespace htm;

case5::case5(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case5::case5(const case5& orig) : mc_transition_case(orig)
{
}

case5::~case5()
{
}

static bool checkStates(mc_state& inState, mc_state& outState)
{
	// inState  [t_R, ..., t_i,     ..., t_1, 0, n    ]
	// outState [t_R, ..., t_i - 1, ..., t_1, 0, n + 1]
	int inNonDang = inState.getThreadsXActsNonDangerous();
	int outNonDang = outState.getThreadsXActsNonDangerous();
	int inNonTransac = inState.getThreadsNonTransac();
	int outNonTransac = outState.getThreadsNonTransac();

	return inState.getThreadsFallback() == 0
		&& outState.getThreadsFallback() == 0
		&& ((inNonTransac + 1) == outNonTransac)
		&& (inNonDang == (outNonDang + 1));
}

double case5::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();
	int i;

	if (checkStates(inState, outState)) {
		double nb_thrs = _formulas.getThreads();
		double nonXActs = inState.getThreadsNonTransac();
		double dang = inState.getThreadsXActsDangerous();

		for (i = 1; i < budget - 1; ++i) {
			// find budget such that t_i - 1 -> n + 1
			if (inState[i] != outState[i]) {
				double m2 = _formulas.m2(inState, _matrix);
				double oneMinusPt = (1.0 - _formulas.pt(inState, _matrix));
				res = inState[i] * m2 * oneMinusPt / (nb_thrs - nonXActs - dang);
			}
		}
	}

	return res;
}

void case5::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state* storeState;
	int budget = _formulas.getBudget();
	int i;

	if (!inState.someXActFallback()) {

		for (i = 1; i < budget - 1; ++i) {
			mc_state outState(inState);

			if (outState[i] > 0) {

				outState[i]--;
				outState[budget + 1]++;

				storeState = new mc_state(outState);

				addToList(inState, outState, storeState, outStates);
			}
		}
	}
}

