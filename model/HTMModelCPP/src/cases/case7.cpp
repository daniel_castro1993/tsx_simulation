#include "cases/case7.h"

using namespace htm;

case7::case7(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case7::case7(const case7& orig) : mc_transition_case(orig)
{
}

case7::~case7()
{
}

static bool checkStates(int budget, mc_state& inState, mc_state& outState)
{
	// inState  [t_R, ..., t_i    , t_{i-1}    , ..., t_1, 0, n]
	// outState [t_R, ..., t_i - 1, t_{i-1} + 1, ..., t_1, 0, n]
	int i;
	bool res = false;

	for (i = 1; i < budget; ++i) {

		if (inState[i - 1] == (outState[i - 1] + 1)
			&& (inState[i] + 1) == outState[i]) {

			if (res == true) {
				// happened twice
				res = false;
				break;
			} else {
				res = true;
			}
		}
	}

	return res
		&& inState.getThreadsNonTransac() == outState.getThreadsNonTransac()
		&& inState.getThreadsFallback() == 0
		&& outState.getThreadsFallback() == 0;
}

double case7::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();
	int i;

	if (checkStates(budget, inState, outState)) {
		double nb_thrs = _formulas.getThreads();
		double nonXActs = inState.getThreadsNonTransac();
		double dang = inState.getThreadsXActsDangerous();

		for (i = 0; i < budget - 1; ++i) {
			// find budget such that t_i - 1 -> t_{i-1} + 1
			if (inState[i] != outState[i]) {
				res = inState[i] * _formulas.m2(inState, _matrix)
					/ (nb_thrs - nonXActs - dang);
				break;
			}
		}
	}

	return res;
}

void case7::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state* storeState;
	int budget = _formulas.getBudget();
	int i;

	if (!inState.someXActFallback()) {

		for (i = budget - 2; !(i < 0); --i) {
			mc_state outState(inState);

			if (outState[i - 1] > 0) {

				outState[i - 1]--;
				outState[i]++;

				storeState = new mc_state(outState);

				addToList(inState, outState, storeState, outStates);
			}
		}
	}
}
