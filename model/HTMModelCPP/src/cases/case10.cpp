#include "cases/case10.h"

using namespace htm;

case10::case10(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case10::case10(const case10& orig) : mc_transition_case(orig)
{
}

case10::~case10()
{
}

static bool checkStates(mc_state& inState, mc_state& outState)
{
	// inState  [t_R, ..., t_i, ..., t_1, t_0    , n    ]
	// outState [t_R, ..., t_i, ..., t_1, t_0 - 1, n + 1]
	return (inState.getThreadsFallback()
		== (outState.getThreadsFallback() + 1))
		&& ((inState.getThreadsNonTransac() + 1)
		== outState.getThreadsNonTransac());
}

double case10::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;

	if (checkStates(inState, outState)) {
		res = _formulas.mf(inState, _matrix)
			* (1.0 - _formulas.pt(inState, _matrix));
	}

	return res;
}

void case10::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state outState(inState);
	mc_state* storeState;
	int budget = _formulas.getBudget();

	if (inState.getThreadsFallback() > 0) {

		outState[budget]--;
		outState[budget + 1]++;

		storeState = new mc_state(outState);

		addToList(inState, outState, storeState, outStates);
	}
}


