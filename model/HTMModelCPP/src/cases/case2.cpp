#include "cases/case2.h"

using namespace htm;

case2::case2(mc_matrix& matrix, mc_formulas& formulas)
: mc_transition_case(matrix, formulas)
{
}

case2::case2(const case2& orig) : mc_transition_case(orig)
{
}

case2::~case2()
{
}

static bool checkStates(int budget, mc_state& inState, mc_state& outState)
{
	// inState  [t_R    , ..., t_i, ..., t_0, n    ]
	// outState [t_R + 1, ..., t_i, ..., t_0, n - 1]
	int inFullBudget = inState.getThreadsWithBudget(budget);
	int outFullBudget = outState.getThreadsWithBudget(budget);
	int inNonTransac = inState.getThreadsNonTransac();
	int outNonTransac = outState.getThreadsNonTransac();

	return (inFullBudget + 1) == outFullBudget
		&& (inNonTransac == (outNonTransac + 1));
}

double case2::rateCase(mc_state& inState, mc_state& outState)
{
	double res = 0;
	int budget = _formulas.getBudget();

	if (checkStates(budget, inState, outState)) {
		res = inState.getThreadsNonTransac()
			* _formulas.mn(inState, _matrix)
			* _formulas.pt(inState, _matrix);
	}

	return res;
}

void case2::outStatesCase(mc_state& inState, list<mc_state*>& outStates)
{

	mc_state outState(inState);
	mc_state* storeState;
	int budget = _formulas.getBudget();

	if (inState.getThreadsNonTransac() > 0) {

		outState[budget + 1]--;
		outState[0]++;

		storeState = new mc_state(outState);

		addToList(inState, outState, storeState, outStates);
	}
}
