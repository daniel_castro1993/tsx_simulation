#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"

#include <algorithm>
#include <cstdlib>
#include <cstdio>

using namespace htm;
using namespace std;

static void copyMatrix(MDL_MATX& dest, MDL_MATX& source);
static int findIndexOfState(mc_state& st, vector<mc_state*>& states);

mc_matrix::mc_matrix(mc_formulas& formulas)
: _vecStates(0), _formulas(formulas), _firstIteration(true), _iterations(0)
{
	resetVars();
}

mc_matrix::mc_matrix(const mc_matrix& orig)
: _vecStates(orig._vecStates), _formulas(orig._formulas),
_firstIteration(true), _iterations(0)
{
	resetVars();
}

mc_matrix::~mc_matrix()
{
}

void mc_matrix::setCoeff(int row, int col, MDL_PREC value)
{
	_matrix.coeffRef(row, col) = value;
}

void mc_matrix::insert(int row, int col, MDL_PREC value)
{
	_matrix.insert(row, col) = value;
}

void mc_matrix::setDiag()
{
	int i;

	for (i = 0; i < _matrix.rows(); i++) {
		_matrix.coeffRef(i, i) = -_matrix.row(i).sum();
	}
}

void mc_matrix::insertDiag()
{
	int i;

	for (i = 0; i < _matrix.rows(); i++) {
		_matrix.insert(i, i) = -_matrix.row(i).sum();
	}
}

void mc_matrix::resize(int rows, int cols)
{
	_rows = rows;
	_cols = cols;
	_matrix.resize(rows, cols);
}

void mc_matrix::clear()
{
	_matrix.resize(0, 0);
	_matrix.resize(_rows, _cols);
}

void mc_matrix::computeSolution()
{
	int i, size = _vecStates.size();

	SparseQR<MDL_MATX, AMDOrdering<int> > solverAug;
	MDL_VECT bAug(size), xAugmt;
	MDL_MATX QAugmt(size, size + 1);

	//  IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
	//  cout << "\nMatrix:\n" << _matrix.toDense().format(CleanFmt) << "\n\n";

#ifdef __CMP_SOLVERS__
	SparseQR<MDL_MATX, AMDOrdering<int> > solverRR;
	MDL_VECT bRowRep(size);
#endif

	for (i = 0; i < size; i++) {
		QAugmt.insert(i, size) = 1.0;
		bAug.coeffRef(i) = 1.0;
	}
	copyMatrix(QAugmt, _matrix);

	solverAug.compute(QAugmt * QAugmt.transpose());
	if (solverAug.info() != Success) {
		cerr << "Error computing QAugmt \n";
	}
	xAugmt = solverAug.solve(bAug);
	_solAugmt = xAugmt;

#ifdef __CMP_SOLVERS__
	for (i = 0; i < size; i++) {
		this->matrix.coeffRef(i, size - 1) = 1.0;
	}
	bRowRep.coeffRef(size - 1) = 1.0;

	solverRR.compute(this->matrix.transpose());
	if (solverRR.info() != Success) {

		cerr << "Error computing QRowRep \n";
	}
	this->solRowRep = solverRR.solve(bRowRep);
#endif
}

void mc_matrix::filterSolution()
{
	int i, k;

	resetVars();

	for (i = 0; i < _vecStates.size(); ++i) {
		mc_state state(*_vecStates[i]);

		double P_R_L_d = _formulas.compute_P_R_L_d(state, *this);
		double R_d = _formulas.compute_R_d(state, *this);
		double P_R_L_n = _formulas.compute_P_R_L_n(state, *this);
		double R_n = _formulas.compute_R_n(state, *this);

		_prev_P_R_L_d[i] = P_R_L_d;
		_prev_R_d[i] = R_d;

		_prev_P_R_L_n[i] = P_R_L_n;
		_prev_R_n[i] = R_n;
	}

	for (i = 0; i < _vecStates.size(); ++i) {
		mc_state state(*_vecStates[i]);

		double P_R_L = _formulas.compute_P_R_L(state, *this);
		double R = _formulas.compute_R(state, *this);

		_prev_P_R_L[i] = P_R_L;
		_prev_R[i] = R;
	}

	_firstIteration = false;
	_iterations++;

#ifdef _CMP_SOLVERS
	// TODO
#endif

	for (k = 0; k < _solAugmt.outerSize(); ++k) {
		for (MDL_VECT::InnerIterator it(_solAugmt, k); it; ++it) {

			for (i = 0; i < _vecStates.size(); i++) {
				if (it.value() > _top_state_prob_aug[i]) {
					_top_state_prob_aug.insert(
						_top_state_prob_aug.begin() + i, it.value());
					_top_state_idx_aug.insert(
						_top_state_idx_aug.begin() + i, it.row());
					break;
				}
			}
			for (i = 0; i < _vecStates.size(); i++) {
				if (it.value() < _bot_state_prob_aug[i]) {
					_bot_state_prob_aug.insert(
						_bot_state_prob_aug.begin() + i, it.value());
					_bot_state_idx_aug.insert(
						_bot_state_idx_aug.begin() + i, it.row());
					break;
				}
			}
		}
	}
}

double mc_matrix::avg_P_R_L()
{
	int i;
	double res = 0;

	for (i = 0; i < _solAugmt.outerSize(); ++i) {
		for (MDL_VECT::InnerIterator it(_solAugmt, i); it; ++it) {
			mc_state state(*_vecStates[it.row()]);
			double dang = state.getThreadsXActsDangerous();
			double nonDang = state.getThreadsXActsNonDangerous();
			double fb = state.getThreadsFallback();
			double tau = state.getThreadsXActs();

			if (fb > 0) {
				res += it.value() * 1.0; // prob. commit == 1
			} else if (tau > 0) {

				double P_R_L_d = _formulas.compute_P_R_L_d(state, *this);
				double P_R_L_n = _formulas.compute_P_R_L_n(state, *this);

				res += it.value() * (
					// dangerous transactions contribution
					dang / tau * P_R_L_d
					+
					// non-dangerous transactions contribution
					nonDang / tau * P_R_L_n
					);
			}
		}
	}

	return res;
}

double mc_matrix::avg_R()
{
	int i;
	double res = 0;

	for (i = 0; i < _solAugmt.outerSize(); ++i) {
		for (MDL_VECT::InnerIterator it(_solAugmt, i); it; ++it) {
			mc_state state(*_vecStates[it.row()]);
			double dang = state.getThreadsXActsDangerous();
			double nonDang = state.getThreadsXActsNonDangerous();
			double fb = state.getThreadsFallback();
			double tau = state.getThreadsXActs();

			if (fb > 0) {
				res += it.value() * _formulas.getC(); // prob. commit == 1
			} else if (tau > 0) {

				double R_d = _formulas.compute_R_d(state, *this);
				double R_n = _formulas.compute_R_n(state, *this);

				res += it.value() * (
					// dangerous transactions contribution
					dang / tau * R_d
					+
					// non-dangerous transactions contribution
					nonDang / tau * R_n
					);
			} else {
				// tau == 0
				res += it.value() * _formulas.getC(); // non-transactions last C
			}
		}
	}

	return res;
}

void mc_matrix::setVecStates(vector<mc_state*>& vecStates)
{
	resize(vecStates.size(), vecStates.size());
	_vecStates = vecStates;
	resetVars();
}

mc_matrix& mc_matrix::operator=(const mc_matrix& right)
{
	// Check for self-assignment!
	if (this == &right) // Same object?
		return *this; // Yes, so skip assignment, and just return *this.
	_vecStates = right._vecStates;
	_matrix = right._matrix;
	_solAugmt = right._solAugmt;
#ifdef __CMP_SOLVERS__
	_solRowRep = right._solRowRep;
#endif
	return *this;
}

vector<pair<mc_state*, double> > mc_matrix::getTopStates()
{
	vector<pair<mc_state*, double> > res(0);
	int i;

	for (i = 0; i < _vecStates.size(); ++i) {
		int idx = _top_state_idx_aug[i];
		double prob = _top_state_prob_aug[i];
		if (idx > -1) {
			pair<mc_state*, double> item(_vecStates[idx], prob);
			res.push_back(item);
		} else {
			break;
		}
	}
	return res;
}

bool mc_matrix::isFirstIteration()
{
	return _firstIteration;
}

double mc_matrix::prevP_R_L(mc_state& st)
{
	int idx = findIndexOfState(st, _vecStates);
	return _prev_P_R_L[idx];
}

double mc_matrix::prevR(mc_state& st)
{
	int idx = findIndexOfState(st, _vecStates);
	return _prev_R[idx];
}

double mc_matrix::prevP_R_L_d(mc_state& st)
{
	int idx = findIndexOfState(st, _vecStates);
	return _prev_P_R_L_d[idx];
}

double mc_matrix::prevR_d(mc_state& st)
{
	int idx = findIndexOfState(st, _vecStates);
	return _prev_R_d[idx];
}

double mc_matrix::prevP_R_L_n(mc_state& st)
{
	int idx = findIndexOfState(st, _vecStates);
	return _prev_P_R_L_n[idx];
}

double mc_matrix::prevR_n(mc_state& st)
{
	int idx = findIndexOfState(st, _vecStates);
	return _prev_R_n[idx];
}

vector<mc_state*> mc_matrix::getVecStates() const
{
	return _vecStates;
}

void mc_matrix::printTopStates(ostream& os, int nb_top_states)
{
	int i, size = nb_top_states > _vecStates.size()
		? _vecStates.size() : nb_top_states;

	for (i = 0; i < size; ++i) {
		int idx = _top_state_idx_aug[i];
		double prob = _top_state_prob_aug[i];
		if (idx > -1) {
			mc_state toPrint(*_vecStates[idx]);
			double R_1 = _formulas.compute_R_1(toPrint, *this);
			double R_2 = _formulas.compute_R_2(toPrint, *this);
			double R_3 = _formulas.compute_R_3(toPrint, *this);
			double P_1 = _formulas.compute_P_1(toPrint, *this);
			double P_2 = _formulas.compute_P_2(toPrint, *this);
			double P_3 = _formulas.compute_P_3(toPrint, *this);
			double t = toPrint.getCurrentXActs();

			double m_1 = _formulas.m1(toPrint, *this);
			double m_2 = _formulas.m2(toPrint, *this);
			double m_3 = _formulas.m3(toPrint, *this);

			double P_a = (m_1 * t + m_2) / (m_1 * t + m_2 + m_3);

			toPrint.printState(os);
			// not working with files
			printf(": %8.5f (P_a: %8.5f, X: %8.5f, P_e1: %6.3f, P_e2: %6.3f, "
				"P_e3: %6.3f, R_e1: %.3e, R_e2: %.3e, R_e3: %.3e)\n",
				prob, P_a, P_3 / R_3, P_1, P_2, P_3, R_1, R_2, R_3);
		}
	}
}

void mc_matrix::resetVars()
{
	int i;

	_top_state_prob_aug.resize(_vecStates.size());
	_bot_state_prob_aug.resize(_vecStates.size());
	_top_state_idx_aug.resize(_vecStates.size());
	_bot_state_idx_aug.resize(_vecStates.size());

	_prev_P_R_L.resize(_vecStates.size(), 0.5);
	_prev_R.resize(_vecStates.size(), _formulas.getC());

	_prev_P_R_L_d.resize(_vecStates.size(), 0.5);
	_prev_R_d.resize(_vecStates.size(), _formulas.getC());

	_prev_P_R_L_n.resize(_vecStates.size(), 0.5);
	_prev_R_n.resize(_vecStates.size(), _formulas.getC());

	for (i = 0; i < _vecStates.size(); ++i) {
		_top_state_prob_aug[i] = -INFINITY;
		_top_state_idx_aug[i] = -1;
		_bot_state_prob_aug[i] = INFINITY;
		_bot_state_idx_aug[i] = -1;
	}
}

static void copyMatrix(MDL_MATX& dest, MDL_MATX& source)
{
	int i;

	for (i = 0; i < source.outerSize(); i++) {
		MDL_MATX::InnerIterator matIt(source, i);
		while (matIt) {
			MDL_PREC value = matIt.value();
			int row = matIt.row();
			dest.insert(row, i) = value;
			++matIt;
		}
	}
}

static int findIndexOfState(mc_state& st, vector<mc_state*>& states)
{
	int i;

	for (i = 0; i < states.size(); ++i) {
		if (st == *(states[i])) {
			return i;
		}
	}

	return -1;
}
