#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"

using namespace htm;

static double rateAtState(mc_formulas& formulas,
                          mc_state& state, mc_matrix& matrix);

mc_formulas::mc_formulas(int threads, int budget, double prob_read,
                         double c, int l, int d)
: _threads(threads), _budget(budget), _PROB_READ(prob_read),
_C(c), _L(l), _D(d)
{
}

mc_formulas::mc_formulas(const mc_formulas& orig)
: _threads(orig._threads), _budget(orig._budget), _PROB_READ(orig._PROB_READ),
_C(orig._C), _L(orig._L), _D(orig._D)
{

    me = orig.me;
    mf = orig.mf;
    mn = orig.mn;
    m1 = orig.m1;
    m2 = orig.m2;

    p1 = orig.p1;
    p2 = orig.p2;
    pe = orig.pe;
    pt = orig.pt;
}

mc_formulas::~mc_formulas()
{
}

int mc_formulas::getBudget() const
{
    return _budget;
}

double mc_formulas::getC() const
{
    return _C;
}

int mc_formulas::getL() const
{
    return _L;
}

double mc_formulas::getProbRead() const
{
    return _PROB_READ;
}

int mc_formulas::getThreads() const
{
    return _threads;
}

double mc_formulas::compute_lambda(mc_state& state, mc_matrix& matrix)
{

    double c_over_l = _W;
    int i, nb_trans = 0;

    for (i = 0; i < _budget; ++i) {
        nb_trans += state[i];
    }

    if (state.getThreadsFallback() != 0) {
        nb_trans = 1;
    }

    nb_trans -= 1;

    return (double) nb_trans / c_over_l;
}

double mc_formulas::compute_H_i(mc_state& state, mc_matrix& matrix,
                                int nb_inst)
{

    if (!(nb_inst > 0)) {
        return 0;
    }

    return compute_lambda(state, matrix) * (1 - _PROB_READ * _PROB_READ)
            * (double) nb_inst / (double) _D;
}

double mc_formulas::compute_P_R_L(mc_state& state, mc_matrix& matrix)
{
    vector<double> p_r(_L, 1);
    compute_P_R_L(p_r, state, matrix);
    return p_r[_L - 1];
}

void mc_formulas::compute_P_R_L(vector<double>& p_r, mc_state& state,
                                mc_matrix& matrix, double W)
{

    int i;
    _W = W;

    p_r[0] = 1;

    for (i = 1; i < _L; i++) {
        if (state.someXActFallback()) {
            p_r[i] = 1;
        }
        else {
            double H_i = compute_H_i(state, matrix, i - 1);
            double exp_p = exp(-H_i * _W);
            double next_p_r = p_r[i - 1] * exp_p;
            p_r[i] = next_p_r;
        }
    }
}

void mc_formulas::compute_P_R_L(vector<double>& p_r, mc_state& state,
                                mc_matrix& matrix)
{

    int i;
    double c_over_l = _C / (double) _L;

    compute_P_R_L(p_r, state, matrix, c_over_l);
}

double mc_formulas::compute_R(mc_state& state, mc_matrix& matrix)
{
    vector<double> p_r(_L, 1);
    double res = 0, lambda = compute_lambda(state, matrix);
    double c_over_l = _C / (double) _L;
    int i;

    _W = c_over_l;

    compute_P_R_L(p_r, state, matrix);

    res = p_r[_L - 1] * _C;

    if (!(lambda > 0)) {
        // this means that there is only one thread (fallback path?)
        return _C;
    }

    for (i = 0; i < _L - 1; i++) {
        double _i = i + 1;
        double H_i = compute_H_i(state, matrix, i + 1);
        double to_sum = 0;

        if (!(H_i > 0)) {
            to_sum = -1.0 * p_r[i + 1] * _W;
        }
        else {
            to_sum = p_r[i + 1] * (_i * _W * (1.0 - exp(-H_i * _W))
                    + 1.0 / H_i - exp(-H_i * _W) * (_W + 1.0 / H_i));
        }

        res += to_sum;
    }

    return res;
}

double mc_formulas::compute_H_i_d(mc_state& state,
                                  mc_matrix& matrix, int nb_inst)
{

    if (!(nb_inst > 0)) {
        return 0;
    }

    double d = state.getThreadsXActsDangerous();
    double fb = state.getThreadsFallback();
    double P_R_L = matrix.prevP_R_L_d(state);
    double R = matrix.prevR_d(state);
    double H_i = compute_H_i(state, matrix, nb_inst);
    double res;

    res = H_i;

    if (d > 0 && fb == 0) {
        res += (d - 1.0) * (1.0 - P_R_L) / R;
    }

    return res;
}

double mc_formulas::compute_P_R_L_d(mc_state& state, mc_matrix& matrix)
{
    vector<double> p_r(_L, 1);
    compute_P_R_L_d(p_r, state, matrix);
    double res = p_r[_L - 1];
    return res;
}

void mc_formulas::compute_P_R_L_d(vector<double>& p_r,
                                  mc_state& state, mc_matrix& matrix)
{

    if (!(state.getThreadsXActsDangerous() > 1)
        || matrix.isFirstIteration()) {
        compute_P_R_L(p_r, state, matrix);
        return;
    }

    int i;
    double c_over_l = _C / (double) _L;

    _W = c_over_l;

    p_r[0] = 1;

    for (i = 1; i < _L; i++) {
        if (state.someXActFallback()) {
            p_r[i] = 1;
        }
        else {
            double H_i_d = compute_H_i_d(state, matrix, i - 1);

            p_r[i] = p_r[i - 1] * exp(-H_i_d * _W);
        }
    }
}

double mc_formulas::compute_R_d(mc_state& state, mc_matrix& matrix)
{

    if (state.someXActFallback()) {
        return _C;
    }

    if (!(state.getThreadsXActsDangerous() > 1)
        || matrix.isFirstIteration()) {
        return compute_R(state, matrix);
    }

    vector<double> p_r(_L, 1);
    double res = 0;
    double c_over_l = _C / (double) _L;
    int i;

    _W = c_over_l;

    compute_P_R_L_d(p_r, state, matrix);

    res = p_r[_L - 1] * _C;

    for (i = 0; i < _L; i++) {
        double _i = i + 1;
        double H_i_d = compute_H_i_d(state, matrix, i);
        double to_sum;

        if (!(H_i_d > 0)) {
            to_sum = -1.0 * p_r[i] * _W;
        }
        else {
            to_sum = p_r[i] * (_i * _W * (1.0 - exp(-H_i_d * _W))
                    + 1.0 / H_i_d - exp(-H_i_d * _W) * (_W + 1.0 / H_i_d));
        }

        res += to_sum;
    }

    return res;
}

double mc_formulas::compute_H_i_n(mc_state& state,
                                  mc_matrix& matrix, int nb_inst)
{

    if (!(nb_inst > 0)) {
        return 0;
    }

    double d = state.getThreadsXActsDangerous();
    double fb = state.getThreadsFallback();
    double P_R_L = matrix.prevP_R_L_d(state);
    double R = matrix.prevR_d(state);
    double H_i = compute_H_i(state, matrix, nb_inst);
    double res;

    res = H_i;

    if (fb == 0) {
        res += d * (1.0 - P_R_L) / R;
    }

    return res;
}

double mc_formulas::compute_P_R_L_n(mc_state& state, mc_matrix& matrix)
{
    vector<double> p_r(_L, 1);
    compute_P_R_L_d(p_r, state, matrix);
    double res = p_r[_L - 1];
    return res;
}

void mc_formulas::compute_P_R_L_n(vector<double>& p_r, mc_state& state,
                                  mc_matrix& matrix)
{

    if (!(state.getThreadsXActsDangerous() > 0)
        || matrix.isFirstIteration()) {
        compute_P_R_L(p_r, state, matrix);
        return;
    }

    int i;
    double c_over_l = _C / (double) _L;

    _W = c_over_l;

    p_r[0] = 1;

    for (i = 1; i < _L; i++) {
        if (state.someXActFallback()) {
            p_r[i] = 1;
        }
        else {
            double H_i_n = compute_H_i_n(state, matrix, i - 1);
            p_r[i] = p_r[i - 1] * exp(-H_i_n * _W);
        }
    }
}

double mc_formulas::compute_R_n(mc_state& state, mc_matrix& matrix)
{

    if (state.someXActFallback()) {
        return _C;
    }

    if (!(state.getThreadsXActsDangerous() > 0)
        || matrix.isFirstIteration()) {
        return compute_R(state, matrix);
    }

    vector<double> p_r(_L, 1);
    double res = 0;
    double c_over_l = _C / (double) _L;
    int i;

    _W = c_over_l;

    compute_P_R_L_n(p_r, state, matrix);

    res = p_r[_L - 1] * _C;

    for (i = 0; i < _L; i++) {
        double _i = i + 1;
        double H_i_n = compute_H_i_n(state, matrix, i);
        double to_sum;

        if (!(H_i_n > 0)) {
            to_sum = -1.0 * p_r[i] * _W;
        }
        else {
            to_sum = p_r[i] * (_i * _W * (1.0 - exp(-H_i_n * _W))
                    + 1.0 / H_i_n - exp(-H_i_n * _W) * (_W + 1.0 / H_i_n));
        }

        res += to_sum;
    }

    return res;
}

double compute_P_R_component(mc_state& state, mc_matrix& matrix,
                             mc_formulas* formulas)
{

    int i;
    vector<double> p_r(formulas->getL(), 1);
    double _C = formulas->getC();
    double _L = formulas->getL();
    double sum = 0;
    double c_over_l = _C / (double) _L;
    double xacts = state.getThreadsXActs();
    double fallback = state.getThreadsFallback();

    if (fallback > 0) {
        return 0.0;
    }

#ifdef __XACTS_FULL_LIFE__

    formulas->_W = c_over_l;

    formulas->compute_P_R_L(p_r, state, matrix, c_over_l);

    for (i = 0; i < _L - 1; ++i) {
        double H_i = formulas->compute_H_i(state, matrix, i);
        sum += pow(p_r[i + 1], xacts) / xacts
                * (1.0 - exp(-c_over_l * xacts * H_i));
    }

#elif defined(__XACTS_HALF_LIFE__)
    int half_L = _L / 2.0;

    c_over_l = _C / ((double) _L - (double) _L / 2.0);
    formulas->_W = c_over_l;

    formulas->compute_P_R_L(p_r, state, matrix, c_over_l);

    for (i = 0; i < _L - _L / 2.0; ++i) {
        int new_i = i + half_L >= _L ? formulas->getL() - 1 : i + half_L;
        double H_i = formulas->compute_H_i(state, matrix, new_i);
        double exp_p = exp(-c_over_l * xacts * H_i);
        double p_r_i = p_r[new_i] / p_r[half_L];
        double pow_p = pow(p_r_i, xacts);
        sum += pow_p / xacts * (1.0 - exp_p);
    }
#else
    double d_g = ((double) _L / 2.0) * (xacts - 1.0) / xacts;
    int g = round(d_g);

    c_over_l = _C / ((double) _L - d_g);
    formulas->_W = c_over_l;

    formulas->compute_P_R_L(p_r, state, matrix, c_over_l);

    for (i = 0; i < _L - g; ++i) {
        int new_i = i + g >= _L ? formulas->getL() - 1 : i + g;
        double H_i = formulas->compute_H_i(state, matrix, new_i);
        double exp_p = exp(-c_over_l * xacts * H_i);
        double p_r_i = p_r[new_i] / p_r[g];
        double pow_p = pow(p_r_i, xacts);
        sum += pow_p / xacts * (1.0 - exp_p);
    }
#endif

    return sum;
}

double compute_R_component(mc_state& state, mc_matrix& matrix,
                           mc_formulas* formulas)
{

    int i;
    vector<double> p_r(formulas->getL(), 1);
    double _C = formulas->getC();
    double _L = formulas->getL();
    double sum = 0;
    double c_over_l = _C / (double) _L;
    double xacts = state.getThreadsXActs();
    double fallback = state.getThreadsFallback();

    if (fallback > 0) {
        return 0.0;
    }


#ifdef __XACTS_FULL_LIFE__

    formulas->_W = c_over_l;

    formulas->compute_P_R_L(p_r, state, matrix, c_over_l);

    for (i = 0; i < _L - 1; i++) {
        double _i = i + 1;
        double H_i = formulas->compute_H_i(state, matrix, i + 1);
        double to_sum = 0;

        if (!(H_i > 0)) {
            to_sum = -1.0 * c_over_l / xacts;
        }
        else {
            to_sum = pow(p_r[i + 1], xacts) / xacts * (_i * c_over_l
                    * (1.0 - exp(-xacts * H_i * c_over_l))
                    + 1.0 / (xacts * H_i) - exp(-xacts * H_i * c_over_l)
                    * (c_over_l + 1.0 / (xacts * H_i)));
        }

        sum += to_sum;
    }
#elif defined(__XACTS_HALF_LIFE__)
    int half_L = _L / 2;

    c_over_l = _C / ((double) _L - (double) _L / 2.0);
    formulas->_W = c_over_l;

    formulas->compute_P_R_L(p_r, state, matrix, c_over_l);

    for (i = 0; i < _L / 2 - 1; i++) {
        //  for (i = 0; i < _L - 1; i++) {
        double _i = i;
        int new_i = i + half_L >= _L ? formulas->getL() - 1 : i + half_L;
        double H_i = formulas->compute_H_i(state, matrix, new_i);
        double to_sum = 0;

        if (!(H_i > 0)) {
            to_sum = 0; // -1.0 * c_over_l / xacts;
        }
        else {
            double exp_p = exp(-xacts * H_i * c_over_l);
            double den_p = xacts * H_i;
            double p_r_i = p_r[new_i] / p_r[half_L];
            double pow_p = pow(p_r_i, xacts) / xacts;

            to_sum = pow_p * (_i * c_over_l * (1.0 - exp_p)
                    + 1.0 / den_p - exp_p * (c_over_l + 1.0 / den_p));
        }

        sum += to_sum;
    }
#else
    double d_g = ((double) _L / 2.0) * (xacts - 1.0) / xacts;
    int g = round(d_g);

    c_over_l = _C / ((double) _L - d_g);
    formulas->_W = c_over_l;

    formulas->compute_P_R_L(p_r, state, matrix, c_over_l);

    for (i = 0; i < _L - g; i++) {
        double _i = i;
        int new_i = i + g >= _L ? formulas->getL() - 1 : i + g;
        double H_i = formulas->compute_H_i(state, matrix, new_i);
        double to_sum = 0;

        if (!(H_i > 0)) {
            to_sum = 0; // -1.0 * c_over_l / xacts;
        }
        else {
            double exp_p = exp(-xacts * H_i * c_over_l);
            double den_p = xacts * H_i;
            double p_r_i = p_r[new_i] / p_r[g];
            double pow_p = pow(p_r_i, xacts) / xacts;

            to_sum = pow_p * (_i * c_over_l * (1.0 - exp_p)
                    + 1.0 / den_p - exp_p * (c_over_l + 1.0 / den_p));
        }

        sum += to_sum;
    }
#endif

    return sum;
}

double mc_formulas::compute_P_1(mc_state& state, mc_matrix& matrix)
{

    double dang = state.getThreadsXActsDangerous();
    double res, sum = 0;

    sum = compute_P_R_component(state, matrix, this);

    res = dang * sum;

    return res < 1 ? res : 1;
}

double mc_formulas::compute_R_1(mc_state& state, mc_matrix& matrix)
{
    vector<double> p_r(_L, 1);
    double res, sum = 0;
    double dang = state.getThreadsXActsDangerous();

    compute_P_R_L(p_r, state, matrix);

    sum = compute_R_component(state, matrix, this);

    res = dang * sum;

    return res;
}

double mc_formulas::compute_P_2(mc_state& state, mc_matrix& matrix)
{

    double dang = state.getThreadsXActsDangerous();
    double xacts = state.getThreadsXActs();
    double res, sum = 0;

    sum = compute_P_R_component(state, matrix, this);

    res = (xacts - dang) * sum;

    return res < 1 ? res : 1;
}

double mc_formulas::compute_R_2(mc_state& state, mc_matrix& matrix)
{
    vector<double> p_r(_L, 1);
    double dang = state.getThreadsXActsDangerous();
    double xacts = state.getThreadsXActs();
    double res, sum = 0;

    compute_P_R_L(p_r, state, matrix);

    sum = compute_R_component(state, matrix, this);

    res = (xacts - dang) * sum;

    return res;
}

double mc_formulas::compute_P_3(mc_state& state, mc_matrix& matrix)
{

    double P_1 = compute_P_1(state, matrix);
    double P_2 = compute_P_2(state, matrix);
    double res = max(1.0 - P_1 - P_2, 0.0);

    return res;
}

double mc_formulas::compute_R_3(mc_state& state, mc_matrix& matrix)
{
    double xacts = state.getThreadsXActs();
    double res;

    if (state.someXActFallback()) {
        res = _C;
    }
    else {
        res = _C / xacts;
    }

    return res;
}

double mc_formulas::compute_R_star(mc_matrix& matrix)
{
    int i;
    double res = 0;
    vector<pair<mc_state*, double> > vecStates = matrix.getTopStates();

    for (i = 0; i < vecStates.size(); ++i) {
        mc_state state(*vecStates[i].first);
        double prob = vecStates[i].second;
        double threads = _threads;
        double X = compute_X(matrix);
        double P_t = pt(state, matrix);
        double R_n = 1.0 / mn(state, matrix);
        double R_t = threads / X - (1.0 - P_t) * R_n;

        res += prob * ((1.0 - P_t) * R_n + P_t * R_t);
    }

    return res;
}

double mc_formulas::compute_X(mc_matrix& matrix)
{
    int i;
    double res = 0;
    vector<pair<mc_state*, double> > vecStates = matrix.getTopStates();

    for (i = 0; i < vecStates.size(); ++i) {
        mc_state state(*vecStates[i].first);
        double prob = vecStates[i].second;
        double rate = rateAtState(*this, state, matrix);

        res += rate * prob;
    }

    return res;
}

double mc_formulas::compute_P_A(mc_matrix& matrix)
{
    int i;
    double res = 0;
    vector<pair<mc_state*, double> > vecStates = matrix.getTopStates();
    double tot_prob_no_fallback = 0;

    for (i = 0; i < vecStates.size(); ++i) {
        mc_state state(*vecStates[i].first);
        double prob = vecStates[i].second;
        double tau = state.getThreadsXActs();

        if (tau > 0 && state.getThreadsFallback() == 0) {
            tot_prob_no_fallback += prob;
        }
    }

    for (i = 0; i < vecStates.size(); ++i) {
        mc_state state(*vecStates[i].first);
        double prob = vecStates[i].second;

        if (!state.someXActFallback()) {

            if (state.getThreadsFallback() > 0) {
                continue;
            }

            double tau = state.getThreadsXActs();

            double m_1 = m1(state, matrix);
            double m_2 = m2(state, matrix);
            double m_3 = m3(state, matrix);

            res += prob * ((m_1 * tau + m_2) / (m_1 * tau + m_2 + m_3))
                    / tot_prob_no_fallback;
        }
    }

    return res;
}

static double rateAtState(mc_formulas& formulas, mc_state& state,
                          mc_matrix& matrix)
{

    double C = formulas.getC();

    double xats = state.getThreadsXActs();
    double nonXActs = state.getThreadsNonTransac();
    double xactsSum = 0, nonXActSum = 0;

    if (state.someXActFallback()) {
        xactsSum = 1.0 / C;
    }
    else if (xats != 0) {
        xactsSum = formulas.m3(state, matrix);
    }

    if (nonXActs != 0) {
        nonXActSum = nonXActs * formulas.mn(state, matrix);
    }

    return xactsSum + nonXActSum;
}
