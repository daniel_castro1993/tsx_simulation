#include "mc_state.h"

using namespace htm;
using namespace std;

mc_state::mc_state(int threads, int budget)
: _state(budget + 2, 0), _size(budget + 2),
_threads(threads), _budget(budget)
{
}

mc_state::mc_state(const mc_state& orig)
: _state(orig._state), _size(orig._budget + 2),
_threads(orig._threads), _budget(orig._budget)
{
}

mc_state::~mc_state()
{
}

int& mc_state::operator[](int index)
{
	return _state[index];
}

ostream& mc_state::printState(ostream& os)
{
	vector<int>::iterator it;

	os << "[";
	for (it = _state.begin(); it != _state.end(); ++it) {
		os << *it;
		if (it != _state.end() - 1) {
			os << " ";
		}
	}
	os << "]";
	return os;
}

int mc_state::shiftXActsRight()
{
	int i;

	for (i = _budget; !(i < 1); --i) {
		_state[i] += _state[i - 1];
		_state[i - 1] = 0;
	}
}

int mc_state::xActsAborts(int budget_thread)
{
	int res = _state[_budget - budget_thread];
	_state[_budget - budget_thread]--;
	_state[_budget - budget_thread + 1]++;

	return res;
}

int mc_state::XActStartsNonTransac(int budget_from)
{
	int res = _state[_budget - budget_from];
	_state[_budget - budget_from]--;
	_state[_budget + 1]++;

	return res;
}

int mc_state::nonTransacStartsXAct()
{
	int res = _state[_budget + 1];
	_state[_budget + 1]--;
	_state[0]++;

	return res;
}

void mc_state::fillBudget(int budget)
{
	int i;

	for (i = 0; i < _size; ++i) {
		_state[i] = 0;
	}

	_state[_budget - budget] = _threads;
}

void mc_state::fillNonTransac()
{
	int i;

	for (i = 0; i < _size; ++i) {
		_state[i] = 0;
	}

	_state[_budget + 1] = _threads;
}

int mc_state::getThreadsWithBudget(int budget)
{
	return _state[_budget - budget];
}

int mc_state::getThreadsNonTransac()
{
	return _state[_budget + 1];

}

int mc_state::getThreadsXActs()
{
	return _threads - getThreadsNonTransac();

}

int mc_state::getThreadsXActsDangerous()
{
	return _state[_budget - 1];
}

int mc_state::getThreadsXActsNonDangerous()
{
	return _threads - getThreadsXActsDangerous()
		- getThreadsNonTransac() - getThreadsFallback();
}

int mc_state::getCurrentXActs()
{
	return someXActFallback() ? 1 : getThreadsXActs();
}

int mc_state::getCurrentXActsDangerous()
{
	return someXActFallback() ? 0 : getThreadsXActsDangerous();
}

int mc_state::getCurrentXActsNonDangerous()
{
	return someXActFallback() ? 0 : getThreadsXActsNonDangerous();
}

bool mc_state::someXActFallback()
{
	return _state[_budget] != 0;
}

int mc_state::getThreadsFallback()
{
	return _state[_budget];
}

bool mc_state::operator==(mc_state& state)
{
	return _state == state._state;
}

bool mc_state::operator>(mc_state& state)
{
	return _state > state._state;
}

bool mc_state::operator<(mc_state& state)
{
	return _state < state._state;
}

