#include <float.h>
#include <list>

#include "mc_transition_case.h"
#include "mc_state.h"
#include "mc_matrix.h"
#include "mc_formulas.h"

using namespace htm;

mc_transition_case::mc_transition_case(mc_matrix& matrix, mc_formulas& formulas)
: _matrix(matrix), _formulas(formulas)
{
}

mc_transition_case::mc_transition_case(const mc_transition_case& orig)
: _matrix(orig._matrix), _formulas(orig._formulas)
{
}

mc_transition_case::~mc_transition_case()
{
}

double mc_transition_case::rateCase(mc_state& inState, mc_state& outState)
{
	// implementation in src/cases
	return 0;
}

void mc_transition_case::outStatesCase(mc_state& inState,
	list<mc_state*>& outStates)
{
	// implementation in src/cases
}

bool mc_transition_case::isInList(mc_state* state,
	list<mc_state*>& outStates)
{

	list<mc_state*>::iterator it;
	bool res = false;

	for (it = outStates.begin(); it != outStates.end(); ++it) {
		if (**it == *state) {
			res = true;
			break;
		}
	}

	return res;
}

bool mc_transition_case::isRateValid(mc_state& inState, mc_state& outState)
{

	int budget = _formulas.getBudget();
	int threads = _formulas.getThreads();

	return !((_formulas.pt(inState, _matrix) < DBL_EPSILON
		&& outState[budget + 1] < threads)
		|| (_formulas.pt(inState, _matrix) > (1.0 - DBL_EPSILON)
		&& outState[budget + 1] > 0));
}

void mc_transition_case::addToList(mc_state& inState, mc_state& outState,
	mc_state* state, list<mc_state*>& states)
{

	if (!isInList(state, states) && isRateValid(inState, outState)) {
		states.push_back(state);
	} else {
		delete state;
	}
}

