#ifndef RATIO_TRAN_NOTX_H
#define RATIO_TRAN_NOTX_H

#include <armadillo>

using namespace std;
using namespace arma;

class Ratio_TRAN_NOTX {
public:
	Ratio_TRAN_NOTX();
	Ratio_TRAN_NOTX(const Ratio_TRAN_NOTX& orig);
	virtual ~Ratio_TRAN_NOTX();
	
	void SetProbCommit(double);
	void SetProbTran(double);
	void SetMuTran(double);
	void SetMuNotx(double);
	
	void Compute();
	
	double GetResTran();
	double GetResNotx();
	
private:
	double prob_commit, prob_tran, mu_tran, mu_notx;
	double res_tran, res_notx;
} ;

#endif /* RATIO_TRAN_NOTX_H */

