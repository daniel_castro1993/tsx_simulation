#ifndef FORMULAS_H
#define FORMULAS_H

class Formulas {
public:
	
	static Formulas* getInstance();
	
	virtual ~Formulas();
	
	void SetPw(double);
	void SetPt(double);
	void SetD(double);
	void SetL(double);
	void SetC(double);
	void SetThreads(double);
	void SetBudget(double);
	void SetRatioTran(double);
	
	double GetPw();
	double GetPt();
	double GetD();
	double GetL();
	double GetC();
	double GetThreads();
	double GetBudget();
	double GetRatioTran();
	
	double H(int);
	double P_R();
	double R();
	
private:
	Formulas();
	
	double pw, pt, D, L, C, threads, budget, r_TRAN;
} ;

#endif /* FORMULAS_H */

