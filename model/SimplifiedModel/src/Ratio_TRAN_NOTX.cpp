#include "Ratio_TRAN_NOTX.h"

#include <cstdio>

Ratio_TRAN_NOTX::Ratio_TRAN_NOTX()
{
}

Ratio_TRAN_NOTX::Ratio_TRAN_NOTX(const Ratio_TRAN_NOTX& orig)
{
}

Ratio_TRAN_NOTX::~Ratio_TRAN_NOTX()
{
}

void Ratio_TRAN_NOTX::SetProbCommit(double _p)
{
	prob_commit = _p;
}

void Ratio_TRAN_NOTX::SetProbTran(double _p)
{
	prob_tran = _p;
}

void Ratio_TRAN_NOTX::SetMuTran(double _mu)
{
	mu_tran = _mu;
}

void Ratio_TRAN_NOTX::SetMuNotx(double _mu)
{
	mu_notx = _mu;
}

void Ratio_TRAN_NOTX::Compute()
{
	mat A;
	mat b;
	mat x;

	A.set_size(2, 3);
	A(0, 0) = -1 * (1 - prob_tran) * prob_commit * mu_tran;
	A(0, 1) = (1 - prob_tran) * prob_commit * mu_tran;
	A(1, 0) = prob_tran * mu_notx;
	A(1, 1) = -1 * prob_tran * mu_notx;
	A(0, 2) = 1;
	A(1, 2) = 1;

	//	printf("===================== \n");
	//	A.print("A: ");
	//	printf("===================== \n");

	b.set_size(2, 1);
	b(0, 0) = 1;
	b(1, 0) = 1;

	x = solve(A * A.st(), b);

	res_tran = x[0];
	res_notx = x[1];
}

double Ratio_TRAN_NOTX::GetResTran()
{
	return res_tran;
}

double Ratio_TRAN_NOTX::GetResNotx()
{
	return res_notx;
}
