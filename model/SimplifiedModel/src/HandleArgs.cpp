#include "HandleArgs.h"
#include "Formulas.h"

#include <string>

using namespace std;

HandleArgs::HandleArgs(int argc, char **argv)
{
	Formulas *f = Formulas::getInstance();
	int i = 0;

	f->SetRatioTran(0.5);
	f->SetD(1024);
	f->SetL(20);
	f->SetC(1.0);
	f->SetThreads(4);
	f->SetBudget(5);
	f->SetPw(0.5);
	f->SetPt(0.5);

	while (++i < argc - 1) {
		string param(argv[i]);
		string value(argv[i + 1]);

		if (param == "D") {
			f->SetD(atoi(value.c_str()));
			++i;
		}
		else if (param == "L") {
			f->SetL(atoi(value.c_str()));
			++i;
		}
		else if (param == "C") {
			f->SetC(atof(value.c_str()));
			++i;
		}
		else if (param == "THREADS") {
			f->SetThreads(atoi(value.c_str()));
			++i;
		}
		else if (param == "BUDGET") {
			f->SetBudget(atoi(value.c_str()));
			++i;
		}
		else if (param == "PW") {
			f->SetPw(atof(value.c_str()));
			++i;
		}
		else if (param == "PT") {
			f->SetPt(atof(value.c_str()));
			++i;
		}
	}
	
	// print args
	printf("ARGS: \n");
	printf(" -       D: %.0f \n", f->GetD());
	printf(" -       L: %.0f \n", f->GetL());
	printf(" -       C: %f   \n", f->GetC());
	printf(" - THREADS: %.0f \n", f->GetThreads());
	printf(" -  BUDGET: %.0f \n", f->GetBudget());
	printf(" -      PW: %f   \n", f->GetPw());
	printf(" -      PT: %f   \n", f->GetPt());
}

HandleArgs::~HandleArgs()
{
}

