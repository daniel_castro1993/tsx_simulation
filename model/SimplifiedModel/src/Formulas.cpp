#include "Formulas.h"
#include <cmath>
#include <cstdlib>

static Formulas *instance;

Formulas::Formulas()
{
}

Formulas::~Formulas()
{
}

double Formulas::H(int i)
{
	if (i == 0) {
		return 0;
	}

	Formulas *f = Formulas::getInstance();
	double W = f->GetC() / f->GetL();
	double conf_rate = f->GetThreads() * f->GetRatioTran() / W;
	double conf_p = pow(1 - (1 - f->GetPw()), 2);

	return conf_rate * conf_p * i / f->GetD();
}

double auxPR(int i, Formulas *f)
{
	if (i == 0) {
		return 1;
	}
	// this could be optimized
	double W = f->GetC() / f->GetL();
	double exp_expr = exp(- f->H(i - 1) * W);

	return auxPR(i - 1, f) * exp_expr;
}

double Formulas::P_R()
{
	Formulas *f = Formulas::getInstance();
	return auxPR(f->GetL(), f);
}

double Formulas::R()
{
	Formulas *f = Formulas::getInstance();
	int i;
	double res = P_R() * f->GetC();

	for (i = 1; i < f->GetL(); ++i) {
		double _i = i;
		double p_r_i = auxPR(i, f);
		double h_i = H(i);
		double w = f->GetC() / f->GetL();
		double exp_expr = exp(-h_i * w);
		res += p_r_i * (_i * w * (1 - exp_expr) + 1.0 / h_i
				- exp_expr * (w + 1.0 / h_i));
	}
	
	return res;
}

void Formulas::SetPw(double _p)
{
	pw = _p;
}

void Formulas::SetPt(double _p)
{
	pt = _p;
}

void Formulas::SetD(double _p)
{
	D = _p;
}

void Formulas::SetL(double _p)
{
	L = _p;
}

void Formulas::SetC(double _p)
{
	C = _p;
}

void Formulas::SetThreads(double _p)
{
	threads = _p;
}

void Formulas::SetBudget(double _p)
{
	budget = _p;
}

void Formulas::SetRatioTran(double _p)
{
	r_TRAN = _p;
}

double Formulas::GetPw()
{
	return pw;
}

double Formulas::GetPt()
{
	return pt;
}

double Formulas::GetD()
{
	return D;
}

double Formulas::GetL()
{
	return L;
}

double Formulas::GetC()
{
	return C;
}

double Formulas::GetThreads()
{
	return threads;
}

double Formulas::GetBudget()
{
	return budget;
}

double Formulas::GetRatioTran()
{
	return r_TRAN;
}

Formulas* Formulas::getInstance()
{
	if (instance == NULL) {
		instance = new Formulas();
	}

	return instance;
}
