#include "Ratio_TRAN_NOTX.h"
#include "HandleArgs.h"
#include "Formulas.h"

#include <cstdlib>
#include <cstdio>

int main(int argc, char **argv)
{
	HandleArgs(argc, argv);
	Ratio_TRAN_NOTX m;
	Formulas *f = Formulas::getInstance();
	int tries = 10;
	double p_NOTX, p_TRAN, p_COMMIT, R;
	double threads = f->GetThreads();

	while (tries-- > 0) {

		R = f->R();
		p_COMMIT = f->P_R();

		m.SetMuNotx(1.0 / f->GetC());
		m.SetMuTran(1.0 / R);
		m.SetProbTran(f->GetPt());
		m.SetProbCommit(p_COMMIT);

		m.Compute();

		p_NOTX = m.GetResNotx();
		p_TRAN = m.GetResTran();

		f->SetRatioTran(p_TRAN);
	}

	p_NOTX = m.GetResNotx();
	p_TRAN = m.GetResTran();

	double p_a_l = 1.0 - f->P_R();
	double R_l = f->R();
	double p_SERL = pow(p_a_l, f->GetBudget());

	double X = (threads / R_l * p_TRAN * (1.0 - p_SERL) // TRAN
			+ 1.0 / f->GetC() * p_TRAN * p_SERL // SERL
			+ threads / f->GetC() * p_NOTX); // NOTX

	printf("\np_TRAN=%f, p_NOTX=%f, p_SERL=%f\n", p_TRAN, p_NOTX, p_SERL);
	printf("p_a=%f, X=%f\n", p_a_l, X);

	return EXIT_SUCCESS;
}

