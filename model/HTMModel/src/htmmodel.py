from matrix_creator import TSXMatrix
import time

pt = 0.5
pa = 0.2
pc = 0.01
#
mt = 100
ma = 50
mf = 1
mn = 300
mc = 10
#msum = mt + ma + mf + mn + mc
#mt /= msum
#ma /= msum
#mf /= msum
#mn /= msum
#mc /= msum
#
#mth = 1 / (pa/ma + (1-pa)/mt + pc/mc)

##for k in range(11):
##        i += [k]
##        j += [10]
##        data += [1]

##        bi += [k]
##        bj += [0]
##        bdata += [1]

##for k in range(10):
##        i += [10]
##        j += [k]
##        data += [0]

#a = dok_matrix((data, (i, j)), shape=(10, 10))
#
#b = dok_matrix((bdata, (bi, bj)), shape=(10, 1))

#i = [0, 0,
#  1, 1, 1, 1,
#  2, 2,
#  3, 3, 3, 3, 3,
#  4, 4,
#  5, 5,
#  6, 6, 6, 6,
#  7, 7,
#  8, 8, 8, 8, 8, 8,
#  9, 9, 9]
#j = [0, 6,
#  0, 1, 6, 7,
#  1, 7,
#  0, 1, 3, 6, 8,
#  3, 8,
#  2, 8,
#  0, 3, 6, 9,
#  6, 9,
#  3, 4, 5, 6, 8, 9,
#  6, 8, 9]
#data = [2 * mn * (1-pt), 2 * mn * pt,
#  mf * (1-pt), mn * (1-pt), mf * pt, mn * pt,
#  mf * (1-pt), mf * pt,
#  mth * (1-pa) * (1-pt), mth * (pa + pc), mn * (1-pt), mth * (1-pa) * pt, mn * pt,
#  mf * (1-pt), mf * pt,
#  2 * mth * (pa + pc), 2 * mth * (1-pa) * pt,
#  mth * (1-pa) * (1-pt), mth * pa, mth * (1-pa) * pt + mn * (1-pt), mn * pt,
#  mf * (1-pt), mf * pt,
#  mth * (1-pa) * (1-pt), mth * (pa + pc), mth * pa, mth * (1-pa) * (1-pt),
#    mth * (1-pa) * pt, mth * (1-pa) * pt,
#  2 * mth * (1-pa) * (1-pt), 2 * pa * pt, 2 * mth * (1-pa) * pt]
#
#bi = []
#bj = []
#bdata = []

if __name__ == "__main__":
  
  ts0 = time.time()
  m = TSXMatrix(8, 5)
  m.setMc(lambda state: mc)
  m.setMa(lambda state: ma)
  m.setMf(lambda state: mf)
  m.setMn(lambda state: mn)
  m.setMt(lambda state: mt)

  m.setPa(lambda state: pa)
  m.setPc(lambda state: pc)
  m.setPt(lambda state: pt)
  m.calculateMth()
  ts1 = time.time()
  
  print("generate states:", (ts1 - ts0), "s")
  
  ts2 = time.time()
  m.generateTransitions()
  ts3 = time.time()
  
  print("generate transitions:", (ts3 - ts2), "s")
  
  ts4 = time.time()
  m.solveModel()
  ts5 = time.time()
  
  print("solve:", (ts5 - ts4), "s")

  print("nb_states:", len(m.states))
#  print("with [Q|1]:\n", m.sol)
  print("with Q last line replaced:\n", m.sol1)
