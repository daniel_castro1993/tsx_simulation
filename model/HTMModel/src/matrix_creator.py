import numpy as np
from scipy.sparse import dok_matrix, csc_matrix
from scipy.sparse.linalg import spsolve
from numpy.linalg import matrix_rank

class TSXMatrix:
  def __init__(self, threads, retries):
    self.threads = threads
    self.retries = retries

    self.states = []
    self.generateStates()

    l = len(self.states)
    
    self.matrix = dok_matrix((l, l), dtype=np.longdouble)

  def setPa(self, pa):
    self.pa = pa

  def setPc(self, pc):
    self.pc = pc

  def setPt(self, pt):
    self.pt = pt

  def setMn(self, mn):
    self.mn = mn

  def setMt(self, mt):
    self.mt = mt

  def setMf(self, mf):
    self.mf = mf

  def setMc(self, mc):
    self.mc = mc
    
  def setMa(self, ma):
    self.ma = ma

  def calculateMth(self):
    self.mth = lambda state: (1 /
                              (self.pa(state) / self.ma(state) +
                              (1-self.pa(state)) / self.mt(state) +
                              self.pc(state) / self.mc(state)))

  def calculateOutStates(self, state):
    ''' Calculates the next states from the given state.

        Does not jump for the same state.

        state -- the state to calculate the out states
        '''
        
    def check(states, state, out_state):
      return state != out_state and out_state not in states
      
    indexState = self.states.index(state)
    res = []

    # non-transactions are able to finish
    # and start other non-transactions
    if state[self.retries + 1] > 0:
      other_tx = state[:]
      other_tx[self.retries + 1] -= 1 
      other_tx[0] += 1
      if check(res, state, other_tx):
        # n * mn * pt
        indexOut = self.states.index(other_tx)
        self.matrix[indexState, indexOut] = (state[self.retries + 1]
                                             * self.mn(state)
                                             * self.pt(state))
        res += [other_tx]
        
    # check if there is serialized mode transaction
    if state[self.retries] > 0:
      # or one serialized transaction starts
      # a new transaction or a non-transaction
      transac = state[:]
      transac[self.retries] -= 1
      transac[0] += 1
      if check(res, state, transac):
        # mf * pt
        indexOut = self.states.index(transac)
        self.matrix[indexState, indexOut] = (self.mf(state)
                                             * self.pt(state))
        res += [transac]

      non_tx = state[:]
      non_tx[self.retries] -= 1
      non_tx[self.retries + 1] += 1
      if check(res, state, non_tx):
        # mf * (1 - pt)
        indexOut = self.states.index(non_tx)
        self.matrix[indexState, indexOut] = (self.mf(state)
                                             * (1 - self.pt(state)))
        res += [non_tx]
    else:
      if state[self.retries - 1] > 0:
        # someone enter serialized mode, shift
        # all transactions right
        new_state = state[:]
        for i in range(self.retries):
          j = self.retries - i
          k = j - 1
          new_state[j] += new_state[k]
          new_state[k] = 0
        if check(res, state, new_state):
          # t1 * mth * (pa + pc)
          indexOut = self.states.index(new_state)
          self.matrix[indexState, indexOut] = (state[self.retries - 1]
                                               * self.mth(state)
                                               * (self.pa(state)
                                               + self.pc(state)))
          res += [new_state]

      for i in range(self.retries):
        if state[i] > 0:
          transac = state[:]
          transac[i] -= 1
          transac[0] += 1
          if check(res, state, transac):
            # ti * mth * (1 - pa) *  pt
            indexOut = self.states.index(transac)
            self.matrix[indexState, indexOut] = (state[self.retries - 1]
                                                 * self.mth(state)
                                                 * (1 - self.pa(state))
                                                 * self.pt(state))
            res += [transac]

          non_tx = state[:]
          non_tx[i] -= 1
          non_tx[self.retries + 1] += 1
          if check(res, state, non_tx):
            # ti * mth * (1 - pa) * (1 - pt)
            indexOut = self.states.index(non_tx)
            self.matrix[indexState, indexOut] = (state[self.retries - 1]
                                                 * self.mth(state)
                                                 * (1 - self.pa(state))
                                                 * (1 - self.pt(state)))
            res += [non_tx]
                
      for i in range(self.retries - 1):
        if state[i] > 0:
          new_state = state[:]
          new_state[i] -= 1
          new_state[i + 1] += 1
          if check(res, state, new_state):
            # ti * mth * pa
            indexOut = self.states.index(new_state)
            self.matrix[indexState, indexOut] = (state[i]
                                                 * self.mth(state)
                                                 * self.pa(state))
            res += [new_state]
            
    res.sort()
    return res
                        
                        
  def generateTransitions(self):
    for state in self.states:
      self.calculateOutStates(state)

    sumRows = self.matrix.sum(axis=1)
    
    for i in range(len(sumRows)):
      self.matrix[i, i] = -sumRows[i]
    
  def solveModel(self):
    l = len(self.states)
    
#    Q = dok_matrix(self.matrix)
#    Q.resize((l, l + 1))
    Q1 = dok_matrix(self.matrix)
    
#    b = dok_matrix((l + 1, 1), dtype=np.longdouble)
#    b[l, 0] = 1
    b1 = dok_matrix((l, 1), dtype=np.longdouble)
    b1[l - 1, 0] = 1
    
    for i in range(l):
#      Q[i, l] = 1
      Q1[i, l - 1] = 1
    
#    print("rank Q:", matrix_rank(dok_matrix(self.matrix, dtype=np.float32).todense()))
#    print("rank [Q|1]:", matrix_rank(dok_matrix(Q, dtype=np.float32).todense()))
    
#    _Q = csc_matrix(Q)
    _Q1 = csc_matrix(Q1)
    
#    sq_Q = _Q * _Q.transpose()
    
#    self.sol = spsolve(sq_Q, _Q * b)
    self.sol1 = spsolve(_Q1.transpose(), b1)
    
#    for i in range(l):
##      self.matrix[i, l - 1] = tmp[i]
#      self.matrix[i, l] = 0
    
  def generateStates(self):
    def stateInc(state, threads, inc):
#      print(state)
      j = 0
      t = len(state)
      for i in range(t):
        j += state[i] * ((threads + 1)**(t-i-1))
        
      j += inc
#      print(j)
      
      for i in range(t):
        state[t-i-1] = j % (threads + 1)
        j = int(j / (threads + 1))
        
#      print(state)
      return state
            
    state = []
        
    for i in range(self.retries + 1):
      state += [0]
    state += [self.threads]

    while state[0] < self.threads:
      self.states += [state[:]]
            
      state = stateInc(state, self.threads, self.threads)
      while sum(state) != self.threads:
        state = stateInc(state, self.threads, self.threads)
        
    self.states += [state[:]]


