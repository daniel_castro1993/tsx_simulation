import numpy as np
import sys

from os import listdir
from os.path import isfile, join

import re
import json

import matplotlib as mpl

mpl.use('Agg')

import matplotlib.pyplot as plt

mpl.rcParams['legend.numpoints'] = 1

data_file = sys.argv[1]

probs_r = []
nb_items = []
nb_ops = []
c_times = []
lambdas = []
R_sim_data = []
R_ana_data = []
PR_sim_data = []
PR_ana_data = []

avg_percent_error_R = 0
avg_percent_error_PR = 0

samples = 0

class PlotFigure:
	def __init__(self, sim_data, ana_data, title, error):
		self.cm = plt.cm.get_cmap('rainbow')
		self.x = np.array(sim_data)
		self.y = np.array(ana_data)
		self.error = error
		self.title = title
		
		self.per_probab_r = {}
		self.per_nb_items = {}
		self.per_nb_opera = {}
		self.per_cm_times = {}
		self.per_lambda_r = {}
		
		for i in range(len(sim_data)):
			if probs_r[i] not in self.per_probab_r:
				self.per_probab_r[probs_r[i]] = {"sim":[], "ana":[], "nit":[], "nop":[], "cti":[], "lam":[], "err":0}
			if nb_items[i] not in self.per_nb_items:
				self.per_nb_items[nb_items[i]] = {"sim":[], "ana":[], "prr":[], "nop":[], "cti":[], "lam":[], "err":0}
			if nb_ops[i] not in self.per_nb_opera:
				self.per_nb_opera[nb_ops[i]] = {"sim":[], "ana":[], "prr":[], "nit":[], "cti":[], "lam":[], "err":0}
			if c_times[i] not in self.per_cm_times:
				self.per_cm_times[c_times[i]] = {"sim":[], "ana":[], "prr":[], "nit":[], "nop":[], "lam":[], "err":0}
			if lambdas[i] not in self.per_lambda_r:
				self.per_lambda_r[lambdas[i]] = {"sim":[], "ana":[], "prr":[], "nit":[], "nop":[], "cti":[], "err":0}
			
			self.per_probab_r[probs_r[i]]["sim"].append(sim_data[i])
			self.per_probab_r[probs_r[i]]["ana"].append(ana_data[i])
			self.per_probab_r[probs_r[i]]["nit"].append(nb_items[i])
			self.per_probab_r[probs_r[i]]["nop"].append(nb_ops[i])
			self.per_probab_r[probs_r[i]]["cti"].append(c_times[i])
			self.per_probab_r[probs_r[i]]["lam"].append(lambdas[i])
			self.per_probab_r[probs_r[i]]["err"] += abs(sim_data[i] - ana_data[i]) / sim_data[i]
			
			self.per_nb_items[nb_items[i]]["sim"].append(sim_data[i])
			self.per_nb_items[nb_items[i]]["ana"].append(ana_data[i])
			self.per_nb_items[nb_items[i]]["prr"].append(probs_r[i])
			self.per_nb_items[nb_items[i]]["nop"].append(nb_ops[i])
			self.per_nb_items[nb_items[i]]["cti"].append(c_times[i])
			self.per_nb_items[nb_items[i]]["lam"].append(lambdas[i])
			self.per_nb_items[nb_items[i]]["err"] += abs(sim_data[i] - ana_data[i]) / sim_data[i]
			
			self.per_nb_opera[nb_ops[i]]["sim"].append(sim_data[i])
			self.per_nb_opera[nb_ops[i]]["ana"].append(ana_data[i])
			self.per_nb_opera[nb_ops[i]]["prr"].append(probs_r[i])
			self.per_nb_opera[nb_ops[i]]["nit"].append(nb_items[i])
			self.per_nb_opera[nb_ops[i]]["cti"].append(c_times[i])
			self.per_nb_opera[nb_ops[i]]["lam"].append(lambdas[i])
			self.per_nb_opera[nb_ops[i]]["err"] += abs(sim_data[i] - ana_data[i]) / sim_data[i]
			
			self.per_cm_times[c_times[i]]["sim"].append(sim_data[i])
			self.per_cm_times[c_times[i]]["ana"].append(ana_data[i])
			self.per_cm_times[c_times[i]]["prr"].append(probs_r[i])
			self.per_cm_times[c_times[i]]["nit"].append(nb_items[i])
			self.per_cm_times[c_times[i]]["nop"].append(nb_ops[i])
			self.per_cm_times[c_times[i]]["lam"].append(lambdas[i])
			self.per_cm_times[c_times[i]]["err"] += abs(sim_data[i] - ana_data[i]) / sim_data[i]
			
			self.per_lambda_r[lambdas[i]]["sim"].append(sim_data[i])
			self.per_lambda_r[lambdas[i]]["ana"].append(ana_data[i])
			self.per_lambda_r[lambdas[i]]["prr"].append(probs_r[i])
			self.per_lambda_r[lambdas[i]]["nit"].append(nb_items[i])
			self.per_lambda_r[lambdas[i]]["nop"].append(nb_ops[i])
			self.per_lambda_r[lambdas[i]]["cti"].append(c_times[i])
			self.per_lambda_r[lambdas[i]]["err"] += abs(sim_data[i] - ana_data[i]) / sim_data[i]
		
		for i in self.per_probab_r:
			self.per_probab_r[i]["err"] = (self.per_probab_r[i]["err"] / len(self.per_probab_r[i]["ana"])) * 100
		for i in self.per_nb_items:
			self.per_nb_items[i]["err"] = (self.per_nb_items[i]["err"] / len(self.per_nb_items[i]["ana"])) * 100
		for i in self.per_nb_opera:
			self.per_nb_opera[i]["err"] = (self.per_nb_opera[i]["err"] / len(self.per_nb_opera[i]["ana"])) * 100
		for i in self.per_cm_times:
			self.per_cm_times[i]["err"] = (self.per_cm_times[i]["err"] / len(self.per_cm_times[i]["ana"])) * 100
		for i in self.per_lambda_r:
			self.per_lambda_r[i]["err"] = (self.per_lambda_r[i]["err"] / len(self.per_lambda_r[i]["ana"])) * 100
		
	def plot_color_data(self, y, x, z, error, color_title, image_name):
		fig = plt.figure()
		ax1 = fig.add_subplot(111)
		
		lim_min = min(min(np.array(x)), min(np.array(y))) - 0.05
		lim_max = max(max(np.array(x)), max(np.array(y))) + 0.05
		
		ax1.set_title(self.title + " (average percent error = {:5.2f}%)".format(error), y=1.06, x=0.60)
		ax1.set_xlabel('simulation')
		ax1.set_ylabel('analytical')
		
		plt.scatter(np.array(x), np.array(y), c=np.array(z), s=15, cmap=self.cm)
		plt.colorbar().set_label(color_title)
		
		ax1.set_xlim([lim_min, lim_max])
		ax1.set_ylim([lim_min, lim_max])

		fig.savefig(image_name + ".png")
		plt.close(fig)
	
		plt.clf()
		plt.cla()	
	
	def plot(self, image_name):
		fig = plt.figure()
		ax1 = fig.add_subplot(111)
		
		ax1.set_title(self.title + " (average percent error = {:5.2f}%)".format(self.error), y=1.06, x=0.50)
		ax1.set_xlabel('simulation')
		ax1.set_ylabel('analytical')
		
		plt.scatter(self.x, self.y)

		fig.savefig(image_name + ".png")
		plt.close(fig)
	
		plt.clf()
		plt.cla()
		
		for i in self.per_probab_r:
			self.plot_color_data(self.per_probab_r[i]["ana"], self.per_probab_r[i]["sim"], self.per_probab_r[i]["nit"],
				self.per_probab_r[i]["err"], "Number of items", image_name + "_prob_r_equal_" + str(i) + "___nb_items")
			self.plot_color_data(self.per_probab_r[i]["ana"], self.per_probab_r[i]["sim"], self.per_probab_r[i]["nop"],
				self.per_probab_r[i]["err"], "Number of operations", image_name + "_prob_r_equal_" + str(i) + "___nb_opera")
			self.plot_color_data(self.per_probab_r[i]["ana"], self.per_probab_r[i]["sim"], self.per_probab_r[i]["cti"],
				self.per_probab_r[i]["err"], "commit time", image_name + "_prob_r_equal_" + str(i) + "___cm_times")
			self.plot_color_data(self.per_probab_r[i]["ana"], self.per_probab_r[i]["sim"], self.per_probab_r[i]["lam"],
				self.per_probab_r[i]["err"], "lambda", image_name + "_prob_r_equal_" + str(i) + "___lambda")
		
		for i in self.per_nb_items:
			self.plot_color_data(self.per_nb_items[i]["ana"], self.per_nb_items[i]["sim"], self.per_nb_items[i]["prr"],
				self.per_nb_items[i]["err"], "Probability of read", image_name + "_nb_items_equal_" + str(i) + "___prob_r")
			self.plot_color_data(self.per_nb_items[i]["ana"], self.per_nb_items[i]["sim"], self.per_nb_items[i]["nop"],
				self.per_nb_items[i]["err"], "Number of operations", image_name + "_nb_items_equal_" + str(i) + "___nb_opera")
			self.plot_color_data(self.per_nb_items[i]["ana"], self.per_nb_items[i]["sim"], self.per_nb_items[i]["cti"],
				self.per_nb_items[i]["err"], "commit time", image_name + "_nb_items_equal_" + str(i) + "___cm_times")
			self.plot_color_data(self.per_nb_items[i]["ana"], self.per_nb_items[i]["sim"], self.per_nb_items[i]["lam"],
				self.per_nb_items[i]["err"], "lambda", image_name + "_nb_items_equal_" + str(i) + "___lambda")
			
		#for i in self.per_nb_opera:
		#	self.plot_color_data(self.per_nb_opera[i]["ana"], self.per_nb_opera[i]["sim"], self.per_nb_opera[i]["nit"],
		#		self.per_nb_opera[i]["err"], "Number of items", image_name + "_nb_opera_equal_" + str(i) + "____nb_items")
		#	self.plot_color_data(self.per_nb_opera[i]["ana"], self.per_nb_opera[i]["sim"], self.per_nb_opera[i]["cti"],
		#		self.per_nb_opera[i]["err"], "commit time", image_name + "_nb_opera_equal_" + str(i) + "___cm_times")
		#	self.plot_color_data(self.per_nb_opera[i]["ana"], self.per_nb_opera[i]["sim"], self.per_nb_opera[i]["lam"],
		#		self.per_nb_opera[i]["err"], "lambda", image_name + "_nb_opera_equal_" + str(i) + "___lambda")
			
		#for i in self.per_cm_times:
		#	self.plot_color_data(self.per_cm_times[i]["ana"], self.per_cm_times[i]["sim"], self.per_cm_times[i]["nit"],
		#		self.per_cm_times[i]["err"], "Number of items", image_name + "_cm_times_equal_" + str(i) + "____nb_items")
		#	self.plot_color_data(self.per_cm_times[i]["ana"], self.per_cm_times[i]["sim"], self.per_cm_times[i]["nop"],
		#		self.per_cm_times[i]["err"], "Number of operations", image_name + "_cm_times_equal_" + str(i) + "___nb_opera")
		#	self.plot_color_data(self.per_cm_times[i]["ana"], self.per_cm_times[i]["sim"], self.per_cm_times[i]["lam"],
		#		self.per_cm_times[i]["err"], "lambda", image_name + "_cm_times_equal_" + str(i) + "___lambda")
			
		for i in self.per_lambda_r:
			self.plot_color_data(self.per_lambda_r[i]["ana"], self.per_lambda_r[i]["sim"], self.per_lambda_r[i]["prr"],
				self.per_lambda_r[i]["err"], "Probability of read", image_name + "_lambda_equal_" + str(i) + "___prob_r")
			self.plot_color_data(self.per_lambda_r[i]["ana"], self.per_lambda_r[i]["sim"], self.per_lambda_r[i]["nit"],
				self.per_lambda_r[i]["err"], "Number of items", image_name + "_lambda_equal_" + str(i) + "___nb_items")
			self.plot_color_data(self.per_lambda_r[i]["ana"], self.per_lambda_r[i]["sim"], self.per_lambda_r[i]["nop"],
				self.per_lambda_r[i]["err"], "Number of operations", image_name + "_lambda_equal_" + str(i) + "___nb_opera")
			self.plot_color_data(self.per_lambda_r[i]["ana"], self.per_lambda_r[i]["sim"], self.per_lambda_r[i]["cti"],
				self.per_lambda_r[i]["err"], "commit time", image_name + "_lambda_equal_" + str(i) + "___cm_times")

with open(data_file) as f:
	data = f.read()
	data = data.split('\n')
	first_line = False
	
	for d in data:
		if not first_line:
			first_line = True
		else:
			m = re.search(r'([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+),([^,]+)', d)
			
			if m != None:
				
				probs_r.append(float(m.group(1)))
				nb_items.append(int(m.group(2)))
				nb_ops.append(int(m.group(3)))
				c_times.append(float(m.group(4)))
				lambdas.append(int(m.group(5)))
				
				R_sim = float(m.group(6))
				R_ana = float(m.group(7))
				PR_sim = float(m.group(8))
				PR_ana = float(m.group(9))
				R_sim_data.append(R_sim)
				R_ana_data.append(R_ana)
				PR_sim_data.append(PR_sim)
				PR_ana_data.append(PR_ana)
				
				avg_percent_error_R += abs(R_ana - R_sim) / R_sim 
				avg_percent_error_PR += abs(PR_ana - PR_sim) / PR_sim 
				
				samples += 1
				
				# print("sample {:d}: {: e} {: e} {: e} {: e}".format(samples, R_sim, R_ana, PR_sim, PR_ana))

avg_percent_error_R = (avg_percent_error_R / samples) * 100
avg_percent_error_PR = (avg_percent_error_PR / samples) * 100

R_data_title = "transaction time" 
plot_r = PlotFigure(R_sim_data, R_ana_data, R_data_title, avg_percent_error_R)
plot_r.plot('./r')

PR_data_title = "probability of reaching last operation"
plot_pr = PlotFigure(PR_sim_data, PR_ana_data, PR_data_title, avg_percent_error_PR)
plot_pr.plot('./pr')

			
