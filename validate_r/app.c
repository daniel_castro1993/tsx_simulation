/**
 * Lock simulation.
 * 
 * Simulates multiple processes grabbing a global lock. Some processes will have
 * to wait for the previous one to unlock.
 */

#include <math.h>

#include "app.h"

#define GEN_ADDR        ((int) round(Random() * arg_nb_items) % arg_nb_items)
#define GEN_ACCESS      (Random() < arg_prob_r ? ACCESS_READ : ACCESS_WRITE)
#define NEXT_TIME       Expent((double) 1 / (double) arg_lambda)
#define C_OVER_L        ((double) arg_c_time / (double) arg_nb_ops)
#define NEXT_TIME_TX    C_OVER_L // Expent(C_OVER_L)

int tx_pid = 0, cnf_pid = 1, txid = 0;

int arg_nb_items = -1, arg_nb_ops = -1, arg_lambda = -1;
double arg_prob_r = -1, arg_c_time = -1;

bool in_array(int elem, int * array, int size) {

  int i;

  for (i = 0; i < size; i++) {
    if (array[i] == elem) {
      return true;
    }
  }
  return false;
}

bool check_cnf(access_op_t op, access_op_t * ops, int size) {

  int i, addr = op.addr, access = op.access;

  for (i = 0; i < size; i++) {
    if (ops[i].addr == addr) {
      if (!(access & (ACCESS_READ | ACCESS_WRITE))) {
        return true;
      } else {
        return ops[i].access == ACCESS_READ && access == ACCESS_WRITE
                || ops[i].access == ACCESS_WRITE && access == ACCESS_READ
                || ops[i].access == ACCESS_WRITE && access == ACCESS_WRITE;
      }
    }
  }
  return false;
}

int generate_addr(access_op_t * addrs, int size) {

  int i, res = GEN_ADDR;

  if (addrs != NULL) {
    int addrs_only[size];

    for (i = 0; i < size; i++) {
      addrs_only[i] = addrs[i].addr;
    }

    while (in_array(res, addrs_only, size)) {
      res = GEN_ADDR;
    }
  }

  return res;
}

void event_init(unsigned int me, simtime_t now) {

  int i;
  UTL_event_t event;
  tx_buffer_t tx_buffer;
  cnf_buffer_t cnf_buffer;

  UTL_event_init(&event);
  event.pid = me;

  if (n_prc_tot != 2) {
    printf("Use 2 LPs\n");
    exit(EXIT_SUCCESS);
  }

  if (me == tx_pid) {
    // tx LP

    tx_state_t * state = new(tx_state_t, 1);
    set_zero(state, tx_state_t, 1);

    global_state.tx_ids[global_state.sample] = 1;

    state->txid = ++txid;
    state->ops = new(access_op_t, arg_nb_ops);

    SetState(state);

    event.cnt = &tx_buffer;
    event.cnt_size = sizeof (tx_buffer_t);
    event.event_code = EVENT_TX_ACCESS;

    tx_buffer.txid = state->txid;
    tx_buffer.last_ts = now;
    tx_buffer.addr = generate_addr(NULL, 0);
    tx_buffer.access = GEN_ACCESS;

    SCHEDULE_EVENT(event, now + ELAPSED_TIME);

  } else {
    // conflict LP

    cnf_state_t * state = new(cnf_state_t, 1);
    set_zero(state, cnf_state_t, 1);
    SetState(state);

    event.cnt = &cnf_buffer;
    event.cnt_size = sizeof (cnf_buffer_t);
    event.event_code = EVENT_CNF_CONFLICT;

    SCHEDULE_EVENT(event, now + ELAPSED_TIME);
  }
}

void event_tx(unsigned int me, simtime_t now, unsigned int event_type,
        tx_buffer_t * buffer, tx_state_t * state) {

  UTL_event_t event;
  tx_buffer_t tx_buffer;
  simtime_t next_time;
  access_op_t op;

  int sample_minus_1 = global_state.sample - 1;
  int sample = global_state.sample;

  if (sample_minus_1 < 0) {
    sample_minus_1 = SAMPLE_SIZE - 1;
  }

  UTL_event_init(&event);

  event.pid = me;
  event.cnt = &tx_buffer;
  event.cnt_size = sizeof (tx_buffer);

  switch (event_type) {
    case EVENT_TX_REBOOT:

      if (global_state.nb_ops[sample_minus_1] < arg_nb_ops) {
        global_state.tx_ids[sample] = global_state.tx_ids[sample_minus_1];
      } else {
        global_state.tx_ids[sample] = state->txid;
      }

      global_state.tx_time[sample] = state->elapsed_time;
      global_state.tx_extra_time[sample] = state->extra_time;
      global_state.nb_ops[sample] = state->nb_op;
      global_state.sample = (sample + 1) % SAMPLE_SIZE;
      global_state.nb_samples++;

      state->elapsed_time = 0;
      state->start_ts = now;
      state->access_ts = now;
      state->nb_op = 0;

      tx_buffer.txid = buffer->txid;
      tx_buffer.last_ts = now;
      tx_buffer.addr = generate_addr(state->ops, state->nb_op);
      tx_buffer.access = GEN_ACCESS;

      event.event_code = EVENT_TX_ACCESS;
      next_time = NEXT_TIME_TX;
      SCHEDULE_EVENT(event, now + next_time);

      break;
    case EVENT_TX_ACCESS:

      if (state->txid == buffer->txid) {
        // did not abort nor complete

        state->ops[state->nb_op].addr = buffer->addr;
        state->ops[state->nb_op].access = buffer->access;
        state->nb_op += 1;
        state->elapsed_time += now - state->access_ts;
        state->access_ts = now;

        if (state->nb_op < arg_nb_ops) {

          tx_buffer.txid = state->txid;
          tx_buffer.last_ts = now;
          tx_buffer.addr = generate_addr(state->ops, state->nb_op);
          tx_buffer.access = GEN_ACCESS;

          event.event_code = EVENT_TX_ACCESS;
          next_time = NEXT_TIME_TX;
          SCHEDULE_EVENT(event, now + next_time);
        }
      }

      if (state->nb_op >= arg_nb_ops) {
        // completed

        state->txid = ++txid;
        state->elapsed_time += now - state->access_ts; // add the last op

        tx_buffer.last_ts = buffer->last_ts;
        tx_buffer.txid = state->txid;

        event.event_code = EVENT_TX_REBOOT;
        SCHEDULE_EVENT(event, now + ELAPSED_TIME);
      }
      // if it aborted do nothing; EVENT_TX_CHECK already scheduled a reboot

      break;
    case EVENT_TX_CHECK:

      op.addr = buffer->addr;
      op.access = buffer->access;

      if (check_cnf(op, state->ops, state->nb_op)) {
        // conflict arrise

        state->txid = ++txid;
        state->elapsed_time += now - state->access_ts;
        state->extra_time = now - state->access_ts;

        tx_buffer.last_ts = buffer->last_ts;
        tx_buffer.txid = state->txid;

        event.event_code = EVENT_TX_REBOOT;
        SCHEDULE_EVENT(event, now + ELAPSED_TIME);
      }

      break;
    default:
      break;
  }

}

void event_cnf(unsigned int me, simtime_t now, unsigned int event_type,
        cnf_buffer_t * buffer, cnf_state_t * state) {

  UTL_event_t event;
  tx_buffer_t tx_buffer;
  simtime_t next_time;

  UTL_event_init(&event);

  event.pid = tx_pid;
  event.cnt = &tx_buffer;
  event.cnt_size = sizeof (tx_buffer);

  switch (event_type) {
    case EVENT_CNF_CONFLICT:

      tx_buffer.addr = generate_addr(NULL, 0);
      tx_buffer.access = GEN_ACCESS;

      event.event_code = EVENT_TX_CHECK;
      SCHEDULE_EVENT(event, now + ELAPSED_TIME);

      event.pid = cnf_pid;
      event.event_code = EVENT_CNF_CONFLICT;
      event.cnt = NULL;
      event.cnt_size = 0;

      next_time = NEXT_TIME;
      SCHEDULE_EVENT(event, now + next_time);

      break;
    default:
      break;
  }

}

/**
 * Processes a event for a given process.
 * 
 * @param me
 * @param now
 * @param event_type
 * @param event_content
 * @param size
 * @param ptr
 */
void ProcessEvent(unsigned int me, simtime_t now, unsigned int event,
        void * buffer, unsigned int size, void * state) {

  switch (event) {
    case INIT:

      // todo check a way of doing this
      if (!IsParameterPresent(buffer, "PROB_R")
              || !IsParameterPresent(buffer, "NB_ITEMS")
              || !IsParameterPresent(buffer, "NB_OPS")
              || !IsParameterPresent(buffer, "C_TIME")
              || !IsParameterPresent(buffer, "LAMBDA")) {

        printf("You must pass the parameters like the following \n"
                "PROB_R val0 NB_ITEMS val1 NB_OPS val2 "
                "C_TIME val3 LAMBDA val4\n");
        exit(EXIT_SUCCESS);

      } else {
        arg_prob_r = GetParameterDouble(buffer, "PROB_R");
        arg_nb_items = GetParameterInt(buffer, "NB_ITEMS");
        arg_nb_ops = GetParameterInt(buffer, "NB_OPS");
        arg_c_time = GetParameterDouble(buffer, "C_TIME");
        arg_lambda = GetParameterInt(buffer, "LAMBDA");
      }

      event_init(me, now);

      break;
    case EVENT_TX_REBOOT:
    case EVENT_TX_ACCESS:
    case EVENT_TX_CHECK:

      event_tx(me, now, event, buffer, state);

      break;
    case EVENT_CNF_CONFLICT:

      event_cnf(me, now, event, buffer, state);

      break;
    case EVENT_END:
    default:

      ScheduleNewEvent(me, now + ELAPSED_TIME, EVENT_END, NULL, 0);

      break;
  }

}

/**
 * Checks the simulation.
 * 
 * @param me the process id
 * @param state the current state
 * @return true to terminate the me process
 */
bool OnGVT(unsigned int me, void * state) {

  static int last_sample = 0;

  if (global_state.nb_samples < SAMPLE_SIZE) {
    return false;
  }

  if (global_state.is_end) {
    return true;
  }

  if (me == tx_pid) {
    // tx's LP

    if (global_state.nb_samples != last_sample
            && global_state.nb_samples > SAMPLE_THRESHOLD) {

      int i, j, nb_aborts = 0;
      double l_ci_time, r_ci_time, ci_width_time, simul_r, sd_simul_r;
      double sqrt_sample_size = sqrt(SAMPLE_SIZE);
      double l = (double) arg_lambda * (1 - SQ(arg_prob_r)),
              D = (double) arg_nb_items, c_over_l = C_OVER_L, simul_c_over_l;

      last_sample = global_state.nb_samples;

      simul_r = CALC_AVG(global_state.tx_time, SAMPLE_SIZE);
      sd_simul_r = sqrt(
              CALC_SQ_STDDEV(global_state.tx_time, SAMPLE_SIZE, simul_r)
              );
      ci_width_time = CALC_CI(simul_r, sd_simul_r, sqrt_sample_size,
              l_ci_time, r_ci_time);

      if (IS_WITHIN_CI(simul_r, ci_width_time)) {
        // store the statistic into a file

        double avg_c_l_array[SAMPLE_SIZE];
        double simul_p_r[arg_nb_ops], analit_p_r[arg_nb_ops];
        double simul_p_a[arg_nb_ops], analit_r = 0;
        double p_abort = 0, abort_op = 0;
        double * tx_times;

        int tx_count = 1;

        for (i = 1; i < SAMPLE_SIZE; i++) {
          if (global_state.tx_ids[i] != global_state.tx_ids[i - 1]) {
            tx_count++;
          }
        }

        tx_times = new(double, tx_count);
        set_zero(tx_times, double, tx_count);

        j = 0;
        for (i = 1; i < SAMPLE_SIZE; i++) {
          tx_times[j] += global_state.tx_time[i];
          if (global_state.tx_ids[i] != global_state.tx_ids[i - 1]) {
            j++;
          }
        }

        // recalculates simul_r, but with the restarting txs
        //        simul_r = CALC_AVG(tx_times, tx_count);
        //        sd_simul_r = sqrt(
        //                CALC_SQ_STDDEV(tx_times, tx_count, simul_r)
        //                );

        for (i = 0; i < SAMPLE_SIZE; i++) {
          if (global_state.nb_ops[i] < arg_nb_ops) {
            p_abort += 1;
            abort_op += global_state.nb_ops[i];
            nb_aborts++;
          }
        }
        p_abort = p_abort / SAMPLE_SIZE;
        abort_op = abort_op / nb_aborts;

        for (i = 0; i < SAMPLE_SIZE; i++) {
          avg_c_l_array[i] =
                  (global_state.tx_time[i] - global_state.tx_extra_time[i])
                  / global_state.nb_ops[i];
        }

        for (i = 0; i < arg_nb_ops; i++) {
          simul_p_r[i] = 0;
          analit_p_r[i] = 0;
        }

        analit_p_r[0] = 1;

        for (i = 1; i < arg_nb_ops; i++) {
          double _i = i + 1;
          analit_p_r[i] = analit_p_r[i - 1]
                  * exp(-l * (_i - 1) * c_over_l / D);
        }

        for (i = 0; i < SAMPLE_SIZE; i++) {
          for (j = 0; j < arg_nb_ops; j++) {
            if (global_state.nb_ops[i] > j) {
              simul_p_r[j] += 1;
            }
          }
        }

        for (i = 0; i < arg_nb_ops; i++) {
          simul_p_r[i] = simul_p_r[i] / SAMPLE_SIZE;
        }

        simul_p_a[0] = 0;
        for (i = 1; i < arg_nb_ops; i++) {
          double _i = (double) i;
          simul_p_a[i] = simul_p_r[i - 1] * (1 - exp(-l * c_over_l * _i / D));
        }

        simul_c_over_l = CALC_AVG(avg_c_l_array, SAMPLE_SIZE);

        analit_r = c_over_l
                + analit_p_r[arg_nb_ops - 1] * (arg_c_time - c_over_l);

        //        analit_r = c_over_l;

        for (i = 0; i < arg_nb_ops - 1; i++) {

          double _i = (double) i + 1, D_over_l_i = D / (_i * l);

          double exp_calc = exp(-l * c_over_l * _i / D);
          //          double exp_calc = exp(-l * c_over_l * SQ(_i) / D);
          //          double exp_calc_1 = exp(-l * c_over_l * _i * (_i - 1) / D);

          //          analit_r += analit_p_r[i] * (-1) * exp_calc * (
          //                  (exp_calc * (D_over_l_i + c_over_l * _i))
          //                  - (exp_calc_1 * (D_over_l_i + (_i - 1) * c_over_l))
          //                  );

          analit_r += analit_p_r[i] * (1 - exp_calc) * c_over_l * (_i - 1);
          if (l > 0) {
            analit_r += analit_p_r[i] *
                    (-exp_calc * (D / (_i * l) + c_over_l) + D / (_i * l));
          }
        }

        data_file = fopen(DATA_FILE, DATA_FILE_MODE);

        // This simulation only outputs a file with the SIMULATED values
        // the ANALYTICAL values come from the other model
        
        START_DATA_FILE;
        fprintf(data_file, "%11.2f,%11u,%11u,%11.2f,%11u,", arg_prob_r,
                arg_nb_items, arg_nb_ops, arg_c_time, arg_lambda);
        fprintf(data_file, "%15.8e,%15.8e,", simul_r, analit_r);
        fprintf(data_file, "%15.8e,%15.8e\n",
                simul_p_r[arg_nb_ops - 1], analit_p_r[arg_nb_ops - 1]);

        fclose(data_file);

        global_state.is_end = true;
      }

    }

  }

  return false;
}

