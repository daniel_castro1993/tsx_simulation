#ifndef __APP_H_GUARD
#define __APP_H_GUARD

#include <stdbool.h>
#include "collections.h"
#include "utils.h"
#include "stats.h"

#ifndef DATA_FILE
#define DATA_FILE       ({                                                    \
    char __buffer[512];                                                       \
    sprintf(__buffer, "./data/file-PR_%.2lf-C_%.2lf-L_%i-D_%i-l_%i.txt",      \
      arg_prob_r, arg_c_time, arg_nb_ops, arg_nb_items, arg_lambda);          \
    __buffer;                                                                 \
  })
#endif
#ifndef DATA_FILE_MODE
#define DATA_FILE_MODE  "w"
#endif
#ifndef START_DATA_FILE
#define START_DATA_FILE                                                       \
  fseek(data_file, 0, SEEK_END);                                              \
  if(ftell(data_file) < 8) {                                                  \
    fprintf(data_file,                                                        \
      "%11s,%11s,%11s,%11s,%11s,%15s,%15s,%15s,%15s\n",                       \
      "PROB_READ", "NB_ITEMS", "NB_OPS", "C_TIME", "LAMBDA", "SIMUL_R",       \
      "ANALIT_R", "SIMUL_P_R(L)", "ANALIT_P_R(L)");                           \
  }
#endif

#define ELAPSED_TIME            0.000001
#define SAMPLE_SIZE             25000
// starts checking whether is inside the confidence interval
#define SAMPLE_THRESHOLD        30000

typedef enum _EVENTS {
  EVENT_TX_REBOOT = 1, // tx LP updates global_state
  EVENT_TX_ACCESS, // tx LP accesses some address and stores it
  EVENT_TX_CHECK, // tx LP checkes if aborts

  EVENT_CNF_CONFLICT, // cnf LP accesses some address

  EVENT_END
} EVENTS;

typedef enum _ACCESS {
  ACCESS_READ = 0x01,
  ACCESS_WRITE = 0x02
} ACCESS;

typedef struct _access_op_t {
  int addr;
  ACCESS access;
} access_op_t;

typedef struct _tx_buffer_t {
  int addr, access, txid;
  simtime_t last_ts;
} tx_buffer_t;

typedef struct _cnf_buffer_t {
  int addr, access;
} cnf_buffer_t;

typedef struct _tx_state {
  int txid, nb_op, current_addr;
  access_op_t * ops;
  simtime_t start_ts, access_ts, elapsed_time, extra_time;
} tx_state_t;

typedef struct _cnf_state {
  int current_addr;
} cnf_state_t;

typedef struct _global_state {
  int sample, nb_samples, nb_ops[SAMPLE_SIZE], tx_ids[SAMPLE_SIZE];
  simtime_t tx_time[SAMPLE_SIZE], tx_extra_time[SAMPLE_SIZE];
  bool is_end;
} global_state_t;

FILE * data_file;
global_state_t global_state;

#endif // define __APP_H_GUARD
