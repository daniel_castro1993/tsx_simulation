#ifndef COLLECTIONS_H
#  define COLLECTIONS_H

#  include <stdlib.h>
#  include <stdbool.h>
#  include <string.h>

#  define new(type, size) (type*) malloc(sizeof(type) * (size))
#  define resize(var, type, size) ({                                          \
  var = (type*) realloc(var, sizeof(type) * (size));                          \
})
#  define set_zero(var, type, size) memset(var, 0, sizeof(type) * (size))
#  define new_init(type, size) ({                                             \
    type * __res = (type*) malloc(sizeof(type) * (size));                     \
    set_zero(__res, type, size);                                              \
    __res;                                                                    \
  })

#  ifndef min
#    define min(a,b)                                                          \
   ({ __typeof__ (a) _a = (a);                                                \
       __typeof__ (b) _b = (b);                                               \
     _a < _b ? _a : _b; })
#  endif
#  ifndef max
#    define max(a,b)                                                          \
   ({ __typeof__ (a) _a = (a);                                                \
       __typeof__ (b) _b = (b);                                               \
     _a > _b ? _a : _b; })
#  endif
#  ifndef swap
#    define swap(A, B, T) ({                                                  \
                            T tmp = A;                                        \
                            A = B;                                            \
                            B = tmp;                                          \
                          })
#  endif
#  define swap_elements(node1, node2) swap(node1->elem, node2->elem, void*)

#  define DEBUG 1


#  define COL_HASHMAP_INITIAL_CAP   16
#  define COL_HASHMAP_BUCKET_CAP    8

// TODO: use a vector to hold multiple elements in the same list node
#  define COL_LLIST_NODE_CAPACITY   8

typedef struct _COL_iterator {
  void * node;
  void * next;
  void * prev;
  bool next_end;
  bool prev_end;
} COL_it_t;

typedef struct _COL_pair {
  void * key;
  void * val;
} COL_pair_t;

#  ifndef _COL_LLIST
#    define _COL_LLIST
// ###########################################
// BEGIN LINKED LIST

//extern struct _COL_llist_node_t;
//extern struct _COL_linked_list_t;

typedef struct _COL_llist_node_t COL_llist_node_t;
typedef struct _COL_linked_list_t COL_llist_t;

COL_llist_t * COL_LLIST_new(void);

/**
 * 
 * @param list
 * @param elem_destroyer callback that knows how to destroy an elem
 */
void COL_LLIST_destroy(COL_llist_t * list, void (*elem_destroyer)(void*));

/**
 * Appends an element to the list.
 * 
 * @param list
 * @param elem
 */
void COL_LLIST_add(COL_llist_t* list, void* elem);

/**
 * 
 * @param list
 * @param elem_comparator a comparator that accepts two keys a and b and
 * returns a - b (negative if a &lt; b; positive if a &gt; b; zero if a = b)
 * @param elem
 * @return NULL if not found, the found element otherwise
 */
void COL_LLIST_add_sorted(COL_llist_t* list,
        int (*elem_comparator)(void*, void*), void* elem);

/**
 * Pushes an element in the beginning of the list.
 * 
 * @param list
 * @param elem
 */
void COL_LLIST_push(COL_llist_t* list, void* elem);

/**
 * Pops the first element. If the list is empty, then NULL is returned.
 * 
 * @param list
 * @return 
 */
void* COL_LLIST_pop(COL_llist_t* list);

/**
 * 
 * @param list
 * @return 
 */
void* COL_LLIST_top(COL_llist_t* list);

/**
 * 
 * @param list
 * @param elem_comparator
 * @param param
 * @return NULL if not found, the found element otherwise
 */
void* COL_LLIST_find(COL_llist_t* list,
        bool (*elem_comparator)(void*, void*), void* param);

/**
 * 
 * @param list
 * @return 
 */
bool COL_LLIST_is_empty(COL_llist_t* list);

/**
 * Clears the list.
 * 
 * @param list
 * @param elem_destroyer if set to NULL the elements will not be destroyed
 */
void COL_LLIST_clear(COL_llist_t* list, void (*elem_destroyer)(void*));

/**
 * 
 * @param list
 * @return 
 */
int COL_LLIST_size(COL_llist_t* list);

/**
 * 
 * @param list
 * @param pos
 * @return 
 */
void* COL_LLIST_get(COL_llist_t* list, int pos);

/**
 * 
 * @param list
 * @param pos
 * @return 
 */
void* COL_LLIST_remove(COL_llist_t* list, int pos);

/**
 * 
 * @param list
 * @param elem
 * @param pos
 */
void COL_LLIST_insert(COL_llist_t* list, void* elem, int pos);

/**
 * 
 * @param list
 * @param iterator a function that iterates over the list, takes the current
 *                 position and element as arguments, also takes the given
 *                 param, return true to break
 * @param param
 */
void COL_LLIST_iterate(COL_llist_t* list,
        bool (*iterator)(int, void*, void*), void* param);

/**
 * Iterates in the reverse order.
 * 
 * @param list
 * @param iterator
 * @param param
 */
void COL_LLIST_iterate_r(COL_llist_t* list,
        bool (*iterator)(int, void*, void*), void* param);

/**
 * Creates a new iterator pointing for the first element.
 * 
 * @param list the list to iterate over
 * @return an iterator
 */
void COL_LLIST_it_new(COL_llist_t* list, COL_it_t* it);

/**
 * Cleans up the iterator object.
 * 
 * @param it
 */
void COL_LLIST_it_destroy(COL_it_t* it);

/**
 * Returns the current elem and moves the iterator forward.
 * 
 * @param it
 * @return 
 */
void* COL_LLIST_it_next(COL_it_t* it);

/**
 * Returns the current elem and moves the iterator backward.
 * 
 * @param it
 * @return 
 */
void* COL_LLIST_it_prev(COL_it_t* it);

/**
 * Returns the current value.
 * 
 * @param it
 * @return 
 */
void* COL_LLIST_it_check(COL_it_t* it);

/**
 * Reverses the order of the elements in the list.
 * 
 * @param list
 */
void COL_LLIST_reverse(COL_llist_t* list);

/**
 * Fills the list with random elements.
 * 
 * @param list
 * @param allow_repeat
 * @param size
 * @param random
 * @param min
 * @param max
 */
void COL_LLIST_fill_random(COL_llist_t* list, bool allow_repeat, int size,
        void* (*random)(int, int), int min, int max);

/**
 * Fills the list with random elements.
 * 
 * @param list
 * @param allow_repeat
 * @param size
 * @param random_dist
 * @param min
 * @param max
 * @param mean_value
 */
void COL_LLIST_fill_random_dist(COL_llist_t* list, bool allow_repeat, int size,
        void* (*random_dist)(int, int, int), int min, int max, int mean_value);

/**
 * Shuffles the list using the provided random function.
 * 
 * @param list
 * @param random function that generates a random int within the given interval
 *               (inclusive both min and max)
 */
void COL_LLIST_shuffle(COL_llist_t* list, int (*random)(int, int));

/**
 * Creates an array for the elements in the list.
 * 
 * @param list
 * @return 
 */
void** COL_LLIST_to_array(COL_llist_t* list);

/**
 * Destroys the array provided by COL_LLIST_to_array(COL_llist_t*).
 * 
 * @param array
 */
void COL_LLIST_array_destroy(void** array);

// END LINKED LIST
// ###########################################
#  endif // _COL_LLIST



#  ifndef _COL_BTREE
#    define _COL_BTREE
// ###########################################
// BEGIN B+ TREE
//extern struct _COL_b_tree_node_t;
//extern struct _COL_b_tree;

typedef struct _COL_b_tree_node_t COL_btree_node_t;
typedef struct _COL_b_tree COL_btree_t;

COL_btree_t* COL_BTREE_new(int fanout, int(*comparator)(void*, void*));
void COL_BTREE_set_print(COL_btree_t* btree,
        int(*key_print)(void*, char*, size_t),
        int(*val_print)(void*, char*, size_t));
void COL_BTREE_destroy(COL_btree_t* btree);

void COL_BTREE_insert(COL_btree_t* btree, void* key, void* value);
void* COL_BTREE_remove(COL_btree_t* btree, void* key);
void* COL_BTREE_search(COL_btree_t* btree, void* key);

char* COL_BTREE_print(COL_btree_t* btree, char * buffer, size_t buffer_size);
COL_llist_t* COL_BTREE_keys(COL_btree_t* btree);

// iterates over the stored values
void COL_BTREE_iterate(COL_btree_t* btree, bool(*iterator)(void*, void*, void*),
        void* param);
COL_it_t* COL_BTREE_it_new(COL_btree_t* btree);
void COL_BTREE_it_destroy(COL_it_t* it);
void* COL_BTREE_it_next(COL_it_t* it);
void* COL_BTREE_it_prev(COL_it_t* it);
void* COL_BTREE_it_check(COL_it_t* it);

// END B+ TREE
// ########################################
#  endif // _COL_HASHMAP

#  ifndef _COL_LTREE
#    define _COL_LTREE
// ###########################################
// BEGIN B+ TREE

// END B+ TREE
// ########################################
#  endif // _COL_HASHMAP

#endif /* COLLECTIONS_H */
