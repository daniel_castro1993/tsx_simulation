#ifndef __UTILS_LOGS_H
#  define __UTILS_LOGS_H

#  define DEBUG_DISABLE_LOG_TO_FILE 1

#  define LOG_FILE    "logs.txt"
#  define LOG_LV_DEBG 0x0000000F
#  define LOG_LV_INFO 0x000000F0
#  define LOG_LV_WANR 0x00000F00
#  define LOG_LV_ERRO 0x0000F000
#  define LOG_LV_CRIT 0x000F0000
#  define LOG_LV_GE_D LOG_LV_DEBG|LOG_LV_INFO|LOG_LV_WANR|LOG_LV_ERRO|LOG_LV_CRIT
#  define LOG_LV_GE_I LOG_LV_INFO|LOG_LV_WANR|LOG_LV_ERRO|LOG_LV_CRIT
#  define LOG_LV_GE_W LOG_LV_WANR|LOG_LV_ERRO|LOG_LV_CRIT
#  define LOG_LV_GE_E LOG_LV_ERRO|LOG_LV_CRIT
#  define LOG_LV_MSG(level) level & LOG_LV_DEBG ? "DEBUG" : \
                            level & LOG_LV_INFO ? "INFO" : \
                            level & LOG_LV_WANR ? "WARNING" : \
                            level & LOG_LV_ERRO ? "ERROR" : \
                            level & LOG_LV_CRIT ? "CRITICAL" : "UNDEFINED"

void UTL_set_log_levels(int log_lvs);
void UTL_add_log_level(int log_lv);
void UTL_rem_log_level(int log_lv);
void UTL_log(int log_level, const char* format, ...);

void UTL_log_d(const char* format, ...);
void UTL_log_i(const char* format, ...);
void UTL_log_w(const char* format, ...);
void UTL_log_e(const char* format, ...);
void UTL_log_c(const char* format, ...);

#endif	/* __UTILS_LOGS_H */

