#include <stdio.h>
#include "collections.h"
#include "utils_logs.h"

struct _COL_llist_node_t {
  struct _COL_llist_node_t * next;
  struct _COL_llist_node_t * prev;
  void * elem;
};

struct _COL_linked_list_t {
  size_t size;
  COL_llist_node_t * head_node;
  COL_llist_node_t * tail_node;
};

#define __COL_LLIST_INSERT_BEFORE(node, _nxt) \
if(_nxt != NULL) {                            \
  node->next = _nxt;                          \
  node->prev = _nxt->prev;                    \
  if(_nxt->prev != NULL) {                    \
    _nxt->prev->next = node;                  \
  }                                           \
  _nxt->prev = node;                          \
}

#define __COL_LLIST_INSERT_AFTER(node, _prv)  \
if(_prv != NULL) {                            \
  node->prev = _prv;                          \
  node->next = _prv->next;                    \
  if(_prv->next != NULL) {                    \
    _prv->next->prev = node;                  \
  }                                           \
  _prv->next = node;                          \
}

#define __COL_LLIST_REMOVE(node)  \
if(node != NULL) {                            \
  if(node->next != NULL) {                    \
    node->next->prev = node->prev;            \
  }                                           \
  if(node->prev != NULL) {                    \
    node->prev->next = node->next;            \
  }                                           \
}

#define __COL_LLIST_FIX_NEXT_PREV(node)       \
node->next = NULL;                            \
node->prev = NULL;

// TODO: store multiple elements in one node
#define __COL_LLIST_NODE_INSERT(node, _el, _ind, i)                           \
if(node->size < COL_LLIST_NODE_CAPACITY) {                                    \
  if(_ind < node->size) {                                                     \
    for(i = node->size; i > _ind; i--) {                                      \
      node->elem[i] = node->elem[i - 1];                                      \
    }                                                                         \
    node->elem[_ind] = _el;                                                   \
    node->size++;                                                             \
  } else if (_ind == node->size) {                                            \
    node->elem[_ind] = _el;                                                   \
    node->size++;                                                             \
  }                                                                           \
}

#define __COL_LLIST_NODE_REMOVE(node, _el, _ind, i)                           \
if(_ind < node->size) {                                                       \
  _el = node->elem[_ind];                                                     \
  for(i = _ind; i < node->size - 1; i++) {                                    \
    node->elem[i] = node->elem[i + 1];                                        \
  }                                                                           \
  node->size--;                                                               \
}

#define __COL_LLIST_NODE_FIND(node, _el, cmp, i, j)                           \
j = -1;                                                                       \
for(i = 0; i < node->size; i++) {                                             \
  if(cmp(node->elem[i], _el) == 0) {                                          \
    j = i;                                                                    \
    break;                                                                    \
  }                                                                           \
}

#define __COL_LLIST_NODE_FIND_SORTED(node, _el, cmp, i, inc, j)               \
inc = node->size << 1;                                                        \
i = inc;                                                                      \
inc++;                                                                        \
j = -1;                                                                       \
while(true) {                                                                 \
  if(cmp(node->elem[i], _el) == 0) {                                          \
    j = i;                                                                    \
    break;                                                                    \
  } else if(cmp(node->elem[i], _el) < 0) {                                    \
    if(i << 1 == 0) {                                                         \
      if(i+1 >= node->size) {                                                 \
        j = i;                                                                \
      } else if(cmp(node->elem[i+1], _el) == 0) {                             \
        j = i+1;                                                              \
      }                                                                       \
      break;                                                                  \
    } else {                                                                  \
      inc <<= 1;                                                              \
      i += inc;                                                               \
    }                                                                         \
  } else {                                                                    \
    if(i << 1 == 0) {                                                         \
      if(i-1 < 0) {                                                           \
        j = i;                                                                \
      } else if(cmp(node->elem[i-1], _el) == 0) {                             \
        j = i-1;                                                              \
      }                                                                       \
      break;                                                                  \
    } else {                                                                  \
      inc <<= 1;                                                              \
      i -= inc;                                                               \
    }                                                                         \
  }                                                                           \
}

// j must come from __COL_LLIST_NODE_FIND_SORTED
#define __COL_LLIST_NODE_INSERT_SORTED(node, _el, cmp, i, inc, j)             \
if(j <= 0) {                                                                  \
  __COL_LLIST_NODE_INSERT(node, _el, 0, i);                                   \
} else if(j >= node->size) {                                                  \
  __COL_LLIST_NODE_INSERT(node, _el, node->size, i);                          \
} else {                                                                      \
  j--;                                                                        \
  while(!(cmp(_el, node->elem[j]) < 0 && cmp(_el, node->elem[j+1]) > 0)) {    \
    j++;                                                                      \
  }                                                                           \
  __COL_LLIST_NODE_INSERT(node, _el, j+1, i);                                 \
}

COL_llist_t* COL_LLIST_new() {
  COL_llist_t* res = new(COL_llist_t, 1);

  res->size = 0;
  res->head_node = NULL;
  res->tail_node = NULL;

  return res;
}

void COL_LLIST_destroy(COL_llist_t* list, void (*elem_destroyer)(void*)) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->tail_node;

  while (head != NULL) {
    COL_llist_node_t* next = head->next;
    if (elem_destroyer != NULL) elem_destroyer(head->elem);
    free(head);
    head = next;
  }

  free(list);
}

void COL_LLIST_add(COL_llist_t* list, void* elem) {
  COL_llist_node_t* tail = (COL_llist_node_t*) list->tail_node;
  COL_llist_node_t* new_node = new(COL_llist_node_t, 1);

  new_node->elem = elem;

  if (new_node != NULL) {
    list->size++;
  } else {
    return; // no more memory
  }

  if (tail == NULL) {
    __COL_LLIST_FIX_NEXT_PREV(new_node);

    list->tail_node = new_node;
    list->head_node = new_node;
  } else {
    __COL_LLIST_INSERT_AFTER(new_node, tail);

    list->tail_node = new_node;
  }
}

void COL_LLIST_add_sorted(COL_llist_t * list,
    int (*elem_comparator)(void*, void*), void * elem) {

  COL_it_t it;
  void * lelem;

  COL_LLIST_it_new(list, &it);

  while ((lelem = COL_LLIST_it_next(&it)) != NULL) {
    if (elem_comparator(lelem, elem) > 0) {
      COL_llist_node_t * prev = (COL_llist_node_t*) it.prev;
      COL_llist_node_t * node = new(COL_llist_node_t, 1);

      node->elem = elem;

      if (prev == NULL | prev == list->head_node) {
        COL_LLIST_push(list, elem);
        return;
      }

      __COL_LLIST_INSERT_BEFORE(node, prev);

      list->size++;

      return;
    }
  }

  // put in the end
  COL_LLIST_add(list, elem);
}

void COL_LLIST_push(COL_llist_t * list, void * elem) {
  COL_llist_node_t * head = (COL_llist_node_t*) list->head_node;
  COL_llist_node_t * new_node = new(COL_llist_node_t, 1);

  if (new_node != NULL) {
    list->size++;
  } else {
    return; // no more memory
  }

  new_node->elem = elem;

  if (head == NULL) {
    __COL_LLIST_FIX_NEXT_PREV(new_node);
    list->head_node = new_node;
    list->tail_node = new_node;
  } else {
    __COL_LLIST_INSERT_BEFORE(new_node, head);
    list->head_node = new_node;
  }
}

void * COL_LLIST_pop(COL_llist_t * list) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  void * res = NULL;

  if (head != NULL) {

    __COL_LLIST_REMOVE(head);
//    if (head->next != NULL) head->next->prev = NULL;
    
    list->head_node = head->next;
    res = head->elem;
    if (list->head_node == NULL) {
      list->tail_node = NULL;
    }

    list->size--;

    free(head);
  }

  return res;
}

void * COL_LLIST_top(COL_llist_t* list) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  void* res = NULL;

  if (head != NULL) {
    res = head->elem;
  }

  return res;
}

bool COL_LLIST_is_empty(COL_llist_t* list) {
  return list->size == 0;
}

void COL_LLIST_clear(COL_llist_t * list, void (*elem_destroyer)(void*)) {
  void * elem;
  while ((elem = COL_LLIST_pop(list)) != NULL) {
    if (elem_destroyer != NULL) {
      elem_destroyer(elem);
    }
  }
}

void * COL_LLIST_find(COL_llist_t* list,
    bool(*elem_comparator)(void*, void*), void* param) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  int i = 0;

  while (head != NULL) {

    if (elem_comparator(head->elem, param)) return head->elem;

    i += 1;
    head = head->next;
  }

  return NULL;
}

int COL_LLIST_size(COL_llist_t* list) {
  return list->size;
}

void * COL_LLIST_get(COL_llist_t* list, int pos) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  void* res = NULL;

  int i = 0;

  while (head != NULL) {

    if (pos == i) {
      res = head->elem;

      break;
    }

    i += 1;
    head = head->next;
  }

  return res;
}

void COL_LLIST_insert(COL_llist_t* list, void* elem, int pos) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  COL_llist_node_t* new_node = new(COL_llist_node_t, 1);
  new_node->elem = elem;

  while (pos < 0) pos += list->size;

  pos = pos % list->size;

  if (new_node != NULL) {
    list->size++;
  } else {
    return; // no more memory
  }

  int i = 0;

  while (head != NULL) {
    if (pos == i) {
      break;
    }
    i += 1;
    head = head->next;
  }

  if (head == NULL) {
    __COL_LLIST_FIX_NEXT_PREV(new_node);

    list->head_node = new_node;
    list->tail_node = new_node;
  } else {
    __COL_LLIST_INSERT_BEFORE(new_node, head);
  }
}

void * COL_LLIST_remove(COL_llist_t* list, int pos) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  void* res = NULL;

  int i = 0;

  while (head != NULL) {

    if (pos == i) {
      res = head->elem;

      __COL_LLIST_REMOVE(head);

      if (head == list->head_node) {
        list->head_node = head->next;
      }
      if (head == list->tail_node) {
        list->tail_node = head->prev;
      }

      free(head);

      list->size--;

      break;
    }

    i++;
    head = head->next;
  }

  return res;
}

void COL_LLIST_iterate(COL_llist_t* list,
    bool(*iterator)(int, void*, void*), void* param) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  int i = 0;

  while (head != NULL) {

    iterator(i, head->elem, param);

    i += 1;
    head = head->next;
  }
}

void COL_LLIST_iterate_r(COL_llist_t* list,
    bool(*iterator)(int, void*, void*), void* param) {
  COL_llist_node_t* tail = (COL_llist_node_t*) list->tail_node;
  int i = list->size - 1;

  while (tail != NULL) {

    iterator(i, tail->elem, param);

    i -= 1;
    tail = tail->prev;
  }
}

void COL_LLIST_it_new(COL_llist_t* list, COL_it_t* it) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;

  it->node = head;
  it->next = head == NULL ? NULL : head->next;
  it->prev = head == NULL ? NULL : head->prev;

  it->next_end = false;
  it->prev_end = false;
}

void COL_LLIST_it_destroy(COL_it_t* it) {
}

void * COL_LLIST_it_next(COL_it_t* it) {
  COL_llist_node_t * node = (COL_llist_node_t*) it->node;
  COL_llist_node_t * next = (COL_llist_node_t*) it->next;
  void * res = node == NULL ? NULL : node->elem;

  if (next == NULL && !it->next_end) {
    it->next_end = true;
    return res;
  } else if (it->prev_end) {
    it->prev_end = false;
    return res;
  } else if (it->next_end) {
    return NULL;
  }

  it->prev_end = false;

  it->prev = node;
  it->node = next;
  it->next = next->next;

  return res;
}

void * COL_LLIST_it_prev(COL_it_t* it) {
  COL_llist_node_t * node = (COL_llist_node_t*) it->node;
  COL_llist_node_t * prev = (COL_llist_node_t*) it->prev;
  void * res = node == NULL ? NULL : node->elem;

  if (prev == NULL && !it->prev_end) {
    it->prev_end = true;
    return res;
  } else if (it->next_end) {
    it->next_end = false;
    return res;
  } else if (it->prev_end) {
    return NULL;
  }

  it->next = node;
  it->node = prev;
  it->prev = prev->prev;

  return res;
}

void * COL_LLIST_it_check(COL_it_t* it) {
  COL_llist_node_t* node = (COL_llist_node_t*) it->node;

  if (node == NULL || it->next_end || it->prev_end) {
    return NULL;
  } else {
    return node->elem;
  }
}

void COL_LLIST_reverse(COL_llist_t* list) {
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  list->head_node = list->tail_node;
  list->tail_node = head;

  while (head != NULL) {
    COL_llist_node_t * tmp = head->prev;
    
    head->prev = head->next;
    head->next = tmp;

    head = head->prev; // due to the swap
  }
}

void COL_LLIST_fill_random(COL_llist_t* list, bool allow_repeat, int size,
    void* (*rand_func)(int, int), int min, int max) {
  
  if (allow_repeat) {
    while (list->size < size) {
      void * rand_elem = rand_func(min, max);
      COL_LLIST_add(list, rand_elem);
    }
  } else {
    int hmin = min;
    int it = (max - min + 1) / size;
    int hmax = min + it;
    while (list->size < size) {
      void * rand_elem = rand_func(hmin, hmax);
      COL_LLIST_add(list, rand_elem);
      hmin += it + 1;
      hmax += it;
    }
  }
}

void COL_LLIST_fill_random_dist(COL_llist_t* list, bool allow_repeat, int size,
    void* (*random_dist)(int, int, int), int min, int max, int mean_value) {
  if (allow_repeat) {
    while (list->size < size) {
      void* rand_elem = random_dist(mean_value, min, max);
      COL_LLIST_add(list, rand_elem);
    }
  } else {
    // TODO
    int hmin = min;
    int it = (max - min + 1) / size;
    int hmax = min + it;
    while (list->size < size) {
      void* rand_elem = random_dist(mean_value, hmin, hmax);
      COL_LLIST_add(list, rand_elem);
      hmin += it + 1;
      hmax += it;
    }
  }
}

void COL_LLIST_shuffle(COL_llist_t* list, int (*rand_func)(int, int)) {
  COL_llist_node_t** tmp = new(COL_llist_node_t*, list->size);
  int i = 0, j;
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;

  while (head != NULL) {
    tmp[i] = head;

    i += 1;
    head = head->next;
  }

  for (j = 0; j < list->size; j++) {
    int k = rand_func(0, j);
    COL_llist_node_t *tmp_j = (COL_llist_node_t*) tmp[j],
        *tmp_k = (COL_llist_node_t*) tmp[k];
    swap_elements(tmp_j, tmp_k);
  }

  free(tmp);
}

void** COL_LLIST_to_array(COL_llist_t* list) {
  void** res = new(void*, list->size);
  COL_llist_node_t* head = (COL_llist_node_t*) list->head_node;
  int i = 0;

  while (head != NULL) {

    res[i] = head->elem;

    i += 1;
    head = head->next;
  }

  return res;
}

void COL_LLIST_array_destroy(void** array) {
  free(array);
}
