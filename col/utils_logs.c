#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "collections.h"
#include "utils_logs.h"

FILE* log_file = NULL;
int log_levels = LOG_LV_GE_D;

void UTL_set_log_levels(int log_lvs) {
  log_levels = log_lvs;
}

void UTL_add_log_level(int log_lv) {
  log_levels |= log_lv;
}

void UTL_rem_log_level(int log_lv) {
  log_levels &= ~log_lv;
}

void log_to_file(int log_level, const char* format, va_list arglist) {
  time_t rawtime;
  struct tm* timeinfo;
  char buffer[80];

#ifndef DEBUG_DISABLE_LOG_TO_FILE
  if (log_file == NULL) {
    time(&rawtime);
    timeinfo = localtime(&rawtime);

    log_file = fopen(LOG_FILE, "a");

    fprintf(log_file, "\n");
    memset(buffer, '=', 80);
    buffer[79] = '\0';
    fprintf(log_file, " %s \n", buffer);

    strftime(buffer, 80, "%c", timeinfo);
    fprintf(log_file, " >>>>> %s\n", buffer);
    memset(buffer, '=', 80);
    buffer[79] = '\0';
    fprintf(log_file, " %s \n\n", buffer);
  }

  fprintf(log_file, "[%s]: ", LOG_LV_MSG(log_level));
  vfprintf(log_file, format, arglist);
  fprintf(log_file, "\n");
#endif
}

void log_to_stdout(int log_level, const char* format, va_list arglist) {
  time_t rawtime;
  struct tm* timeinfo;
  char buffer[80];

  if (!(log_level & log_levels)) {
    return;
  }

  time(&rawtime);
  timeinfo = localtime(&rawtime);
  strftime(buffer, 80, "%c", timeinfo);

  printf("[%s]: ", LOG_LV_MSG(log_level));
  vprintf(format, arglist);

  printf("\n");
}

void UTL_log(int log_level, const char* format, ...) {
  va_list arglist;

  va_start(arglist, format);
  log_to_file(log_level, format, arglist);
  va_end(arglist);

  va_start(arglist, format);
  log_to_stdout(log_level, format, arglist);
  va_end(arglist);
}

void UTL_log_d(const char* format, ...) {
  va_list arglist;

  va_start(arglist, format);
  log_to_file(LOG_LV_DEBG, format, arglist);
  va_end(arglist);

  va_start(arglist, format);
  log_to_stdout(LOG_LV_DEBG, format, arglist);
  va_end(arglist);
}

void UTL_log_i(const char* format, ...) {
  va_list arglist;

  va_start(arglist, format);
  log_to_file(LOG_LV_INFO, format, arglist);
  va_end(arglist);

  va_start(arglist, format);
  log_to_stdout(LOG_LV_INFO, format, arglist);
  va_end(arglist);
}

void UTL_log_w(const char* format, ...) {
  va_list arglist;

  va_start(arglist, format);
  log_to_file(LOG_LV_WANR, format, arglist);
  va_end(arglist);

  va_start(arglist, format);
  log_to_stdout(LOG_LV_WANR, format, arglist);
  va_end(arglist);
}

void UTL_log_e(const char* format, ...) {
  va_list arglist;

  va_start(arglist, format);
  log_to_file(LOG_LV_ERRO, format, arglist);
  va_end(arglist);

  va_start(arglist, format);
  log_to_stdout(LOG_LV_ERRO, format, arglist);
  va_end(arglist);
}

void UTL_log_c(const char* format, ...) {
  va_list arglist;

  va_start(arglist, format);
  log_to_file(LOG_LV_CRIT, format, arglist);
  va_end(arglist);

  va_start(arglist, format);
  log_to_stdout(LOG_LV_CRIT, format, arglist);
  va_end(arglist);
}
