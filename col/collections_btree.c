#include <stdio.h>
#include "collections.h"
#include "utils_logs.h"

#define __BUFFER_SIZE 512

struct _COL_b_tree_node_t {
  struct _COL_b_tree_node_t * parent;
  struct _COL_b_tree_node_t * next;
  struct _COL_b_tree_node_t ** children;
  void ** values;
  void ** keys;
  bool is_leaf;
  bool is_root;
  size_t size;
};

struct _COL_b_tree {
  size_t size;
  size_t bfactor;
  size_t height;
  int (*comparator)(void*, void*);
  int (*key_print)(void*, char*, size_t);
  int (*val_print)(void*, char*, size_t);
  COL_btree_node_t* root_node;
};

char* BT_print_key(COL_btree_t * btree, void * key, char * buffer,
        size_t buffer_size) {

  char * ptr = buffer;

  if (btree->key_print != NULL) {
    ptr += btree->key_print(key, ptr, buffer_size);
  }

  return buffer;
}

char* BT_print_key_val(COL_btree_t* btree, void* key, void* val,
        char* buffer, size_t buffer_size) {

  char * ptr = buffer;
  int size = buffer_size, ptr_size;

  ptr_size = snprintf(ptr, size, "(");
  ptr += ptr_size;
  size -= ptr_size;

  if (btree->key_print != NULL) {
    ptr_size += btree->key_print(key, ptr, size);
    ptr += ptr_size;
    size -= ptr_size;
  }

  ptr_size += snprintf(ptr, size, ", ");
  ptr += ptr_size;
  size -= ptr_size;

  if (btree->val_print != NULL) {
    ptr_size += btree->val_print(val, ptr, size);
    ptr += ptr_size;
    size -= ptr_size;
  }

  ptr_size += snprintf(ptr, size, ")");
  ptr += ptr_size;
  size -= ptr_size;

  return buffer;
}

char* BT_print_node(COL_btree_t * btree, COL_btree_node_t * node,
        char * buffer, size_t buffer_size) {

  char buffer_key[__BUFFER_SIZE];
  int i, size = buffer_size;

  memset(buffer, '\0', __BUFFER_SIZE);

  strncat(buffer, "[ ", size);
  size -= 2;

  for (i = 0; i < node->size; i++) {
    if (btree->key_print != NULL) {
      btree->key_print(node->keys[i], buffer_key, __BUFFER_SIZE);
    }
    strncat(buffer, buffer_key, size);
    size -= strlen(buffer_key);
    strncat(buffer, " ", size);
    size -= 1;
  }
  strncat(buffer, "] ", size);
  return buffer;
}

void BT_print(COL_btree_t* btree, COL_btree_node_t* node, char* buffer,
        size_t buffer_size, int height) {

  char tmp_buffer[__BUFFER_SIZE];
  int i, ptr_size, size = buffer_size;

  if (node->is_leaf || height < 0) {
    return;
  }

  for (i = 0; i < node->size + 1; i++) {
    ptr_size = snprintf(tmp_buffer, __BUFFER_SIZE, "%*s (%i, %i):", height * 4,
            "", (int) (btree->height - height), i);
    strncat(buffer, tmp_buffer, size);
    size -= ptr_size;
    tmp_buffer[0] = '\0';
    BT_print_node(btree, node->children[i], tmp_buffer, __BUFFER_SIZE);
    strncat(buffer, tmp_buffer, size);
    size -= strlen(tmp_buffer);
    strncat(buffer, "\n", 1);
    size -= 1;

#ifdef DEBUG_BTREE
    char* non_leaf = "non-leaf node";
    char* leaf = "leaf node";
    if (node->children[i]->is_leaf) {
      UTL_log_d("%s (%i, %i): %s", leaf, btree->height - height, i, tmp_buffer);
    } else {
      UTL_log_d("%s (%i, %i): %s", non_leaf, btree->height - height, i, tmp_buffer);
    }
#endif

    if (!node->children[i]->is_leaf) {
      BT_print(btree, node->children[i], buffer, size, height - 1);
    }
  }

  strncat(buffer, "\n", 1);
}

bool BT_N_is_full(COL_btree_t* btree, COL_btree_node_t* node) {
  return node->size >= btree->bfactor - 1;
}

bool BT_N_is_empty(COL_btree_t* btree, COL_btree_node_t* node) {
  return node->size == 0;
}

COL_btree_node_t* BT_N_new(COL_btree_t* btree, bool isLeaf) {
  COL_btree_node_t* res = new(COL_btree_node_t, 1);
  set_zero(res, COL_btree_node_t, 1);

  res->keys = new(void*, btree->bfactor - 1);
  res->is_leaf = isLeaf;
  if (isLeaf) {
    res->values = new(void*, btree->bfactor - 1);
  } else {
    res->children = new(COL_btree_node_t*, btree->bfactor);
  }
}

void BT_N_delete(COL_btree_node_t* node) {
  if (node->is_leaf) {
    free(node->values);
  } else {
    free(node->children);
  }
  free(node->keys);

  free(node);
}

bool BT_N_contains(COL_btree_t* btree, COL_btree_node_t* node, void* key) {
  if (!node->is_leaf || node->size == 0) {
    return false;
  }

  int i;

  for (i = 0; i < node->size; i++) {
    if (btree->comparator(key, node->keys[i]) == 0) {
      return true;
    }
  }

  return false;
}

void* BT_LF_get(COL_btree_t* btree, COL_btree_node_t* node, void* key) {
  if (!node->is_leaf || node->size == 0) {
    return NULL;
  }

  int i;

  for (i = 0; i < node->size; i++) {
    if (btree->comparator(key, node->keys[i]) == 0) {
      return node->values[i];
    }
  }

  return NULL;
}

/**
 * 
 * @param btree
 * @param node the parent node
 * @param key
 * @return the child index to choose
 */
int BT_NL_child_pos(COL_btree_t* btree, COL_btree_node_t* node, void* key,
        void** split_key) {

  if (node == NULL || node->is_leaf || node->size == 0) {
    return -1;
  }

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_node(btree, node, buffer);
  UTL_log_d("btree_non_leaf_child - node %s", buffer);
#endif

  int i;

  for (i = 0; i < node->size; i++) {
    if (btree->comparator(key, node->keys[i]) < 0) {
#ifdef DEBUG_BTREE
      BT_print_key(btree, node->keys[i], buffer);
      BT_print_key(btree, key, buffer2);
      UTL_log_d("btree_non_leaf_child - found node[%s] >= key[%s], "
              "returning child %i", buffer, buffer2, i);
#endif
      if (split_key != NULL) {
        if (i == 0) {
          *split_key = node->keys[i];
        } else {
          *split_key = node->keys[i - 1];
        }
      }
      return i;
    }
  }

#ifdef DEBUG_BTREE
  BT_print_key(btree, key, buffer);
  UTL_log_d("btree_non_leaf_child - key[%s] is bigger than all, "
          "returning child %i", buffer, i);
#endif
  if (split_key != NULL) {
    *split_key = node->keys[i - 1];
  }
  return i; // node->size
}

COL_btree_node_t* BT_N_find_recursive(COL_btree_t* btree,
        COL_btree_node_t* node, void* key) {

  if (!node->is_leaf) {
    int child = BT_NL_child_pos(btree, node, key, NULL);
    BT_N_find_recursive(btree, node->children[child], key);
  } else {
    // hit a leaf
    return node;
  }
}

COL_btree_node_t* BT_N_find(COL_btree_t* btree, void* key) {
  if (btree->root_node == NULL) {
    return NULL;
  }
  BT_N_find_recursive(btree, btree->root_node, key);
}

/**
 * 
 * @param btree
 * @param node
 * @param key
 * @param value
 * @return true if successful
 */
bool BT_LF_insert(COL_btree_t* btree, COL_btree_node_t* node, void* key,
        void* value) {
  int i;

  if (BT_N_is_full(btree, node) || !node->is_leaf) {
    return false;
  }

  // fill the spaces after the key
  for (i = node->size - 1; i >= 0; i--) {
    if (btree->comparator(key, node->keys[i]) < 0) {
      node->keys[i + 1] = node->keys[i];
      node->values[i + 1] = node->values[i];
    } else {
      break;
    }
  }
  // insert
  node->keys[i + 1] = key;
  node->values[i + 1] = value;
  node->size++;

  // the previous values are in the correct place

  return true;
}

/**
 * 
 * @param btree
 * @param node
 * @param child
 * @param child_key
 * @return true if successful
 */
bool BT_NL_insert(COL_btree_t* btree, COL_btree_node_t* node,
        COL_btree_node_t* child, void* child_key) {
  int i;

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];
  char buffer3[512];

  BT_print_node(btree, child, buffer);
  BT_print_node(btree, node, buffer2);
  BT_print_key(btree, child_key, buffer3);
  UTL_log_d("BT_NL_insert - child %s in node %s (key: %s)", buffer,
          buffer2, buffer3);
#endif

  if (node->size >= btree->bfactor - 1 || node->is_leaf) {
    return false;
  }

  child->parent = node;

  // fill the spaces after the key
  for (i = node->size; i >= 0; i--) {
    COL_btree_node_t* sibling = node->children[i];
    COL_btree_node_t* sibling_key = sibling->keys[0];

#ifdef DEBUG_BTREE
    BT_print_node(btree, sibling, buffer);
    BT_print_key(btree, sibling_key, buffer2);
    UTL_log_d("BT_NL_insert - sibling %i [%s] (key: %s)", i, buffer,
            buffer2);
#endif

    if (btree->comparator(child_key, sibling_key) < 0) {
#ifdef DEBUG_BTREE
      BT_print_key(btree, child_key, buffer);
      BT_print_key(btree, sibling_key, buffer2);
      UTL_log_d("BT_NL_insert - child_key (%s) < sibling_key (%s)",
              buffer, buffer2);
#endif

      if (i > 0) {
        node->keys[i] = node->keys[i - 1];
      }
      node->children[i + 1] = node->children[i];
    } else {
      break;
    }
  }
  // insert
  if (i == 0 && btree->comparator(child_key, node->keys[0]) < 0) {
#ifdef DEBUG_BTREE
    BT_print_key(btree, child_key, buffer);
    BT_print_key(btree, node->keys[0], buffer2);
    UTL_log_d("BT_NL_insert - i (%i) == 0 && child_key (%s) < node_key[0] (%s)",
            i, buffer, buffer2);
#endif
    COL_btree_node_t* sibling = node->children[0];
    COL_btree_node_t* sibling_key = sibling->keys[0];

    if (btree->comparator(child_key, sibling_key) < 0) {
#ifdef DEBUG_BTREE
      BT_print_key(btree, sibling_key, buffer);
      BT_print_node(btree, child, buffer2);
      BT_print_node(btree, node->children[0], buffer3);
      UTL_log_d("BT_NL_insert - sibling_key (%s) - (%s, %s)", buffer, buffer2,
              buffer3);
#endif
      node->keys[0] = sibling_key;
      node->children[0] = child;
      node->children[1] = node->children[0];
    } else {
#ifdef DEBUG_BTREE
      BT_print_key(btree, child_key, buffer);
      BT_print_node(btree, node->children[0], buffer2);
      BT_print_node(btree, child, buffer3);
      UTL_log_d("BT_NL_insert - child_key (%s) - (%s, %s)", buffer, buffer2,
              buffer3);
#endif
      node->keys[0] = child_key;
      node->children[0] = node->children[0];
      node->children[1] = child;
    }

  } else {
#ifdef DEBUG_BTREE
    BT_print_key(btree, child_key, buffer);
    BT_print_key(btree, node->keys[0], buffer2);
    UTL_log_d("BT_NL_insert - i (%i) != 0 || child_key (%s) <= sibling_key (%s)",
            i, buffer, buffer2);
#endif
    node->keys[i] = child_key;
    node->children[i + 1] = child;
  }

  node->size++;

#ifdef DEBUG_BTREE
  BT_print_node(btree, node, buffer);
  UTL_log_d("BT_NL_insert - resulting node: %s", buffer);

  for (i = 0; i <= node->size; i++) {
    BT_print_node(btree, node->children[i], buffer);
    UTL_log_d("BT_NL_insert - child %i: %s", i, buffer);
  }
#endif

  // the previous children are in the correct place

  return true;
}

/**
 * Splits a leaf. This method do not fix the parent pointers.
 * 
 * @param btree
 * @param node
 * @return 
 */
COL_btree_node_t* BT_LF_split_insert(COL_btree_t* btree, COL_btree_node_t* node,
        void* key, void* value) {

  if (!node->is_leaf || !BT_N_is_full(btree, node)) {
    // if not leaf or not full do nothing
    return NULL;
  }

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_node(btree, node, buffer);
  BT_print_key_val(btree, key, value, buffer2);
  UTL_log_d("BT_LF_split_insert - split node %s with tuple %s",
          buffer, buffer2);
#endif

  COL_btree_node_t* res = BT_N_new(btree, true);
  int i, size = node->size + 1, middle = node->size / 2;
  void** keys = new(void*, size);
  void** values = new(void*, size);
  bool inserted = false;

  res->next = node->next;
  node->next = res;

  res->parent = node->parent;

  for (i = 0; i < size; i++) {

    int index = inserted ? i - 1 : i;

    if (!inserted && (i >= size - 1
            || btree->comparator(key, node->keys[index]) < 0)) {
#ifdef DEBUG_BTREE
      BT_print_key(btree, key, buffer);
      UTL_log_d("BT_LF_split_insert - key %s", buffer);
#endif
      keys[i] = key;
      values[i] = value;
      inserted = true;
    } else {
#ifdef DEBUG_BTREE
      BT_print_key(btree, node->keys[index], buffer);

      if (!inserted) {
        UTL_log_d("BT_LF_split_insert - %s - before key", buffer);
      } else {
        UTL_log_d("BT_LF_split_insert - %s - after key", buffer);
      }
#endif
      keys[i] = node->keys[index];
      values[i] = node->values[index];
    }
  }

  // refill node
  node->size = 0;

  // insert the last values of node into res
  for (i = 0; i < size; i++) {
    if (i > middle) {
      res->keys[i - middle - 1] = keys[i];
      res->values[i - middle - 1] = values[i];
      res->size++;
    } else {
      node->keys[i] = keys[i];
      node->values[i] = values[i];
      node->size++;
    }
  }

  free(keys);
  free(values);

  return res;
}

/**
 * Splits a leaf. This method do not fix the parent pointers.
 * 
 * 
 * @param btree
 * @param node
 * @return 
 */
COL_btree_node_t* BT_NL_split(COL_btree_t* btree, COL_btree_node_t* node,
        void** overflow_key) {

  if (node->is_leaf || !BT_N_is_full(btree, node)) {
    // if leaf or not full do nothing
    return NULL;
  }

  COL_btree_node_t* res = BT_N_new(btree, false);
  int i, size = node->size, middle = node->size / 2;

  res->next = node->next;
  node->next = res;

  // insert the last values of node into res
  for (i = size; i >= 0; i--) {
    if (i > middle) {
      if (i - middle - 2 >= 0) {
        res->keys[i - middle - 2] = node->keys[i - 1];
        node->size--;
        res->size++;
      }
      res->children[i - middle - 1] = node->children[i];
    } else {
      break;
    }
  }
  // the remaining values are in the correct positions
  node->size--;
  *overflow_key = node->keys[node->size];

  return res;
}

/**
 * 
 * @param btree
 * @param node
 * @param child
 * @param ins_index
 * @param node_key
 * @param overflow_key
 * @return 
 */
COL_btree_node_t* BT_NL_split_insert(COL_btree_t* btree, COL_btree_node_t* node,
        COL_btree_node_t* child, void* node_key, void** overflow_key) {

  if (node->is_leaf || !BT_N_is_full(btree, node)) {
    // if leaf or not full do nothing
    return NULL;
  }

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];
  char buffer3[512];

  BT_print_node(btree, node, buffer);
  BT_print_node(btree, child, buffer2);
  UTL_log_d("BT_NL_split_insert - split node %s with child %s", buffer, buffer2);
#endif

  COL_btree_node_t* res = BT_N_new(btree, false);
  int i, size = node->size + 1, middle = size / 2;
  void** keys = new(void*, size);
  COL_btree_node_t** children = new(COL_btree_node_t*, size + 1);
  bool inserted = false;

  res->next = node->next;
  node->next = res;

  res->parent = node->parent;

  for (i = 0; i < size; i++) {

    int index = inserted ? i - 1 : i;

    if (!inserted && i == 0
            && btree->comparator(node_key, node->keys[index]) < 0) {
      // insert at beginning
#ifdef DEBUG_BTREE
      BT_print_key(btree, node_key, buffer);
      BT_print_node(btree, node->children[0], buffer2);
      BT_print_node(btree, child, buffer3);
      UTL_log_d("BT_NL_split_insert - key %s at beginning (%s, %s)", buffer,
              buffer2, buffer3);
#endif

      keys[0] = node_key;
      children[0] = node->children[0];
      children[1] = child;
      // TODO: if node->children[0]->keys[0] < child->keys[0]
      // then: children[0] = node->children[0] else: children[0] = child

      inserted = true;
    } else if (!inserted && i >= size - 1) {
      // insert at ending
#ifdef DEBUG_BTREE
      BT_print_key(btree, node_key, buffer);
      BT_print_node(btree, node->children[i], buffer2);
      BT_print_node(btree, child, buffer3);
      UTL_log_d("BT_NL_split_insert - key %s at ending (%s, %s)", buffer,
              buffer2, buffer3);
#endif

      keys[i] = node_key;
      children[i] = node->children[i];
      children[i + 1] = child;

      inserted = true;
    } else if (!inserted
            && btree->comparator(node_key, node->keys[index]) < 0) {
      // insert here
#ifdef DEBUG_BTREE
      BT_print_key(btree, node_key, buffer);
      BT_print_node(btree, node->children[i], buffer2);
      BT_print_node(btree, child, buffer3);
      UTL_log_d("BT_NL_split_insert - key %s (%s, %s)", buffer, buffer2,
              buffer3);
#endif

      keys[i] = node_key;
      children[i] = node->children[i];
      children[i + 1] = child;

      inserted = true;
    } else {
#ifdef DEBUG_BTREE
      BT_print_key(btree, node->keys[index], buffer);
      BT_print_node(btree, node->children[i], buffer2);

      if (!inserted) {
        UTL_log_d("BT_NL_split_insert - %s (%s) - before key", buffer, buffer2);
      } else {
        UTL_log_d("BT_NL_split_insert - %s (%s)- after key", buffer, buffer2);
      }

#endif
      keys[i] = node->keys[index];
      if (inserted) {
        // do not replace the value
        children[i + 1] = node->children[i];
      } else {
        children[i] = node->children[i];
      }
    }
  }

  // refill node
  node->size = 0;

  // insert the last values of node into res
  for (i = 0; i < size; i++) {
    if (i > middle) {
      if (i == middle + 1) {
#ifdef DEBUG_BTREE
        BT_print_node(btree, children[i], buffer);

        UTL_log_d("BT_NL_split_insert - children at %i (after middle): %s",
                i, buffer);
#endif

        res->children[i - middle - 1] = children[i];
        children[i]->parent = res;
      }

#ifdef DEBUG_BTREE
      BT_print_node(btree, children[i + 1], buffer);

      UTL_log_d("BT_NL_split_insert - children at %i + 1 (after middle): %s",
              i, buffer);
#endif

      res->keys[i - middle - 1] = keys[i];
      res->children[i - middle] = children[i + 1];
      children[i + 1]->parent = res;
      res->size++;
    } else if (i == middle) {
#ifdef DEBUG_BTREE
      BT_print_key(btree, keys[i], buffer);

      UTL_log_d("BT_NL_split_insert - overflow at %i: %s", i, buffer);
#endif

      *overflow_key = keys[i];
    } else {
      if (i == 0) {
#ifdef DEBUG_BTREE
        BT_print_node(btree, children[i], buffer);

        UTL_log_d("BT_NL_split_insert - children at %i (before middle): %s", i,
                buffer);
#endif
        node->children[i] = children[i];
        children[i]->parent = node;
      }

#ifdef DEBUG_BTREE
      BT_print_node(btree, children[i + 1], buffer);

      UTL_log_d("BT_NL_split_insert - children at %i + 1 (before middle): %s", i,
              buffer);
#endif

      node->keys[i] = keys[i];
      node->children[i + 1] = children[i + 1];
      children[i + 1]->parent = node;
      node->size++;
    }
  }

#ifdef DEBUG_BTREE
  BT_print_node(btree, node, buffer);
  UTL_log_d("BT_NL_split_insert - node: %s", buffer);

  BT_print_node(btree, res, buffer);
  UTL_log_d("BT_NL_split_insert - res: %s", buffer);
#endif

  free(keys);
  free(children);

  return res;
}

/**
 * Fixes the root in case of split overflow.
 * 
 * @param btree
 * @param old the old node (will stay at left)
 * @param overflow the new node (will stay at right)
 * @param split_key the key to choose whether to go left or right
 */
void BT_fix_root(COL_btree_t* btree, COL_btree_node_t* old,
        COL_btree_node_t* overflow, void* split_key) {

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_node(btree, old, buffer);
  BT_print_node(btree, overflow, buffer2);
  UTL_log_d("BT_fix_root - old: %s, overflow: %s", buffer, buffer2);
#endif

  COL_btree_node_t* root = BT_N_new(btree, false);

  old->next = overflow;
  overflow->next = NULL;

  root->is_root = true;
  root->children[0] = old;
  root->children[1] = overflow;
  root->keys[0] = split_key;
  root->size++;
  btree->height += 1; // root split, the tree grew a level

  old->parent = root;
  overflow->parent = root;

  btree->root_node = root;
}

/**
 * 
 * @param btree
 * @param node
 * @param key the key of the element to remove
 * @return 
 */
COL_btree_node_t* BT_N_sibling(COL_btree_t* btree, COL_btree_node_t* node,
        void** split_key) {

  if (node->is_root) {
    return NULL;
  }

  COL_btree_node_t* sibbling;
  int index = BT_NL_child_pos(btree, node->parent, node->keys[0],
          split_key);

  if (index < 0) {
    // no sibbling (This should not occur)
    UTL_log_e("Invalid BTREE detected!");
    sibbling = NULL;
  } else if (index == 0) {
    // the sibbling is the next
    sibbling = node->parent->children[index + 1];
  } else {
    // the sibbling is the previous
    sibbling = node->parent->children[index - 1];
  }

  return sibbling;
}

void BT_N_set_root(COL_btree_t* btree, COL_btree_node_t* node) {
  BT_N_delete(btree->root_node);
  btree->root_node = node;
  node->is_root = true;
}

void BT_LF_remove(COL_btree_t* btree, COL_btree_node_t* node, void* key) {

  if (!node->is_leaf) {
    return;
  }

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_key(btree, key, buffer);
  UTL_log_d("BT_LF_remove - key: %s", buffer);
#endif

  int i;
  bool removed = false;

  for (i = 0; i < node->size; i++) {
    int cmp;

    if (!removed) {
      cmp = btree->comparator(node->keys[i], key);
    }

#ifdef DEBUG_BTREE
    BT_print_key(btree, node->keys[i], buffer2);
    UTL_log_d("BT_LF_remove - test key [%s > %s]?", buffer2, buffer);
#endif
    if (removed) {
      // not the removing key: insert
      node->keys[i - 1] = node->keys[i];
      node->values[i - 1] = node->values[i];
    } else if (cmp == 0) {
#ifdef DEBUG_BTREE
      UTL_log_d("BT_LF_remove - [%s == %s]", buffer2, buffer);
#endif
      removed = true;
    }
  }
  node->size--;
}

void BT_NL_remove(COL_btree_t* btree, COL_btree_node_t* node, int index) {

  if (node->is_leaf) {
    return;
  }

#ifdef DEBUG_BTREE
  char buffer[512];

  BT_print_node(btree, node, buffer);
  UTL_log_d("BT_NL_remove - node: %s, index: %i", node, index);
#endif

  int i;
  bool removed = false;

  for (i = index; i < node->size; i++) {
    if (index == 0 && !removed) {
      node->children[0] = node->children[1];
      removed == true;
    } else if (!removed) {
      node->keys[i - 1] = node->keys[i];
      node->children[i] = node->children[i + 1];
      removed == true;
    } else if (removed) {
      node->keys[i - 1] = node->keys[i];
      node->children[i] = node->children[i + 1];
    }
  }
  node->size--;
}

/**
 * 
 * @param btree
 * @param left
 * @param right
 * @param key the key to remove
 * @return true if only the left node has values
 */
bool BT_LF_merge(COL_btree_t* btree, COL_btree_node_t* left,
        COL_btree_node_t* right, void* key) {

  if (!left->is_leaf || !right->is_leaf || left->parent != right->parent) {
    return false;
  }

  int i, index = 0,
          l_size = left->size,
          r_size = right->size,
          total = l_size + r_size - 1,
          mid = total / 2;
  void **keys = new(void*, total),
          **values = new(void*, total);

  for (i = 0; i < l_size; i++) {
    if (btree->comparator(left->keys[i], key)) {
      // not the removing key: insert
      keys[index] = left->keys[i];
      values[index] = left->values[i];
      index++;
    }
  }

  for (i = 0; i < r_size; i++) {
    if (btree->comparator(right->keys[i], key)) {
      // not the removing key: insert
      keys[index] = right->keys[i];
      values[index] = right->values[i];
      index++;
    }
  }

  left->size = 0;
  right->size = 0;

  for (index = 0; index < total; index++) {
    if (index < mid || mid == 0) {
      left->keys[index] = keys[index];
      left->values[index] = values[index];
      left->size++;
    } else {
      right->keys[index - mid] = keys[index];
      right->values[index - mid] = values[index];
      right->size++;
    }
  }

  if (right->size == 0) {
    left->next = right->next;
    BT_N_delete(right);
    return true;
  } else {
    return false;
  }
}

/**
 * 
 * @param btree
 * @param left
 * @param right
 * @param key the key that must be removed due to children deficit
 * @param parent_key the parent key that divides the left and right nodes
 * @return true if only the left node has children
 */
bool BT_N_merge(COL_btree_t* btree, COL_btree_node_t* left,
        COL_btree_node_t* right, void* key, void* parent_key) {

  if (left->is_leaf || right->is_leaf
          || left->parent == NULL || right->parent == NULL
          || left->parent != right->parent) {
    return false;
  }

  int i, index = 0,
          l_size = left->size,
          r_size = right->size,
          total = l_size + r_size,
          mid = total / 2;
  void** keys = new(void*, total);
  COL_btree_node_t** children = new(COL_btree_node_t*, total + 2);

  for (i = 0; i < l_size; i++) {
    if (i == 0) {
      children[index] = left->children[i];
    }
    children[index + 1] = left->children[i + 1];
    if (btree->comparator(left->keys[i], key)) {
      keys[index] = left->keys[i];
    } else {
      // insert parent key instead
      keys[index] = parent_key;
    }
    index++;
  }

  for (i = 0; i < r_size; i++) {
    if (i == 0) {
      children[index + 1] = right->children[i];
    }
    children[index + 2] = right->children[i + 1];
    if (btree->comparator(right->keys[i], key)) {
      keys[index] = right->keys[i];
    } else {
      // insert parent key instead
      keys[index] = parent_key;
    }
    index++;
  }

  left->size = 0;
  right->size = 0;

  for (index = 0; index < total; index++) {
    if (index < mid) {
      left->keys[index] = keys[index];
      if (index == 0) {
        right->children[index] = children[index];
      }
      left->children[index + 1] = children[index + 1];
      left->size++;
    } else {
      right->keys[index - mid - 1] = keys[index];
      if (index == mid) {
        right->children[index - mid - 1] = children[index + 1];
      }
      right->children[index - mid] = children[index + 2];
      right->size++;
    }
  }

  return right->size == 0;
}

/**
 * 
 * @param btree
 * @param left
 * @param right
 * @param key the key that must be removed due to children deficit
 * @return true if only the left node has children
 */
void BT_NL_rot_left(COL_btree_t* btree, COL_btree_node_t* left,
        COL_btree_node_t* right, void* key) {

  if (left->is_leaf || right->is_leaf
          || left->parent == NULL || right->parent == NULL
          || left->parent != right->parent) {
    return;
  }

  COL_btree_node_t* parent = right->parent;
  void* parent_key;
  int i, index = BT_NL_child_pos(btree, parent, right, &key),
          p_size = parent->size,
          r_size = right->size;

  left->keys[0] = parent_key;
  BT_N_delete(left->children[1]);
  left->children[1] = right->children[0];

  for (i = 1; i < r_size; i++) {
    right->keys[i - 1] = right->keys[i];
    right->children[i - 1] = right->children[i];
  }
  right->children[i - 1] = right->children[i];

  right->size--;

  for (i = index; i < p_size; i++) {
    parent->keys[i - 1] = parent->keys[i];
    parent->children[i - 1] = parent->children[i];
  }
  parent->children[i - 1] = parent->children[i];

  parent->size--;
}

/**
 * 
 * @param btree
 * @param left
 * @param right
 * @param key the key that must be removed due to children deficit
 * @return true if only the left node has children
 */
void BT_NL_rot_right(COL_btree_t* btree, COL_btree_node_t* left,
        COL_btree_node_t* right, void* key) {

  if (left->is_leaf || right->is_leaf
          || left->parent == NULL || right->parent == NULL
          || left->parent != right->parent) {
    return;
  }

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_node(btree, left, buffer);
  BT_print_node(btree, right, buffer2);
  UTL_log_d("btree_BT_NL_rot_right - left: %s, right: %s", buffer, buffer2);
#endif

  COL_btree_node_t* parent = right->parent;
  void* parent_key;
  int i, index = BT_NL_child_pos(btree, parent, right, &parent_key),
          p_size = parent->size,
          l_size = left->size;

  right->keys[0] = parent_key;
  right->children[1] = left->children[0];

#ifdef DEBUG_BTREE
  BT_print_node(btree, right, buffer);
  UTL_log_d("btree_BT_NL_rot_right - right: %s", buffer);
#endif

  for (i = 1; i < l_size; i++) {
    left->keys[i - 1] = left->keys[i];
    left->children[i - 1] = left->children[i];
  }
  left->children[i - 1] = left->children[i];

  left->size--;

#ifdef DEBUG_BTREE
  BT_print_node(btree, left, buffer);
  UTL_log_d("btree_BT_NL_rot_right - left: %s", buffer);
#endif

  for (i = index; i < p_size; i++) {
    parent->keys[i - 1] = parent->keys[i];
    parent->children[i - 1] = parent->children[i];
  }
  parent->children[i - 1] = parent->children[i];

  parent->size--;

#ifdef DEBUG_BTREE
  BT_print_node(btree, parent, buffer);
  UTL_log_d("btree_BT_NL_rot_right - parent: %s", buffer);
#endif
}

void BT_NL_rot(COL_btree_t* btree, COL_btree_node_t* node) {

  if (node->is_leaf || node->parent == NULL) {
    return;
  }

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_node(btree, node, buffer);
  UTL_log_d("btree_BT_NL_rot - node: %s", buffer, buffer2);
#endif

  void* parent_key;
  COL_btree_node_t* sibling = BT_N_sibling(btree, node, &parent_key);

  if (btree->comparator(sibling->keys[0], node->keys[0]) > 0) {
#ifdef DEBUG_BTREE
    BT_print_node(btree, node, buffer);
    BT_print_node(btree, sibling, buffer2);
    UTL_log_d("btree_BT_NL_rot - left: %s, right: %s", buffer, buffer2);
#endif
    // sibling is at right
    BT_NL_rot_left(btree, node, sibling, parent_key);
  } else {
#ifdef DEBUG_BTREE
    BT_print_node(btree, sibling, buffer);
    BT_print_node(btree, node, buffer2);
    UTL_log_d("btree_BT_NL_rot - left: %s, right: %s", buffer, buffer2);
#endif
    // sibling is at left
    BT_NL_rot_right(btree, sibling, node, parent_key);
  }
}

/**
 * 
 * @param btree
 * @param node
 * @param key the key of the removing value
 */
void BT_N_remove(COL_btree_t* btree, COL_btree_node_t* node, void* key) {
  // TODO

  //  bool invalid = node1->is_leaf != node2->is_leaf;
  //  bool overflow_leaf = node1->size + node2->size >= btree->bfactor - 1
  //      && node1->is_leaf;
  //  bool overflow_non_leaf = node1->size + node2->size > btree->bfactor - 1
  //      && !node1->is_leaf;

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_node(btree, node, buffer);
  BT_print_key(btree, key, buffer2);
  UTL_log_d("BT_N_remove - node: %s, key: %s", buffer, buffer2);
#endif

  if (node->size > (btree->bfactor / 2 - 1) || node->is_root) {
    BT_LF_remove(btree, node, key);
  } else {
    void* parent_key;
    COL_btree_node_t *left, *right, *parent = node->parent,
            *sibling = BT_N_sibling(btree, node, &parent_key);
    int index;

    if (btree->comparator(sibling->keys[0], node->keys[0]) > 0) {
      right = sibling;
      left = node;
    } else {
      right = node;
      left = sibling;
    }

#ifdef DEBUG_BTREE
    BT_print_node(btree, left, buffer);
    BT_print_node(btree, right, buffer2);
    UTL_log_d("BT_N_remove - left: %s, right: %s", buffer, buffer2);
    BT_print_node(btree, parent, buffer);
    UTL_log_d("BT_N_remove - parent: %s", buffer);
#endif

    index = BT_NL_child_pos(btree, parent, left->keys[0], NULL);

    if (BT_LF_merge(btree, left, right, key)) {
      // right node is size zero
      if (parent->size > 1) {
        BT_NL_remove(btree, parent, index + 1);
      } else while (parent->size == 1) {
          BT_NL_rot(btree, parent);

          // TODO: check if parent siblings are weighted (size > 0)
          // parent = parent->parent
          break;
        }
    } else {
      // must fix the parent key
      parent->keys[index] = right->keys[0];
    }

  }

  // merge the two

  // if sibbling || node == NULL (merged into one)
  //   if parent.size == 1 && parent.isRoot
  //     node.isRoot = true
  //   else if parent.size > 1
  //     remove one key from parent and re-arrange children
  //   else parent is not root but only has 1 child
  //     
}

void BT_insert(COL_btree_t* btree, void* key, void* value) {

#ifdef DEBUG_BTREE
  char buffer[4096];

  BT_print_key_val(btree, key, value, buffer);
  UTL_log_d("BT_insert - %s", buffer);
#endif

  COL_btree_node_t* node = BT_N_find(btree, key);

  if (BT_N_is_full(btree, node)) {
    COL_btree_node_t* other;
    COL_btree_node_t* parent = node->parent;

    other = BT_LF_split_insert(btree, node, key, value);
    void* overflow_key = other->keys[0];

#ifdef DEBUG_BTREE
    BT_print_node(btree, node, buffer);
    UTL_log_d("BT_insert - node: %s", buffer);

    BT_print_node(btree, other, buffer);
    UTL_log_d("BT_insert - other: %s", buffer);
#endif

    if (parent == NULL) {
#ifdef DEBUG_BTREE
      UTL_log_d("BT_insert - parent is NULL", buffer);
#endif

      // Create new parent
      BT_fix_root(btree, node, other, overflow_key);

    } else {

      while (parent != NULL) {
#ifdef DEBUG_BTREE
        BT_print_node(btree, parent, buffer);
        UTL_log_d("BT_insert - parent: %s", buffer);
#endif

        // other should be at right from node
        if (BT_N_is_full(btree, parent)) {
          COL_btree_node_t* parent_split = BT_NL_split_insert(
                  btree, parent, other, overflow_key, &overflow_key);

          other = parent_split;
          node = parent;
          parent = parent->parent;

#ifdef DEBUG_BTREE
          BT_print_node(btree, node, buffer);
          UTL_log_d("BT_insert - iteration node: %s", buffer);
          BT_print_node(btree, other, buffer);
          UTL_log_d("BT_insert - iteration other: %s", buffer);
#endif
          if (parent == NULL) {
#ifdef DEBUG_BTREE
            UTL_log_d("BT_insert - parent is null");
#endif
            BT_fix_root(btree, node, other, overflow_key);
          }

        } else {
          BT_NL_insert(btree, parent, other, overflow_key);
          break;
        }
      }
    }

#ifdef DEBUG_BTREE
    COL_BTREE_print(btree, buffer);
#endif

  } else {
    BT_LF_insert(btree, node, key, value);
  }

}

// =============== ===================== ====================

COL_btree_t* COL_BTREE_new(int bfactor, int(*comparator)(void*, void*)) {
  COL_btree_t* res = new(COL_btree_t, 1);
  set_zero(res, COL_btree_t, 1);
  int even_bfactor = bfactor & 0xFE;

  res->comparator = comparator;

  // less than 4 won't work, bfactor must be even and less than 254
  res->bfactor = max(even_bfactor, 4);

  return res;
}

void COL_BTREE_set_print(COL_btree_t* btree,
        int(*key_print)(void*, char*, size_t),
        int(*val_print)(void*, char*, size_t)) {
  btree->key_print = key_print;
  btree->val_print = val_print;
}

void COL_BTREE_destroy(COL_btree_t* btree) {
  // TODO
  free(btree);
}

void BT_iterate(COL_btree_node_t* node, bool(*iterator)(void*, void*, void*),
        void* param) {

  int i;

  if (node != NULL) {
    if (node->is_leaf) {
      for (i = 0; i < node->size; i++) {
        if (iterator(node->keys[i], node->values[i], param)) return;
      }
      BT_iterate(node->next, iterator, param);
    } else {
      BT_iterate(node->children[0], iterator, param);
    }
  }
}

void COL_BTREE_iterate(COL_btree_t* btree, bool(*iterator)(void*, void*, void*), void* param) {
  BT_iterate(btree->root_node, iterator, param);
}

char * COL_BTREE_print(COL_btree_t * btree, char * buffer, size_t buffer_size) {
  COL_btree_node_t * root = btree->root_node;
  COL_btree_node_t * node;
  char tmp_buffer[__BUFFER_SIZE];
  int height = btree->height, size = buffer_size;

  if (btree->key_print == NULL) {
    return buffer;
  }

  buffer[0] = '\0';
  tmp_buffer[0] = '\0';

  BT_print_node(btree, root, tmp_buffer, __BUFFER_SIZE);

  snprintf(buffer, __BUFFER_SIZE, "root:   %*s", (int) btree->height * 4, "");
  strncat(buffer, tmp_buffer, size);
  size -= strlen(buffer);
  strncat(buffer, "\n\n\0", size);
  size -= 2;

#ifdef DEBUG_BTREE
  UTL_log_d("root node: %s", tmp_buffer);
#endif

  node = root;

  BT_print(btree, node, buffer, size, height);

  return buffer;
}

void COL_BTREE_insert(COL_btree_t* btree, void* key, void* value) {
  COL_btree_node_t* root = btree->root_node;

#ifdef DEBUG_BTREE
  char buffer[128];

  BT_print_key_val(btree, key, value, buffer);
  UTL_log_d("BTREE_insert - %s", buffer);
#endif

  if (COL_BTREE_search(btree, key)) {
    // already inserted
    return;
  }

  btree->size += 1;

  if (root == NULL) {
    COL_btree_node_t* res = BT_N_new(btree, true);
    btree->root_node = res;
    res->is_root = true;

    BT_LF_insert(btree, res, key, value);

  } else {
    BT_insert(btree, key, value);
  }
}

COL_btree_node_t* BT_search(COL_btree_t* btree, COL_btree_node_t* node,
        void* key) {

  if (node == NULL) {
    return NULL;
  }

  int i;

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_key(btree, key, buffer);
  BT_print_node(btree, node, buffer2);
  UTL_log_d("BT_search - key: %s, node: %s", buffer, buffer2);
#endif

  if (node->is_leaf) {
#ifdef DEBUG_BTREE
    BT_print_node(btree, node, buffer);
    UTL_log_d("BT_search - return node: %s", buffer);
#endif
    return node;
  }

  for (i = 0; i < node->size; i++) {
#ifdef DEBUG_BTREE
    BT_print_key(btree, node->keys[i], buffer);
    UTL_log_d("BT_search - key %i (%s), node_size: %i",
            i, buffer, node->size);
#endif
    if (btree->comparator(node->keys[i], key) > 0) {
      return BT_search(btree, node->children[i], key);
    }
  }

  if (btree->comparator(key, node->keys[i - 1]) >= 0) {
    return BT_search(btree, node->children[i], key);
  }

  return NULL;
}

void* COL_BTREE_remove(COL_btree_t* btree, void* key) {
  COL_btree_node_t* node = BT_search(btree, btree->root_node, key);
  void* res;

  if (node == NULL) {
    // not found
    return NULL;
  }

  res = BT_LF_get(btree, node, key);

  if (BT_LF_get(btree, node, key)) {
    // found value, must be removed
    BT_N_remove(btree, node, key);
  }

  return res;
}

void* COL_BTREE_search(COL_btree_t* btree, void* key) {

  if (btree->root_node == NULL) {
    return NULL;
  }

  COL_btree_node_t* node = BT_search(btree, btree->root_node, key);
  int i;

#ifdef DEBUG_BTREE
  char buffer[512];
  char buffer2[512];

  BT_print_key(btree, key, buffer);
  BT_print_node(btree, node, buffer2);
  UTL_log_d("COL_BTREE_search - key: %s, node: %s", buffer, buffer2);
#endif

  for (i = 0; i < node->size; i++) {
    if (btree->comparator(node->keys[i], key) == 0) {
      return node->values[i];
    }
  }

  return NULL;
}
