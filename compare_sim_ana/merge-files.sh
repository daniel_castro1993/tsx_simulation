for f in data/ana* ;
do
	echo $f
	if [ ! -f ana-file.txt ] ; then
		sed "1q;d" $f > ana-file.txt ;
	fi
	sed "2q;d" $f >> ana-file.txt ;
done
for f in data/sim* ; 
do
	echo $f
	if [ ! -f sim-file.txt ] ; then
		sed "1q;d" $f > sim-file.txt ;
	fi
	sed "2q;d" $f >> sim-file.txt ;
done
