import matplotlib as mpl
import numpy as np
import operator
import os
import re
import sys

mpl.use('Agg')

import matplotlib.pyplot as plt

mpl.rcParams['legend.numpoints'] = 1

"""
call this program with 2 arguments:

python plot.py <ana_file> <sim_file>
"""

ana_data_file = sys.argv[1]
sim_data_file = sys.argv[2]

line_patt = r'([^,]+)'

class PlotFigure:
  def __init__(self):
    self.cm = plt.cm.get_cmap('rainbow')
        
  def plot_color_data(self, sim, ana, z, error, title, color_title, image_name):
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
        
    lim_min = min(min(sim), min(ana)) - 0.05
    lim_max = max(max(sim), max(ana)) + 0.05
        
    ax1.set_title(title + " (average percent error = {:5.2f}%)"
                  .format(error), y=1.06, x=0.60)
    ax1.set_xlabel('simulation')
    ax1.set_ylabel('analytical')
        
    plt.scatter(sim, ana, c=z, s=15, cmap=self.cm)
    plt.colorbar().set_label(color_title)
        
    ax1.set_xlim([lim_min, lim_max])
    ax1.set_ylim([lim_min, lim_max])

    fig.savefig(image_name + ".png")
    plt.close(fig)
    
    plt.clf()
    plt.cla()   
    
  def plot(self, sim, ana, error, title, image_name):
    fig = plt.figure()
    ax1 = fig.add_subplot(111)
        
    lim_min = min(min(sim), min(ana)) - 0.05
    lim_max = max(max(sim), max(ana)) + 0.05
        
    ax1.set_xlim([lim_min, lim_max])
    ax1.set_ylim([lim_min, lim_max])
        
    ax1.set_title(title + " (average percent error = {:5.2f}%)"
                  .format(error), y=1.06, x=0.50)
    ax1.set_xlabel('simulation')
    ax1.set_ylabel('analytical')
        
    plt.scatter(sim, ana)

    fig.savefig(image_name + ".png")
    plt.close(fig)
    
    plt.clf()
    plt.cla()
        
                               
class DataFileHandler:
  def read_data(self, file, prefix, data_cnt):
    with open(file) as f:
      lines = f.read().split('\n')
      first_line = False
            
      for l in lines:
        if not first_line:
          first_line = True
        else:
          m = re.findall(line_patt, l)
          if m != None and len(m) > 0:
            obj = {
              "PROB_READ": float(m[0]),
              "D": int(m[1]),
              "L": int(m[2]),
              "C": float(m[3]),
              "THREADS": int(m[4]),
              "BUDGET": int(m[5]),
              "P_R(L)": float(m[6]),
              "R": float(m[7]),
              "X": float(m[8]),
              "R*": float(m[9]),
              "P_A": float(m[10]),
            }
            
            # check if the markov chain files exist
            mc_file = ("data/mc-{:s}-PR_{:4.2f}-C_{:4.2f}-L_{:d}-D_{:d}-"
                       + "T_{:d}-B_{:d}.txt").format(prefix, float(m[0]),
                             float(m[3]), int(m[2]), int(m[1]), int(m[4]),
                             int(m[5]));
                         
            if os.path.isfile(mc_file):
              mc_file = open(mc_file, "r")
              mc_lines = mc_file.read().split('\n')
              first_line_mc = False
              mc = {}

              for l in mc_lines:
                if not first_line_mc:
                  first_line_mc = True
                else:
                  m = re.findall(line_patt, l)
                  if m != None and len(m) > 0:
                    mc[m[0].strip()] = float(m[1])
                    
              obj["MARKOV_CHAIN"] = mc
              
            data_cnt += [obj]

  def merge_data(self):
    for sim in self.sim_data:
      find = [a for a in self.ana_data
        if a["PROB_READ"] == sim["PROB_READ"] and
          a["D"] == sim["D"] and
          a["L"] == sim["L"] and
          a["C"] == sim["C"] and
          a["THREADS"] == sim["THREADS"] and
          a["BUDGET"] == sim["BUDGET"]]
      if len(find) > 0:
        ana = find[0]
        merged_obj = {
          "PROB_READ": ana["PROB_READ"],
          "D": ana["D"],
          "L": ana["L"],
          "C": ana["C"],
          "THREADS": ana["THREADS"],
          "BUDGET": ana["BUDGET"],
          "ana_P_R(L)": ana["P_R(L)"],
          "ana_R": ana["R"],
          "ana_X": ana["X"],
          "ana_R*": ana["R*"],
          "ana_PA": ana["P_A"],
          "sim_P_R(L)": sim["P_R(L)"],
          "sim_R": sim["R"],
          "sim_X": sim["X"],
          "sim_R*": sim["R*"],
          "sim_PA": sim["P_A"],
          "error_P_R(L)": abs(sim["P_R(L)"] - ana["P_R(L)"]) / sim["P_R(L)"],
          "error_R": abs(sim["R"] - ana["R"]) / sim["R"],
          "error_X": abs(sim["X"] - ana["X"]) / sim["X"]
        }
        if "MARKOV_CHAIN" in ana:
          err_mc = {}
          ana_mc = set(ana["MARKOV_CHAIN"].keys())
          sim_mc = set(sim["MARKOV_CHAIN"].keys())
          intersect_mc_states = ana_mc & sim_mc
          for s in intersect_mc_states:
            if sim["MARKOV_CHAIN"][s] > 0:
              err_mc[s] = abs(sim["MARKOV_CHAIN"][s] - ana["MARKOV_CHAIN"][s]) / sim["MARKOV_CHAIN"][s]
          err_mc = sorted(err_mc.items(), key=operator.itemgetter(1),
                          reverse=True)
          merged_obj["MARKOV_CHAIN"] = err_mc
        
        self.merged_data += [merged_obj]
        
    self.outliers = sorted(self.merged_data, key=lambda k: k['error_X'],
                           reverse=True)
                        
  def __init__(self, ana_file, sim_file):
    self.ana_data = []
    self.sim_data = []
    self.merged_data = []
        
    self.read_data(ana_file, "ana", self.ana_data)
    self.read_data(sim_file, "sim", self.sim_data)
    self.merge_data()
        
    self.PROB_READ = np.array([x["PROB_READ"] for x in self.merged_data])
    self.D = np.array([x["D"] for x in self.merged_data])
    self.C = np.array([x["C"] for x in self.merged_data])
    self.L = np.array([x["L"] for x in self.merged_data])
    self.THREADS = np.array([x["THREADS"] for x in self.merged_data])
    self.BUDGET = np.array([x["BUDGET"] for x in self.merged_data])
        
    self.sim_P_R = np.array([x["sim_P_R(L)"] for x in self.merged_data])
    self.sim_R = np.array([x["sim_R*"] for x in self.merged_data])
    self.sim_X = np.array([x["sim_X"] for x in self.merged_data])
        
    self.ana_P_R = np.array([x["ana_P_R(L)"] for x in self.merged_data])
    self.ana_R = np.array([x["ana_R*"] for x in self.merged_data])
    self.ana_X = np.array([x["ana_X"] for x in self.merged_data])
        
    self.samples = len(self.merged_data)
    self.sum_error_P_R = sum([err["error_P_R(L)"] for err in self.merged_data])
    self.sum_error_R = sum([err["error_R"] for err in self.merged_data])
    self.sum_error_X = sum([err["error_X"] for err in self.merged_data])
        
    self.error_P_R = self.sum_error_P_R / self.samples
    self.error_R = self.sum_error_R / self.samples
    self.error_X = self.sum_error_X / self.samples


h = DataFileHandler(ana_data_file, sim_data_file)
p = PlotFigure()

p.plot(h.sim_X, h.ana_X, h.error_X * 100.0, "Throughput", "throughput")
p.plot_color_data(h.sim_X, h.ana_X, h.D, h.error_X * 100.0,
                  "Throughput", "D", "throughput_per_D")
p.plot_color_data(h.sim_X, h.ana_X, h.L, h.error_X * 100.0,
                  "Throughput", "L", "throughput_per_L")
p.plot_color_data(h.sim_X, h.ana_X, h.BUDGET, h.error_X * 100.0,
                  "Throughput", "BUDGET", "throughput_per_BUDGET")
# p.plot(h.sim_R, h.ana_R, h.error_R * 100.0, "R*", "R_star")
# p.plot(h.sim_P_R, h.ana_P_R, h.error_P_R * 100.0, "P_R(L)", "P_R_L")

print(("{:>6s}, {:>6s}, {:>4s}, {:>4s}, {:>4s}, {:>4s}, {:>9s}, {:>9s}, {:>9s},"
      + " {:>9s}, {:>9s}, {:>9s}, {:>9s}, {:>9s}, {:>9s}, {:>9s}, {:>9s}")
      .format("P_READ", "D", "L", "C", "THRS", "BUDG", "S_P_R(L)", "A_P_R(L)",
      "S_R", "A_R", "S_X", "A_X", "S_R*", "A_R*", "S_PA", "A_PA",
      "ERR_X"))
for o in h.outliers:
  print(("{:6.2f}, {:6d}, {:4d}, {:4.2f}, {:4d}, {:4d}, {:9f}, {:9f}, "
        + "{:9f}, {:9f}, {:9f}, {:9f}, {:9f}, {:9f}, {:9f}, {:9f}, {:9f}")
        .format(o["PROB_READ"], o["D"], o["L"], o["C"], o["THREADS"], 
        o["BUDGET"], o["sim_P_R(L)"], o["ana_P_R(L)"],
        o["sim_R"], o["ana_R"], o["sim_X"], o["ana_X"],
        o["sim_R*"], o["ana_R*"], o["sim_PA"], o["ana_PA"], o["error_X"]))
        
for o in h.outliers:
  if "MARKOV_CHAIN" in o:
    print(o["MARKOV_CHAIN"])

            
