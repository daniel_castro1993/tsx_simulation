#include "app.h"
#include "ROOT-Sim.h"

#ifndef DATA_FILE
#  define DATA_FILE "data_file.txt"
#endif

#define RAND_R_FNC(_SEED, _AUX) ({                                            \
    _AUX = _SEED;                                                             \
    _AUX *= 1103515245;                                                       \
    _AUX += 12345;                                                            \
    _SEED = (_AUX / 65536) % 2048;                                            \
    _AUX *= 1103515245;                                                       \
    _AUX += 12345;                                                            \
    _SEED <<= 10;                                                             \
    _SEED ^= (unsigned int) (_AUX / 65536) % 1024;                            \
    _AUX *= 1103515245;                                                       \
    _AUX += 12345;                                                            \
    _SEED <<= 10;                                                             \
    _SEED ^= (unsigned int) (_AUX / 65536) % 1024;                            \
    _SEED;                                                                    \
  })


int nb_sets__ = 8;
int nb_ways__ = 2;

bool is_init = false;

int sample_indx = 0;

unsigned seed = 1103515245;

void ProcessEvent(unsigned int me, simtime_t now, unsigned int event,
        void * buffer, unsigned int size, void * state) {

  state_t* state_;
  int set;
  register unsigned aux;

  switch (event) {
    case INIT:

      if (!is_init) {
        is_init = true;

        if (IsParameterPresent(buffer, "NB_SETS"))
          nb_sets__ = GetParameterInt(buffer, "NB_SETS");

        if (IsParameterPresent(buffer, "NB_WAYS"))
          nb_ways__ = GetParameterInt(buffer, "NB_WAYS");

        if (n_prc_tot > 1) {
          printf("You must use only 1 LP\n");
          exit(EXIT_SUCCESS);
        }

#if defined(INTER_ACC) && INTER_ACC > 0
        printf("sequential access with spacing=%i\n", INTER_ACC);
#endif

      }
      state_ = (state_t*) malloc(sizeof (state_t));

      state_->setXway = (int*) calloc(nb_sets__, sizeof (int));
      state_->end = false;
      state_->nb_acc = 0;
      state_->last_set = (int) round(Random() * 9721) % nb_sets__;

      SetState(state_);
      state_->end = false;
      ScheduleNewEvent(me, now + ELAPSED_TIME, EVENT_THROW_ADDRESS, NULL, 0);

      break;
    case EVENT_THROW_ADDRESS:

      state_ = (state_t*) state;

#if defined(INTER_ACC) && INTER_ACC > 0
      set = (state_->last_set + INTER_ACC) % nb_sets__;
#else
      //      seed = RAND_R_FNC(seed, aux);
      //      set = seed % nb_sets__;
      set = RandomRange(0, nb_sets__ - 1);
      //      set = (int) round(Random() * 9721) % nb_sets__;
      //      set = (int) round(Random() * 1e6) % nb_sets__;
#endif

      state_->last_set = set;
      if (state_->setXway[set] < nb_ways__) {
        // continue
        ScheduleNewEvent(me, now + ELAPSED_TIME, EVENT_THROW_ADDRESS, NULL, 0);
        state_->setXway[set]++;
        state_->nb_acc++;
      } else {
        // capacity exception
        ScheduleNewEvent(me, now + ELAPSED_TIME, EVENT_END, NULL, 0);
        state_->end = true;
      }

      break;

    case EVENT_END:
    default:

      state_ = (state_t*) state;

      if (state_->end) {
        ScheduleNewEvent(me, now + ELAPSED_TIME * 10000, EVENT_END, NULL, 0);
      } else {
        ScheduleNewEvent(me, now + ELAPSED_TIME, EVENT_THROW_ADDRESS, NULL, 0);
      }

      break;
  }

}

bool OnGVT(unsigned int me, void * state) {

  static double* sets = NULL;
  static int count = 0;
  static double avg_nb_acc = 0;
  static double sd_nb_acc = 0;

  state_t* state_ = (state_t*) state;
  int i, j;

  if (state_->end) {

    if (sample_indx < NUMBER_SAMPLES) {
      sample_indx++;

      if (sets == NULL) {
        sets = (double*) calloc(nb_ways__ + 1, sizeof (double));
      }

      count++;

      for (i = 0; i < nb_sets__; ++i) {
        sets[state_->setXway[i]] += 1;
        state_->setXway[i] = 0;
      }

      if (count > 2) {
        double r = ((double) count - 2) / ((double) count - 1);
        double pow_diff_acc = pow((double) state_->nb_acc - avg_nb_acc, 2);
        sd_nb_acc = r * sd_nb_acc + pow_diff_acc / (double) count;
      }

      if (count == 1) {
        avg_nb_acc = state_->nb_acc;
      } else {
        avg_nb_acc += ((double) state_->nb_acc - avg_nb_acc) / (double) count;
      }

      state_->end = false;
      state_->nb_acc = 0;

    } else {
      FILE* fp = fopen(DATA_FILE, "a");

      double avg_nb_ways = 0;
      double sd_nb_ways = 0;

      for (i = 0; i < nb_ways__ + 1; ++i) {
        avg_nb_ways += (double) i * sets[i] / (double) count;
      }
      avg_nb_ways /= (double) nb_sets__;

      if (nb_ways__ < 1) {
        sd_nb_ways = 0;
      } else {
        for (i = 0; i < nb_ways__ + 1; ++i) {
          sd_nb_ways += pow(sets[i] / (double) count - avg_nb_ways, 2);
        }
        sd_nb_ways /= (double) nb_sets__;
      }

      fprintf(fp, "NB_SETS=%i, NB_WAYS=%i", nb_sets__, nb_ways__);
#if defined(INTER_ACC) && INTER_ACC > 0
      fprintf(fp, ", INTER_ACC=%i", INTER_ACC);
#endif
      fprintf(fp, "\n");
      fprintf(fp, "Distribution: \n");

      for (i = 0; i < nb_ways__ + 1; ++i) {
        fprintf(fp, " [%6i] ", i);
      }
      fprintf(fp, "\n");
      for (i = 0; i < nb_ways__ + 1; ++i) {
        fprintf(fp, " %8.5f ", sets[i] / (double) count);
      }
      fprintf(fp, "\n");
      fprintf(fp, "avg nb of accesses before capacity: %lf\n", avg_nb_acc);
      fprintf(fp, "sd nb of accesses before capacity: %lf\n", sqrt(sd_nb_acc));
      fprintf(fp, "avg nb of filled ways: %lf\n", avg_nb_ways);
      fprintf(fp, "sd nb of filled ways: %lf\n", sqrt(sd_nb_ways));
      fprintf(fp, "\n");
      fclose(fp);
      return true;
    }
  }
  return false;
}

