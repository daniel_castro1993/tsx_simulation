#ifndef __APP_H_GUARD
#define __APP_H_GUARD

#include <math.h>
#include <stdint.h>
#include <stdbool.h>

#define ELAPSED_TIME 1e-6
#define NUMBER_SAMPLES 100000

typedef enum _EVENTS {
  EVENT_THROW_ADDRESS = 1,
  EVENT_END
} EVENTS;

typedef struct _state_t {
  bool end;
  int* setXway;
  int nb_acc, last_set;
} state_t;

#endif // define __APP_H_GUARD
