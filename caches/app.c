#include "app.h"

#define _INC_TS(_TS) (_TS += 100)

#define _CACHE(_ID) (global_info.tot_nb_thrs + (_ID))

FILE* state_file = NULL;

void open_file() {
  if (state_file == NULL) state_file = fopen("cache_stats.txt", "w");
}

void* random_int(int min, int max) {
  int* res = new(int, 1);
  *res = RandomRange(min, max);
  return res;
}

void* random_int_dist(int mean, int min, int max) {
  int* res = new(int, 1);
  *res = RandomRangeNonUniform(mean, min, max);
  return res;
}

/**
 * Last LP -- memory
 * Second last LP -- scache
 * Third last LP -- cpu
 * i+1 to j -- pcaches
 * 0 to i last LP -- threads
 * 
 * @param me
 * @param now
 */
void EVENT_init(unsigned int me, simtime_t now) {

  lp_state_t * lp_state;
  HTM_pc_state_t * pc_state;
  HTM_sc_state_t * sc_state;
  HTM_mem_state_t * mem_state;
  HTM_cpu_state_t * cpu_state;
  evt_cnt_t event;
  HTM_pc_buffer_t pc_buffer;
  HTM_sc_buffer_t sc_buffer;
  HTM_mem_buffer_t mem_buffer;
  HTM_cpu_buffer_t cpu_buffer;
  int i, s_evt = sizeof (evt_cnt_t);

#ifdef __DEBUG_FILE__
  __OPEN_DEBUG_FILE;
#endif

  if (n_prc_tot != 11) {
    //    printf("Use at least 5 LPs (and an odd number)\n");
    printf("Use 11 LPs for this test\n");
    exit(EXIT_FAILURE);
  }

  if (!global_info.is_init) {
    int i, j, k;

    global_info.is_init = true;
    
    global_info.lat_mem_r = 0.01;
    global_info.lat_mem_w = 0.02;
    global_info.lat_sc_r = 0.004;
    global_info.lat_sc_w = 0.008;
    global_info.lat_pc_r = 0.001;
    global_info.lat_pc_w = 0.002;
    
    global_info.tot_nb_thrs = (n_prc_tot - 3) / 2;
    global_info.thrs_pids = new(int, global_info.tot_nb_thrs);
    global_info.tot_nb_pcs = (n_prc_tot - 3) / 2;
    global_info.pcs_pids = new(int, global_info.tot_nb_pcs);
    global_info.cpu_pid = 2 * global_info.tot_nb_thrs;
    global_info.sc_pid = 2 * global_info.tot_nb_thrs + 1;
    global_info.mem_pid = 2 * global_info.tot_nb_thrs + 2;

    global_info.pcs_lines = new(HTM_pc_line_t*, global_info.tot_nb_pcs);

    for (i = 0; i < global_info.tot_nb_pcs; i++) {
      global_info.thrs_pids[i] = i;
      global_info.pcs_pids[i] = global_info.tot_nb_thrs + i;
      INIT_LINES(global_info.pcs_pids[i], j, k);
    }
  }

  if (me == 2 * global_info.tot_nb_thrs + 2) {
    NEW_MEM_STATE(mem_state);
    SetState(mem_state);
    mem_state->now = now;
    mem_state->pid = me;
    HTM_mem_state_init(mem_state);
  } else if (me == 2 * global_info.tot_nb_thrs + 1) {
    NEW_SC_STATE(sc_state);
    SetState(sc_state);
    sc_state->now = now;
    sc_state->pid = me;
    HTM_sc_state_init(sc_state);
  } else if (me == 2 * global_info.tot_nb_thrs) {
    NEW_CPU_STATE(cpu_state);
    SetState(cpu_state);
    cpu_state->now = now;
    cpu_state->pid = me;
    HTM_cpu_state_init(cpu_state);
  } else if (me >= global_info.tot_nb_pcs) {
    NEW_PC_STATE(pc_state);
    SetState(pc_state);
    pc_state->now = now;
    pc_state->pid = me;
    HTM_pc_state_init(pc_state);
  } else {
    int s_evt = sizeof (evt_cnt_t), i = 1;

    lp_state = new(lp_state_t, 1);
    set_zero(lp_state, lp_state_t, 1);

    SetState(lp_state);

    if (me == 0) {
      event.npr = _CACHE(0);
      event.addr = 0x00;
      ScheduleNewEvent(me, now + 100, EVENT_READ, &event, s_evt);

      event.npr = _CACHE(0);
      event.addr = 0x00;
      ScheduleNewEvent(me, now + 200, EVENT_WRITE, &event, s_evt);
    }

    if (me == 1) {
      event.npr = _CACHE(1);
      event.addr = 0x10;
      ScheduleNewEvent(me, now + 310, EVENT_READ, &event, s_evt);
    }

    if (me == 0) {
      event.npr = _CACHE(0);
      event.addr = 0x10;
      ScheduleNewEvent(me, now + 400, EVENT_WRITE, &event, s_evt);
    }

    if (me == 1) {
      event.npr = _CACHE(1);
      event.addr = 0x01;
      ScheduleNewEvent(me, now + 510, EVENT_READ, &event, s_evt);
    }

    if (me == 2) {
      event.npr = _CACHE(2);
      event.addr = 0x01;
      ScheduleNewEvent(me, now + 620, EVENT_READ, &event, s_evt);
    }

    if (me == 3) {
      event.npr = _CACHE(3);
      event.addr = 0x01;
      ScheduleNewEvent(me, now + 730, EVENT_READ, &event, s_evt);
    }

    if (me == 3) {
      event.npr = _CACHE(3);
      event.addr = 0x00;
      ScheduleNewEvent(me, now + 830, EVENT_WRITE, &event, s_evt);
    }

    if (me == 2) {
      event.npr = _CACHE(2);
      event.addr = 0x01;
      ScheduleNewEvent(me, now + 920, EVENT_WRITE, &event, s_evt);
    }

    if (me == 1) {
      event.npr = _CACHE(1);
      event.addr = 0x01;
      ScheduleNewEvent(me, now + 1010, EVENT_READ, &event, s_evt);
    }

    if (me == 3) {
      event.npr = _CACHE(3);
      event.addr = 0x01;
      ScheduleNewEvent(me, now + 1130, EVENT_READ, &event, s_evt);

      event.npr = _CACHE(3);
      event.addr = 0x01;
      ScheduleNewEvent(me, now + 1230, EVENT_WRITE, &event, s_evt);

      event.npr = _CACHE(3);
      ScheduleNewEvent(me, now + 1330, EVENT_TX_BEGIN, &event, s_evt);

      event.npr = _CACHE(3);
      event.addr = 0x00;
      ScheduleNewEvent(me, now + 1430, EVENT_TX_READ, &event, s_evt);
    }

    if (me == 2) {
      // aborts TX from processor 3
      event.npr = _CACHE(2);
      event.addr = 0x01;
      ScheduleNewEvent(me, now + 1520, EVENT_WRITE, &event, s_evt);
    }

    if (me == 1) {
      event.npr = _CACHE(1);
      event.addr = 0x12;
      ScheduleNewEvent(me, now + 1610, EVENT_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x22;
      ScheduleNewEvent(me, now + 1710, EVENT_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x32;
      ScheduleNewEvent(me, now + 1810, EVENT_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x12;
      ScheduleNewEvent(me, now + 1910, EVENT_READ, &event, s_evt);

      event.npr = _CACHE(1);
      ScheduleNewEvent(me, now + 2010, EVENT_TX_BEGIN, &event, s_evt);
    }

    if (me == 2) {
      event.npr = _CACHE(2);
      ScheduleNewEvent(me, now + 2120, EVENT_TX_BEGIN, &event, s_evt);
    }

    if (me == 1) {
      event.npr = _CACHE(1);
      event.addr = 0x02;
      ScheduleNewEvent(me, now + 2210, EVENT_TX_READ, &event, s_evt);
    }

    if (me == 2) {
      event.npr = _CACHE(2);
      event.addr = 0x02;
      ScheduleNewEvent(me, now + 2320, EVENT_TX_READ, &event, s_evt);
    }

    if (me == 1) {
      event.npr = _CACHE(1);
      event.addr = 0x12;
      ScheduleNewEvent(me, now + 2410, EVENT_TX_READ, &event, s_evt);

      // aborts
      event.npr = _CACHE(1);
      event.addr = 0x22;
      ScheduleNewEvent(me, now + 2510, EVENT_TX_READ, &event, s_evt);
    }

    if (me == 2) {
      event.npr = _CACHE(2);
      ScheduleNewEvent(me, now + 2620, EVENT_TX_COMMIT, &event, s_evt);
    }

    if (me == 1) {
      event.npr = _CACHE(1);
      ScheduleNewEvent(me, now + 2710, EVENT_TX_BEGIN, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x02;
      ScheduleNewEvent(me, now + 2810, EVENT_TX_WRITE, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x12;
      ScheduleNewEvent(me, now + 2910, EVENT_TX_WRITE, &event, s_evt);

      // capacity abort
      event.npr = _CACHE(1);
      event.addr = 0x22;
      ScheduleNewEvent(me, now + 3010, EVENT_TX_WRITE, &event, s_evt);

      event.npr = _CACHE(1);
      ScheduleNewEvent(me, now + 3110, EVENT_TX_BEGIN, &event, s_evt);

      // Fill the cache
      event.npr = _CACHE(1);
      event.addr = 0x00;
      ScheduleNewEvent(me, now + 3210, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x02;
      ScheduleNewEvent(me, now + 3310, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x04;
      ScheduleNewEvent(me, now + 3410, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x06;
      ScheduleNewEvent(me, now + 3510, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x08;
      ScheduleNewEvent(me, now + 3610, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x0A;
      ScheduleNewEvent(me, now + 3710, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x0C;
      ScheduleNewEvent(me, now + 3810, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x0E;
      ScheduleNewEvent(me, now + 3910, EVENT_TX_READ, &event, s_evt);

      // capacity exception
      event.npr = _CACHE(1);
      event.addr = 0x10;
      ScheduleNewEvent(me, now + 4010, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      ScheduleNewEvent(me, now + 4110, EVENT_TX_BEGIN, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x00;
      ScheduleNewEvent(me, now + 4210, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x02;
      ScheduleNewEvent(me, now + 4310, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x04;
      ScheduleNewEvent(me, now + 4410, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x06;
      ScheduleNewEvent(me, now + 4510, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x08;
      ScheduleNewEvent(me, now + 4610, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x0A;
      ScheduleNewEvent(me, now + 4710, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x0C;
      ScheduleNewEvent(me, now + 4810, EVENT_TX_READ, &event, s_evt);

      event.npr = _CACHE(1);
      event.addr = 0x0E;
      ScheduleNewEvent(me, now + 4910, EVENT_TX_READ, &event, s_evt);
    }

    if (me == 2) {
      event.npr = _CACHE(2);
      ScheduleNewEvent(me, now + 5020, EVENT_TX_BEGIN, &event, s_evt);

      event.npr = _CACHE(2);
      event.addr = 0x02;
      ScheduleNewEvent(me, now + 5120, EVENT_TX_READ, &event, s_evt);
    }

    if (me == 1) {
      // aborts tx2
      event.npr = _CACHE(1);
      event.addr = 0x02;
      ScheduleNewEvent(me, now + 5210, EVENT_TX_WRITE, &event, s_evt);
    }

    if (me == 2) {
      event.npr = _CACHE(2);
      ScheduleNewEvent(me, now + 5320, EVENT_TX_BEGIN, &event, s_evt);

      // aborts tx1
      event.npr = _CACHE(2);
      event.addr = 0x02;
      ScheduleNewEvent(me, now + 5420, EVENT_TX_READ, &event, s_evt);
    }
    
    if (me == 3) {
      event.npr = _CACHE(3);
      ScheduleNewEvent(me, now + 5500, EVENT_TX_BEGIN, &event, s_evt);

      event.npr = _CACHE(3);
      event.addr = 0x77;
      ScheduleNewEvent(me, now + 5600, EVENT_TX_READ, &event, s_evt);
      
      event.npr = _CACHE(3);
      event.addr = 0x77;
      ScheduleNewEvent(me, now + 5700, EVENT_TX_WRITE, &event, s_evt);
      
      event.npr = _CACHE(3);
      event.addr = 0x77;
      ScheduleNewEvent(me, now + 5800, EVENT_TX_READ, &event, s_evt);
    }

    ScheduleNewEvent(me, now + 10000, EVENT_END, NULL, 0);
  }
}

void event_populate(int pid, SIM_STATE event_code, simtime_t now, int addr) {

  UTL_callback_t callback;
  UTL_event_t event;
  HTM_pc_buffer_t pc_buffer;

  callback.pid = pid;
  callback.event_code = EVENT_OP_DONE;

  UTL_event_init(&event);

  pc_buffer.addr = addr;
  pc_buffer.callback = callback;
  pc_buffer.tx_abort = callback;
  pc_buffer.acc_lat = 0;
  pc_buffer.thr_pid = pid;

  event.pid = _CACHE(pid);
  event.event_code = event_code;
  event.cnt = &pc_buffer;
  event.cnt_size = sizeof (HTM_pc_buffer_t);

  SCHEDULE_EVENT(event, now + DELTA_TIME);
}

/**
 * Processes a event for a given process.
 * 
 * @param me
 * @param now
 * @param event_type
 * @param event_content
 * @param size
 * @param ptr
 */
void ProcessEvent(unsigned int me, simtime_t now, int event_type,
        void * buffer, unsigned int size, void * state) {

  lp_state_t * lp_state;
  evt_cnt_t * app_buffer;
  HTM_pc_buffer_t pc_buffer;
  HTM_thr_buffer_t thr_buffer;
  UTL_event_t event;
  UTL_callback_t callback;

  switch (event_type) {
    case INIT:
      EVENT_init(me, now);
      break;

    case EVENT_OP_DONE:

      // TODO: handle the chain of events response

      thr_buffer = *((HTM_thr_buffer_t*) buffer);
      printf("[TS%5.0f][me %2u] last_pc_op=%7s acc_lat=%.3f ",
              now, me, PRINT_PC_STATE(thr_buffer.pc_op),
              thr_buffer.acc_lat);

      switch (thr_buffer.pc_op) {
        case PC_READ:
        case PC_WRIT:
        case PC_TXRE:
        case PC_TXWR:
        printf("addr=0x%08x ", (unsigned int) thr_buffer.addr);
      }
      
      if (thr_buffer.pc_op == PC_TXAB) {
        printf("is_abort=%s ", PRINT_ABORT_CAUSE(thr_buffer.code));
      }
      printf("\n");
      break;

    case EVENT_READ:
      app_buffer = (evt_cnt_t*) buffer;
      event_populate(me, PC_READ, now, app_buffer->addr);
      break;
    case EVENT_WRITE:
      app_buffer = (evt_cnt_t*) buffer;
      event_populate(me, PC_WRIT, now, app_buffer->addr);
      break;
    case EVENT_TX_READ:
      app_buffer = (evt_cnt_t*) buffer;
      event_populate(me, PC_TXRE, now, app_buffer->addr);
      break;
    case EVENT_TX_WRITE:
      app_buffer = (evt_cnt_t*) buffer;
      event_populate(me, PC_TXWR, now, app_buffer->addr);
      break;
    case EVENT_TX_BEGIN:
      app_buffer = (evt_cnt_t*) buffer;
      event_populate(me, PC_TXBG, now, app_buffer->addr);
      break;
    case EVENT_TX_COMMIT:
      app_buffer = (evt_cnt_t*) buffer;
      event_populate(me, PC_TXCM, now, app_buffer->addr);
      break;
    case EVENT_TX_ABORT:
      app_buffer = (evt_cnt_t*) buffer;
      event_populate(me, PC_TXAB, now, app_buffer->addr);
      break;

    case EVENT_END:
      lp_state = state;
      lp_state->end = true;
      ScheduleNewEvent(me, now + D_TIME, EVENT_END, NULL, 0);
      break;
    default:
      HTM_state_handler(state, me, event_type, now, buffer);
      break;
  }
}

/**
 * Checks the simulation.
 * 
 * @param me the process id
 * @param state the current state
 * @return true to terminate the me process
 */
bool OnGVT(unsigned int me, lp_state_t *state) {


  if (me < global_info.tot_nb_thrs) {
    if (state->end) {
      printf("[PRC %2u]: ENDED \n", me);

      HTM_OnGVT(true);

      return true;
    }
  } else if (HTM_OnGVT_check_exit()) {
#ifdef __DEBUG_FILE__
    __CLOSE_DEBUG_FILE;
#endif
    printf("[PRC %2u]: ENDED \n", me);
    return true;
  }

  return false;
}

