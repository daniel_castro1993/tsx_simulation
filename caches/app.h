#ifndef __APP_H_GUARD
#define __APP_H_GUARD

#include <stdbool.h>
#include <ROOT-Sim.h>
#include "collections.h"
#include "utils.h"
#include "utils_logs.h"
#include "htm.h"

#define D_TIME                  10
#define BACKOFF_TIME            41
#define TRIES_TO_DEADLOCK       5
#define N_PROCESSORS            4

#define EVENT_READ              1
#define EVENT_WRITE             2
#define EVENT_TX_READ           3
#define EVENT_TX_WRITE          4
#define EVENT_TX_BEGIN          5
#define EVENT_TX_COMMIT         6
#define EVENT_TX_ABORT          7
#define EVENT_END               8
#define EVENT_OP_DONE           9

#define PRINT_EVENT(state) (state == EVENT_READ ? "READ" :                    \
                            state == EVENT_WRITE ? "WRITE" :                  \
                            state == EVENT_TX_READ ? "TX_READ" :              \
                            state == EVENT_TX_WRITE ? "TX_WRITE" :            \
                            state == EVENT_TX_BEGIN ? "TX_BEGIN" :            \
                            state == EVENT_TX_COMMIT ? "TX_COMMIT" :          \
                            state == EVENT_TX_ABORT ? "TX_ABORT" : "")

#define RESOURCES_NUM           0xf

// if 0, 0 or 1, 1 all the locks are locked/unlocked in a ordered manner
// 0, 1 some are ordered ascending and others descenting
// 2, 2 or other options - shuffle
#define LOCKS_ORDER             0, 0
// manages the average number of locks (higher more locks)
#define RESOURCE_AMOUNT         3

// This is the events' payload which is exchanged across LPs

typedef struct _event_content_type {
  int addr, npr, abort_code, last_event;
} evt_cnt_t;

// LP simulation state

typedef struct _lp_state {
  HTM_cache_tx_t * tx[N_PROCESSORS];
  bool end;
} lp_state_t;

void EVENT_init(unsigned int me, simtime_t now);

#endif // define __APP_H_GUARD
