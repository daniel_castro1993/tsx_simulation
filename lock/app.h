#ifndef __APP_H_GUARD
#  define __APP_H_GUARD

#  include <stdbool.h>
#  include "collections.h"
#  include "utils.h"
#  include "htm.h"

typedef enum _EVENTS {
  EVENT_LOCK = 1,
  EVENT_UNLOCK,
  EVENT_CHECK,
  EVENT_ADD,
  EVENT_OP_DONE,
  EVENT_END
} EVENTS;

typedef struct _event_content_type {
  bool is_done;
  simtime_t timestamp;
} evt_cnt_t;

typedef struct _lp_state {
  bool is_end, already_locked, already_checked, already_unlocked, already_added;
  simtime_t begin, end;
} lp_state_t;

#endif // define __APP_H_GUARD
