/**
 * Lock simulation.
 * 
 * Simulates multiple processes grabbing a global lock. Some processes will have
 * to wait for the previous one to unlock.
 */

#include "app.h"

int ended_prcs = 0;

void EVENT_init(unsigned int me, simtime_t now) {

  if (n_prc_tot < 2) {
    printf("Use at least 2 LPs\n");
    exit(EXIT_SUCCESS);
  }

#ifdef __DEBUG_FILE__
  __OPEN_DEBUG_FILE;
#endif

  if (!global_info.is_init) {
    global_info.is_init = true;
    global_info.tot_nb_thrs = n_prc_tot - 1;
    global_info.lock_pid = 0;
  }

  if (me == 0) {
    // lock pid
    printf("[TS%4.0f]: init lock at pid: %2u\n", now, me);
    HTM_lock_state_t * state;
    NEW_LOCK_STATE(state);

    SetState(state);
    ScheduleNewEvent(me, now + DELTA_TIME, LCK_INIT, NULL, 0);

  } else {
    printf("[TS%4.0f]: init thread at pid: %2u\n", now, me);
    lp_state_t * state;
    state = new(lp_state_t, 1);
    set_zero(state, lp_state_t, 1);
    state->end = false;

    // schedules what to lock
    evt_cnt_t * event_content = new(evt_cnt_t, 1);
    set_zero(event_content, evt_cnt_t, 1);

    SetState(state);

    ScheduleNewEvent(me, now + 2 * DELTA_TIME, EVENT_LOCK, NULL, 0);
  }
}

void schedule_lock_event(int pid, unsigned int event_code, simtime_t now) {

  UTL_event_t next_event;
  HTM_lock_buf_t lock_buffer;

  lock_buffer.callback.pid = pid;
  lock_buffer.callback.event_code = EVENT_OP_DONE;

  UTL_event_init(&next_event);

  next_event.cnt = &lock_buffer;
  next_event.cnt_size = sizeof (HTM_lock_buf_t);
  next_event.pid = global_info.lock_pid;
  next_event.event_code = event_code;

  SCHEDULE_EVENT(next_event, now + DELTA_TIME);
}

/**
 * Processes a event for a given process.
 * 
 * @param me
 * @param now
 * @param event_type
 * @param event_content
 * @param size
 * @param ptr
 */
void ProcessEvent(unsigned int me, simtime_t now, int event,
        void * buffer, unsigned int size, void * state) {

  int i, lat;
  lp_state_t * lp_state = state;
  //  HTM_thr_buffer_t thr_buffer;

  switch (event) {
    case INIT:
      EVENT_init(me, now);
      break;
    case EVENT_LOCK:

      lp_state->begin = now;
      schedule_lock_event(me, LCK_LOCK, now);

      break;
    case EVENT_UNLOCK:

      lp_state->begin = now;
      schedule_lock_event(me, LCK_UNLA, now);

      break;
    case EVENT_CHECK:

      lp_state->begin = now;
      schedule_lock_event(me, LCK_CHKL, now);

      break;
    case EVENT_ADD:

      lp_state->begin = now;
      schedule_lock_event(me, LCK_ADDT, now);

      break;
    case EVENT_OP_DONE:

      lp_state = (lp_state_t*) state;

      lat = now - lp_state->begin;

      if (!lp_state->already_locked) {
        lp_state->already_locked = true;
        ScheduleNewEvent(me, now + DELTA_TIME, EVENT_UNLOCK, NULL, 0);
        printf("[TS%4.0f]: LP %3u - lock done   [lat=%3i]!\n", now, me, lat);
      } else {
        ScheduleNewEvent(me, now + DELTA_TIME, EVENT_END, NULL, 0);
        printf("[TS%4.0f]: LP %3u - unlock done [lat=%3i]!\n", now, me, lat);
      }
      break;
    case EVENT_END:
      lp_state = (lp_state_t*) state;
      lp_state->is_end = true;
      ScheduleNewEvent(me, now + DELTA_TIME, EVENT_END, NULL, 0);
      break;
    case LCK_UNLA:
      HTM_state_handler(state, me, event, now, buffer);
      break;
    default:
      HTM_state_handler(state, me, event, now, buffer);
      break;
  }

}

/**
 * Checks the simulation.
 * 
 * @param me the process id
 * @param state the current state
 * @return true to terminate the me process
 */
bool OnGVT(unsigned int me, lp_state_t * state) {
  // the lock never ends

  bool thr_ended = false;

  if (me > global_info.lock_pid) { // one of the threads
    if (state->is_end) {
      thr_ended = true;
      printf("process %2u finished \n", me);

      HTM_OnGVT(thr_ended);

      return true;
    }
  } else if (HTM_OnGVT_check_exit()) {
    // the lock is the last
#ifdef __DEBUG_FILE__
    fclose(__debug_file);
#endif
    return true;
  }

  return false;
}

