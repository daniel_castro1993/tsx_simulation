#ifndef __APP_H_GUARD
#  define __APP_H_GUARD

#  include <stdbool.h>
#  include <sys/stat.h>
#  include "collections.h"
#  include "utils.h"
#  include "stats.h"

#  ifndef DATA_FILE_NAME
#    define DATA_FILE_NAME "sim"
#  endif

#  ifndef DATA_FILE
#    define DATA_FILE       ({                                                \
    char __buffer[512];                                                       \
    sprintf(__buffer, "./data/" DATA_FILE_NAME "-PR_%.2lf-C_%.2lf-L_%i-"      \
    "D_%i-T_%i-B_%i.txt",                                                     \
      global_info.prob_read, global_info.C, global_info.L, global_info.D,     \
      global_info.tot_nb_thrs, global_info.budget);                           \
    mkdir("data", S_IRWXU);                                                   \
    data_file == NULL ? data_file = fopen(__buffer, DATA_FILE_MODE)           \
      : data_file;                                                            \
    data_file;                                                                \
  })
#  endif
#  ifndef DATA_FILE_MODE
#    define DATA_FILE_MODE  "w"
#  endif
#  ifndef START_DATA_FILE
#    define START_DATA_FILE  ({                                               \
    fseek(DATA_FILE, 0, SEEK_END);                                            \
    if(ftell(DATA_FILE) < 8) {                                                \
      fprintf(DATA_FILE,                                                      \
        "%11s,%11s,%11s,%11s,%11s,%11s,%15s,%15s,%15s,%15s,%15s\n",           \
        "PROB_READ", "D", "L", "C", "THREADS", "BUDGET",                      \
        "SIM_P_R(L)", "SIM_R", "X", "SIM_R*", "SIM_P_AB");                    \
    }                                                                         \
  })
#  endif

#  ifndef MARKOV_CHAIN_FILE
#    define MARKOV_CHAIN_FILE ({                                              \
    char __buffer[512];                                                       \
    sprintf(__buffer, "./data/mc-" DATA_FILE_NAME "-PR_%.2lf-C_%.2lf"         \
            "-L_%i-D_%i-T_%i-B_%i.txt", global_info.prob_read, global_info.C, \
            global_info.L, global_info.D, global_info.tot_nb_thrs,            \
            global_info.budget);                                              \
    mkdir("data", S_IRWXU);                                                   \
    markov_data_file == NULL                                                  \
      ? markov_data_file = fopen(__buffer, MARKOV_CHAIN_FILE_MODE)            \
      : markov_data_file;                                                     \
    markov_data_file;                                                         \
  })
#  endif
#  ifndef MARKOV_CHAIN_FILE_MODE
#    define MARKOV_CHAIN_FILE_MODE  "w"
#  endif
#  ifndef START_MARKOV_CHAIN_FILE
#    define START_MARKOV_CHAIN_FILE  ({                                           \
    fseek(MARKOV_CHAIN_FILE, 0, SEEK_END);                                    \
    if(ftell(MARKOV_CHAIN_FILE) < 8) {                                        \
      fprintf(MARKOV_CHAIN_FILE, "%30s, %15s\n", "STATE", "PROB");            \
    }                                                                         \
  })
#  endif

#  define ELAPSED_TIME            0.0000001
#  define SAMPLE_SIZE             200000
#  define MAX_TIME                5*60 // 15min = 15*60
#  define INIT_RESET              1000 // resets after this amount of samples

FILE * data_file;
FILE * markov_data_file;

#endif // define __APP_H_GUARD
