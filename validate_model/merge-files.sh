for f in data/sim-det*.txt
do
  echo $f
	if [ ! -f sim-det-file.txt ] ; then
		sed "1q;d" $f > sim-det-file.txt ;
	fi
	sed "2q;d" $f >> sim-det-file.txt ;
done
for f in data/sim-ndt*.txt 
do
  echo $f
	if [ ! -f sim-ndt-file.txt ] ; then
		sed "1q;d" $f > sim-ndt-file.txt ;
	fi
	sed "2q;d" $f >> sim-ndt-file.txt ;
done
