#include "app.h"
#include "htm.h"

#include <math.h>
#include <time.h>

double def_prob_write = 1;
double def_prob_tran = 1;
double def_C = 1.4;
int def_L = 20;
int def_D = 1000;
double def_budget = 2;
double def_op_cost = 0;
//double def_lat_read = 0;
//double def_lat_write = 0;
double def_lat_begin = DELTA_TIME;
double def_lat_abort = DELTA_TIME;
double def_lat_commit = DELTA_TIME;
double def_lat_lock = DELTA_TIME;
double def_lat_unlock = DELTA_TIME;
double def_lat_spin = DELTA_TIME;

simtime_t init_experiment_time = 0;

bool * all_complete, * all_reset;
HTM_smpl_stats_t * stats;
static time_t begin_time = -1;

void ProcessEvent(unsigned int me, simtime_t now, unsigned int event,
        void * buffer, unsigned int size, void * state) {

  int i, nb_thrs;
  HTM_simple_state_t* smpl_state;
  UTL_event_t smpl_event;

  switch (event) {
    case INIT:

      if (!global_info.is_init) {
        global_info.is_init = true;

        printf("Given number of LPs: %u; number of threads: %u \n",
                n_prc_tot, n_prc_tot);

        nb_thrs = n_prc_tot;

        global_info.prob_read = IsParameterPresent(buffer, "PROB_WRITE") ?
                1 - GetParameterDouble(buffer, "PROB_WRITE")
                : 1 - def_prob_write;

        global_info.prob_tran = IsParameterPresent(buffer, "PROB_TRAN") ?
                GetParameterDouble(buffer, "PROB_TRAN")
                : def_prob_tran;

        global_info.C = IsParameterPresent(buffer, "C") ?
                GetParameterDouble(buffer, "C")
                : def_C;

        global_info.D = IsParameterPresent(buffer, "D") ?
                GetParameterInt(buffer, "D")
                : def_D;

        global_info.L = IsParameterPresent(buffer, "L") ?
                GetParameterInt(buffer, "L")
                : def_L;

        global_info.budget = IsParameterPresent(buffer, "BUDGET") ?
                GetParameterInt(buffer, "BUDGET")
                : def_budget;

        global_info.op_cost = IsParameterPresent(buffer, "OP_COST") ?
                GetParameterDouble(buffer, "OP_COST")
                : def_op_cost;

        global_info.lat_pc_r = IsParameterPresent(buffer, "LAT_R") ?
                GetParameterDouble(buffer, "LAT_R")
                : global_info.C / (double) global_info.L;
        global_info.lat_pc_w = IsParameterPresent(buffer, "LAT_W") ?
                GetParameterDouble(buffer, "LAT_W")
                : global_info.C / (double) global_info.L;

        global_info.lat_pc_txbg = IsParameterPresent(buffer, "LAT_BEGIN") ?
                GetParameterDouble(buffer, "LAT_BEGIN")
                : def_lat_begin;
        global_info.lat_pc_txab = IsParameterPresent(buffer, "LAT_ABORT") ?
                GetParameterDouble(buffer, "LAT_ABORT")
                : def_lat_abort;
        global_info.lat_pc_txcm = IsParameterPresent(buffer, "LAT_COMMIT") ?
                GetParameterDouble(buffer, "LAT_COMMIT")
                : def_lat_commit;

        global_info.lat_lock = IsParameterPresent(buffer, "LAT_LOCK") ?
                GetParameterDouble(buffer, "LAT_LOCK")
                : def_lat_lock;
        global_info.lat_unlock = IsParameterPresent(buffer, "LAT_UNLOCK") ?
                GetParameterDouble(buffer, "LAT_UNLOCK")
                : def_lat_unlock;

        global_info.lat_spin = IsParameterPresent(buffer, "LAT_SPIN") ?
                GetParameterDouble(buffer, "LAT_SPIN")
                : def_lat_spin;

        global_info.nb_garbage_granules =
                IsParameterPresent(buffer, "NB_GARBAGE_ADDR") ?
                GetParameterDouble(buffer, "NB_GARBAGE_ADDR")
                : 0;

        global_info.is_continuous_garbage =
                IsParameterPresent(buffer, "GARBAGE_CNT") ?
                GetParameterBool(buffer, "GARBAGE_CNT")
                : true;

        global_info.is_random_garbage =
                IsParameterPresent(buffer, "GARBAGE_RND") ?
                GetParameterBool(buffer, "GARBAGE_RND")
                : false;

        global_info.start_garbage_granule_addr = 0;
        global_info.end_garbage_granule_addr =
                global_info.nb_garbage_granules
                * global_info.cache_line_size;

        global_info.random_garbage_granule_addr =
                new(int, global_info.nb_garbage_granules);

        for (i = 0; i < global_info.nb_garbage_granules; ++i) {
          global_info.random_garbage_granule_addr[i] =
                  RandomRange(0, global_info.D - 1);
        }

        global_info.cache_line_size = // In number of granules
                IsParameterPresent(buffer, "CACHE_LINE_SIZE") ?
                GetParameterInt(buffer, "CACHE_LINE_SIZE")
                : 1;
        global_info.nb_ways = IsParameterPresent(buffer, "NB_WAYS") ?
                GetParameterInt(buffer, "NB_WAYS")
                : 4;
        global_info.nb_sets = IsParameterPresent(buffer, "NB_SETS") ?
                GetParameterInt(buffer, "NB_SETS")
                : 16;

        global_info.tot_nb_thrs = nb_thrs;

        printf(" =================== HTM Simulation =================== \n");
        printf(" > > parameters                                         \n"
                "          D: %10i                                      \n"
                "          C: %10f                                      \n"
                "          L: %10i                                      \n"
                "    THREADS: %10i                                      \n"
                "     BUDGET: %10i                                      \n"
                " PROB_WRITE: %10f                                      \n"
                "  PROB_TRAN: %10f                                      \n"
                "    OP_COST: %10f                                      \n"
                "      LAT_R: %10f                                      \n"
                "      LAT_W: %10f                                      \n"
                "   LAT_LOCK: %10f                                      \n"
                " LAT_UNLOCK: %10f                                      \n"
                "  LAT_BEGIN: %10f                                      \n"
                "  LAT_ABORT: %10f                                      \n"
                " LAT_COMMIT: %10f                                      \n"
                "   LAT_SPIN: %10f                                      \n",
                global_info.D, global_info.C, global_info.L,
                global_info.tot_nb_thrs, global_info.budget,
                1.0 - global_info.prob_read, global_info.prob_tran,
                global_info.op_cost, global_info.lat_pc_r,
                global_info.lat_pc_w,
                global_info.lat_lock, global_info.lat_unlock,
                global_info.lat_pc_txbg, global_info.lat_pc_txab,
                global_info.lat_pc_txcm, global_info.lat_spin);
        printf(" ====================================================== \n");

        if (global_info.thrs_pids == NULL) {
          global_info.thrs_pids = new(int, nb_thrs);
          for (i = 0; i < nb_thrs; i++) {
            global_info.thrs_pids[i] = i;
          }
        }
      }

      smpl_state = new_init(HTM_simple_state_t, 1);

      SetState(smpl_state);

      UTL_event_init(&smpl_event);

      // inits the state
      smpl_event.pid = me;
      smpl_event.event_code = SMPL_INIT;
      SCHEDULE_EVENT(smpl_event, NEXT_TICK(now));

      // starts the simulation
      smpl_event.pid = me;
      smpl_event.event_code = SMPL_INTL;
      SCHEDULE_EVENT(smpl_event, NEXT_TICK(NEXT_TICK(now)));

      break;
    default:
      HTM_state_handler(state, me, event, now, buffer);
      break;
  }
}

bool OnGVT(unsigned int me, HTM_simple_state_t * state) {

  static bool reset = false;
  time_t now = time(NULL);
  double delta_secs;

  if (begin_time == -1) {
    begin_time = time(NULL);
  }

  delta_secs = now - begin_time;

  if (global_info.is_exit) {
    return true;
  }

  int events_count = state->stats.counter[TXCM] + state->stats.counter[TXAB]
          + state->stats.counter[SERL] + state->stats.counter[NOTX];

  if (all_reset == NULL) {
    all_reset = new_init(bool, n_prc_tot);
  }

  int i, j, k, p, p_txab, p_txcm, p_notx, p_serl, tot_events;
  bool all_samples_taken = true;

  if ((events_count > INIT_RESET && !all_reset[me]) || reset) {
    all_reset[me] = true;
    reset = true;
    events_count = 0;

    mod_accReset();
    BEGIN_FOR_STAT(k);
    state->stats.counter[k] = 0;
    state->stats.counter_time[k] = 0;
    state->stats.stats_pointer[k] = 0;
    END_FOR_STAT;

    init_experiment_time = state->now;

    for (i = 0; i < NB_SMPL_ABORTS; ++i) {
      state->stats.abort_type_count[i] = 0;
    }

    for (i = 0; i < n_prc_tot; ++i) {
      all_samples_taken = all_reset[i] && all_samples_taken;
    }
    if (all_samples_taken) {
      reset = false;
    }
  }

  if (events_count > SAMPLE_SIZE
#ifndef __DISABLE_TIMEOUT__
          || delta_secs > MAX_TIME
#endif
          ) {
    if (all_complete == NULL) {
      all_complete = new_init(bool, n_prc_tot);
    }
    if (stats == NULL) {
      stats = new(HTM_smpl_stats_t, n_prc_tot);
    }
    all_complete[me] = true;
    stats[me] = state->stats;

    for (i = 0; i < n_prc_tot; ++i) {
      all_samples_taken = all_complete[i] && all_samples_taken;
    }

    if (all_samples_taken) {
      // process the information

      mod_probFromAcc();

      int p_events[NB_STATS][global_info.tot_nb_thrs];
      double avg_p_events[global_info.tot_nb_thrs];
      double sd_p_events[global_info.tot_nb_thrs];

      double avg_times[NB_STATS][global_info.tot_nb_thrs];
      double avg_ops_before_abort[global_info.tot_nb_thrs];
      double avg_budget_at_commit[global_info.tot_nb_thrs];
      double tot_avg_times[NB_STATS];

      double sq_sd_times[NB_STATS][global_info.tot_nb_thrs];
      double sq_sd_ops_before_abort[global_info.tot_nb_thrs];
      double sq_sd_budget_at_commit[global_info.tot_nb_thrs];
      double tot_sq_sd_times[NB_STATS];
      double count_times[NB_STATS];
      double count_stats[NB_STATS];
      double count_stats_per_thr[NB_STATS][global_info.tot_nb_thrs];

      double avg_count_stats[NB_STATS];
      double sd_count_stats[NB_STATS];

      double tot_events_count, events_no_serl_count = 0, tot_time;
      double sd_events_count;

      double tot_avg_budget_at_commit;
      double tot_avg_ops_before_abort;

      double tot_sq_sd_budget_at_commit;
      double tot_sq_sd_ops_before_abort;

      int tot_cnfl[global_info.tot_nb_thrs];
      int tot_lock_cnfl[global_info.tot_nb_thrs];
      int tot_tran_cnfl[global_info.tot_nb_thrs];
      int tot_notx_cnfl[global_info.tot_nb_thrs];
      int tot_capa_cnfl[global_info.tot_nb_thrs];
      double all_cnfl = 0, all_lock_cnfl = 0, all_tran_cnfl = 0,
              all_notx_cnfl = 0, all_capa_cnfl = 0;
      double avg_cnfl, avg_lock_cnfl, avg_tran_cnfl, avg_notx_cnfl,
              avg_capa_cnfl;
      double sd_cnfl, sd_lock_cnfl, sd_tran_cnfl, sd_notx_cnfl, sd_capa_cnfl;

      double time_cnfl[global_info.tot_nb_thrs];
      double time_lock_cnfl[global_info.tot_nb_thrs];
      double time_tran_cnfl[global_info.tot_nb_thrs];
      double time_notx_cnfl[global_info.tot_nb_thrs];
      double time_capa_cnfl[global_info.tot_nb_thrs];
      double avg_time_cnfl, avg_time_lock_cnfl, avg_time_tran_cnfl,
              avg_time_notx_cnfl, avg_time_capa_cnfl;
      double sd_time_cnfl, sd_time_lock_cnfl, sd_time_tran_cnfl,
              sd_time_notx_cnfl, sd_time_capa_cnfl;

      double sim_R, sim_R_star, sim_P_R, ana_R, ana_P_R, throughput;
      double sim_pa, sim_pa_locks, sim_pa_tran, sim_pa_notx, sim_pa_capa,
              sim_serl;

      double count_cnfl = 0, count_lock = 0, count_notx = 0, count_capa = 0;

      set_zero(count_stats, double, NB_STATS);
      set_zero(count_times, double, NB_STATS);
      set_zero(count_stats_per_thr, double, NB_STATS * global_info.tot_nb_thrs);
      set_zero(tot_cnfl, int, global_info.tot_nb_thrs);
      set_zero(tot_lock_cnfl, int, global_info.tot_nb_thrs);
      set_zero(tot_tran_cnfl, int, global_info.tot_nb_thrs);
      set_zero(tot_notx_cnfl, int, global_info.tot_nb_thrs);
      set_zero(tot_capa_cnfl, int, global_info.tot_nb_thrs);

      set_zero(time_cnfl, double, global_info.tot_nb_thrs);
      set_zero(time_lock_cnfl, double, global_info.tot_nb_thrs);
      set_zero(time_tran_cnfl, double, global_info.tot_nb_thrs);
      set_zero(time_notx_cnfl, double, global_info.tot_nb_thrs);
      set_zero(time_capa_cnfl, double, global_info.tot_nb_thrs);

      for (i = 0; i < global_info.tot_nb_thrs; ++i) {

        BEGIN_FOR_STAT(k);
        p_events[k][i] = stats[i].counter[k] > stats[i].stats_pointer[k] ?
                SMPL_NB_SAMPLES : stats[i].stats_pointer[k];
        END_FOR_STAT;

        count_lock += stats[i].abort_type_count[SMPL_LOCK_CNFL];
        count_cnfl += stats[i].abort_type_count[SMPL_TRAN_CNFL];
        count_notx += stats[i].abort_type_count[SMPL_NOTX_CNFL];
        count_capa += stats[i].abort_type_count[SMPL_CAPA_CNFL];

        p_txab = p_events[TXAB][i];
        avg_ops_before_abort[i] = CALC_AVG(stats[i].ops_before_abort, p_txab);
        sq_sd_ops_before_abort[i] = CALC_SQ_STDDEV(
                stats[i].ops_before_abort, p_txab, avg_ops_before_abort[i]);

        for (j = 0; j < p_txab; ++j) {
          tot_cnfl[i]++;
          all_cnfl++;
          time_cnfl[i] += stats[i].stat_time[TXAB][j];
          if (stats[i].abort_type[j] == SMPL_TRAN_CNFL) {
            all_tran_cnfl++;
            tot_tran_cnfl[i]++;
            time_tran_cnfl[i] += stats[i].stat_time[TXAB][j];
          } else if (stats[i].abort_type[j] == SMPL_LOCK_CNFL) {
            all_lock_cnfl++;
            tot_lock_cnfl[i]++;
            time_lock_cnfl[i] += stats[i].stat_time[TXAB][j];
          } else if (stats[i].abort_type[j] == SMPL_NOTX_CNFL) {
            all_notx_cnfl++;
            tot_notx_cnfl[i]++;
            time_notx_cnfl[i] += stats[i].stat_time[TXAB][j];
          } else if (stats[i].abort_type[j] == SMPL_CAPA_CNFL) {
            all_capa_cnfl++;
            tot_capa_cnfl[i]++;
            time_capa_cnfl[i] += stats[i].stat_time[TXAB][j];
          }
        }

        p_txcm = p_events[TXCM][i];
        avg_budget_at_commit[i] = CALC_AVG(stats[i].budget_at_commit, p_txcm);
        sq_sd_budget_at_commit[i] = CALC_SQ_STDDEV(
                stats[i].budget_at_commit, p_txcm, avg_budget_at_commit[i]);

        BEGIN_FOR_STAT(k);
        p = p_events[k][i];

        avg_times[k][i] = CALC_AVG(stats[i].stat_time[k], p);
        sq_sd_times[k][i] = CALC_SQ_STDDEV(stats[i].stat_time[k], p,
                avg_times[k][i]);

        count_times[k] += stats[i].counter_time[k];
        count_stats[k] += stats[i].counter[k];
        count_stats_per_thr[k][i] += stats[i].counter[k];
        END_FOR_STAT;
      }

      count_lock /= global_info.tot_nb_thrs;
      count_cnfl /= global_info.tot_nb_thrs;
      count_notx /= global_info.tot_nb_thrs;
      count_capa /= global_info.tot_nb_thrs;

      avg_cnfl = CALC_AVG(tot_cnfl, global_info.tot_nb_thrs);
      avg_lock_cnfl = CALC_AVG(tot_lock_cnfl, global_info.tot_nb_thrs);
      avg_tran_cnfl = CALC_AVG(tot_tran_cnfl, global_info.tot_nb_thrs);
      avg_notx_cnfl = CALC_AVG(tot_notx_cnfl, global_info.tot_nb_thrs);
      avg_capa_cnfl = CALC_AVG(tot_capa_cnfl, global_info.tot_nb_thrs);

      sd_cnfl = CALC_SQ_STDDEV(tot_cnfl,
              global_info.tot_nb_thrs, avg_cnfl);
      sd_lock_cnfl = CALC_SQ_STDDEV(tot_lock_cnfl,
              global_info.tot_nb_thrs, avg_lock_cnfl);
      sd_tran_cnfl = CALC_SQ_STDDEV(tot_tran_cnfl,
              global_info.tot_nb_thrs, avg_tran_cnfl);
      sd_notx_cnfl = CALC_SQ_STDDEV(tot_notx_cnfl,
              global_info.tot_nb_thrs, avg_notx_cnfl);
      sd_capa_cnfl = CALC_SQ_STDDEV(tot_capa_cnfl,
              global_info.tot_nb_thrs, avg_capa_cnfl);

      avg_time_cnfl = CALC_AVG(time_cnfl, global_info.tot_nb_thrs);
      avg_time_lock_cnfl = CALC_AVG(time_lock_cnfl, global_info.tot_nb_thrs);
      avg_time_tran_cnfl = CALC_AVG(time_tran_cnfl, global_info.tot_nb_thrs);
      avg_time_notx_cnfl = CALC_AVG(time_notx_cnfl, global_info.tot_nb_thrs);
      avg_time_capa_cnfl = CALC_AVG(time_capa_cnfl, global_info.tot_nb_thrs);

      sd_time_cnfl = CALC_SQ_STDDEV(time_cnfl,
              global_info.tot_nb_thrs, avg_time_cnfl);
      sd_time_lock_cnfl = CALC_SQ_STDDEV(time_lock_cnfl,
              global_info.tot_nb_thrs, avg_time_lock_cnfl);
      sd_time_tran_cnfl = CALC_SQ_STDDEV(time_tran_cnfl,
              global_info.tot_nb_thrs, avg_time_tran_cnfl);
      sd_time_notx_cnfl = CALC_SQ_STDDEV(time_notx_cnfl,
              global_info.tot_nb_thrs, avg_time_notx_cnfl);
      sd_time_capa_cnfl = CALC_SQ_STDDEV(time_capa_cnfl,
              global_info.tot_nb_thrs, avg_time_capa_cnfl);

      BEGIN_FOR_STAT(k);
      tot_time += count_times[k];
      tot_events += count_stats[k];

      avg_count_stats[k] = CALC_AVG(count_stats_per_thr[k],
              global_info.tot_nb_thrs);
      sd_count_stats[k] = CALC_SQ_STDDEV(count_stats_per_thr[k],
              global_info.tot_nb_thrs, avg_count_stats[k]);

      avg_p_events[k] = CALC_AVG(p_events[k], global_info.tot_nb_thrs);
      sd_p_events[k] = CALC_SQ_STDDEV(p_events[k],
              global_info.tot_nb_thrs, avg_p_events[k]);
      END_FOR_STAT;

      printf("============================================================ \n");

      BEGIN_FOR_STAT(k);
      tot_avg_times[k] = CALC_AVG(avg_times[k], global_info.tot_nb_thrs);
      tot_sq_sd_times[k] = CALC_AVG(sq_sd_times[k], global_info.tot_nb_thrs);

      printf(" %11s (TIME): %9.3e ? %9.3e\n", STAT_STR(k), tot_avg_times[k],
              sqrt(tot_sq_sd_times[k]));
      END_FOR_STAT;

      tot_events_count = count_stats[NOTX] + count_stats[TXAB]
              + count_stats[TXCM] + count_stats[SERL];
      events_no_serl_count = count_stats[NOTX] + count_stats[TXAB]
              + count_stats[TXCM];
      sd_events_count = sd_count_stats[NOTX] + sd_count_stats[TXAB]
              + sd_count_stats[TXCM] + sd_count_stats[SERL];

      sim_R = (count_times[TXCM] + count_times[TXAB]
              + count_times[SERL] + count_times[NOTX]) /
              (count_stats[TXCM] + count_stats[TXAB]
              + count_stats[SERL] + count_stats[NOTX]);
      ana_R = mod_compute_R(global_info.tot_nb_thrs);

      sim_P_R = (count_stats[TXCM] + count_stats[SERL]) / tot_events_count;
      ana_P_R = mod_compute_P_R_L(global_info.tot_nb_thrs, NULL);

      printf("\n     R (SIMULATION): %9.3e \n", sim_R);
      printf("     R (ANALYTICAL): %9.3e   \n", ana_R);

      printf("P_R(L) (SIMULATION): %8.4f%% \n", sim_P_R * 100);
      printf("P_R(L) (ANALYTICAL): %8.4f%% \n", ana_P_R * 100);

      sim_R_star = ((count_times[SRSP] + count_times[TXSP]
              + count_times[TXCM] + count_times[TXAB]
              + count_times[SERL] + count_times[NOTX]) /
              (count_stats[TXCM] + count_stats[SERL] + count_stats[NOTX]));

      throughput = (count_stats[TXCM] + count_stats[SERL] + count_stats[NOTX])
              / (state->now - init_experiment_time);

      printf("         THROUGHPUT: %9.3e code blocks per time unit\n",
              throughput);
      printf("                 R*: %9.3e\n", sim_R_star);

      sim_pa = count_stats[TXAB] / events_no_serl_count;
      printf("\n         ABORT_PROB: %8.4f%% \n", sim_pa * 100.0);

      sim_serl = count_stats[SERL] / tot_events_count;
      printf("          SERL_PROB: %8.4f%% \n\n", sim_serl * 100.0);

      tot_avg_budget_at_commit = CALC_AVG(avg_budget_at_commit,
              global_info.tot_nb_thrs);
      tot_sq_sd_budget_at_commit = CALC_AVG(sq_sd_budget_at_commit,
              global_info.tot_nb_thrs);

      tot_avg_ops_before_abort = CALC_AVG(avg_ops_before_abort,
              global_info.tot_nb_thrs);
      tot_sq_sd_ops_before_abort = CALC_AVG(sq_sd_ops_before_abort,
              global_info.tot_nb_thrs);

      printf("   BUDGET_AT_COMMIT: %9.4f ? %9.5f \n\n",
              tot_avg_budget_at_commit,
              sqrt(tot_sq_sd_budget_at_commit));

      printf("   OPS_BEFORE_ABORT: %9.4f ? %9.5f \n\n",
              tot_avg_ops_before_abort,
              sqrt(tot_sq_sd_ops_before_abort));

      sim_pa_locks = all_lock_cnfl / all_cnfl;
      printf(" ABORTS_DUE_TO_LOCK: %8.4f%% \n", sim_pa_locks * 100.0);

      printf("   LOCK_CNFL (TIME): %9.3e ? %9.3e \n\n",
              avg_time_lock_cnfl / avg_lock_cnfl,
              sqrt(sd_time_lock_cnfl / sd_lock_cnfl));

      // CONFL
      sim_pa_tran = all_tran_cnfl / all_cnfl;
      printf(" ABORTS_TX_CONFLICT: %8.4f%% \n", sim_pa_tran * 100.0);

      printf("   TRAN_CNFL (TIME): %9.3e ? %9.3e \n\n",
              avg_time_tran_cnfl / avg_tran_cnfl,
              sqrt(sd_time_tran_cnfl / avg_tran_cnfl));

      // NON-TRANSAC
      sim_pa_notx = all_notx_cnfl / all_cnfl;
      printf(" ABORTS_NOTX_CNFLCT: %8.4f%% \n", sim_pa_notx * 100.0);

      printf("   NOTX_CNFL (TIME): %9.3e ? %9.3e \n\n",
              avg_time_notx_cnfl / avg_notx_cnfl,
              sqrt(sd_time_notx_cnfl / sd_notx_cnfl));

#ifdef __CAPACITIES__
      // CAPACITIES
      sim_pa_capa = all_capa_cnfl / all_cnfl;
      printf(" ABORTS_CAPA_EXCEPT: %8.4f%% \n", sim_pa_capa * 100.0);

      printf("   CAPA_CNFL (TIME): %9.3e ? %9.3e \n",
              avg_time_capa_cnfl / avg_capa_cnfl,
              sqrt(sd_time_capa_cnfl / sd_capa_cnfl));
#endif

      printf("============================================================ \n");

#ifndef __DISABLE_MRKV__
      printf("\nstate distribution (x100%%)\n");
      printf(" > top 12 states:\n");
      mod_printTopProbs(stdout, 12);
      printf(" > bottom 12 states (x100%%):\n");
      mod_printBotProbs(stdout, 12);
      printf(" > unreachable:\n");
      int maxDivBudg = max(global_info.budget / 2, 1),
              lineAmount = 10 / maxDivBudg;
      mod_printUnreachable(stdout, lineAmount);
#endif

      START_DATA_FILE;
      fprintf(DATA_FILE, "%11.2f,%11u,%11u,%11.2f,%11u,%11u,",
              global_info.prob_read, global_info.D, global_info.L,
              global_info.C, global_info.tot_nb_thrs, global_info.budget);
      fprintf(DATA_FILE, "%15.8e,%15.8e,", sim_P_R, sim_R);
      fprintf(DATA_FILE, "%15.8e,%15.8e,%15.8e",
              throughput, sim_R_star, sim_pa);
#ifdef __CAPACITIES__
      fprintf(DATA_FILE, ",%15.8e\n", count_capa
              / (count_stats[TXCM] + count_stats[TXAB]));
#else
      fprintf(DATA_FILE, "\n");
#endif
      fclose(DATA_FILE);

      global_info.is_exit = true;
    }
  }

  return false;
}

