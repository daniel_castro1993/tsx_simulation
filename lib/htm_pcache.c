#include <stdio.h>
#include <stdlib.h>
#include "htm.h"
#include "htm_utils.h"

#define MODE_TX         0x01
#define MODE_NON_TX     0x02
#define MODE_ALL        0x03

void tx_abort(bool, HTM_pc_state_t*, HTM_pc_buffer_t*,
        HTM_cache_tx_t*, ABORT_CODE);
void tx_commit(simtime_t, HTM_cache_tx_t*);
void cache_line_invalidate(HTM_pc_line_t*, bool);
int count_words(int, MESI_STATE*, COL_llist_t*);

void HTM_pc_state_handler(HTM_pc_state_t * state, int pid, SIM_STATE event,
        simtime_t now, HTM_pc_buffer_t * buffer) {

  if (pid < 0 || pid >= n_prc_tot) {
    printf("ERROR[HTM_pc_state_handler]: invalid pid: %i \n", pid);
  }

  state->now = now;
  state->pid = pid;

  switch (event) {
    case PC_INIT:
      HTM_pc_state_init(state);
      break;
    case PC_SCRP:
      HTM_pc_state_scache_response(state, buffer);
      break;
    case PC_READ:
      HTM_pc_state_read(state, buffer);
      break;
    case PC_WRIT:
      HTM_pc_state_write(state, buffer);
      break;
    case PC_TXBG:
      HTM_pc_state_tx_begin(state, buffer);
      break;
    case PC_TXAB:
      HTM_pc_state_tx_abort(state, buffer);
      break;
    case PC_TXCM:
      HTM_pc_state_tx_commit(state, buffer);
      break;
    case PC_TXRE:
      HTM_pc_state_tx_read(state, buffer);
      break;
    case PC_TXWR:
      HTM_pc_state_tx_write(state, buffer);
      break;
    case PC_INVA:
      HTM_pc_state_invalidate(state, buffer);
      break;
    case PC_INVW:
      HTM_pc_state_invalidate_modified(state, buffer);
      break;
    case PC_GETS:
      HTM_pc_state_get_word_state(state, buffer);
      break;
    case PC_SETS:
      HTM_pc_state_set_word_state(state, buffer);
      break;
    default:
      break;
  }
}

void call_thr(simtime_t next_tick, simtime_t acc_lat, int addr,
        SIM_STATE pc_event, bool aborted, ABORT_CODE abort_code,
        UTL_callback_t * callback) {

  HTM_thr_buffer_t thr_buffer;

  thr_buffer.acc_lat = acc_lat;
  thr_buffer.addr = addr;
  thr_buffer.pc_op = pc_event;

  thr_buffer.aborted = aborted;
  thr_buffer.code = abort_code;

  thr_buffer.is_broadcast = false;

  HTM_do_thr_event(next_tick, callback->pid,
          callback->event_code, &thr_buffer);
}

void cpu_invw(simtime_t next_tick, int pid, int addr) {
  HTM_cpu_buffer_t cpu_buffer;
  cpu_buffer.pc_pid = pid;
  cpu_buffer.addr = addr;
  cpu_buffer.do_callback = false;
  HTM_do_cpu_event(next_tick, CPU_INVW, &cpu_buffer);
}

void cpu_inva(simtime_t next_tick, int pid, int addr) {
  HTM_cpu_buffer_t cpu_buffer;
  cpu_buffer.pc_pid = pid;
  cpu_buffer.addr = addr;
  cpu_buffer.do_callback = false;
  HTM_do_cpu_event(next_tick, CPU_INVA, &cpu_buffer);
}

void sc_read(simtime_t next_tick, int pid, int addr, int inst_id,
        SIM_STATE pc_event, UTL_callback_t * callback, int thr_pid,
        simtime_t acc_lat, bool no_callback) {

  HTM_do_sc_event(next_tick, pid, addr, inst_id, SC_READ, pc_event,
          callback, thr_pid, acc_lat, no_callback);
}

void sc_write(simtime_t next_tick, int pid, int addr, int inst_id,
        SIM_STATE pc_event, UTL_callback_t * callback, int thr_pid,
        simtime_t acc_lat, bool no_callback) {

  HTM_do_sc_event(next_tick, pid, addr, inst_id, SC_WRIT, pc_event,
          callback, thr_pid, acc_lat, no_callback);
}

void thr_response(simtime_t next_tick, HTM_pc_buffer_t * buffer,
        simtime_t acc_lat, SIM_STATE pc_op, HTM_pc_state_t * state) {

  HTM_thr_buffer_t thr_buffer;

  thr_buffer.addr = buffer->addr;
  thr_buffer.inst_id = buffer->inst_id;
  thr_buffer.acc_lat = acc_lat;
  thr_buffer.pc_op = pc_op;
  thr_buffer.tx_id = state->tx_id;
  thr_buffer.aborted = state->aborted;
  thr_buffer.code = state->last_abort_code;

  HTM_do_thr_event(next_tick, buffer->callback.pid,
          buffer->callback.event_code, &thr_buffer);
}

void cpu_handle_mesi(simtime_t next_tick, int pid, int addr) {

  HTM_cpu_buffer_t cpu_buffer;

  cpu_buffer.addr = addr;
  cpu_buffer.pid_except = pid;

  HTM_do_cpu_event(next_tick, CPU_MESI, &cpu_buffer);
}

void do_sc_event(simtime_t next_tick, int pid, SIM_STATE sc_op,
        simtime_t acc_lat, SIM_STATE pc_op, HTM_pc_buffer_t * buffer,
        bool no_callback) {

  HTM_do_sc_event(next_tick, pid, buffer->addr, buffer->inst_id, sc_op, pc_op,
          &(buffer->callback), buffer->thr_pid, acc_lat, no_callback);
}

void commit_sc_event(simtime_t next_tick, SIM_STATE sc_op,
        simtime_t acc_lat, int addr) {

  UTL_callback_t callback;

  HTM_do_sc_event(next_tick, -1, addr, -1, sc_op, PC_TXCM,
          &callback, -1, acc_lat, true);
}

void HTM_pc_state_init(HTM_pc_state_t * state) {

  int i, j, pid = state->pid;
  simtime_t now = state->now;
  UTL_event_t event;

  state->r_lat = global_info.lat_pc_r;
  state->w_lat = global_info.lat_pc_w;
  state->tx_begin_lat = global_info.lat_pc_txbg;
  state->tx_abort_lat = global_info.lat_pc_txab;
  state->tx_commit_lat = global_info.lat_pc_txcm;
  state->nb_pcs = global_info.tot_nb_pcs;

  state->tx_counter = 0;

  state->aborted = false;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_PC_STATE(PC_INIT));
  __DEBUG_FILE_FLUSH;
#endif

  state->lines = global_info.pcs_lines[pid % global_info.tot_nb_pcs];

  UTL_event_init(&event);

  event.pid = pid;
  event.event_code = CHK_EXIT;

  SCHEDULE_EVENT(event, now + CHECK_EXIT_DELTA_TIME);
}

void HTM_pc_state_scache_response(HTM_pc_state_t * state,
        HTM_pc_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = now + DELTA_TIME;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x tx_id=%i "
          "thr_event=%s thr_pid=%i\n", now, pid, PRINT_PC_STATE(PC_SCRP),
          buffer->addr, state->tx_id,
          PRINT_THR_STATE(buffer->callback.event_code),
          buffer->callback.pid);
  __DEBUG_FILE_FLUSH;
#endif

  switch (buffer->pc_event) {
    case PC_READ:
    case PC_WRIT:
    case PC_TXRE:
    case PC_TXWR:
    default:

      thr_response(next_tick, buffer, buffer->acc_lat,
              buffer->pc_event, state);

      break;
  }
}

void HTM_pc_state_read(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer) {

  int pid = state->pid;
  int addr, nb_lines, nb_words;
  simtime_t now = state->now, r_lat = state->r_lat;
  HTM_pc_line_t * line;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x \n",
          now, pid, PRINT_PC_STATE(PC_READ), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_PCACHE;
  nb_words = N_ASSOC_PCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  if (word->address == addr && word->state != MESI_STATE_I) {
    // found the word to read

    thr_response(now + r_lat, buffer, buffer->acc_lat + r_lat, PC_READ, state);

  } else {

    // TODO: check if it aborts some other transaction (CAPACITY)

    SIM_STATE sc_code = word->state == MESI_STATE_M ? SC_WRIT : SC_READ;

    do_sc_event(now + r_lat, pid, sc_code, buffer->acc_lat + r_lat,
            PC_READ, buffer, false);
  }

  word->timestamp = now;
  word->address = addr;
  word->state = MESI_STATE_E; // CPU_MESI event will check this

  cpu_handle_mesi(now + DELTA_TIME, pid, buffer->addr);
}

void HTM_pc_state_write(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer) {

  int pid = state->pid;
  int addr, nb_lines, nb_words;
  simtime_t now = state->now, w_lat = state->w_lat;
  HTM_pc_line_t * line;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x \n",
          now, pid, PRINT_PC_STATE(PC_WRIT), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_PCACHE;
  nb_words = N_ASSOC_PCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  if (word->address == addr
          && !(word->state & (MESI_STATE_M | MESI_STATE_I))) {
    // found the word to write

    thr_response(now + w_lat, buffer, buffer->acc_lat + w_lat, PC_WRIT, state);

  } else {
    // or didn't find or is in MODIFIED state
    do_sc_event(now + w_lat, pid, SC_WRIT, buffer->acc_lat + w_lat,
            PC_WRIT, buffer, false);
  }

  word->timestamp = now;
  word->address = addr;
  word->state = MESI_STATE_M; // CPU_MESI event will check this

  cpu_handle_mesi(now + DELTA_TIME, pid, buffer->addr);
}

void HTM_pc_state_tx_begin(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer) {

  simtime_t now = state->now, tx_begin_lat = state->tx_begin_lat;
  int pid = state->pid;

  state->do_tx_abort = buffer->do_tx_abort;
  state->tx_callback = buffer->tx_abort;

  state->tx_id = ++state->tx_counter;
  state->thr_pid = buffer->thr_pid;

  state->tx_id <<= (global_info.tot_nb_pcs + 1) >> 1;
  state->tx_id |= pid % global_info.tot_nb_pcs;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "tx_id=%i\n",
          now, pid, PRINT_PC_STATE(PC_TXBG), state->tx_id);
  __DEBUG_FILE_FLUSH;
#endif

  state->aborted = false;

  if (state->tx != NULL) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
    if (state->tx->is_running) {
      fprintf(__debug_file, __LOG_TS_PID_EVENT
              "WARN began tx on core with an already running tx \n",
              now, pid, PRINT_PC_STATE(PC_TXBG));
      __DEBUG_FILE_FLUSH;
    }
#endif
    // tx_abort?
    COL_LLIST_destroy(state->tx->lines, NULL);
    free(state->tx);
    //    thr_response(now + tx_begin_lat, buffer, tx_begin_lat, PC_TXBG, state);
    //    return;
  }

  CREATE_TX(state->tx, pid, state->tx_id);

  thr_response(now + tx_begin_lat, buffer, tx_begin_lat, PC_TXBG, state);
}

void HTM_pc_state_tx_abort(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer) {

  simtime_t now = state->now, tx_abort_lat = state->tx_abort_lat;
  int pid = state->pid;
  HTM_cache_tx_t * tx = state->tx;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_PC_STATE(PC_TXAB));
  __DEBUG_FILE_FLUSH;
#endif

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  if (state->tx == NULL) {
    fprintf(__debug_file, __LOG_TS_PID_EVENT
            "ERROR tx already aborted \n",
            now, pid, PRINT_PC_STATE(PC_TXAB));
  } else if (!state->tx->is_running) {
    fprintf(__debug_file, __LOG_TS_PID_EVENT
            "ERROR tx not running [%s] \n",
            now, pid, PRINT_PC_STATE(PC_TXAB),
            state->tx->abort_code == ABORT_CODE_COMMIT ? "COMMIT" : "ABORT");
  }
  __DEBUG_FILE_FLUSH;
#endif
  if (state->tx != NULL && !state->tx->is_running) {
    tx_abort(false, state, buffer, tx, ABORT_CODE_EXPL);
  }

  state->aborted = true;
  state->last_abort_code = ABORT_CODE_EXPL;

  if (buffer->do_callback) {
    thr_response(now + tx_abort_lat, buffer, tx_abort_lat, PC_TXAB, state);
  }
}

void HTM_pc_state_tx_commit(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer) {

  simtime_t now = state->now, tx_commit_lat = state->tx_commit_lat;
  int pid = state->pid;
  HTM_cache_tx_t * tx = state->tx;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_PC_STATE(PC_TXCM));
  __DEBUG_FILE_FLUSH;
#endif

  if (state->tx == NULL) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
    fprintf(__debug_file, __LOG_TS_PID_EVENT
            "ERROR tx already aborted \n",
            now, pid, PRINT_PC_STATE(PC_TXCM));
    __DEBUG_FILE_FLUSH;
#endif

    state->aborted = true;
    return;
  } else {
    tx->abort_code = ABORT_CODE_COMMIT;
    tx->is_running = false;
    state->tx = NULL;
    state->aborted = false;
    state->last_abort_code = ABORT_CODE_COMMIT;
  }

  tx_commit(now, tx);
  COL_LLIST_destroy(tx->lines, NULL);
  free(state->tx);

  thr_response(now + tx_commit_lat, buffer, tx_commit_lat, PC_TXCM, state);
}

void HTM_pc_state_tx_read(HTM_pc_state_t * state,
        HTM_pc_buffer_t * buffer) {

  simtime_t now = state->now, r_lat = state->r_lat;
  int pid = state->pid, addr = SANITIZE_ADDRESS(buffer->addr);
  int nb_lines = N_LINES_PCACHE, nb_words = N_ASSOC_PCACHE;
  HTM_cache_tx_t * tx;
  HTM_pc_line_t * line, * lines = state->lines;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x ",
          now, pid, PRINT_PC_STATE(PC_TXRE), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  tx = state->tx;

  if (tx == NULL || !tx->is_running) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
    fprintf(__debug_file, " ERROR tx not running [%s] \n",
            state->tx->abort_code == ABORT_CODE_COMMIT ? "COMMIT" : "ABORT");
    __DEBUG_FILE_FLUSH;
#endif

    state->aborted = true;
    thr_response(now + DELTA_TIME, buffer, DELTA_TIME, PC_TXRE, state);
    return;
  }

  if (tx != NULL && tx->is_running) {

    FIND_CACHE_LINE(line, addr, lines, nb_lines);

    if (CACHE_LINE_IS_TRANSACTIONAL(line) && line->tx->tx_id != tx->tx_id) {
      // not the same transaction (should not happen)

      //      if (!state->do_tx_abort) {
      //        state->tx_callback = buffer->callback;
      //      }
      tx_abort(false, state, buffer, line->tx, ABORT_CODE_TXCF);
    }

    CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

    // invalidate txs with this word in MODIFIED state

    if (word->address == addr && word->state != MESI_STATE_I) {
      // hit

      if (!CACHE_LINE_IS_TRANSACTIONAL(line)) {
        MESI_STATE s = word->state;
        cache_line_invalidate(line, true);
        word->state = s; // TODO: should the line be emptied???
        word->timestamp = now;
        word->address = addr;
        CACHE_LINE_MAKE_TRANSACTIONAL(tx, line);
      }

      cpu_invw(now + DELTA_TIME, pid, addr);

    } else {
      // miss

      if (CACHE_LINE_IS_TRANSACTIONAL(line)) {
        if (word->state != MESI_STATE_I) {
          // line is full
          state->aborted = true;
          state->last_abort_code = ABORT_CODE_CAPA;

          if (!state->do_tx_abort) {
            state->tx_callback = buffer->callback;
          }

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
          fprintf(__debug_file, " ABORTED \n");
          __DEBUG_FILE_FLUSH;
#endif

          tx_abort(false, state, buffer, tx, ABORT_CODE_CAPA);
          thr_response(now + DELTA_TIME, buffer, DELTA_TIME, PC_TXRE, state);
          return;
        }
      } else {
        cache_line_invalidate(line, true);
        CACHE_LINE_MAKE_TRANSACTIONAL(tx, line);
      }

      cpu_invw(now + DELTA_TIME, pid, addr);

      word->timestamp = now;
      word->address = addr;
      word->state = MESI_STATE_E;

      do_sc_event(now + r_lat, pid, SC_READ, buffer->acc_lat + r_lat,
              PC_TXRE, buffer, false);
    }
  }

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, " state=%s \n", PRINT_MESI_STATE(word->state));
  __DEBUG_FILE_FLUSH;
#endif

  thr_response(now + r_lat, buffer, r_lat, PC_TXRE, state);
}

void HTM_pc_state_tx_write(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer) {

  simtime_t now = state->now, w_lat = state->w_lat;
  int pid = state->pid, addr = SANITIZE_ADDRESS(buffer->addr);
  int nb_lines = N_LINES_PCACHE, nb_words = N_ASSOC_PCACHE;
  HTM_cache_tx_t * tx;
  HTM_pc_line_t * line, * lines = state->lines;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x ",
          now, pid, PRINT_PC_STATE(PC_TXWR), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  tx = state->tx;

  if (tx == NULL || !tx->is_running) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
    fprintf(__debug_file, " ERROR tx not running \n");
    __DEBUG_FILE_FLUSH;
#endif

    state->aborted = true;

    //    if (state->do_tx_abort) {
    //      tx_abort(true, state, buffer, tx, state->last_abort_code);
    //    } else {
    thr_response(now + DELTA_TIME, buffer, DELTA_TIME, PC_TXWR, state);
    //    }

    return;
  }

  if (tx != NULL && tx->is_running) {

    FIND_CACHE_LINE(line, addr, lines, nb_lines);

    if (CACHE_LINE_IS_TRANSACTIONAL(line) && line->tx->tx_id != tx->tx_id) {
      // not the same transaction (should not happen)

      if (!state->do_tx_abort) {
        state->tx_callback = buffer->callback;
      }

      tx_abort(true, state, buffer, line->tx, ABORT_CODE_TXCF);
    }

    CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

    // invalidate txs with this word in ANY state

    if (word->address == addr && word->state != MESI_STATE_I) {
      // hit

      if (!CACHE_LINE_IS_TRANSACTIONAL(line)) {
        cache_line_invalidate(line, true);
        CACHE_LINE_MAKE_TRANSACTIONAL(tx, line);
      }

      word->state = MESI_STATE_M; // TODO: should the line be emptied???
      word->timestamp = now;
      word->address = addr;

      cpu_inva(now + DELTA_TIME, pid, addr);

    } else {
      // miss

      if (CACHE_LINE_IS_TRANSACTIONAL(line)) {
        if (word->state != MESI_STATE_I) {
          // line full
          state->aborted = true;
          state->last_abort_code = ABORT_CODE_CAPA;

          //          if (!state->do_tx_abort) {
          //            state->tx_callback = buffer->callback;
          //          }

          tx_abort(false, state, buffer, tx, ABORT_CODE_CAPA);
          thr_response(now + DELTA_TIME, buffer, DELTA_TIME, PC_TXWR, state);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
          fprintf(__debug_file, " ABORTED \n");
          __DEBUG_FILE_FLUSH;
#endif
          return;
        }
      } else {
        cache_line_invalidate(line, true);
        CACHE_LINE_MAKE_TRANSACTIONAL(tx, line);
      }

      cpu_inva(now + DELTA_TIME, pid, addr);

      word->state = MESI_STATE_M;
      word->timestamp = now;
      word->address = addr;
    }
  }

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, " state=%s \n", PRINT_MESI_STATE(word->state));
  __DEBUG_FILE_FLUSH;
#endif

  thr_response(now + w_lat, buffer, w_lat, PC_TXWR, state);
}

void HTM_pc_state_get_word_state(HTM_pc_state_t * state,
        HTM_pc_buffer_t * buffer) {

  simtime_t now = state->now;
  int addr, nb_lines, nb_words, nb_pcs = state->nb_pcs, pid = state->pid;
  HTM_pc_line_t * line;
  HTM_cache_word_t * word;
  HTM_cpu_buffer_t cpu_buffer;
  UTL_event_t cpu_event;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x ",
          now, pid, PRINT_PC_STATE(PC_GETS), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_PCACHE;
  nb_words = N_ASSOC_PCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  cpu_buffer.pc_pid = pid;
  cpu_buffer.addr = buffer->addr;
  cpu_buffer.is_transac = CACHE_LINE_IS_TRANSACTIONAL(line);

  UTL_callback_to_event(&cpu_event, &(buffer->callback));

  cpu_event.cnt = &cpu_buffer;
  cpu_event.cnt_size = sizeof (HTM_cpu_buffer_t);

  if (word->address == addr) {
    cpu_buffer.word_state = word->state;
  } else {
    cpu_buffer.word_state = MESI_STATE_I;
  }

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, "state: %s\n", PRINT_MESI_STATE(cpu_buffer.word_state));
  __DEBUG_FILE_FLUSH;
#endif

  SCHEDULE_EVENT(cpu_event, now + DELTA_TIME);
}

void HTM_pc_state_set_word_state(HTM_pc_state_t * state,
        HTM_pc_buffer_t * buffer) {

  simtime_t now = state->now;
  int addr, nb_lines, nb_words, nb_pcs = state->nb_pcs, pid = state->pid;
  HTM_pc_line_t * line;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x state=%s \n",
          now, pid, PRINT_PC_STATE(PC_SETS), buffer->addr,
          PRINT_MESI_STATE(buffer->state));
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_PCACHE;
  nb_words = N_ASSOC_PCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  if (word->address == addr && word->state != MESI_STATE_I) {
    // found the word
    if (CACHE_LINE_IS_TRANSACTIONAL(line)
            && (buffer->state & (MESI_STATE_M | MESI_STATE_I))) {

      tx_abort(state->do_tx_abort, state, buffer, line->tx, ABORT_CODE_NTXC);
    }
    if (!CACHE_LINE_IS_TRANSACTIONAL(line)) {
      word->state = buffer->state;
    }
  }
}

void HTM_pc_state_invalidate(HTM_pc_state_t * state,
        HTM_pc_buffer_t * buffer) {

  simtime_t now = state->now;
  int pid = state->pid;
  int addr, nb_lines, nb_words;
  HTM_pc_line_t * line;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x",
          now, pid, PRINT_PC_STATE(PC_INVA), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_PCACHE;
  nb_words = N_ASSOC_PCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  if (word->address == addr && word->state != MESI_STATE_I) {
    // found the word
    if (CACHE_LINE_IS_TRANSACTIONAL(line)) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
      fprintf(__debug_file, " ABORTED");
      __DEBUG_FILE_FLUSH;
#endif
      tx_abort(state->do_tx_abort, state, buffer, line->tx, ABORT_CODE_TXCF);
      word->state = MESI_STATE_I;
    }
  }
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, "\n");
  __DEBUG_FILE_FLUSH;
#endif
}

void HTM_pc_state_invalidate_modified(HTM_pc_state_t * state,
        HTM_pc_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now;
  int addr, nb_lines, nb_words;
  HTM_pc_line_t * line;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x",
          now, pid, PRINT_PC_STATE(PC_INVW), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_PCACHE;
  nb_words = N_ASSOC_PCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  if (word->address == addr && word->state != MESI_STATE_I) {
    // found the word
    if (CACHE_LINE_IS_TRANSACTIONAL(line) && word->state == MESI_STATE_M) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
      fprintf(__debug_file, " ABORTED");
      __DEBUG_FILE_FLUSH;
#endif
      tx_abort(state->do_tx_abort, state, buffer, line->tx, ABORT_CODE_TXCF);
      word->state = MESI_STATE_I;
    }
  }
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_PC_DEB__)
  fprintf(__debug_file, "\n");
  __DEBUG_FILE_FLUSH;
#endif
}

void cache_line_invalidate(HTM_pc_line_t * line, bool all) {
  HTM_cache_word_t * words = line->words;
  int i;

  for (i = 0; i < N_ASSOC_PCACHE; i++) {
    HTM_cache_word_t * word = &words[i];

    if (all) {
      word->state = MESI_STATE_I;
    } else if (word->state == MESI_STATE_M) {
      word->state = MESI_STATE_I;
    }
  }
}

void tx_abort(bool do_callback, HTM_pc_state_t * state,
        HTM_pc_buffer_t * buffer, HTM_cache_tx_t * tx,
        ABORT_CODE cause) {

  simtime_t now = state->now;
  HTM_pc_line_t * line;

  if (tx != NULL && tx->is_running) {
    tx->abort_code = cause;
    tx->is_running = false;

    // TODO: there is some error that breaks the tx
    if (tx->tx_id == state->tx_id) {
      while (line = COL_LLIST_pop(tx->lines)) {
        line->tx = NULL;
        cache_line_invalidate(line, false);
      }
    }
  }

  state->aborted = true;
  state->last_abort_code = cause;

  if (do_callback) {
    buffer->callback.pid = state->thr_pid;
    buffer->callback.event_code = THR_TXAB;
    thr_response(now + state->tx_abort_lat, buffer,
            state->tx_abort_lat, PC_TXAB, state);
  }
}

void tx_commit(simtime_t now, HTM_cache_tx_t * tx) {

  HTM_pc_line_t * line;

  tx->abort_code = ABORT_CODE_COMMIT;
  tx->is_running = false;

  while (line = COL_LLIST_pop(tx->lines)) {
    int i;
    for (i = 0; i < N_ASSOC_PCACHE; i++) {
      if (line->words[i].state != MESI_STATE_I) {
        int addr = line->words[i].address;
        commit_sc_event(now + DELTA_TIME, SC_WRIT, global_info.lat_pc_w, addr);
      }
    }
    line->tx = NULL;
  }
}

int count_words(int addr, MESI_STATE * state, COL_llist_t * list) {

  int i, res = 0;

  for (i = 0; i < global_info.tot_nb_pcs; i++) {
    HTM_pc_line_t * lines = global_info.pcs_lines[i];
    HTM_pc_line_t * line;
    HTM_cache_word_t * word;
    FIND_CACHE_LINE(line, addr, lines, N_LINES_PCACHE);
    CACHE_LINE_FIND_WORD(word, addr, line, N_ASSOC_PCACHE);

    if (word->address == addr && word->state != MESI_STATE_I) {
      res++;
      *state = word->state;
      if (list != NULL) {
        int * pid = new(int, 1);
        *pid = i;
        COL_LLIST_push(list, pid);
      }
    }
  }

  return res;
}

inline int mask_value(int value) {
  int i = 0x01;
  int mask = 0;
  while (i < value) {
    mask = mask | i;
    i = i << 1;
  }
  return mask;
}
