#ifndef HTM_SIMPLE_H
#  define HTM_SIMPLE_H

#  include "htm.h"
#  include "model_states.h"

#  define SMPL_NB_SAMPLES 5000

#  define STATE_SMPL_STACK         SMPL_INIT:                                 \
                              case SMPL_INTL:                                 \
                              case SMPL_TXIN:                                 \
                              case SMPL_TXSP:                                 \
                              case SMPL_WAKE:                                 \
                              case SMPL_TRAN:                                 \
                              case SMPL_TXCM:                                 \
                              case SMPL_TXAB:                                 \
                              case SMPL_SERL:                                 \
                              case SMPL_NOTX:                                 \
                              case SMPL_CKLK:                                 \
                              case SMPL_LOCK:                                 \
                              case SMPL_ADDR:                                 \
                              case SMPL_MRKV:                                 \
                              case SMPL_MRKL:                                 \
                              case SMPL_MRKD

#  define PRINT_SMPL_STATE(state) (state == SMPL_INIT ? "SMPL_INIT" :         \
                                   state == SMPL_INTL ? "SMPL_INTL" :         \
                                   state == SMPL_TXIN ? "SMPL_TXIN" :         \
                                   state == SMPL_TXSP ? "SMPL_TXSP" :         \
                                   state == SMPL_TXWK ? "SMPL_TXWK" :         \
                                   state == SMPL_LKWK ? "SMPL_LKWK" :         \
                                   state == SMPL_TRAN ? "SMPL_TRAN" :         \
                                   state == SMPL_TXCM ? "SMPL_TXCM" :         \
                                   state == SMPL_TXAB ? "SMPL_TXAB" :         \
                                   state == SMPL_SERL ? "SMPL_SERL" :         \
                                   state == SMPL_NOTX ? "SMPL_NOTX" :         \
                                   state == SMPL_CKLK ? "SMPL_CKLK" :         \
                                   state == SMPL_LOCK ? "SMPL_LOCK" :         \
                                   state == SMPL_ADDR ? "SMPL_ADDR" :         \
                                   state == SMPL_MRKV ? "SMPL_MRKV" :         \
                                   state == SMPL_MRKL ? "SMPL_MRKL" :         \
                                   state == SMPL_MRKD ? "SMPL_MRKD" : "")

#  define SMPL_STATS_INC(_NOW, _STATS, _STAT, _CUR_BUDG, _NB_OPS, _AB_TYPE) ({\
    double __elapsed_time = _NOW - _STATS.begin_ts;                           \
    int __pointer = _STATS.stats_pointer[_STAT];                              \
    _STATS.counter[_STAT]++;                                                  \
    _STATS.counter_time[_STAT] += __elapsed_time;                             \
    _STATS.stat_time[_STAT][__pointer] = __elapsed_time;                      \
    if (_STAT == TXCM) {                                                      \
      _STATS.budget_at_commit[__pointer] = _CUR_BUDG;                         \
    }                                                                         \
    if (_STAT == TXAB) {                                                      \
      _STATS.ops_before_abort[__pointer] = _NB_OPS;                           \
      _STATS.abort_type[__pointer] = _AB_TYPE;                                \
      _STATS.abort_type_count[_AB_TYPE]++;                                    \
    }                                                                         \
    _STATS.begin_ts = _NOW;                                                   \
    _STATS.stats_pointer[_STAT] = (__pointer + 1) % SMPL_NB_SAMPLES;          \
  })


#  define MASK_FROM_VALUE(_VALUE) ({                                          \
    unsigned __mask = -1, __shift = 0, __s = _VALUE;                          \
    while (__s > 1) {                                                         \
      __s >>= 1;                                                              \
      __shift++;                                                              \
    }                                                                         \
    __s <<= __shift;                                                           \
    __s - 1;                                                                  \
  })

#  define SET_ZERO_CAPACITIES(_CAP) ({                                          \
    int __i, __j;                                                             \
    for (__i = 0; __i < global_info.nb_sets; ++__i) {                         \
      for (__j = 0; __j < global_info.nb_ways; ++__j) {                       \
        _CAP[__i][__j].access = ACC_T_NONE;                                   \
      }                                                                       \
    }                                                                         \
  })

#  define INIT_CAPACITIES(_CAP) ({                                              \
    int __i;                                                                  \
    _CAP = new(cache_line_info*, global_info.nb_sets);                        \
    for (__i = 0; __i < global_info.nb_sets; ++__i) {                         \
      _CAP[__i] = new(cache_line_info, global_info.nb_ways);                  \
    }                                                                         \
  })


// TODO: mode these functions to MACROs
int GET_CACHE_LINE(int _ADDR);
int GET_CACHE_WAY(int _ADDR);
int GET_CACHE_SET(int _ADDR);
int GET_CACHE_TAG(int _ADDR);

#  define GET_EMPTY_WAY(_CAP, _SET) ({                                          \
    int __i, __res = -1;                                                      \
    for (__i = 0; __i < global_info.nb_ways; ++__i) {                         \
      int __j = i;                                                            \
      if (_CAP[_SET][__j].access == ACC_T_NONE) {                             \
        __res = __j;                                                          \
        break;                                                                \
      }                                                                       \
    }                                                                         \
    __res;                                                                    \
  })

typedef enum _SMST_T {
  SMST_TRAN = 0, SMST_NOTX, SMST_SERL
} SMST_T;

typedef enum _SMPL_ABORT_T {
  SMPL_LOCK_CNFL = 0, SMPL_TRAN_CNFL, SMPL_NOTX_CNFL, SMPL_CAPA_CNFL
} EVENT_ABORT_T;

#  define NB_SMPL_ABORTS        4

typedef enum _EVENT_TYPE_T {
  IS_REQUEST = 0, IS_RESPONSE, IS_NEIGHBOR, IS_LAST
} EVENT_TYPE_T;

typedef enum _ACCESS_TYPE {
  ACC_T_NONE = 0, ACC_T_READ, ACC_T_WRIT
} ACCESS_TYPE;

typedef struct _cache_line_info_t {
  int address;
  ACCESS_TYPE access;
} cache_line_info;

bool READ_ADDR(cache_line_info** _CAP, int _ADDR);

bool WRITE_ADDR(cache_line_info** _CAP, int _ADDR);

typedef struct _HTM_simple_stats_t {
  simtime_t begin_ts;
  int stats_pointer[NB_STATS], counter[NB_STATS];
  simtime_t counter_time[NB_STATS];
  int budget_at_commit[SMPL_NB_SAMPLES];
  int ops_before_abort[SMPL_NB_SAMPLES];
  int abort_type[SMPL_NB_SAMPLES];
  int abort_type_count[NB_SMPL_ABORTS];
  simtime_t stat_time[NB_STATS][SMPL_NB_SAMPLES];
  int count_C_L;
  double avg_C_L, avg_C, avg_L;
} HTM_smpl_stats_t;

typedef struct _HTM_simple_state_t {
  int pid;

  // model related
  int cur_budget, prev_budget, cur_L;
  double cur_C;
  SMST_T cur_state;
  EVENT_ABORT_T abort_reason;
  int * addrs;
  int addr_len, addrs_size, count_aborts;
  bool * is_write;

  // capacity related
  cache_line_info ** capacities;

  // consistency related
  simtime_t now, last_serl_ts;
  int inst_count, controlstamp;
  int c_genetated_values_index;
  double* c_genetated_values;
  bool has_lock, lock_free, is_running, some_using_lock;

  // mrkd
  int * other_budget, * other_prev_budget;
  SMST_T * other_xact;
  mod_state_t mrkd_pp_state, mrkd_prev_state;
  mod_state_t mrkd_state, mrkd_next_state;
  bool mrkd_is_commit, mrkd_is_abort, commit_from_serl, abort_from_lock;
  int mrkd_counter;
  bool pid_entered_mrkd;
  double others_avg_L;
  simtime_t mrkd_ts;

  // stats
  HTM_smpl_stats_t stats;
} HTM_simple_state_t;

typedef struct _HTM_simple_buffer_t {
  int controlstamp, addr, pid, cur_budget, prev_budget, cur_inst;
  simtime_t last_serl_ts;
  bool has_lock, has_addr, is_write, had_to_abort;
  bool is_abort, is_commit, is_abort_lock, is_commit_serl;
  EVENT_TYPE_T type;
  SMST_T req_state;
  EVENT_ABORT_T ab_type;
} HTM_simple_buffer_t;

void HTM_simple_state_handler(HTM_simple_state_t*, int, SIM_STATE,
        simtime_t, HTM_simple_buffer_t*);

void HTM_simple_INIT(HTM_simple_state_t*);
void HTM_simple_INTL(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_TXIN(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_TXSP(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_WAKE(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_TRAN(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_TXCM(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_TXAB(HTM_simple_state_t*, HTM_simple_buffer_t*);

void HTM_simple_SERL(HTM_simple_state_t*, HTM_simple_buffer_t*);

void HTM_simple_NOTX(HTM_simple_state_t*, HTM_simple_buffer_t*);

// deals with the lock
void HTM_simple_CKLK(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_LOCK(HTM_simple_state_t*, HTM_simple_buffer_t*);

// for invalidating addrs
void HTM_simple_ADDR(HTM_simple_state_t*, HTM_simple_buffer_t*);

// deals with the Markov state
void HTM_simple_MRKV(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_MRKL(HTM_simple_state_t*, HTM_simple_buffer_t*);
void HTM_simple_MRKD(HTM_simple_state_t*, HTM_simple_buffer_t*);

#endif /* HTM_FAKE_H */

