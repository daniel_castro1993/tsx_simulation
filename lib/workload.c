#include <sys/types.h>
#include <regex.h>
#include <stdio.h>

#include "workload.h"

#define REGEX_N_MATCH 10

typedef struct _sub_expr_t {
  char* full_expr;
  int start;
  int finish;
} sub_expr_t;

typedef struct _sub_exprs_t {
  int max_exprs;
  int cur_exprs;
  sub_expr_t* exprs;
} sub_exprs_t;

/**
 * 
 * @param fp
 * @param buffer to store the line
 * @return false if end of file
 */
bool read_line(FILE* fp, char* buffer) {
  int res;
  int input;

  while ((input = fgetc(fp)) != '\n') {
    if (input == EOF) {
      *buffer = '\0';
      return false;
    }
    res++;
    *buffer = (char) input;
    buffer++;
  }
  *buffer = '\0';
  return true;
}

bool compile_regex(regex_t * r, const char * regex_text) {
  int status = regcomp(r, regex_text, REG_EXTENDED | REG_NEWLINE);
  if (status != 0) {
    char buffer[512];
    regerror(status, r, buffer, 512);
    UTL_log_e("Regex error compiling '%s': %s", regex_text, buffer);
    return false;
  }
  return true;
}

bool match_regex(regex_t* r, char* to_match, sub_exprs_t* exprs) {
  const char* prev = to_match; // points end of previous match
  static regmatch_t m[REGEX_N_MATCH];
  int num_expr = 0;
  bool gotSomething = false;
#ifdef DEBUG
  char buffer[128];
#endif

  while (true) {
    int i = 0;
    int nomatch = regexec(r, prev, REGEX_N_MATCH, m, 0);
    if (nomatch) {
      return gotSomething;
    }
    gotSomething = true;
    for (i = 0; i < REGEX_N_MATCH; i++) {
      int start;
      int finish;
      if (m[i].rm_so == -1) {
        break;
      }
      start = m[i].rm_so + (prev - to_match);
      finish = m[i].rm_eo + (prev - to_match);
#ifdef DEBUG
      if (i == 0) {
        sprintf(buffer, "$& is");
      } else {
        sprintf(buffer, "$%d is", i);
      }
      UTL_log_d("%s '%.*s' (bytes %d:%d)", buffer, (finish - start),
              to_match + start, start, finish);
#endif
      if (i > 0 && num_expr < exprs->max_exprs) {
        exprs->exprs[num_expr].full_expr = to_match;
        exprs->exprs[num_expr].start = start;
        exprs->exprs[num_expr].finish = finish;
        num_expr++;
        exprs->cur_exprs = num_expr;
      }
    }
    prev += m[0].rm_eo;
  }
  return true;
}

UTL_application_t* UTL_load_application(const char* file_name) {
  FILE* fp;
  regex_t trans_start, op_read, op_write, op_do_stuff, trans_end;
  char* regex_trans_start = "transaction[[:blank:]]+([[:alnum:]]+)[[:blank:]]+"
          "\\([[:blank:]]*([[:digit:]]+)[[:blank:]]*\\)";
  char* regex_op_read = "read[[:blank:]]*\\([[:blank:]]*(0x[[:xdigit:]]+)"
          "[[:blank:]]*\\)";
  char* regex_op_write = "write[[:blank:]]*\\([[:blank:]]*(0x[[:xdigit:]]+)"
          "[[:blank:]]*\\)";
  char* regex_op_do_stuff = "do_stuff[[:blank:]]*\\([[:blank:]]*([[:digit:]]+)"
          "[[:blank:]]*\\)";
  char* regex_trans_end = "end[[:blank:]]+transaction";
  bool reti, within_trans = false;
  char buffer[512];
  sub_exprs_t exprs;
  UTL_transaction_t* trans;
  UTL_application_t* res = new(UTL_application_t, 1);

  res->transactions = COL_LLIST_new();

  exprs.max_exprs = REGEX_N_MATCH;
  exprs.cur_exprs = 0;
  exprs.exprs = new(sub_expr_t, REGEX_N_MATCH);

  fp = fopen(file_name, "r");

  if (fp == NULL) {
    UTL_log_e("UTL_load_application - file %s not found", file_name);
    return NULL;
  }

  // TODO check error
  reti = compile_regex(&trans_start, regex_trans_start);
  reti = compile_regex(&trans_end, regex_trans_end);
  reti = compile_regex(&op_read, regex_op_read);
  reti = compile_regex(&op_write, regex_op_write);
  reti = compile_regex(&op_do_stuff, regex_op_do_stuff);

  while (read_line(fp, buffer)) {
    UTL_log_d("got line: %s", buffer);

    if (!within_trans) {
      UTL_log_d("Not in transaction");
      if (match_regex(&trans_start, buffer, &exprs)) {
        char id_buffer[128];
        char start_time_buffer[128];
        char* fullExpr = buffer;
        int idStart = exprs.exprs[0].start;
        int idFinish = exprs.exprs[0].finish;
        int timeStart = exprs.exprs[1].start;
        int timeFinish = exprs.exprs[1].finish;
        int start_time, id;

        trans = new(UTL_transaction_t, 1);
        COL_LLIST_add(res->transactions, trans);

        sprintf(id_buffer, "%.*s", idFinish - idStart, fullExpr + idStart);
        sprintf(start_time_buffer, "%.*s", timeFinish - timeStart,
                fullExpr + timeStart);

        UTL_log_d("id: %s", id_buffer);
        UTL_log_d("start time: %s", start_time_buffer);

        start_time = atoi(start_time_buffer);
        id = atoi(id_buffer);

        UTL_log_d("parsed id: %i, start time: %i", id, start_time);

        trans->id = id;
        trans->start = (simtime_t) start_time;

        within_trans = true;
      }
    } else {
      UTL_log_d("In transaction");
      if (match_regex(&trans_end, buffer, &exprs)) {
        // Got a end transaction
        UTL_log_d("End transaction");
        
        trans = NULL; // do not free
        
        within_trans = false;
      } else if (match_regex(&op_read, buffer, &exprs)) {
        // Got a read
        char addr_buffer[128];
        char* fullExpr = buffer;
        int addrStart = exprs.exprs[0].start;
        int addrFinish = exprs.exprs[0].finish;
        int addr;
        UTL_op_t* op = new(UTL_op_t, 1);

        sprintf(addr_buffer, "%.*s", addrFinish - addrStart,
                fullExpr + addrStart);

        addr = (int) strtol(addr_buffer, NULL, 0);

        op->address = addr;
        op->event = OP_READ;
        op->is_done = false;
        COL_LLIST_add(trans->ops, op);
        
        UTL_log_d("read - got addr 0x%x", addr);

      } else if (match_regex(&op_write, buffer, &exprs)) {
        // Got a write
        char addr_buffer[128];
        char* fullExpr = buffer;
        int addrStart = exprs.exprs[0].start;
        int addrFinish = exprs.exprs[0].finish;
        int addr;
        UTL_op_t* op = new(UTL_op_t, 1);

        sprintf(addr_buffer, "%.*s", addrFinish - addrStart,
                fullExpr + addrStart);

        addr = (int) strtol(addr_buffer, NULL, 0);

        op->address = addr;
        op->event = OP_WRITE;
        op->is_done = false;
        
        COL_LLIST_add(trans->ops, op);
        
        UTL_log_d("write - got addr 0x%x", addr);

      } else if (match_regex(&op_do_stuff, buffer, &exprs)) {
        // Got a do_stuff
        char elapsed_time_buffer[128];
        char* fullExpr = buffer;
        int timeStart = exprs.exprs[0].start;
        int timeFinish = exprs.exprs[0].finish;
        int time;
        UTL_op_t* op = new(UTL_op_t, 1);

        sprintf(elapsed_time_buffer, "%.*s", timeFinish - timeStart,
                fullExpr + timeStart);

        time = (int) strtol(elapsed_time_buffer, NULL, 10);

        op->event = OP_DO_STUFF;
        op->duration = time;
        op->is_done = false;
        
        COL_LLIST_add(trans->ops, op);
        
        UTL_log_d("do_stuff - got time %d", time);
      }
    }

  }

  COL_LLIST_it_new(res->transactions, &res->current_transaction);

  return res;
}

UTL_transaction_t *UTL_app_get_next_transaction() {

  return NULL;
}

void UTL_destroi_application(UTL_transaction_t* app) {

}

