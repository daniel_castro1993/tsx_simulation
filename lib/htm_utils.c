#include "htm_utils.h"
#include "htm_pcache.h"

void HTM_do_lock_event(simtime_t next_tick, int pid, SIM_STATE lock_state,
        SIM_STATE callback_event) {

  UTL_event_t lock_event;
  HTM_lock_buf_t lock_buffer;

  if (pid < 0 || pid >= n_prc_tot) {
    printf("ERROR[HTM_do_lock_event]: invalid pid: %i \n", pid);
  }

  UTL_event_init(&lock_event);

  lock_buffer.callback.pid = pid;
  lock_buffer.callback.event_code = callback_event;

  lock_event.cnt = &lock_buffer;
  lock_event.cnt_size = sizeof (HTM_lock_buf_t);
  lock_event.pid = global_info.lock_pid;
  lock_event.event_code = lock_state;

  SCHEDULE_EVENT(lock_event, next_tick);
}

void HTM_do_pc_event(simtime_t next_tick, int pid, int addr,
        int inst_id, SIM_STATE pc_event_code, bool do_callback,
        SIM_STATE callback_event_code) {

  UTL_event_t pc_event;
  HTM_pc_buffer_t pc_buffer;
  int cache_index = pid % global_info.tot_nb_thrs;

  if (pid < 0 || pid >= n_prc_tot) {
    printf("ERROR[HTM_do_pc_event]: invalid pid: %i \n", pid);
  }

  UTL_event_init(&pc_event);

  pc_buffer.thr_pid = pid;
  pc_buffer.acc_lat = 0;
  pc_buffer.addr = addr;
  pc_buffer.inst_id = inst_id;

  pc_buffer.do_tx_abort = true;
  pc_buffer.tx_abort.pid = pid;
  pc_buffer.tx_abort.event_code = THR_TXAB;

  pc_buffer.do_callback = do_callback;
  pc_buffer.callback.event_code = callback_event_code;
  pc_buffer.callback.pid = pid;

  pc_event.cnt = &pc_buffer;
  pc_event.cnt_size = sizeof (HTM_pc_buffer_t);
  pc_event.pid = global_info.pcs_pids[cache_index];
  pc_event.event_code = pc_event_code;

  SCHEDULE_EVENT(pc_event, next_tick);
}

void HTM_do_sc_event(simtime_t next_tick, int pid, int addr, int inst_id,
        SIM_STATE sc_event_code, SIM_STATE pc_event,
        UTL_callback_t * callback, int thr_pid,
        simtime_t acc_lat, bool no_callback) {

  UTL_event_t sc_event;
  HTM_sc_buffer_t sc_buffer;

  if (!no_callback && (pid < 0 || pid >= n_prc_tot)) {
    printf("ERROR[HTM_do_sc_event]: invalid pid: %i \n", pid);
  }

  UTL_event_init(&sc_event);

  sc_buffer.acc_lat = acc_lat;
  sc_buffer.addr = addr;
  sc_buffer.inst_id = inst_id;
  sc_buffer.pc_event = pc_event;
  sc_buffer.pc_pid = pid;
  sc_buffer.thr_pid = thr_pid;
  sc_buffer.callback = *callback;
  sc_buffer.no_callback = no_callback;

  sc_event.cnt = &sc_buffer;
  sc_event.cnt_size = sizeof (HTM_sc_buffer_t);
  sc_event.pid = global_info.sc_pid;
  sc_event.event_code = sc_event_code;

  SCHEDULE_EVENT(sc_event, next_tick);
}

void HTM_do_mem_event(simtime_t next_tick, int pid, int addr, int inst_id,
        SIM_STATE sc_event_code, SIM_STATE pc_event,
        UTL_callback_t * callback, int thr_pid,
        simtime_t acc_lat, bool no_callback) {

  UTL_event_t sc_event;
  HTM_sc_buffer_t sc_buffer;

  if (!no_callback && (pid < 0 || pid >= n_prc_tot)) {
    printf("ERROR[HTM_do_sc_event]: invalid pid: %i \n", pid);
  }

  UTL_event_init(&sc_event);

  sc_buffer.acc_lat = acc_lat;
  sc_buffer.addr = addr;
  sc_buffer.inst_id = inst_id;
  sc_buffer.pc_event = pc_event;
  sc_buffer.pc_pid = pid;
  sc_buffer.thr_pid = thr_pid;
  sc_buffer.callback = *callback;
  sc_buffer.no_callback = no_callback;

  sc_event.cnt = &sc_buffer;
  sc_event.cnt_size = sizeof (HTM_sc_buffer_t);
  sc_event.pid = global_info.sc_pid;
  sc_event.event_code = sc_event_code;

  SCHEDULE_EVENT(sc_event, next_tick);
}

void HTM_do_cpu_event(simtime_t next_tick, SIM_STATE cpu_state,
        HTM_cpu_buffer_t * cpu_buffer) {

  UTL_event_t cpu_event;

  UTL_event_init(&cpu_event);

  cpu_event.cnt = cpu_buffer;
  cpu_event.cnt_size = sizeof (HTM_cpu_buffer_t);
  cpu_event.pid = global_info.cpu_pid;
  cpu_event.event_code = cpu_state;

  SCHEDULE_EVENT(cpu_event, next_tick);
}

void HTM_do_thr_event(simtime_t next_tick, int pid, SIM_STATE thr_event_code,
        HTM_thr_buffer_t * thr_buffer) {

  UTL_event_t thr_event;

  if (pid < 0 || pid >= n_prc_tot) {
    printf("ERROR[HTM_do_cpu_event]: invalid pid: %i \n", pid);
  }

  UTL_event_init(&thr_event);

  thr_event.cnt = thr_buffer;
  thr_event.cnt_size = sizeof (HTM_thr_buffer_t);
  thr_event.pid = pid;
  thr_event.event_code = thr_event_code;

  SCHEDULE_EVENT(thr_event, next_tick);
}
