#include "htm_simple.h"
#include "stats.h"

#define EXP_BOUND 100
#define MAX_C_STORAGE 1000

#ifdef __EXP_C_L__
#define LAT_NEXT_OP(_STATE) ({                                              \
    double __c_l = _STATE->cur_C / (double) _STATE->cur_L;                    \
    double __new_c_l, __r;                                                    \
    __r = max(Expent(__c_l), DELTA_TIME);                                     \
    if (__r > __c_l * EXP_BOUND) {                                            \
      __r = __c_l * EXP_BOUND;                                                \
    }                                                                         \
    __new_c_l = __r;                                                          \
    _STATE->stats.count_C_L++;                                                \
    _STATE->stats.avg_C_L = _STATE->stats.avg_C_L + (__new_c_l                \
            - _STATE->stats.avg_C_L) / (double) _STATE->stats.count_C_L;      \
    _STATE->stats.avg_C = _STATE->stats.avg_C + (_STATE->cur_C                \
            - _STATE->stats.avg_C) / (double) _STATE->stats.count_C_L;        \
    _STATE->stats.avg_L = _STATE->stats.avg_L + (_STATE->cur_L                \
            - _STATE->stats.avg_L) / (double) _STATE->stats.count_C_L;        \
    __new_c_l;                                                                \
  })
#else
#define LAT_NEXT_OP(_STATE) ({_STATE->cur_C / (double) _STATE->cur_L;})
#endif

static mod_state_t prev_mrkd_state = NULL, next_mrkd_state = NULL;
static int* budgets = NULL;
static SMST_T* states = NULL;
static bool is_mrkd_init = false;
static double prev_mrkd_ts = 0;

static int* lps_budget;

static void shift_mrkd_state(mod_state_t mrkd_state)
{
	int i, j, k;

	for (i = 0; i < global_info.budget; i++) {
		j = global_info.budget - i;
		k = j - 1;
		mrkd_state[j] += mrkd_state[k];
		mrkd_state[k] = 0;
	}
}

static void shift_left_mrkd_state(mod_state_t mrkd_state)
{
	int i, j, k;

	for (i = global_info.budget; i > 0; --i) {
		j = global_info.budget - i;
		k = j + 1;
		mrkd_state[j] += mrkd_state[k];
		mrkd_state[k] = 0;
	}
}

static bool update_global_state(int cur_budget,
	bool is_commit, bool is_commit_serl,
	bool is_abort, bool is_lock_abort)
{

	mod_cpyState(prev_mrkd_state, next_mrkd_state);

	if (is_commit_serl) {
		next_mrkd_state[global_info.budget]--;
		next_mrkd_state[0]++;
	} else if (is_commit) {
		next_mrkd_state[global_info.budget - cur_budget]--;
		next_mrkd_state[0]++;
	} else if (is_abort && !is_lock_abort) {
		next_mrkd_state[global_info.budget - cur_budget]--;
		next_mrkd_state[global_info.budget - (cur_budget - 1)]++;
	} else if (is_lock_abort) {
		shift_mrkd_state(next_mrkd_state);
	}

	if (!mod_isStateValid(next_mrkd_state)) {
		// shouldn't happen
		mod_cpyState(next_mrkd_state, prev_mrkd_state);
		return false;
	}
	return true;
}

double GENERATE_RANDOM_C(double* _C_STORAGE, int* _INDEX)
{
	if (*_INDEX > -1 && *_INDEX < MAX_C_STORAGE) {
		*_INDEX += 1;
	} else {
		int i;
		for (i = 0; i < MAX_C_STORAGE; ++i) {
			_C_STORAGE[i] = max(Expent(global_info.C), DELTA_TIME);
			if (_C_STORAGE[i] > EXP_BOUND * global_info.C) {
				_C_STORAGE[i] = EXP_BOUND * global_info.C;
			}
			*_INDEX = 0;
		}
	}
	return _C_STORAGE[*_INDEX];
}

int GET_CACHE_LINE(int _ADDR)
{
	int __line_mask = MASK_FROM_VALUE(global_info.cache_line_size);
	return _ADDR & __line_mask;
}

int GET_CACHE_WAY(int _ADDR)
{
	int __way_mask = MASK_FROM_VALUE(global_info.nb_ways);
	__way_mask <<= (int) log2(global_info.cache_line_size);
	__way_mask <<= (int) log2(global_info.nb_sets);
	return _ADDR & __way_mask;
}

int GET_CACHE_SET(int _ADDR)
{
	int __set_mask = MASK_FROM_VALUE(global_info.nb_sets);
	__set_mask <<= (int) log2(global_info.cache_line_size);
	return _ADDR & __set_mask;
}

int GET_CACHE_TAG(int _ADDR)
{
	int __tag_mask = -1;
	__tag_mask <<= (int) log2(global_info.cache_line_size);
	__tag_mask <<= (int) log2(global_info.nb_sets);
	__tag_mask <<= (int) log2(global_info.nb_ways);
	return _ADDR & __tag_mask;
}

bool WRITE_ADDR(cache_line_info** _CAP, int _ADDR)
{
	int i, j;
	bool res = false;
	int set = GET_CACHE_SET(_ADDR);
	int way = GET_CACHE_WAY(_ADDR);
	int tag = GET_CACHE_TAG(_ADDR);
	int line = GET_CACHE_LINE(_ADDR);
	i = set >> (int) log2(global_info.cache_line_size);
	i = i % global_info.nb_sets;
	for (j = 0; j < global_info.nb_ways; ++j) {
		int w_set = GET_CACHE_SET(_CAP[i][j].address);
		int w_tag = GET_CACHE_TAG(_CAP[i][j].address);
		int w_way = GET_CACHE_WAY(_CAP[i][j].address);
		if (w_tag == tag && _CAP[i][j].access != ACC_T_NONE) {
			_CAP[i][j].access = ACC_T_WRIT;
			_CAP[i][j].address = _ADDR;
			res = true;
			break;
		} else if (_CAP[i][j].access == ACC_T_NONE) {
			_CAP[i][j].access = ACC_T_READ;
			_CAP[i][j].address = _ADDR;
			res = true;
			break;
		}
	}
	return res;
}

bool READ_ADDR(cache_line_info** _CAP, int _ADDR)
{
	int i, j, res = false;
	int set = GET_CACHE_SET(_ADDR);
	int way = GET_CACHE_WAY(_ADDR);
	int tag = GET_CACHE_TAG(_ADDR);
	int line = GET_CACHE_LINE(_ADDR);
	i = set >> (int) log2(global_info.cache_line_size);
	i = i % global_info.nb_sets;
	for (j = 0; j < global_info.nb_ways; ++j) {
		int w_set = GET_CACHE_SET(_CAP[i][j].address);
		int w_tag = GET_CACHE_TAG(_CAP[i][j].address);
		int w_way = GET_CACHE_WAY(_CAP[i][j].address);
		if (w_tag == tag && _CAP[i][j].access == ACC_T_READ) {
			_CAP[i][j].access = ACC_T_READ;
			_CAP[i][j].address = _ADDR;
			res = true;
			break;
		} else if (_CAP[i][j].access == ACC_T_NONE) {
			_CAP[i][j].access = ACC_T_READ;
			_CAP[i][j].address = _ADDR;
			res = true;
			break;
		}
	}
	return res;
}

void HTM_simple_state_handler(HTM_simple_state_t * state, int pid,
	SIM_STATE event, simtime_t now, HTM_simple_buffer_t * buffer)
{

	state->pid = pid;
	state->now = now;

	switch (event) {
		case SMPL_INIT:
			HTM_simple_INIT(state);
			break;
		case SMPL_INTL:
			HTM_simple_INTL(state, buffer);
			break;
		case SMPL_TXIN:
			HTM_simple_TXIN(state, buffer);
			break;
		case SMPL_TXSP:
			HTM_simple_TXSP(state, buffer);
			break;
		case SMPL_WAKE:
			HTM_simple_WAKE(state, buffer);
			break;
		case SMPL_TRAN:
			HTM_simple_TRAN(state, buffer);
			break;
		case SMPL_TXCM:
			HTM_simple_TXCM(state, buffer);
			break;
		case SMPL_TXAB:
			HTM_simple_TXAB(state, buffer);
			break;
		case SMPL_SERL:
			HTM_simple_SERL(state, buffer);
			break;
		case SMPL_NOTX:
			HTM_simple_NOTX(state, buffer);
			break;
		case SMPL_CKLK:
			HTM_simple_CKLK(state, buffer);
			break;
		case SMPL_LOCK:
			HTM_simple_LOCK(state, buffer);
			break;
		case SMPL_ADDR:
			HTM_simple_ADDR(state, buffer);
			break;
		case SMPL_MRKV:
			HTM_simple_MRKV(state, buffer);
			break;
		case SMPL_MRKL:
			HTM_simple_MRKL(state, buffer);
			break;
		case SMPL_MRKD:
			HTM_simple_MRKD(state, buffer);
			break;
		case CHK_EXIT:
			HTM_simple_MRKV(state, buffer);
			break;
	}
}

void call_event(simtime_t next_tick, int pid, SIM_STATE event_code,
	int stamp, EVENT_ABORT_T ab_type)
{

	UTL_event_t event;
	HTM_simple_buffer_t buffer;

	UTL_event_init(&event);

	event.cnt = &buffer;
	event.cnt_size = sizeof (HTM_simple_buffer_t);
	event.pid = pid;
	event.event_code = event_code;

	buffer.controlstamp = stamp;
	buffer.ab_type = ab_type;

	SCHEDULE_EVENT(event, next_tick);
}

void call_CHK_EXIT(simtime_t now, int pid)
{
	call_event(now + CHECK_EXIT_DELTA_TIME, pid, CHK_EXIT, -1, -1);
}

void call_INTL(simtime_t now, int pid)
{
	call_event(NEXT_TIMESTAMP(now), pid, SMPL_INTL, -1, -1);
}

void call_TXIN(simtime_t now, int pid, int stamp)
{
	call_event(NEXT_TIMESTAMP(now), pid, SMPL_TXIN, stamp, -1);
}

void call_NOTX(simtime_t now, int pid)
{
	call_event(NEXT_TIMESTAMP(now), pid, SMPL_NOTX, -1, -1);
}

void call_TRAN(simtime_t now, int pid, int stamp)
{
	call_event(NEXT_TIMESTAMP(now), pid, SMPL_TRAN, stamp, -1);
}

void call_TXSP(simtime_t now, int pid, int stamp)
{
	call_event(NEXT_TIMESTAMP(now), pid, SMPL_TXSP, stamp, -1);
}

void call_TXAB(simtime_t now, int pid, int stamp, EVENT_ABORT_T ab_type)
{
	call_event(NEXT_TIMESTAMP(now), pid, SMPL_TXAB, stamp, ab_type);
}

void call_TXCM(simtime_t now, int pid, int stamp)
{
	call_event(NEXT_TIMESTAMP(now), pid, SMPL_TXCM, stamp, -1);
}

void call_SERL(simtime_t now, int pid)
{
	call_event(NEXT_TIMESTAMP(now), pid, SMPL_SERL, -1, -1);
}

void call_next_op(simtime_t now, int pid, int stamp,
	SMST_T cur_state)
{

	SIM_STATE state = cur_state == SMST_TRAN ? SMPL_TRAN :
		cur_state == SMST_SERL ? SMPL_SERL : SMPL_NOTX;

	call_event(NEXT_TIMESTAMP(now), pid, state, stamp, -1);
}

void call_broadcast_event(simtime_t now, int pid, SIM_STATE event_code,
	int addr, bool is_write, int stamp, SMST_T req_state,
	simtime_t last_serl_ts, int cur_budget, int prev_budget,
	bool is_abort, bool is_commit, bool abort_from_lock,
	bool commit_serl)
{

	UTL_event_t event;
	HTM_simple_buffer_t buffer;
	int i;
	simtime_t next_tick;

	UTL_event_init(&event);

	event.cnt = &buffer;
	event.cnt_size = sizeof (HTM_simple_buffer_t);
	event.event_code = event_code;

	buffer.pid = pid;
	buffer.addr = addr;
	buffer.is_write = is_write;
	buffer.controlstamp = stamp;
	buffer.req_state = req_state;
	buffer.last_serl_ts = last_serl_ts;
	buffer.cur_budget = cur_budget;
	buffer.prev_budget = prev_budget;
	buffer.is_abort = is_abort;
	buffer.is_commit = is_commit;
	buffer.is_commit_serl = commit_serl;
	buffer.is_abort_lock = abort_from_lock;

	for (i = 0; i < global_info.tot_nb_thrs; ++i) {
		event.pid = global_info.thrs_pids[i];
		if (global_info.thrs_pids[i] == pid) {
			// waits a larger time to the responds to come
			next_tick = NEXT_N_TIMESTAMP(now, 3);
			buffer.type = IS_LAST;
			SCHEDULE_EVENT(event, next_tick);
		} else {
			// the others respond
			next_tick = now;
			buffer.type = IS_REQUEST;
			SCHEDULE_EVENT(event, next_tick);
		}
	}
}

void call_CKLK(simtime_t now, int pid, simtime_t last_serl_ts)
{
	call_broadcast_event(now, pid, SMPL_CKLK, -1, -1, -1, -1,
		last_serl_ts, -1, -1, false, false, false, false);
}

void call_LOCK(simtime_t now, int pid, simtime_t last_serl_ts)
{
	call_broadcast_event(now, pid, SMPL_LOCK, -1, -1, -1, -1,
		last_serl_ts, -1, -1, false, false, false, false);
}

void call_WAKE(simtime_t now, int pid)
{
	call_broadcast_event(now, pid, SMPL_WAKE, -1, -1, -1, -1, -1,
		-1, -1, false, false, false, false);
}

void call_ADDR(simtime_t now, int pid, int addr, bool is_write,
	int stamp, SMST_T req_state)
{

	call_broadcast_event(now, pid, SMPL_ADDR, addr, is_write, stamp,
		req_state, -1, -1, -1, false, false, false, false);
}

void clearState(mod_state_t mrkv)
{
	int i;
	for (i = 0; i < global_info.budget + 2; ++i) {
		mrkv[i] = 0;
	}
}

void updateState(mod_state_t mrkv, int budget, SMST_T state)
{
	if (state == SMST_NOTX) {
		mrkv[global_info.budget + 1]++;
	} else {
		mrkv[global_info.budget - budget]++;
	}
}

/**
 * Updates the state.
 * 
 * @param mrkv the current state (it will be updated)
 * @param budget the budget before the commit/abort
 * @param isCommit
 * @param isAbort
 * @param startXAct
 */
void updateWithTransition(mod_state_t mrkv, int budget, bool isCommit,
	bool isAbort, bool startXAct)
{

	// TODO: capacities

	int i;

	if (isCommit && !startXAct) {
		// start non-transaction
		if (mrkv[global_info.budget - budget] != 0) {
			mrkv[global_info.budget - budget]--;
			mrkv[global_info.budget + 1]++;
		}
	} else if (isCommit && startXAct) {
		if (mrkv[global_info.budget - budget] != 0) {
			mrkv[global_info.budget - budget]--;
			mrkv[0]++;
		}
	} else if (isAbort && budget == 1) {
		// fall-back
		mrkv[global_info.budget - 1]--;
		mrkv[global_info.budget] += mrkv[global_info.budget - 1];
		for (i = global_info.budget - 1; i > 0; --i) {
			mrkv[i] = mrkv[i - 1];
		}
		mrkv[global_info.budget]++;
		mrkv[0] = 0;
	} else if (isAbort) {
		if (mrkv[global_info.budget] == 0) {
			mrkv[global_info.budget - budget]--;
			mrkv[global_info.budget - budget + 1]++;
		}
	}
}

void call_MRKV(HTM_simple_state_t* state)
{
	// TODO: erase
}

void call_MRKL(simtime_t now, int pid, mod_state_t mrkl_state)
{
	// TODO: erase
}

void call_MRKD(HTM_simple_state_t* state)
{

	call_broadcast_event(state->now, state->pid, SMPL_MRKD, -1, -1, -1,
		state->cur_state, -1, state->cur_budget, state->prev_budget,
		state->mrkd_is_abort, state->mrkd_is_commit,
		state->abort_from_lock, state->commit_from_serl);

	clearState(state->mrkd_state);

	state->other_budget[state->pid] = state->cur_budget;
	state->other_prev_budget[state->pid] = state->prev_budget;
	state->other_xact[state->pid] = state->cur_state;
}

void respond_broadcast_event(simtime_t now, int pid, int requester_pid,
	SIM_STATE event_code, bool has_lock, bool has_addr, bool is_write,
	SMST_T cur_state, int cur_budget, int prev_budget, int cur_inst,
	bool respond_neighbors, bool is_abort, bool is_commit,
	bool is_abort_lock, bool is_commit_serl, bool had_to_abort)
{

	UTL_event_t event;
	HTM_simple_buffer_t buffer;

	UTL_event_init(&event);

	event.cnt = &buffer;
	event.cnt_size = sizeof (HTM_simple_buffer_t);
	event.event_code = event_code;
	event.pid = requester_pid;

	buffer.pid = pid;
	buffer.has_lock = has_lock;
	buffer.type = IS_RESPONSE;
	buffer.has_addr = has_addr;
	buffer.is_write = is_write;
	buffer.req_state = cur_state;
	buffer.cur_budget = cur_budget;
	buffer.prev_budget = prev_budget;
	buffer.cur_inst = cur_inst;
	buffer.is_abort = is_abort;
	buffer.is_abort_lock = is_abort_lock;
	buffer.is_commit = is_commit;
	buffer.is_commit_serl = is_commit_serl;
	buffer.had_to_abort = had_to_abort;

	SCHEDULE_EVENT(event, NEXT_TIMESTAMP(now));

	if (respond_neighbors) {
		// also respond to all the others
		int i;
		for (i = 0; i < global_info.tot_nb_thrs; ++i) {
			simtime_t next_tick;
			event.pid = global_info.thrs_pids[i];
			if (global_info.thrs_pids[i] != requester_pid
				&& global_info.thrs_pids[i] != pid) {

				next_tick = now;
				buffer.type = IS_NEIGHBOR;
				SCHEDULE_EVENT(event, next_tick);
			}
		}
	}
}

void respond_CKLK(simtime_t now, int pid, int requester_pid, bool has_lock)
{
	respond_broadcast_event(now, pid, requester_pid, SMPL_CKLK, has_lock,
		-1, -1, -1, -1, -1, -1, false, false, false, false, false, -1);
}

void respond_LOCK(simtime_t now, int pid, int requester_pid,
	bool has_lock, bool had_to_abort)
{

	respond_broadcast_event(now, pid, requester_pid, SMPL_LOCK, has_lock,
		-1, -1, -1, -1, -1, -1, false, false, false, false, false,
		had_to_abort);
}

void respond_ADDR(simtime_t now, int pid, int requester_pid,
	bool has_addr, bool is_write)
{

	respond_broadcast_event(now, pid, requester_pid, SMPL_LOCK,
		-1, has_addr, is_write, -1, -1, -1, -1, false, false,
		false, false, false, -1);
}

void respond_MRKV(simtime_t now, int pid, int requester_pid,
	SMST_T cur_state, int cur_budget)
{

	respond_broadcast_event(now, pid, requester_pid, SMPL_MRKV,
		-1, -1, -1, cur_state, cur_budget, -1, -1,
		false, false, false, false, false, -1);
}

void respond_MRKL(simtime_t now, int pid, int requester_pid,
	SMST_T cur_state, int cur_budget, int cur_inst)
{

	respond_broadcast_event(now, pid, requester_pid, SMPL_MRKL,
		-1, -1, -1, cur_state, cur_budget, -1, cur_inst,
		false, false, false, false, false, -1);
}

void respond_MRKD(simtime_t now, int pid, int requester_pid,
	SMST_T cur_state, int cur_budget, int prev_budget, int cur_inst,
	bool is_abort, bool is_commit,
	bool is_abort_lock, bool is_commit_serl)
{

	respond_broadcast_event(now, pid, requester_pid, SMPL_MRKD,
		-1, -1, -1, cur_state, cur_budget, prev_budget, cur_inst, true,
		is_abort, is_commit, is_abort_lock, is_commit_serl, -1);
}

void HTM_simple_INIT(HTM_simple_state_t * state)
{
	int pid = state->pid;
	simtime_t now = state->now;

	set_zero(state, HTM_simple_state_t, 1);

	state->addrs = new(int, global_info.L);
	state->is_write = new(bool, global_info.L);
	state->addrs_size = global_info.L;
	state->cur_state = global_info.prob_tran < 1.0 ? SMST_NOTX : SMST_TRAN;
	state->last_serl_ts = pid;

	state->is_running = false;
	state->some_using_lock = false;

	state->others_avg_L = 0;

	state->stats.count_C_L = 0;
	state->stats.avg_C_L = (double) global_info.C / (double) global_info.L;
	state->stats.avg_C = global_info.C;
	state->stats.avg_L = global_info.L;

	state->other_budget = new_init(int, global_info.tot_nb_thrs);
	state->other_prev_budget = new_init(int, global_info.tot_nb_thrs);
	state->other_xact = new(SMST_T, global_info.tot_nb_thrs);

	state->c_genetated_values = new(double, MAX_C_STORAGE);
	state->c_genetated_values_index = -1;
	state->commit_from_serl = false;

#ifdef __CAPACITIES__
	INIT_CAPACITIES(state->capacities);
#endif

	if (lps_budget == NULL ) {
		lps_budget = new_init(int, global_info.tot_nb_thrs);
	}

#ifndef __DISABLE_MRKV__

	if (prev_mrkd_state == NULL) {
		prev_mrkd_state = mod_newState();
		next_mrkd_state = mod_newState();
		budgets = new_init(int, global_info.budget);
		states = new_init(SMST_T, global_info.budget);
	}

	state->mrkd_counter = 0;
	state->mrkd_pp_state = mod_newState();
	state->mrkd_prev_state = mod_newState();
	state->mrkd_state = mod_newState();
	state->mrkd_next_state = mod_newState();
	state->mrkd_is_abort = false;
	state->mrkd_is_commit = false;
	if (!mod_isInit()) {
		mod_setInit(true);
		mod_set_L(global_info.L);
		mod_set_C(global_info.C);
		mod_set_D(global_info.D);
		mod_set_P_read(global_info.prob_read);
		mod_generateStates(global_info.tot_nb_thrs, global_info.budget);
	}
#endif

	call_CHK_EXIT(now + pid, pid);
}

void set_C_L(HTM_simple_state_t * state)
{
#ifdef __EXP_C__
	//  double C_L = (double) global_info.C / (double) global_info.L;
	double new_C_L;

	state->cur_L = global_info.L; // deterministic
	//  state->cur_L = Expent(global_info.L);
	//  state->cur_L = max(min(state->cur_L, global_info.D), 1);

	double r = GENERATE_RANDOM_C(state->c_genetated_values,
		&state->c_genetated_values_index);

	state->cur_C = r;

	new_C_L = (double) state->cur_C / (double) state->cur_L;

	state->stats.count_C_L++;
	state->stats.avg_C_L = state->stats.avg_C_L + (new_C_L
		- state->stats.avg_C_L) / (double) state->stats.count_C_L;
	state->stats.avg_C = state->stats.avg_C + (state->cur_C
		- state->stats.avg_C) / (double) state->stats.count_C_L;
	state->stats.avg_L = state->stats.avg_L + (state->cur_L
		- state->stats.avg_L) / (double) state->stats.count_C_L;

	if (state->cur_L > state->addrs_size) {
		state->addrs = realloc(state->addrs, state->cur_L * sizeof (int));
		state->is_write = realloc(state->is_write, state->cur_L * sizeof (bool));
		state->addrs_size = state->cur_L;
	}

#else
	state->cur_C = global_info.C;
	state->cur_L = min(global_info.L, global_info.D);
#endif
}

void HTM_simple_INTL(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now;

	// toss a coin and see if it is a TRAN or NOTX

	state->lock_free = true;
	state->addr_len = 0;
	state->prev_budget = state->cur_budget;
	state->cur_budget = global_info.budget;
	lps_budget[pid] = state->cur_budget;

#ifndef __EXP_C_RE__
	set_C_L(state);
#endif

	if (DO_INIT_TRANS) {
		// TRAN

#ifndef __DISABLE_MRKV__
		budgets[pid] = global_info.budget;
		states[pid] = SMST_TRAN;
#endif
		state->cur_state = SMST_TRAN;
		state->controlstamp++;
		call_TXIN(now, pid, state->controlstamp);

	} else {
		// NOTX

#ifndef __DISABLE_MRKV__
		budgets[pid] = global_info.budget;
		states[pid] = SMST_NOTX;
#endif
		state->cur_state = SMST_NOTX;
		call_NOTX(now, pid);
	}
}

void HTM_simple_TXIN(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int i, pid = state->pid;
	simtime_t now = state->now;

	if (buffer->controlstamp != state->controlstamp) {
		// then it aborted somewhere
		return;
	}

#ifdef __CAPACITIES__
	SET_ZERO_CAPACITIES(state->capacities);

	for (i = 0; i < global_info.nb_garbage_granules; ++i) {
		if (global_info.is_continuous_garbage) {

			READ_ADDR(state->capacities, global_info.start_garbage_granule_addr
				+ i * global_info.cache_line_size);

		} else {

			READ_ADDR(state->capacities,
				global_info.random_garbage_granule_addr[i]);
		}
	}
#endif

	state->addr_len = 0;
	state->cur_state = state->cur_budget > 0 ? SMST_TRAN : SMST_SERL;
	state->lock_free = true;
#ifdef __EXP_C_RE__
	set_C_L(state);
#endif

	call_CKLK(now, pid, state->last_serl_ts);
}

void HTM_simple_TXSP(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now, now_lat;
	STATS_T stat = state->cur_state == SMST_TRAN ? TXSP : SRSP;

	SMPL_STATS_INC(now, state->stats, stat, -1, -1, -1);

	//  now_lat = state->cur_state == SMST_TRAN ?
	//          now + 2 * DELTA_TIME : now + DELTA_TIME;
	now_lat = state->cur_budget == 0 ? NEXT_TIMESTAMP(now)
		: NEXT_N_TIMESTAMP(now, 10);

	call_TXIN(now_lat, pid, state->controlstamp);
}

void HTM_simple_WAKE(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{
	int pid = state->pid;
	simtime_t now = state->now, lat_spin = global_info.lat_spin;

	switch (buffer->type) {
		case IS_REQUEST:
			if (!state->is_running) {
				state->controlstamp++;
				state->some_using_lock = false;
				call_TXSP(now, pid, state->controlstamp);
			}
			break;
		case IS_RESPONSE:
			// nothing
			break;
		case IS_LAST:
			// nothing
			break;
	}
}

void HTM_simple_TXCM(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now;

	if (buffer->controlstamp != state->controlstamp) {
		// it aborted somewhere
		return;
	}

	SMPL_STATS_INC(now, state->stats, TXCM,
		state->cur_budget, -1, -1);

	state->controlstamp++; // committed
	state->is_running = false;
	call_INTL(now, pid);
	state->commit_from_serl = false;

#ifndef __DISABLE_MRKV__
	state->mrkd_is_abort = false;
	state->abort_from_lock = false;
	state->mrkd_is_commit = true;
	state->commit_from_serl = false;
	call_MRKD(state);
#endif
}

void HTM_simple_TXAB(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now, lat_abort = global_info.lat_pc_txab;

	if (buffer->controlstamp != state->controlstamp) {
		// it aborted somewhere else
		return;
	}

#ifndef __DISABLE_MRKV__
	if (!state->some_using_lock && state->cur_budget > 1) {
		// the budget was already updated, do not count fallback abort
		state->mrkd_is_abort = true;
		state->abort_from_lock = false;
		state->mrkd_is_commit = false;
		state->commit_from_serl = false;
		call_MRKD(state);
	}
#endif

	SMPL_STATS_INC(now, state->stats, TXAB, -1,
		state->addr_len, buffer->ab_type);
	state->controlstamp++; // aborted
	state->abort_reason = buffer->ab_type;

	state->abort_from_lock = state->some_using_lock;

	if (!state->some_using_lock) {
		// the budget was already updated

		state->prev_budget = state->cur_budget;
		state->cur_budget--;
		if (state->cur_budget < 0) {
			state->cur_budget = 0;
		}
		lps_budget[pid] = state->cur_budget;
	}
	call_TXIN(NEXT_N_TIMESTAMP(now, 3), pid, state->controlstamp);
}

bool check_repeated_addr(int* addrs, int len, int addr)
{
	int i;

	for (i = 0; i < len; ++i) {
		if (addrs[i] == addr) {
			return true;
		}
	}

	return false;
}

void HTM_simple_TRAN(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int i, pid = state->pid, L = state->cur_L, addr;
	simtime_t now = state->now, commit_cost = global_info.lat_pc_txcm,
		op_cost = LAT_NEXT_OP(state), now_lat;
	bool is_write, write_suc = false, read_suc = false;

	if (buffer->controlstamp != state->controlstamp || state->some_using_lock) {
		// it aborted somewhere
		state->is_running = false;
		return;
	}

	state->is_running = true;

	if (state->addr_len < L) {
		// continue
		addr = CHOOSE_ADDR(state->addrs, state->is_write, state->addr_len);
		is_write = state->is_write[state->addr_len];

#ifdef __CAPACITIES__

		if (global_info.is_continuous_garbage) {
			while (addr > global_info.start_garbage_granule_addr
				&& addr < global_info.end_garbage_granule_addr) {
				addr = CHOOSE_ADDR(state->addrs, state->is_write, state->addr_len);
			}
		} else {
			while (check_repeated_addr(global_info.random_garbage_granule_addr,
				global_info.nb_garbage_granules, addr)) {
				addr = CHOOSE_ADDR(state->addrs, state->is_write, state->addr_len);
			}
		}

		if (is_write) {
			write_suc = WRITE_ADDR(state->capacities, addr);
		} else {
			read_suc = READ_ADDR(state->capacities, addr);
		}
		if ((is_write && !write_suc) || (!is_write && !read_suc)) {
			call_TXAB(now, pid, state->controlstamp, SMPL_CAPA_CNFL);
		} else {
			now_lat = now + op_cost;
			call_ADDR(now_lat, pid, addr, is_write,
				state->controlstamp, state->cur_state);
		}
#else
		now_lat = now + op_cost;
		call_ADDR(now_lat, pid, addr, is_write,
			state->controlstamp, state->cur_state);
#endif
	} else {
		// commit
		call_TXCM(now, pid, state->controlstamp);
	}
}

void HTM_simple_SERL(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid, L = state->cur_L, addr;
	simtime_t now = state->now,
		C = state->cur_C,
		next_ts = now + C;

	state->is_running = true;

	// if this always commit, it is not needed to process the accesses
	SMPL_STATS_INC(now + C, state->stats, SERL, -1, -1, -1);
	state->has_lock = false;
	//  state->cur_state = SMST_TRAN;
	//  state->cur_budget = global_info.budget;
	call_INTL(NEXT_TIMESTAMP(next_ts), pid);
	call_WAKE(next_ts, pid);
	state->last_serl_ts = next_ts;
	state->addr_len = 0;

	state->is_running = false;

#ifndef __DISABLE_MRKV__
	state->now = next_ts;
	state->mrkd_is_abort = false;
	state->abort_from_lock = false;
	state->mrkd_is_commit = true;
	state->commit_from_serl = true;
	call_MRKD(state);
	state->now = now;
#endif
}

void HTM_simple_NOTX(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid, L = state->cur_L, addr;
	simtime_t now = state->now, now_lat,
		lat_access = LAT_NEXT_OP(state);
	bool is_write;

	state->is_running = true;

	if (state->addr_len < L) {
		// continue
		addr = CHOOSE_ADDR(state->addrs, state->is_write, state->addr_len);
		is_write = state->is_write[state->addr_len];
		now_lat = now + lat_access;
		call_ADDR(now_lat, pid, addr, is_write,
			state->controlstamp, state->cur_state);
	} else {
		// done
		SMPL_STATS_INC(now, state->stats, NOTX, -1, -1, -1);
		state->has_lock = false;
		call_INTL(now, pid);
		//#ifndef __DISABLE_MRKV__
		//    call_MRKD(state);
		//#endif
	}
}

void HTM_simple_CKLK(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now, lat_begin = global_info.lat_pc_txbg;

	switch (buffer->type) {
		case IS_REQUEST:
			if (state->cur_state == SMST_SERL
				&& buffer->last_serl_ts > state->last_serl_ts) {
				respond_CKLK(now, pid, buffer->pid, true);
			} else {
				respond_CKLK(now, pid, buffer->pid, state->has_lock);
			}
			break;
		case IS_RESPONSE:
			state->lock_free = state->lock_free && !buffer->has_lock;
			break;
		case IS_NEIGHBOR:
			break;
		case IS_LAST:
			if (state->lock_free && !state->some_using_lock) {
				if (state->cur_budget > 0) {
					call_TRAN(now, pid, state->controlstamp);
				} else {
					state->cur_state = SMST_SERL;
					state->lock_free = true;
					state->count_aborts = 0;
					call_LOCK(now, pid, state->last_serl_ts);
				}
			} else {
				// must spin for a while (SERL calls WAKE on end)
				state->is_running = false;
			}
			break;
	}
}

void HTM_simple_LOCK(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now, lat_lock = global_info.lat_lock;
	bool has_lock_response = state->has_lock;
	bool has_to_abort = state->cur_state == SMST_TRAN && state->is_running
		&& !state->some_using_lock;

	switch (buffer->type) {
		case IS_REQUEST:

			if (state->cur_state == SMST_SERL
				&& buffer->last_serl_ts > state->last_serl_ts) {

				has_lock_response = true;
				respond_LOCK(now, pid, buffer->pid, has_lock_response, has_to_abort);
			} else {
				respond_LOCK(now, pid, buffer->pid, has_lock_response, has_to_abort);
			}

#ifndef __DISABLE_LOCK__
			if (has_to_abort) {
				// abort due to lock
				state->is_running = false;
				state->prev_budget = state->cur_budget;
				state->cur_budget--;
				if (state->cur_budget < 0) {
					state->cur_budget = 0;
				}
				lps_budget[pid] = state->cur_budget;
				state->abort_reason = SMPL_LOCK_CNFL;
				call_TXAB(now, pid, state->controlstamp, SMPL_LOCK_CNFL);
			}
#endif

			state->some_using_lock = !state->has_lock && !has_lock_response;

			break;
		case IS_RESPONSE:
			state->lock_free = state->lock_free && !buffer->has_lock;
			if (buffer->had_to_abort) {
				state->count_aborts++;
			}
			break;
		case IS_NEIGHBOR:
			break;
		case IS_LAST:

			SMPL_STATS_INC(now, state->stats, SRSP, -1, -1, -1);

			if (state->lock_free) {
				state->has_lock = true;
				call_SERL(NEXT_N_TIMESTAMP(now, 6), pid);

#ifndef __DISABLE_MRKV__
				if (state->count_aborts > 0) {
					state->mrkd_is_commit = false;
					state->commit_from_serl = false;
					state->mrkd_is_abort = true;
					state->abort_from_lock = true;
					state->now = NEXT_N_TIMESTAMP(state->now, 3);
					call_MRKD(state);
				} else {
					// no fallback, should call MRKD?
					state->abort_from_lock = true;
				}
#endif

			} else {
				// must spin for a while
				state->is_running = false;
			}
			break;
	}
}

void HTM_simple_ADDR(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now;
	int i, addr = buffer->addr;
	bool is_write = buffer->is_write;

	switch (buffer->type) {
		case IS_REQUEST:

			for (i = 0; i < state->addr_len; ++i) {
				if (state->addrs[i] == addr) {
					if ((is_write || state->is_write[i])
						&& state->cur_state == SMST_TRAN) {
						// abort, W/R or W/W conflict
						EVENT_ABORT_T ab_type = buffer->req_state == SMST_TRAN ?
							SMPL_TRAN_CNFL : SMPL_NOTX_CNFL;
						call_TXAB(now, pid, state->controlstamp, ab_type);
					}
				}
			}
			break;
		case IS_RESPONSE:
			// do nothing, the others must abort upon conflict
			break;
		case IS_NEIGHBOR:
			break;
		case IS_LAST:
			if (buffer->controlstamp != state->controlstamp) {
				// it aborted somewhere else
				return;
			}
			state->addr_len++;
			call_next_op(now, pid, buffer->controlstamp, state->cur_state);
			break;
	}
}

void HTM_simple_MRKV(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now;

	switch (buffer->type) {
		case IS_REQUEST:
			respond_MRKV(now, pid, buffer->pid, state->cur_state, state->cur_budget);
			break;
		case IS_RESPONSE:
			updateState(state->mrkd_pp_state, buffer->cur_budget, buffer->req_state);
			break;
		case IS_NEIGHBOR:
			break;
		case IS_LAST:
			updateState(state->mrkd_pp_state, state->cur_budget, state->cur_state);
			mod_addAcc(now, state->mrkd_pp_state);
			break;
	}
}

void HTM_simple_MRKL(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int pid = state->pid;
	simtime_t now = state->now;
	double other_avg_L;

	switch (buffer->type) {
		case IS_REQUEST:
			respond_MRKL(now, pid, buffer->pid, state->cur_state,
				state->cur_budget, state->addr_len);
			break;
		case IS_RESPONSE:
			updateState(state->mrkd_prev_state, buffer->cur_budget, buffer->req_state);
			state->others_avg_L += buffer->cur_inst;
			break;
		case IS_NEIGHBOR:
			break;
		case IS_LAST:
			updateState(state->mrkd_prev_state, state->cur_budget, state->cur_state);
			other_avg_L = (state->others_avg_L + state->addr_len) /
				(double) (global_info.tot_nb_thrs - 1);
			mod_addAvgL(other_avg_L, state->mrkd_prev_state);
			state->others_avg_L = 0;
			break;
	}
}

void set_states(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int i;

	clearState(state->mrkd_state);
	clearState(state->mrkd_next_state);
	for (i = 0; i < global_info.tot_nb_thrs; ++i) {

		if (i == state->pid && buffer->is_abort_lock) {

			//			updateState(state->mrkd_state, 1, state->other_xact[i]);
			//			updateState(state->mrkd_next_state, 0, state->other_xact[i]);

			updateState(state->mrkd_state, 1, state->other_xact[i]);
			updateState(state->mrkd_next_state, 0, state->other_xact[i]);

		} else if (i == state->pid && buffer->is_commit_serl) {

			//			updateState(state->mrkd_state, 0, state->other_xact[i]);
			//			updateState(state->mrkd_next_state, global_info.budget,
			//				state->other_xact[i]);

			updateState(state->mrkd_state, 0, state->other_xact[i]);
			updateState(state->mrkd_next_state, global_info.budget,
				state->other_xact[i]);

		} else {

			//      TODO: sometimes the xacts do not fall back correctly
			//      if (buffer->is_abort_lock) {
			//      }

			//			updateState(state->mrkd_state, state->other_prev_budget[i],
			//				state->other_xact[i]);
			//			updateState(state->mrkd_next_state, state->other_budget[i],
			//				state->other_xact[i]);

			updateState(state->mrkd_state, state->other_budget[i],
				state->other_xact[i]);
			updateState(state->mrkd_next_state, lps_budget[i],
				state->other_xact[i]);
		}

		if (state->mrkd_state[global_info.budget] > 0
			&& !buffer->is_commit_serl) {
			// can't abort in serialized path
			mod_cpyState(state->mrkd_state, state->mrkd_next_state);
			shift_left_mrkd_state(state->mrkd_state);
		}

		if (state->mrkd_state[global_info.budget - 1] == global_info.tot_nb_thrs
			&& !buffer->is_abort_lock) {
			// non-dang abort in a dang threads only state: wrong state
			mod_cpyState(state->mrkd_state, state->mrkd_next_state);
			state->mrkd_state[global_info.budget - buffer->cur_budget]--;
			state->mrkd_state[global_info.budget - state->cur_budget]++;
		}
	}
}

void HTM_simple_MRKD(HTM_simple_state_t * state,
	HTM_simple_buffer_t * buffer)
{

	int i, pid = state->pid;
	simtime_t now = state->now;
	double delta = now - state->mrkd_ts;
	double other_avg_L;

	switch (buffer->type) {
		case IS_REQUEST:
			state->other_budget[buffer->pid] = buffer->cur_budget;
			state->other_prev_budget[buffer->pid] = buffer->prev_budget;
			state->other_xact[buffer->pid] = buffer->req_state;
			state->other_budget[pid] = state->cur_budget;
			state->other_prev_budget[pid] = state->prev_budget;
			state->other_xact[pid] = state->cur_state;
			respond_MRKD(now, pid, buffer->pid, state->cur_state, state->cur_budget,
				state->prev_budget, state->addr_len, buffer->is_abort,
				buffer->is_commit, buffer->is_abort_lock,
				buffer->is_commit_serl);
			state->mrkd_ts = now; // to not duplicate the accumulated time

			if (global_info.tot_nb_thrs == 2) {
				state->mrkd_counter = 0;

				state->pid_entered_mrkd = false;
			}

			state->prev_budget = state->cur_budget;
			state->mrkd_ts = now;

			break;
		case IS_RESPONSE:
			state->other_budget[buffer->pid] = buffer->cur_budget;
			state->other_prev_budget[buffer->pid] = buffer->prev_budget;
			state->other_xact[buffer->pid] = buffer->req_state;
			state->others_avg_L += buffer->cur_inst;
			break;
		case IS_NEIGHBOR:
			state->other_budget[buffer->pid] = buffer->cur_budget;
			state->other_prev_budget[buffer->pid] = buffer->prev_budget;
			state->other_xact[buffer->pid] = buffer->req_state;
			state->mrkd_ts = now; // to not duplicate the accumulated time

			state->mrkd_counter++;
			if (state->mrkd_counter >= global_info.tot_nb_thrs - 2) {
				state->mrkd_counter = 0;
				set_states(state, buffer);

				state->pid_entered_mrkd = false;

				mod_cpyState(state->mrkd_pp_state, state->mrkd_prev_state);
				mod_cpyState(state->mrkd_prev_state, state->mrkd_state);
			}

			break;
		case IS_LAST:

			for (i = 0; i < global_info.tot_nb_thrs; ++i) {
				if (state->other_xact[i] != SMST_NOTX
					&& state->other_xact[i] != SMST_SERL
					&& state->other_xact[i] != SMST_TRAN) {
					return;
				}
			}

			if (state->pid_entered_mrkd && buffer->is_abort
				&& !buffer->is_abort_lock) {

				state->other_budget[pid] = state->cur_budget;
				state->other_prev_budget[pid] = state->prev_budget;
			} else {
				state->other_budget[pid] = buffer->cur_budget;
				state->other_prev_budget[pid] = buffer->prev_budget;
			}

			set_states(state, buffer);

			if (!is_mrkd_init) {
				is_mrkd_init = true;
				mod_cpyState(next_mrkd_state, state->mrkd_state);
			}

			// TODO: prev_mrkd_state is the state where the event happened
			//      mod_addDelta(delta_glob, prev_mrkd_state);
			//      delta_glob = now;
			//      if (mod_cmpState(next_mrkd_state, prev_mrkd_state) !=) {
			//        mod_incEnter(next_mrkd_state);
			//        mod_incExit(prev_mrkd_state);
			//      }

			if (!update_global_state(
				buffer->cur_budget,
				buffer->is_commit,
				buffer->is_commit_serl,
				buffer->is_abort,
				buffer->is_abort_lock
				)) {
				// wrong state detected, fallback to the sampled one
				mod_cpyState(next_mrkd_state, state->mrkd_state);
			}

			// L != in SERL
			other_avg_L = (state->others_avg_L + state->addr_len) /
				(double) global_info.tot_nb_thrs;
			mod_addAvgL(other_avg_L, state->mrkd_state);
			state->others_avg_L = 0;

			mod_addDelta(delta, state->mrkd_state);

			buffer->cur_budget;

			if (state->mrkd_is_abort) {
				for (i = 0; i < global_info.tot_nb_thrs; ++i) {
					mod_incAbort(state->mrkd_state);
				}

				if (buffer->is_abort_lock) {
					mod_addDandAbort(delta, state->mrkd_state);
				} else {
					mod_addNonDandAbort(delta, state->mrkd_state);
				}

			} else if (state->mrkd_is_commit) {
				mod_incCommit(state->mrkd_state);
				mod_addCommit(delta, state->mrkd_state);
			}

			if (mod_cmpState(state->mrkd_state, state->mrkd_next_state) != 0) {
				// exit state
				mod_incEnter(state->mrkd_next_state);
				mod_incExit(state->mrkd_state);
			} else {
				// continue state
				mod_incContinue(state->mrkd_state);
			}
			mod_cpyState(state->mrkd_pp_state, state->mrkd_prev_state);
			mod_cpyState(state->mrkd_prev_state, state->mrkd_state);

			if (state->commit_from_serl) {
				// if the thread commits again the budget is wrong
				state->prev_budget = global_info.budget;
			}

			state->pid_entered_mrkd = true;
			state->mrkd_ts = now;

			break;
	}
}
