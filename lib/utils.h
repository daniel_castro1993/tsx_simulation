#ifndef __UTILS_H
#  define __UTILS_H

#  include <stdio.h>
#  include <stdlib.h>
#  include <string.h>
#  include <float.h>
#  include <math.h>
#  include "utils_logs.h"
#  include <ROOT-Sim.h>

#  define DELTA_TIME                1e-6

#  define NEXT_TIMESTAMP(_TS)       ({ nextafter(_TS, DBL_MAX); })
#  define NEXT_N_TIMESTAMP(_TS, _N) ({                                        \
  double __ts = _TS;                                                          \
  int __i = _N;                                                               \
  while (__i-- > 0) {                                                         \
    __ts = nextafter(__ts, DBL_MAX);                                          \
  }                                                                           \
  __ts;                                                                       \
})

// possible operation
#  define OP_LOCK       0x0001
#  define OP_UNLOCK     0x0002
#  define OP_LOCK_CHECK 0x0004
#  define OP_READ       0x0008
#  define OP_WRITE      0x0010
#  define OP_TX_BEGIN   0x0020
#  define OP_TX_READ    0x0040
#  define OP_TX_WRITE   0x0080
#  define OP_TX_ABORT   0x0100
#  define OP_TX_COMMIT  0x0200
#  define OP_NOP        0x0400
#  define OP_DO_STUFF   0x0800

#  define PRINT_OP(op) (op == OP_LOCK ? "OP_LOCK" :                           \
                        op == OP_UNLOCK ? "OP_UNLOCK" :                       \
                        op == OP_LOCK_CHECK ? "OP_LOCK_CHECK" :               \
                        op == OP_READ ? "OP_READ" :                           \
                        op == OP_WRITE ? "OP_WRITE" :                         \
                        op == OP_TX_BEGIN ? "OP_TX_BEGIN" :                   \
                        op == OP_TX_READ ? "OP_TX_READ" :                     \
                        op == OP_TX_WRITE ? "OP_TX_WRITE" :                   \
                        op == OP_TX_ABORT ? "OP_TX_ABORT" :                   \
                        op == OP_TX_COMMIT ? "OP_TX_COMMIT" :                 \
                        op == OP_NOP ? "OP_NOP" :                             \
                        op == OP_DO_STUFF ? "OP_DO_STUFF" : "")

#  define SCHD_NEXT_EVENT(_NEXT_EVENT, _NEXT_TICK, _PID, _EVT_TYPE)           \
  _NEXT_EVENT.ts = _NEXT_TICK;                                                \
  _NEXT_EVENT.pid = _PID;                                                     \
  _NEXT_EVENT.event_code = _EVT_TYPE;                                         \
  if (_NEXT_EVENT.pid < 0 || _NEXT_EVENT.pid >= n_prc_tot) {                  \
    printf("ERROR[SCHD_NEXT_EVENT]: invalid pid %i \n", _NEXT_EVENT.pid);     \
  }                                                                           \
  UTL_schedule(&_NEXT_EVENT);

//#  define SCHEDULE_EVENT(_NEXT_EVENT, _NEXT_TICK)                             \
//  _NEXT_EVENT.ts = _NEXT_TICK;                                                \
//  UTL_schedule(&_NEXT_EVENT);                                                 \
//  if (_NEXT_EVENT.pid < 0 || _NEXT_EVENT.pid >= n_prc_tot) {                  \
//    printf("ERROR[SCHEDULE_EVENT]: invalid pid %i \n", _NEXT_EVENT.pid);      \
//  }                                                                           \
//  if (_NEXT_EVENT.is_destroy) {                                               \
//    UTL_event_destroy(&_NEXT_EVENT, false);                                   \
//  }

// stats

typedef enum _STATS_T {
  NOTX = 0, // non-transactional
  TXCM, // transactions that commit
  TXAB, // transactions that abort
  TXSP, // transactions that spin
  SERL, // in serialized mode
  SRSP // waiting for serialized mode
} STATS_T;
#  define NB_STATS        6 // number of statistics

#  define BEGIN_FOR_STAT(_STAT) for(_STAT = 0; _STAT < NB_STATS; _STAT++) {
#  define END_FOR_STAT          }
#  define STAT_STR(_STAT) (_STAT == NOTX ? "NOTX" :                           \
                           _STAT == TXCM ? "TXCM" :                           \
                           _STAT == TXAB ? "TXAB" :                           \
                           _STAT == TXSP ? "TXSP" :                           \
                           _STAT == SERL ? "SERL" :                           \
                           _STAT == SRSP ? "SRSP" : "")

typedef struct _UTL_callback {
  int pid;
  simtime_t ts;
  unsigned int event_code;
} UTL_callback_t;

typedef struct _UTL_event {
  int pid;
  simtime_t ts;
  unsigned int event_code;
  void * cnt;
  int cnt_size;
  void (*scheduler) (struct _UTL_event*);

  bool is_destroy;

  // frees cnt sub pointer (do not free(cnt) here)
  void (*cnt_destroyer) (void*);
} UTL_event_t;

typedef struct _UTL_operation_t {
  bool is_done;
  int address;
  int duration; // for do_stuff operation
  int result;
  int event;
} UTL_op_t;

UTL_event_t * UTL_event_new(void);
void UTL_event_init(UTL_event_t*);
void UTL_event_destroy(UTL_event_t*, bool);
UTL_event_t * UTL_event_copy(UTL_event_t*);

UTL_event_t * UTL_callback_to_event(UTL_event_t * dest, UTL_callback_t * src);
UTL_callback_t * UTL_callback_dump(UTL_callback_t * src);

void UTL_schedule(UTL_event_t*);

void SCHEDULE_EVENT(UTL_event_t _NEXT_EVENT, simtime_t _NEXT_TICK);

#endif /* __UTILS_H */

