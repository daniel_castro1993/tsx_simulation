#ifndef HTM_DEFS_H
#  define HTM_DEFS_H

#  include "utils.h"

// INIT defined as 0

typedef enum _SIM_STATE {
  // INIT event is equal to 0, then came the application events
  THR_INIT = 20,
  THR_INTL, // Choose STATE_TRAN vs STATE_NOTX
  THR_TXIN, // Transaction: initial
  THR_TXCK, // Transaction: check lock callback on init transaction
  THR_TRAN, // Transaction: lock free
  THR_TXBG, // callback from cache transaction begin
  THR_TXAB, // callback from cache transaction abort
  THR_TXCM, // callback from cache transaction commit
  THR_TXSP, // Transaction spinning
  THR_NOTX, // Non-transactional
  THR_SERL, // Serialized mode
  THR_SRSP, // Waiting for serialized 
  THR_SRRS, // private cache response to serialized mode
  THR_TXRS, // private cache response to transactions
  THR_NTRS, // private cache response to non-transactions
  THR_GETS, // responds to CPU with transac or non-tx, and the current budget

  THR_PCRP, // response from the pcache

  LCK_INIT,
  LCK_LOCK, // locks
  LCK_UNLA, // unlocks all processes
  LCK_UNLC,
  LCK_CHKL, // checks if locked
  LCK_ADDT, // adds to locked processes

  MEM_INIT,
  MEM_READ,
  MEM_WRIT,

  SC_INIT,
  SC_MEMR, // memory response
  SC_READ,
  SC_WRIT,
  SC_SETS, // Set state of a word

  PC_INIT,
  PC_SCRP, // shared cache response
  PC_READ,
  PC_WRIT,
  PC_TXBG, // transaction begin
  PC_TXAB, // transaction abort
  PC_TXCM, // transaction commit
  PC_TXRE, // transactional read
  PC_TXWR, // transactional write
  PC_INVW, // invalidates all transactions with value in MODIFIED state
  PC_INVA, // invalidates all transactions with value
  PC_GETS, // gets the state of word
  PC_SETS, // set state of word to the given state

  CPU_INIT,
  CPU_INVA, // invalidate txs with address
  CPU_INVW, // invalidate txs with address in MODIFIED state
  CPU_PCRP, // handles pcache response
  CPU_MESI, // handles the MESI state for a given word
  CPU_SETS, // sets the state of cheched words with address
  CPU_ICTX, // increment the number of active transactions 
  CPU_DCTX, // decrement the number of active transactions 
  CPU_ACTX, // get the number of active transactions 
  CPU_BRST, // Broadcast get state request
  CPU_UPST, // Update the state statistic
          
  // This events are for the simplefied simulator
  SMPL_INIT,
  SMPL_INTL,
  SMPL_TXIN,
  SMPL_TXSP,
  SMPL_WAKE,
  SMPL_TRAN,
  SMPL_TXCM,
  SMPL_TXAB,
  SMPL_SERL,
  SMPL_NOTX,
  SMPL_CKLK,
  SMPL_LOCK,
  SMPL_ADDR,
  SMPL_MRKV,
  SMPL_MRKL,
  SMPL_MRKD,

  CHK_EXIT
} SIM_STATE;

// private cache
#  ifndef N_ASSOC_PCACHE
#    define N_ASSOC_PCACHE          2
#  endif
#  ifndef N_LINES_PCACHE
#    define N_LINES_PCACHE          4
#  endif

// in bytes (4Bytes == 32bits; 8Bytes == 64bits)
#  ifndef WORD_SIZE
#    define WORD_SIZE               1
#  endif

// shared cache
#  ifndef N_ASSOC_SCACHE
#    define N_ASSOC_SCACHE  (2 * 4)
#  endif
#  ifndef N_LINES_SCACHE
#    define N_LINES_SCACHE  (4 * 4)
#  endif

typedef enum _IS_STATE {
  STATE_THR = 1, STATE_MEM, STATE_SC, STATE_PC, STATE_CPU
} IS_STATE;

#  define SANITIZE_ADDRESS(_ADDR) ((_ADDR / WORD_SIZE) * WORD_SIZE)

int mask_value(int value);

// _LINE is a HTM_sc_line_t* or HTM_pc_line_t*
#  define FIND_CACHE_LINE(_LINE, _ADDR, _LINES, _NB_LINES)                    \
{                                                                             \
  int __shift, __i, __sanitized_addr = SANITIZE_ADDRESS(_ADDR), __mask;       \
  int __masked;                                                               \
  __mask = mask_value(_NB_LINES);                                             \
  __shift = (WORD_SIZE + 1) >> 1;                                             \
  __mask <<= __shift;                                                         \
  __masked = __sanitized_addr & __mask;                                       \
  __masked >>= __shift;                                                       \
  _LINE = &(_LINES[__masked]);                                                \
}

// gives the actual word, a free word, or the oldest (if not in line and full)
#  define CACHE_LINE_FIND_WORD(_WORD, _ADDR, _LINE, _NB_WORDS)                \
{                                                                             \
  int __i, __sanitized_addr = SANITIZE_ADDRESS(_ADDR);                        \
  _WORD = &(_LINE->words[0]);                                                 \
  for (__i = 0; __i < _NB_WORDS; __i++) {                                     \
    if (_LINE->words[__i].address == __sanitized_addr) {                      \
      if (_LINE->words[__i].state != MESI_STATE_I) {                          \
        _WORD = &(line->words[__i]);                                          \
        break;                                                                \
      }                                                                       \
    }                                                                         \
    if(_LINE->words[__i].timestamp < _WORD->timestamp) {                      \
      _WORD = &(line->words[__i]);                                            \
    }                                                                         \
  }                                                                           \
}

typedef enum _ABORT_CODE {
  ABORT_CODE_NONE = 0, // running normally
  ABORT_CODE_TXCF = 0x01, // tx aborted due to conflict with other tx
  ABORT_CODE_NTXC = 0x02, // tx aborted due to a non transaction block
  ABORT_CODE_CAPA = 0x04, // tx aborted due to capacity limitations
  ABORT_CODE_EXPL = 0x08,
  ABORT_CODE_OTHE = 0x10, // error in simulation ?
  ABORT_CODE_COMMIT = 0x80
} ABORT_CODE;

typedef enum _MESI_STATE {
  MESI_STATE_NONE = 0,
  MESI_STATE_M = 0x01,
  MESI_STATE_E = 0x02,
  MESI_STATE_S = 0x04,
  MESI_STATE_I = 0x08
} MESI_STATE;

typedef SIM_STATE state_t;
typedef int budget_t;

typedef struct _HTM_cache_word_t {
  simtime_t timestamp;
  int address, state;
} HTM_cache_word_t;

#endif /* HTM_DEFS_H */

