#include "htm.h"

void HTM_state_handler(void * state, int pid, int event, simtime_t now,
    void * buffer) {
  
  switch(event) {
    case STATE_THR_STACK:
      HTM_thr_state_handler(state, pid, event, now, buffer);
      break;
    case STATE_LOCK_STACK:
      HTM_lock_state_handler(state, pid, event, now, buffer);
      break;
    case STATE_MEM_STACK:
      HTM_mem_state_handler(state, pid, event, now, buffer);
      break;
    case STATE_SC_STACK:
      HTM_sc_state_handler(state, pid, event, now, buffer);
      break;
    case STATE_PC_STACK:
      HTM_pc_state_handler(state, pid, event, now, buffer);
      break;
    case STATE_CPU_STACK:
      HTM_cpu_state_handler(state, pid, event, now, buffer);
      break;
    case STATE_SMPL_STACK:
      HTM_simple_state_handler(state, pid, event, now, buffer);
      break;
    case CHK_EXIT:
      HTM_state_check_exit(pid, now);
    default:
      break;
  }
}

void HTM_state_check_exit(int pid, simtime_t now) {
  UTL_event_t event;

  UTL_event_init(&event);
  event.pid = pid;
  event.event_code = CHK_EXIT;
  SCHEDULE_EVENT(event, now + CHECK_EXIT_DELTA_TIME);
}

bool HTM_OnGVT_check_exit() {
  return global_info.is_exit;
}

void HTM_OnGVT(bool thread_ended) {
  if (thread_ended) {
    global_info.exit_thrds++;
  }

  if (global_info.exit_thrds >= global_info.tot_nb_thrs) {
    global_info.is_exit = true;
  }
}
