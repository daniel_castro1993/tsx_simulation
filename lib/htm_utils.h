#ifndef HTM_UTILS_H
#define HTM_UTILS_H

#include "htm.h"

void HTM_do_cpu_event(simtime_t next_tick, SIM_STATE cpu_state,
        HTM_cpu_buffer_t * cpu_buffer);

void HTM_do_lock_event(simtime_t next_tick, int pid, SIM_STATE lock_state,
        SIM_STATE callback_event);

void HTM_do_pc_event(simtime_t next_tick, int pid, int addr, int inst_id,
        SIM_STATE cache_event, bool do_callback, SIM_STATE thr_callback);

void HTM_do_sc_event(simtime_t next_tick, int pid, int addr, int inst_id,
        SIM_STATE sc_event_code, SIM_STATE pc_event,
        UTL_callback_t * callback, int thr_pid,
        simtime_t acc_lat, bool no_callback);

void HTM_do_thr_event(simtime_t next_tick, int pid, SIM_STATE thr_event_code,
        HTM_thr_buffer_t * thr_buffer);

#endif /* HTM_UTILS_H */

