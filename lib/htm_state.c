#include "htm_state.h"
#include "htm_utils.h"
#include "stats.h"

#define RESET_COUNTER(_STATE)                                                 \
  _STATE->counter.begin_ts = _STATE->now;                                     \
  _STATE->counter.nb_acc_mem = 0;

#define ELAPSED_TIME(_NOW, _STATE) (_NOW - _STATE->last_ts)

#define STATS_INC(_NOW, _STATE, _STAT) ({                                     \
    double __elapsed_time = _NOW - _STATE->counter.begin_ts;                  \
    int __pointer = _STATE->stats.stats_pointer[_STAT];                       \
    _STATE->stats.counter[_STAT]++;                                           \
    _STATE->stats.stat_time[_STAT][__pointer] = __elapsed_time;               \
    if (_STAT == TXCM) {                                                      \
      _STATE->stats.budget_at_commit[__pointer] =                             \
        _STATE->counter.current_budget;                                       \
    }                                                                         \
    if (_STAT == TXAB) {                                                      \
      _STATE->stats.ops_before_abort[__pointer] =                             \
        _STATE->counter.nb_acc_mem;                                           \
    }                                                                         \
    _STATE->counter.begin_ts = _NOW;                                          \
    _STATE->stats.stats_pointer[_STAT] =                                      \
      (__pointer + 1) % STATS_SAMPLE;                                         \
  })

//void STATS_INC(simtime_t _NOW, HTM_thr_state_t * _STATE, int _STAT) {
//  double __elapsed_time = _NOW - _STATE->counter.begin_ts;
//  int __pointer = _STATE->stats.stats_pointer[_STAT];
//
//  _STATE->stats.counter[_STAT]++;
//  _STATE->stats.stat_time[_STAT][__pointer] = __elapsed_time;
//  _NOW - _STATE->counter.begin_ts;
//
//  if (_STAT == TXCM) {
//    _STATE->stats.budget_at_commit[__pointer] =
//            _STATE->counter.current_budget;
//  }
//
//  if (_STAT == TXAB) {
//    _STATE->stats.ops_before_abort[__pointer] = _STATE->counter.nb_acc_mem;
//  }
//
//  _STATE->counter.begin_ts = _NOW;
//  _STATE->stats.stats_pointer[_STAT] = (__pointer + 1) % STATS_SAMPLE;
//}

void HTM_thr_state_handler(HTM_thr_state_t * state, int pid, SIM_STATE event,
        simtime_t now, HTM_thr_buffer_t * buffer) {

  state->now = now;
  state->pid = pid;

  switch (event) {
    case THR_INIT:
      HTM_thr_state_init(state);
      break;
    case THR_INTL:
      HTM_thr_state_initial(state, buffer);
      break;
    case THR_TXIN:
      HTM_thr_state_transaction_init(state, buffer);
      break;
    case THR_TXCK:
      HTM_thr_state_transaction_check(state, buffer);
      break;
    case THR_TRAN:
      HTM_thr_state_transaction(state, buffer);
      break;
    case THR_TXBG:
      HTM_thr_state_transaction_begin(state, buffer);
      break;
    case THR_TXAB:
      HTM_thr_state_transaction_abort(state, buffer);
      break;
    case THR_TXCM:
      HTM_thr_state_transaction_commit(state, buffer);
      break;
    case THR_TXSP:
      HTM_thr_state_transaction_spinning(state, buffer);
      break;
    case THR_NOTX:
      HTM_thr_state_non_transaction(state, buffer);
      break;
    case THR_SERL:
      HTM_thr_state_serial(state, buffer);
      break;
    case THR_SRSP:
      HTM_thr_state_serial_wait(state, buffer);
      break;
    case THR_SRRS:
      HTM_thr_state_serial_response(state, buffer);
      break;
    case THR_TXRS:
      HTM_thr_state_transaction_response(state, buffer);
      break;
    case THR_NTRS:
      HTM_thr_state_non_transac_response(state, buffer);
      break;
    case THR_GETS:
      HTM_thr_state_get_state(state, buffer);
      break;
    default:
      break;
  }

  state->last_ts = now;
}

// ############################################################################
// #### Lock callbacks                                                      ###
// ############################################################################

void check_lock_before_start(simtime_t next_tick, int pid) {
  HTM_do_lock_event(next_tick, pid, LCK_CHKL, THR_TXCK);
}

void check_lock_spin(simtime_t next_tick, int pid) {
  HTM_do_lock_event(next_tick, pid, LCK_CHKL, THR_TXSP);
}

void lock(simtime_t next_tick, int pid) {
  HTM_do_lock_event(next_tick, pid, LCK_LOCK, THR_SRSP);
}

void unlock(simtime_t next_tick, int pid) {
  HTM_do_lock_event(next_tick, pid, LCK_UNLC, THR_INTL);
}
// ############################################################################

// ############################################################################
// #### CPU callbacks                                                       ###
// ############################################################################

void increment_nb_active_trans(simtime_t next_tick, int pid) {
  HTM_cpu_buffer_t cpu_buffer;
  cpu_buffer.do_callback = false;
  cpu_buffer.thr_pid = pid;
  HTM_do_cpu_event(next_tick, CPU_ICTX, &cpu_buffer);
}

void decrement_nb_active_trans(simtime_t next_tick, int pid) {
  HTM_cpu_buffer_t cpu_buffer;
  cpu_buffer.do_callback = false;
  cpu_buffer.thr_pid = pid;
  HTM_do_cpu_event(next_tick, CPU_DCTX, &cpu_buffer);
}

void get_nb_active_trans(simtime_t next_tick, int pid) {
  HTM_cpu_buffer_t cpu_buffer;
  cpu_buffer.do_callback = true;
  cpu_buffer.callback.event_code = THR_SRSP;
  cpu_buffer.callback.pid = pid;
  HTM_do_cpu_event(next_tick, CPU_ACTX, &cpu_buffer);
}

void update_state(int pid, simtime_t next_tick, bool is_tx, int budget) {

  HTM_cpu_buffer_t cpu_buffer;

  cpu_buffer.is_transac = is_tx;
  cpu_buffer.budget = budget;
  cpu_buffer.do_callback = false;
  cpu_buffer.pid_except = pid;

  HTM_do_cpu_event(next_tick, CPU_BRST, &cpu_buffer);
}
// ############################################################################

// ############################################################################
// #### Private Caches (PC) callbacks                                       ###
// ############################################################################

void start_transac(simtime_t next_tick, int pid) {
  HTM_do_pc_event(next_tick, pid, -1, -1, PC_TXBG, true, THR_TXBG);
}

void abort_transac(simtime_t next_tick, int pid, bool do_callback) {
  HTM_do_pc_event(next_tick, pid, -1, -1, PC_TXAB, do_callback, THR_TXIN);
}

void commit_transac(simtime_t next_tick, int pid) {
  HTM_do_pc_event(next_tick, pid, -1, -1, PC_TXCM, true, THR_TXCM);
}

void pc_read(simtime_t next_tick, int pid, int addr) {
  HTM_do_pc_event(next_tick, pid, addr, -1, PC_READ, true, THR_TXRS);
}

void pc_write(simtime_t next_tick, int pid, int addr) {
  HTM_do_pc_event(next_tick, pid, addr, -1, PC_READ, true, THR_TXRS);
}

void pc_tx_read(simtime_t next_tick, int pid, int addr, int inst_id) {
  HTM_do_pc_event(next_tick, pid, addr, inst_id, PC_TXRE, true, THR_TXRS);
}

void pc_tx_write(simtime_t next_tick, int pid, int addr, int inst_id) {
  HTM_do_pc_event(next_tick, pid, addr, inst_id, PC_TXWR, true, THR_TXRS);
}
// ############################################################################

// ############################################################################
// #### Threads callbacks                                                   ###
// ############################################################################

void go_spin(simtime_t next_tick, int pid, bool is_locked) {

  HTM_thr_buffer_t thr_buffer;
  thr_buffer.is_locked = is_locked;
  HTM_do_thr_event(next_tick, pid, THR_TXSP, &thr_buffer);
}

void thr_txin(simtime_t next_tick, int pid) {

  HTM_thr_buffer_t thr_buffer;
  HTM_do_thr_event(next_tick, pid, THR_TXIN, &thr_buffer);
}

void start_notx(simtime_t next_tick, int pid) {

  HTM_thr_buffer_t thr_buffer;
  HTM_do_thr_event(next_tick, pid, THR_NOTX, &thr_buffer);
}

void go_intl(simtime_t next_tick, int pid) {

  HTM_thr_buffer_t thr_buffer;
  HTM_do_thr_event(next_tick, pid, THR_INTL, &thr_buffer);
}

void go_tran(simtime_t next_tick, int pid, int tx_id) {

  HTM_thr_buffer_t thr_buffer;
  thr_buffer.tx_id = tx_id;
  HTM_do_thr_event(next_tick, pid, THR_TRAN, &thr_buffer);
}

void go_serl(simtime_t next_tick, int pid) {

  HTM_thr_buffer_t thr_buffer;
  HTM_do_thr_event(next_tick, pid, THR_SERL, &thr_buffer);
}

void broadcast_abort(simtime_t next_tick, int pid) {

  HTM_thr_buffer_t thr_buffer;
  int i;

  thr_buffer.is_broadcast = true;

  for (i = 0; i < global_info.tot_nb_thrs; ++i) {
    if (global_info.thrs_pids[i] != pid) {
      HTM_do_thr_event(next_tick, global_info.thrs_pids[i],
              THR_TXAB, &thr_buffer);
    }
  }
}
// ############################################################################

bool assync_abort_handler(HTM_thr_state_t * state) {
  bool res = !state->tx_is_running || state->aborted;
  int pid = state->pid;
  simtime_t now = state->now, next_tick = now + DELTA_TIME;

  if (res) {
//    int p = state->stats.stats_pointer[LOST_ASSYNC];
//    state->stats.stat_time[LOST_ASSYNC][p] = now - state->abort_ts;
//    state->stats.stats_pointer[LOST_ASSYNC] =
//            (state->stats.stats_pointer[LOST_ASSYNC] + 1) % STATS_SAMPLE;
//    state->stats.counter[LOST_ASSYNC]++;

    state->is_broadcast = false;

    // restart
    thr_txin(next_tick, pid);
  }

  return res;
}

void HTM_thr_state_init(HTM_thr_state_t * state) {

  int k, pid = state->pid;
  simtime_t now = state->now;
  UTL_event_t event, thr_event;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_THR_STATE(THR_INIT));
  __DEBUG_FILE_FLUSH;
#endif

  state->nb_tasks = NB_TASKS;
  state->counter.addrs = new(int, global_info.L);
  state->counter.is_write = new(bool, global_info.L);
  state->stats.budget_at_commit = new(int, STATS_SAMPLE);
  state->stats.ops_before_abort = new(int, STATS_SAMPLE);
  BEGIN_FOR_STAT(k);
  state->stats.stat_time[k] = new_init(simtime_t, STATS_SAMPLE);
  state->stats.stats_pointer[k] = 0;
  END_FOR_STAT;

  UTL_event_init(&event);

  event.pid = pid;
  event.event_code = CHK_EXIT;

  SCHEDULE_EVENT(event, now + CHECK_EXIT_DELTA_TIME);

  UTL_event_init(&thr_event);

  thr_event.pid = pid;
  thr_event.event_code = THR_INTL;

  SCHEDULE_EVENT(thr_event, now + DELTA_TIME);
}

void HTM_thr_state_initial(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid, next_state;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

  if (state->nb_tasks > 0) {
    next_state = DO_INIT_TRANS ? THR_TXIN : THR_NOTX;

    state->counter.begin_ts = now;
    state->counter.gen_length = global_info.C;
    state->counter.nb_acc_mem_tot = global_info.L;
    state->counter.current_budget = global_info.budget;

    RESET_COUNTER(state);

    state->nb_tasks--;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
            now, pid, PRINT_THR_STATE(THR_INTL));
    __DEBUG_FILE_FLUSH;
#endif

    if (next_state == THR_NOTX) {
      state->tx_is_running = false;
      start_notx(next_tick, pid);

    } else {
      state->tx_is_running = true;
      state->is_broadcast = false;
      thr_txin(next_tick, pid);
    }

  } else {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, __LOG_TS_PID_EVENT " EXIT NOW\n",
            now, pid, PRINT_THR_STATE(THR_INTL));
    __DEBUG_FILE_FLUSH;
#endif
  }
}

void HTM_thr_state_transaction_init(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_THR_STATE(THR_TXIN));
  __DEBUG_FILE_FLUSH;
#endif

#if !defined(__DISABLE_LOCK_CHECK__)
  check_lock_before_start(next_tick, pid);
#else
  if (state->counter.current_budget > 0) {
    // start the transaction
    state->lock_detected = false;
    start_transac(next_tick, pid);
    state->counter.begin_ts = now - DELTA_TIME;

  } else {
    lock(next_tick, pid);
  }
#endif
}

void HTM_thr_state_transaction_check(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT,
          now, pid, PRINT_THR_STATE(THR_TXCK));
  __DEBUG_FILE_FLUSH;
#endif

  if (buffer->is_locked) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, "is locked\n");
    __DEBUG_FILE_FLUSH;
#endif
    // lock in use: go spin a bit
    go_spin(next_tick, pid, buffer->is_locked);
    state->counter.begin_ts = now - buffer->acc_lat - DELTA_TIME;

  } else if (state->counter.current_budget > 0) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, "not locked: start transaction (budget=%i)\n",
            state->counter.current_budget);
    __DEBUG_FILE_FLUSH;
#endif
    // start the transaction
    state->lock_detected = false;
    start_transac(next_tick, pid);
    state->counter.begin_ts = now - DELTA_TIME;

  } else {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, "go serialized mode (budget=%i)\n",
            state->counter.current_budget);
    __DEBUG_FILE_FLUSH;
#endif
    lock(next_tick, pid);
  }
}

void HTM_thr_state_transaction_spinning(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now;
  simtime_t next_tick = now + global_info.lat_spin;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "is_locked=%i\n",
          now, pid, PRINT_THR_STATE(THR_TXSP), buffer->is_locked);
  __DEBUG_FILE_FLUSH;
#endif

  if (buffer->is_locked) {
    // is locked: spin
    check_lock_spin(next_tick + global_info.lat_spin, pid);

    decrement_nb_active_trans(next_tick, pid); // may fix a weird bug

  } else {
    // is unlocked: start the transaction

    STATS_INC(now + DELTA_TIME, state, TXSP);

    state->lock_detected = false;
    start_transac(next_tick, pid);
    state->counter.begin_ts = now - DELTA_TIME;
  }
}

void HTM_thr_state_transaction(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);
  int operation_code, addr;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT,
          now, pid, PRINT_THR_STATE(THR_TRAN));
  __DEBUG_FILE_FLUSH;
#endif

  if (assync_abort_handler(state)) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, " ABORTED \n");
    __DEBUG_FILE_FLUSH;
#endif
  } else {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, "\n");
    __DEBUG_FILE_FLUSH;
#endif

    // generate read or write
    operation_code = DO_OP_READ ? PC_TXRE : PC_TXWR;
    addr = CHOOSE_ADDR(state->counter.addrs, state->counter.is_write,
            state->counter.nb_acc_mem);

    state->inst_id++;
    buffer->inst_id = state->inst_id;

    // READ/WRITE the private cache
    HTM_do_pc_event(next_tick, pid, addr, state->inst_id,
            operation_code, true, THR_TXRS);

    update_state(pid, next_tick, true, state->counter.current_budget);
  }
}

void HTM_thr_state_transaction_response(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "tx_id=%i state->inst_id=%i "
          "buffer->inst_id=%i", now, pid, PRINT_THR_STATE(THR_TXRS),
          state->current_tx_id, state->inst_id, buffer->inst_id);
  __DEBUG_FILE_FLUSH;
#endif

  if (assync_abort_handler(state)) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, " is_broadcast=%i ABORTED\n",
            state->is_broadcast);
    __DEBUG_FILE_FLUSH;
#endif
  } else {
    // check if ended
    state->counter.nb_acc_mem++;
    if (state->counter.nb_acc_mem < state->counter.nb_acc_mem_tot) {
      // continue

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
      fprintf(__debug_file, " continue \n");
      __DEBUG_FILE_FLUSH;
#endif

      go_tran(now + global_info.op_cost, pid, buffer->tx_id);

    } else {
      // end

      commit_transac(next_tick, pid);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
      fprintf(__debug_file, " commit \n");
      __DEBUG_FILE_FLUSH;
#endif
    }
  }
}

void HTM_thr_state_transaction_begin(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "tx_id=%i\n",
          now, pid, PRINT_THR_STATE(THR_TXBG), buffer->tx_id);
  __DEBUG_FILE_FLUSH;
#endif

  state->current_tx_id = buffer->tx_id;
  state->aborted = false;

  increment_nb_active_trans(next_tick, pid);

  go_tran(next_tick, pid, buffer->tx_id);
}

void HTM_thr_state_transaction_abort(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "tx_id=%i",
          now, pid, PRINT_THR_STATE(THR_TXAB), state->current_tx_id);
  __DEBUG_FILE_FLUSH;
#endif

  state->abort_ts = now;

  if (state->aborted) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, " error, already aborted\n");
    __DEBUG_FILE_FLUSH;
#endif

    decrement_nb_active_trans(next_tick, pid); // may fix a bug
    return;
  }

  STATS_INC(now, state, TXAB);
  state->counter.begin_ts = now;

  if (buffer->is_broadcast) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, " broadcast\n");
    __DEBUG_FILE_FLUSH;
#endif

    state->is_broadcast = true;

    if (state->tx_is_running && !state->aborted) {
      // go to TXIN
      state->current_tx_id++;
      state->aborted = true;
      abort_transac(next_tick, pid, false);

      state->counter.current_budget--;
      state->counter.nb_acc_mem = 0;

      decrement_nb_active_trans(next_tick, pid);

      //      thr_txin(next_tick, pid);
    }

    return;
  }

  if (state->tx_is_running && !state->aborted) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, " budget == %i, wait restart\n",
            state->counter.current_budget);
    __DEBUG_FILE_FLUSH;
#endif
    state->aborted = true;

    state->counter.current_budget--;
    state->counter.nb_acc_mem = 0;

    state->current_tx_id++;
    state->inst_id++;

    decrement_nb_active_trans(next_tick, pid);

  } else {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, " ERROR: none transaction running\n");
    __DEBUG_FILE_FLUSH;
#endif
  }
}

void HTM_thr_state_transaction_commit(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "tx_id=%i",
          now, pid, PRINT_THR_STATE(THR_TXCM), state->current_tx_id);
  __DEBUG_FILE_FLUSH;
#endif

  if (assync_abort_handler(state)) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, " aborted\n");
    __DEBUG_FILE_FLUSH;
#endif
    // go to TXIN
    //    abort_transac(next_tick, pid, true);
  } else {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, " committed\n");
    __DEBUG_FILE_FLUSH;
#endif
    STATS_INC(now, state, TXCM);
    go_intl(next_tick, pid);
    state->current_tx_id++;

    decrement_nb_active_trans(next_tick, pid);
  }
}

void HTM_thr_state_serial_wait(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "check_serial=%i active_txs=%i\n",
          now, pid, PRINT_THR_STATE(THR_SRSP), state->check_serial,
          buffer->active_txs);
  __DEBUG_FILE_FLUSH;
#endif

  if (state->check_serial || buffer->active_txs > 0) {
    // keep spinning
    state->check_serial = false;
    get_nb_active_trans(now + global_info.lat_spin, pid);

  } else {
    // start the serialized mode

    STATS_INC(now, state, SRSP);
    state->counter.begin_ts = now;

    go_serl(next_tick, pid);
  }
}

void HTM_thr_state_serial(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);
  int operation_code, addr;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_THR_STATE(THR_SERL));
  __DEBUG_FILE_FLUSH;
#endif

  // generate read or write
  operation_code = DO_OP_READ ? PC_READ : PC_WRIT;
  addr = CHOOSE_ADDR(state->counter.addrs, state->counter.is_write,
          state->counter.nb_acc_mem);

  // READ/WRITE the private cache
  HTM_do_pc_event(next_tick, pid, addr, -1, operation_code, true, THR_SRRS);

  update_state(pid, next_tick, true, 0);
}

void HTM_thr_state_serial_response(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT,
          now, pid, PRINT_THR_STATE(THR_SRRS));
  __DEBUG_FILE_FLUSH;
#endif

  // check if ended
  state->counter.nb_acc_mem++;
  if (state->counter.nb_acc_mem < state->counter.nb_acc_mem_tot) {
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, "continue\n");
    __DEBUG_FILE_FLUSH;
#endif
    // continue
    go_serl(now + global_info.op_cost, pid);

  } else {
    // ended
#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
    fprintf(__debug_file, "ended\n");
    __DEBUG_FILE_FLUSH;
#endif
    STATS_INC(now, state, SERL);
    unlock(next_tick, pid);
  }
}

void HTM_thr_state_non_transaction(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);
  int operation_code, addr;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_THR_STATE(THR_NOTX));
  __DEBUG_FILE_FLUSH;
#endif

  // generate read or write
  operation_code = DO_OP_READ ? PC_READ : PC_WRIT;
  addr = CHOOSE_ADDR(state->counter.addrs, state->counter.is_write,
          state->counter.nb_acc_mem);

  // READ/WRITE the private cache
  HTM_do_pc_event(next_tick, pid, addr, -1, operation_code, true, THR_NTRS);

  update_state(pid, next_tick, false, 0);
}

void HTM_thr_state_non_transac_response(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, next_tick = NEXT_TICK(now);
  UTL_event_t thr_event;
  HTM_thr_buffer_t thr_buffer;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_THR_STATE(THR_NTRS));
  __DEBUG_FILE_FLUSH;
#endif

  UTL_event_init(&thr_event);

  thr_event.cnt = &thr_buffer;
  thr_event.cnt_size = sizeof (HTM_thr_buffer_t);
  thr_event.pid = pid;

  // check if ended
  state->counter.nb_acc_mem++;
  if (state->counter.nb_acc_mem < state->counter.nb_acc_mem_tot) {
    // continue
    thr_event.event_code = THR_NOTX;
  } else {
    // ended
    thr_event.event_code = THR_INTL;

    STATS_INC(now, state, NOTX);
  }

  SCHEDULE_EVENT(thr_event, now + global_info.op_cost);
}

void HTM_thr_state_get_state(HTM_thr_state_t * state,
        HTM_thr_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now;
  UTL_event_t cpu_event;
  HTM_cpu_buffer_t cpu_buffer;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_THR_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_THR_STATE(THR_GETS));
  __DEBUG_FILE_FLUSH;
#endif

  UTL_event_init(&cpu_event);

  cpu_buffer.budget = state->counter.current_budget;
  if (state->tx_is_running) { // debug proposes
    cpu_buffer.is_transac = true;
  } else {
    cpu_buffer.is_transac = false;
  }
  cpu_buffer.thr_pid = pid;

  cpu_event.cnt = &cpu_buffer;
  cpu_event.cnt_size = sizeof (HTM_cpu_buffer_t);
  cpu_event.pid = global_info.cpu_pid;
  cpu_event.event_code = CPU_UPST;

  SCHEDULE_EVENT(cpu_event, now + DELTA_TIME);
}
