#ifndef HTM_LOCKS_H
#  define HTM_LOCKS_H

#include "utils.h"
#include "htm_defs.h"

#  define STATE_LOCK_STACK     LCK_INIT:                                      \
                          case LCK_LOCK:                                      \
                          case LCK_UNLA:                                      \
                          case LCK_UNLC:                                      \
                          case LCK_CHKL:                                      \
                          case LCK_ADDT

#  define PRINT_LOCK_STATE(state) (state == LCK_INIT ? "LCK_INIT" :           \
                                   state == LCK_LOCK ? "LCK_LOCK" :           \
                                   state == LCK_UNLA ? "LCK_UNLA" :           \
                                   state == LCK_UNLC ? "LCK_UNLC" :           \
                                   state == LCK_CHKL ? "LCK_CHKL" :           \
                                   state == LCK_ADDT ? "LCK_ADDT" : "")

#  define NEW_LOCK_STATE(_STATE)                                              \
  _STATE = new(HTM_lock_state_t, 1);                                          \
  set_zero(_STATE, HTM_lock_state_t, 1);

#  define NEW_LOCK_BUFFER(_BUFFER)                                            \
  _BUFFER = new(HTM_lock_state_t 1);                                          \
  set_zero(_BUFFER, HTM_lock_state_t, 1);

typedef struct _HTM_lock_state_t {
  int pid;
  simtime_t now, l_lat, u_lat;
  bool is_free;
  COL_llist_t * waiting_queue;
  SIM_STATE state;
} HTM_lock_state_t;

typedef struct _HTM_lock_buffer_t {
  UTL_callback_t callback;
} HTM_lock_buf_t;

/**
 * 
 * @param state
 * @param process_id
 * @param now
 */
void HTM_lock_state_handler(HTM_lock_state_t*, int, SIM_STATE, simtime_t,
    HTM_lock_buf_t*);

/**
 * 
 * @param state
 * @param now
 */
void HTM_lock_state_init(HTM_lock_state_t*);

void HTM_lock_state_lock(HTM_lock_state_t*, HTM_lock_buf_t*);
void HTM_lock_state_unlock_all(HTM_lock_state_t*, HTM_lock_buf_t*);
void HTM_lock_state_unlock(HTM_lock_state_t*, HTM_lock_buf_t*);
void HTM_lock_state_check_locked(HTM_lock_state_t*, HTM_lock_buf_t*);
void HTM_lock_state_add_to_lock(HTM_lock_state_t*, HTM_lock_buf_t*);


#endif /* HTM_LOCKS_H */

