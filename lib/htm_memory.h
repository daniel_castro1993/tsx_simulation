#ifndef HTM_MEMORY_H
#  define HTM_MEMORY_H

#  include "htm.h"


#  define STATE_MEM_STACK     MEM_INIT:                                       \
                              case MEM_READ:                                  \
                              case MEM_WRIT

#  define PRINT_MEM_STATE(state) (state == MEM_INIT ? "MEM_INIT" :            \
                                  state == MEM_READ ? "MEM_READ" :            \
                                  state == MEM_WRIT ? "MEM_WRIT" : "")

#  define NEW_MEM_STATE(_STATE)                                               \
  _STATE = new(HTM_mem_state_t, 1);                                           \
  set_zero(_STATE, HTM_mem_state_t, 1);

#  define NEW_MEM_BUFFER(_BUFFER)                                             \
  _BUFFER = new(HTM_mem_buffer_t, 1);                                         \
  set_zero(_BUFFER, HTM_mem_buffer_t, 1);

typedef struct _HTM_mem_state_t {
  int pid;
  simtime_t r_lat, w_lat, now;
} HTM_mem_state_t;

typedef struct _HTM_mem_buffer_t {
  int pc_pid, thr_pid, addr, inst_id;
  bool no_callback;
  simtime_t acc_lat;
  SIM_STATE pc_event, sc_event;
  UTL_callback_t callback;
} HTM_mem_buffer_t;

/**
 * 
 * @param state
 * @param process_id
 * @param now
 */
void HTM_mem_state_handler(HTM_mem_state_t*, int, SIM_STATE, simtime_t,
    HTM_mem_buffer_t*);

/**
 * 
 * @param state
 * @param now
 */
void HTM_mem_state_init(HTM_mem_state_t*);

void HTM_mem_state_read(HTM_mem_state_t*, HTM_mem_buffer_t*);
void HTM_mem_state_write(HTM_mem_state_t*, HTM_mem_buffer_t*);

#endif /* HTM_MEMORY_H */

