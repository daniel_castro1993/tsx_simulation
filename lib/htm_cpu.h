#ifndef HTM_CPU_H
#  define HTM_CPU_H

#  include "htm_cpu.h"
#  include "htm_defs.h"
#  include "model_states.h"

#  define STATE_CPU_STACK         CPU_INIT:                                   \
                             case CPU_INVA:                                   \
                             case CPU_INVW:                                   \
                             case CPU_PCRP:                                   \
                             case CPU_MESI:                                   \
                             case CPU_SETS:                                   \
                             case CPU_ICTX:                                   \
                             case CPU_DCTX:                                   \
                             case CPU_ACTX:                                   \
                             case CPU_BRST:                                   \
                             case CPU_UPST

#  define PRINT_CPU_STATE(state) (state == CPU_INIT ? "CPU_INIT" :            \
                                  state == CPU_INVA ? "CPU_INVA" :            \
                                  state == CPU_INVW ? "CPU_INVW" :            \
                                  state == CPU_PCRP ? "CPU_PCRP" :            \
                                  state == CPU_MESI ? "CPU_MESI" :            \
                                  state == CPU_SETS ? "CPU_SETS" :            \
                                  state == CPU_ICTX ? "CPU_ICTX" :            \
                                  state == CPU_DCTX ? "CPU_DCTX" :            \
                                  state == CPU_ACTX ? "CPU_ACTX" :            \
                                  state == CPU_BRST ? "CPU_BRST" :            \
                                  state == CPU_UPST ? "CPU_UPST" : "")

#  define NEW_CPU_STATE(_STATE)                                               \
  _STATE = new(HTM_cpu_state_t, 1);                                           \
  set_zero(_STATE, HTM_cpu_state_t, 1);

#  define NEW_CPU_BUFFER(_BUFFER)                                             \
  _BUFFER = new(HTM_cpu_buffer_t, 1);                                         \
  set_zero(_BUFFER, HTM_cpu_buffer_t, 1);

//extern UTL_event_t;

typedef struct _HTM_cpu_state_t {
  int pid, active_txs, state, pc_responses, pid_except, addr, pc_pid;
  int state_replies;
  simtime_t now, last_inv;
  mod_state_t mod_state;
  MESI_STATE * word_states;
} HTM_cpu_state_t;

typedef struct _HTM_cpu_buffer_t {
  bool is_transac, do_callback;
  int nb_pcaches, pid_except, addr, pc_pid, budget, thr_pid;
  int * states, state;
  MESI_STATE word_state;
  UTL_callback_t callback;
} HTM_cpu_buffer_t;

void HTM_cpu_state_handler(HTM_cpu_state_t * state, int pid, SIM_STATE event,
    simtime_t now, HTM_cpu_buffer_t * buffer);

void HTM_cpu_state_init(HTM_cpu_state_t*);

void HTM_cpu_state_invalidate(HTM_cpu_state_t*, HTM_cpu_buffer_t*);
void HTM_cpu_state_invalidate_modified(HTM_cpu_state_t*, HTM_cpu_buffer_t*);

/**
 * Calls all caches to set the state of some word.
 * 
 * @param state
 * @param buffer
 */
void HTM_cpu_state_set_word_state(HTM_cpu_state_t*, HTM_cpu_buffer_t*);

void HTM_cpu_state_pcache_response(HTM_cpu_state_t*, HTM_cpu_buffer_t*);
void HTM_cpu_state_mesi_handler(HTM_cpu_state_t*, HTM_cpu_buffer_t*);

void HTM_cpu_state_inc_active_txs(HTM_cpu_state_t*, HTM_cpu_buffer_t*);
void HTM_cpu_state_dec_active_txs(HTM_cpu_state_t*, HTM_cpu_buffer_t*);
void HTM_cpu_state_get_active_txs(HTM_cpu_state_t*, HTM_cpu_buffer_t*);

void HTM_cpu_state_broadcast_state(HTM_cpu_state_t*, HTM_cpu_buffer_t*);
void HTM_cpu_state_update_state(HTM_cpu_state_t*, HTM_cpu_buffer_t*);

#endif /* HTM_CPU_H */

