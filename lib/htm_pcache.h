#ifndef HTM_PCACHE_H
#  define HTM_PCACHE_H

#  include "htm_defs.h"

#  define STATE_PC_STACK           PC_INIT:                                   \
                              case PC_SCRP:                                   \
                              case PC_READ:                                   \
                              case PC_WRIT:                                   \
                              case PC_TXBG:                                   \
                              case PC_TXAB:                                   \
                              case PC_TXCM:                                   \
                              case PC_TXRE:                                   \
                              case PC_TXWR:                                   \
                              case PC_INVW:                                   \
                              case PC_INVA:                                   \
                              case PC_GETS:                                   \
                              case PC_SETS

#  define PRINT_PC_STATE(state) (state == PC_INIT ? "PC_INIT" :               \
                                 state == PC_SCRP ? "PC_SCRP" :               \
                                 state == PC_READ ? "PC_READ" :               \
                                 state == PC_WRIT ? "PC_WRIT" :               \
                                 state == PC_TXBG ? "PC_TXBG" :               \
                                 state == PC_TXAB ? "PC_TXAB" :               \
                                 state == PC_TXCM ? "PC_TXCM" :               \
                                 state == PC_TXRE ? "PC_TXRE" :               \
                                 state == PC_TXWR ? "PC_TXWR" :               \
                                 state == PC_INVW ? "PC_INVW" :               \
                                 state == PC_INVA ? "PC_INVA" :               \
                                 state == PC_GETS ? "PC_GETS" :               \
                                 state == PC_SETS ? "PC_SETS" : "")

#  define TX_IS_ABORT(_CODE) (_CODE & (ABORT_CODE_TXCF|                       \
                                       ABORT_CODE_NTXC|                       \
                                       ABORT_CODE_CAPA|                       \
                                       ABORT_CODE_EXPL|                       \
                                       ABORT_CODE_OTHE))

#  define PRINT_ABORT_CAUSE(cause) (cause == ABORT_CODE_NONE ? "running" :    \
                                    cause == ABORT_CODE_TXCF ? "TX_CNFL" :    \
                                    cause == ABORT_CODE_NTXC ? "NTX_CFL" :    \
                                    cause == ABORT_CODE_CAPA ? "TX_CAPA" :    \
                                    cause == ABORT_CODE_EXPL ? "EXPLICT" :    \
                                    cause == ABORT_CODE_OTHE ? "C_OTHER" :    \
                                    cause == ABORT_CODE_COMMIT ? "COMMIT" : "")


#  define PRINT_MESI_STATE(state) (state == MESI_STATE_M ? "M" :              \
                                   state == MESI_STATE_E ? "E" :              \
                                   state == MESI_STATE_S ? "S" :              \
                                   state == MESI_STATE_I ? "I" : "")

#  define CACHE_LINE_IS_TRANSACTIONAL(_LINE) (_LINE->tx != NULL)

#  define CACHE_LINE_MAKE_TRANSACTIONAL(_TX, _LINE)                           \
  _LINE->tx = _TX;                                                            \
  COL_LLIST_add(_TX->lines, _LINE);

#  define NEW_PC_STATE(_STATE)                                                \
  _STATE = new(HTM_pc_state_t, 1);                                            \
  set_zero(_STATE, HTM_pc_state_t, 1);

#  define NEW_PC_BUFFER(_BUFFER)                                              \
  _BUFFER = new(HTM_pc_buffer_t, 1);                                          \
  set_zero(_BUFFER, HTM_pc_buffer_t, 1);

#  define CREATE_TX(_TX, _PID, _TX_ID)                                        \
  _TX = new(HTM_cache_tx_t, 1);                                               \
  _TX->tx_id = state->tx_id;                                                  \
  _TX->pid = _TX_ID;                                                          \
  _TX->abort_code = ABORT_CODE_NONE;                                          \
  _TX->is_running = true;                                                     \
  _TX->lines = COL_LLIST_new();

#  define DELETE_TX(_TX)                                                      \
  COL_LLIST_destroy(_TX->lines, NULL);                                        \
  free(_TX);

#  define INIT_LINES(_PID, _I, _J)                                            \
  global_info.pcs_lines[_PID % global_info.tot_nb_pcs]                        \
          = new(HTM_pc_line_t, N_LINES_PCACHE);                               \
  for (_I = 0; _I < N_LINES_PCACHE; _I++) {                                   \
    global_info.pcs_lines[_PID % global_info.tot_nb_pcs][_I].tx = NULL;       \
    for (_J = 0; _J < N_ASSOC_PCACHE; _J++) {                                 \
      global_info.pcs_lines[_PID % global_info.tot_nb_pcs][_I]                \
          .words[_J].state = MESI_STATE_I;                                    \
    }                                                                         \
  }                                                                           \


typedef struct _HTM_cache_tx_t {
  int tx_id, pid;
  bool is_running;
  ABORT_CODE abort_code;
  COL_llist_t * lines;
} HTM_cache_tx_t;

typedef struct _HTM_pc_line_t {
  HTM_cache_tx_t * tx;
  HTM_cache_word_t words [N_ASSOC_PCACHE];
} HTM_pc_line_t;

typedef struct _HTM_pc_state_t {
  int pid, nb_pcs, tx_id, thr_pid, tx_counter;
  simtime_t r_lat, w_lat, tx_begin_lat, tx_abort_lat, tx_commit_lat, now;
  bool do_tx_abort, aborted;
  ABORT_CODE last_abort_code;
  HTM_pc_line_t * lines;
  HTM_cache_tx_t * tx;
  UTL_callback_t tx_callback;
} HTM_pc_state_t;

typedef struct _HTM_pc_buffer_t {
  int addr, pc_pid, thr_pid, inst_id;
  MESI_STATE state;
  SIM_STATE pc_event, sc_event;
  simtime_t acc_lat;
  bool do_tx_abort, do_callback;
  UTL_callback_t callback, tx_abort;
} HTM_pc_buffer_t;

typedef struct _HTM_cache_stats_t {
  int address, processor, latency, line_nb, word_nb;
  int operation; // OP_READ|OP_WRITE|...
  int * states; // M|E|S|I from every private cache
  int * position[2]; // [line_nb, word_nb]
  int begin; // timestamp of begining
  int abort_code;
} HTM_cache_stats_t;

void HTM_pc_state_handler(HTM_pc_state_t * state, int pid, SIM_STATE event,
    simtime_t now, HTM_pc_buffer_t * buffer);

void HTM_pc_state_init(HTM_pc_state_t * state);

/**
 * 
 * @param 
 * @param 
 */
void HTM_pc_state_scache_response(HTM_pc_state_t*, HTM_pc_buffer_t*);

/**
 * 
 * @param state
 * @param buffer initializate addr, acc_lat, callback
 */
void HTM_pc_state_read(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer);

/**
 * 
 * @param state
 * @param buffer initializate addr, acc_lat, callback
 */
void HTM_pc_state_write(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer);

/**
 * 
 * @param state
 * @param buffer 
 */
void HTM_pc_state_tx_begin(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer);

/**
 * 
 * @param state
 * @param buffer
 */
void HTM_pc_state_tx_abort(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer);

/**
 * 
 * @param state
 * @param buffer
 */
void HTM_pc_state_tx_commit(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer);

/**
 * 
 * @param state
 * @param buffer initializate addr, acc_lat, callback
 */
void HTM_pc_state_tx_read(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer);

/**
 * 
 * @param state
 * @param buffer initializate addr, acc_lat, callback
 */
void HTM_pc_state_tx_write(HTM_pc_state_t * state, HTM_pc_buffer_t * buffer);

/**
 * Sets the state of some word. Does not callback.
 * 
 * @param state
 * @param buffer must have an addr and a state
 */
void HTM_pc_state_set_word_state(HTM_pc_state_t*, HTM_pc_buffer_t*);

/**
 * Sets the state of some word.
 * 
 * @param state
 * @param buffer must have an addr and a callback
 */
void HTM_pc_state_get_word_state(HTM_pc_state_t*, HTM_pc_buffer_t*);

/**
 * Invalidates all transactions with some word. Does not callback.
 * 
 * @param state
 * @param buffer
 */
void HTM_pc_state_invalidate(HTM_pc_state_t*, HTM_pc_buffer_t*);

/**
 * Invalidates all transactions with some word in MODIFIED state.
 * Does not callback.
 * 
 * @param state
 * @param buffer
 */
void HTM_pc_state_invalidate_modified(HTM_pc_state_t*, HTM_pc_buffer_t*);

#endif /* HTM_PCACHE_H */

