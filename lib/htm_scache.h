#ifndef HTM_SCACHE_H
#  define HTM_SCACHE_H

#  include "htm_defs.h"

#  define STATE_SC_STACK           SC_INIT:                                   \
                              case SC_MEMR:                                   \
                              case SC_READ:                                   \
                              case SC_WRIT:                                   \
                              case SC_SETS

#  define PRINT_SC_STATE(state) (state == SC_INIT ? "SC_INIT" :               \
                                 state == SC_MEMR ? "SC_MEMR" :               \
                                 state == SC_READ ? "SC_READ" :               \
                                 state == SC_WRIT ? "SC_WRIT" :               \
                                 state == SC_SETS ? "SC_SETS" : "")

#  define NEW_SC_STATE(_STATE)                                                \
  _STATE = new(HTM_sc_state_t, 1);                                            \
  set_zero(_STATE, HTM_sc_state_t, 1);

#  define NEW_SC_BUFFER(_BUFFER)                                              \
  _BUFFER = new(HTM_sc_buffer_t, 1);                                          \
  set_zero(_BUFFER, HTM_sc_buffer_t, 1);

typedef struct _HTM_sc_line_t {
  HTM_cache_word_t words [N_ASSOC_SCACHE];
} HTM_sc_line_t;

typedef struct _HTM_sc_state_t {
  int pid;
  simtime_t r_lat, w_lat, now;
  HTM_sc_line_t * lines;
} HTM_sc_state_t;

typedef struct _HTM_sc_buffer_t {
  int addr, pc_pid, thr_pid, inst_id;
  bool no_callback;
  SIM_STATE mem_event, sc_event, pc_event;
  MESI_STATE state;
  simtime_t acc_lat;
  UTL_callback_t callback;
} HTM_sc_buffer_t;

void HTM_sc_state_handler(HTM_sc_state_t * state, int pid, SIM_STATE event,
    simtime_t now, HTM_sc_buffer_t * buffer);

void HTM_sc_state_init(HTM_sc_state_t * state);

void HTM_sc_state_memory_response(HTM_sc_state_t * state,
    HTM_sc_buffer_t * buffer);
void HTM_sc_state_read(HTM_sc_state_t * state, HTM_sc_buffer_t * buffer);
void HTM_sc_state_write(HTM_sc_state_t * state, HTM_sc_buffer_t * buffer);
void HTM_sc_state_set_state(HTM_sc_state_t * state, HTM_sc_buffer_t * buffer);

#endif /* HTM_SCACHE_H */

