#ifndef HTM_H
#  define HTM_H

#  include <stdio.h>
#  include <math.h>

#  include "utils.h"
#  include "collections.h"

#  include "htm_cpu.h"
#  include "htm_locks.h"
#  include "htm_memory.h"
#  include "htm_scache.h"
#  include "htm_pcache.h"
#  include "htm_state.h"
#  include "htm_simple.h"
#  include "model_states.h"
#  include "htm_defs.h"


#  define CHECK_EXIT_DELTA_TIME 1000.0

#  ifdef __DEBUG_FILE__
// is to debug htm to this file
FILE * __debug_file;
#    define __OPEN_DEBUG_FILE (__debug_file = __debug_file == NULL            \
                          ? __debug_file = fopen(__DEBUG_FILE__, "w")         \
                          : __debug_file)                                     
#    define __CLOSE_DEBUG_FILE                                                \
  if(__debug_file) {                                                          \
    fclose(__debug_file);                                                     \
    __debug_file = NULL;                                                      \
  } 

#    define __LOG_TS_PID_EVENT "[TS%12.6f][me%3u][%8s]: "

#    ifdef __DEBUG_FILE_FORCE_FLUSH__
#      define __DEBUG_FILE_FLUSH fflush(__debug_file);
#    else
#      define __DEBUG_FILE_FLUSH ;
#    endif
#  endif

#  define DO_OP_READ        (Random() < global_info.prob_read) 
#  define DO_INIT_TRANS     (Random() < global_info.prob_tran) 
#  define NEXT_TICK(_NOW)   (_NOW + DELTA_TIME)

#  define CHOOSE_ADDR(_ADDRS, _IS_WRITE, _LEN) ({                             \
  int __rand, __i;                                                            \
  bool __some_equal = false;                                                  \
  do {                                                                        \
    __rand = RandomRange(0, global_info.D - 1);                               \
    __some_equal = false;                                                     \
    for(__i = 0; __i < _LEN; ++__i) {                                         \
      if (_ADDRS[__i] == __rand) {                                            \
        __some_equal = true;                                                  \
        break;                                                                \
      }                                                                       \
    }                                                                         \
  } while(__some_equal);                                                      \
  _IS_WRITE[_LEN] = !DO_OP_READ;                                              \
  _ADDRS[_LEN] = __rand;                                                      \
  __rand;                                                                     \
  });

typedef struct _HTM_global_info {
  bool is_init, is_exit;
  int tot_nb_thrs, tot_nb_pcs, cpu_pid, mem_pid, sc_pid, lock_pid;
  int exit_thrds;
  int * thrs_pids;
  int * pcs_pids;
  simtime_t C, op_cost;
  simtime_t lat_pc_r, lat_pc_w;
  simtime_t lat_sc_r, lat_sc_w;
  simtime_t lat_mem_r, lat_mem_w;
  simtime_t lat_pc_txbg, lat_pc_txab, lat_pc_txcm;
  simtime_t lat_lock, lat_unlock;
  simtime_t lat_spin;
  int L, D, budget;
  int nb_cache_lines, nb_sets, nb_ways, cache_line_size, granule_size;
  int nb_garbage_granules;
  bool is_random_garbage, is_continuous_garbage;
  int start_garbage_granule_addr, end_garbage_granule_addr;
  int * random_garbage_granule_addr;

  double prob_read, prob_tran;
  HTM_pc_line_t ** pcs_lines;
} HTM_global_info;

HTM_global_info global_info;

void HTM_state_handler(void * state, int pid, int event, simtime_t now,
        void * buffer);
void HTM_state_check_exit(int pid, simtime_t now);
bool HTM_OnGVT_check_exit();
void HTM_OnGVT(bool thread_ended);

#endif /* HTM_H */
