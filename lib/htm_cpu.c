#include "htm.h"
#include "htm_utils.h"

int find_pc_index(int pid, int * pids);

void HTM_cpu_state_handler(HTM_cpu_state_t * state, int pid, SIM_STATE event,
        simtime_t now, HTM_cpu_buffer_t * buffer) {

  if (pid < 0 || pid >= n_prc_tot) {
    printf("ERROR[HTM_cpu_state_handler]: invalid pid: %i \n", pid);
  }

  state->now = now;
  state->pid = pid;

  switch (event) {
    case CPU_INIT:
      HTM_cpu_state_init(state);
      break;
    case CPU_INVA:
      HTM_cpu_state_invalidate(state, buffer);
      break;
    case CPU_INVW:
      HTM_cpu_state_invalidate_modified(state, buffer);
      break;
    case CPU_PCRP:
      HTM_cpu_state_pcache_response(state, buffer);
      break;
    case CPU_MESI:
      HTM_cpu_state_mesi_handler(state, buffer);
      break;
    case CPU_SETS:
      HTM_cpu_state_set_word_state(state, buffer);
      break;
    case CPU_ICTX:
      HTM_cpu_state_inc_active_txs(state, buffer);
      break;
    case CPU_DCTX:
      HTM_cpu_state_dec_active_txs(state, buffer);
      break;
    case CPU_ACTX:
      HTM_cpu_state_get_active_txs(state, buffer);
      break;
    case CPU_BRST:
      HTM_cpu_state_broadcast_state(state, buffer);
      break;
    case CPU_UPST:
      HTM_cpu_state_update_state(state, buffer);
      break;
    default:
      break;
  }
}

void HTM_cpu_state_init(HTM_cpu_state_t * state) {

  UTL_event_t event;
  int pid = state->pid, pcs_num = global_info.tot_nb_pcs;
  simtime_t now = state->now;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_CPU_STATE(CPU_INIT));
  __DEBUG_FILE_FLUSH;
#endif

  state->word_states = new(MESI_STATE, pcs_num);

  UTL_event_init(&event);

  event.pid = pid;
  event.event_code = CHK_EXIT;

  mod_generateStates(global_info.tot_nb_thrs, global_info.budget);
  state->mod_state = mod_newState();

  SCHEDULE_EVENT(event, now + CHECK_EXIT_DELTA_TIME);
}

void HTM_cpu_state_invalidate(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  simtime_t now = now = state->now;
  int i, pid = state->pid, nb_pcs = global_info.tot_nb_pcs;
  int * pcs_pids = global_info.pcs_pids;
  int addr = buffer->addr;
  UTL_event_t pc_event;
  HTM_pc_buffer_t pc_buffer;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_CPU_STATE(CPU_INVA));
  __DEBUG_FILE_FLUSH;
#endif

  for (i = 0; i < nb_pcs; i++) {
    if (pcs_pids[i] != buffer->pc_pid) {
      UTL_event_init(&pc_event);
      pc_event.cnt = &pc_buffer;
      pc_event.cnt_size = sizeof (HTM_pc_buffer_t);
      pc_event.event_code = PC_INVA;
      pc_event.pid = pcs_pids[i];

      pc_buffer.addr = addr;
      pc_buffer.thr_pid = global_info.thrs_pids[i];

      simtime_t next_tick = (now - state->last_inv) < (2 * DELTA_TIME) ?
              now + 5 * DELTA_TIME : now + DELTA_TIME;

      SCHEDULE_EVENT(pc_event, next_tick);
    }
  }
  state->last_inv = now;
}

void HTM_cpu_state_invalidate_modified(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  simtime_t now = state->now;
  int i, pid = state->pid, nb_pcs = global_info.tot_nb_pcs;
  int * pcs_pids = global_info.pcs_pids;
  int addr = buffer->addr;
  UTL_event_t event;
  HTM_pc_buffer_t pc_buffer;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_CPU_STATE(CPU_INVW));
  __DEBUG_FILE_FLUSH;
#endif

  for (i = 0; i < nb_pcs; i++) {
    if (pcs_pids[i] != buffer->pc_pid) {
      UTL_event_init(&event);

      event.cnt = &pc_buffer;
      event.cnt_size = sizeof (HTM_pc_buffer_t);
      event.event_code = PC_INVW;
      event.pid = pcs_pids[i];

      pc_buffer.addr = addr;
      pc_buffer.thr_pid = global_info.thrs_pids[i];

      simtime_t next_tick = now - state->last_inv < 2 * DELTA_TIME ?
              now + 5 * DELTA_TIME : now + DELTA_TIME;

      SCHEDULE_EVENT(event, next_tick);
    }
  }
  state->last_inv = now;
}

void HTM_cpu_state_set_word_state(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  int i, pid = state->pid, nb_pcs = global_info.tot_nb_pcs;
  int pid_except = buffer->pid_except;
  int * pcs_pids = global_info.pcs_pids;
  int addr = buffer->addr;
  MESI_STATE mesi_state = buffer->word_state;
  simtime_t now = state->now;
  HTM_pc_buffer_t pc_buffer;
  UTL_event_t event;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT " pid_except: %i, addr: 0x%08x, "
          "state: %s\n", now, pid, PRINT_CPU_STATE(CPU_SETS),
          pid_except, addr, PRINT_MESI_STATE(mesi_state));
  __DEBUG_FILE_FLUSH;
#endif

  UTL_event_init(&event);

  for (i = 0; i < nb_pcs; i++) {
    if (i != pid_except) {
      pc_buffer.addr = addr;
      pc_buffer.state = mesi_state;

      event.pid = pcs_pids[i];
      event.event_code = PC_SETS;
      event.cnt = &pc_buffer;
      event.cnt_size = sizeof (HTM_pc_buffer_t);

      SCHEDULE_EVENT(event, now + DELTA_TIME);
    }
  }
}

void HTM_cpu_state_pcache_response(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  int pc, i, pcs_num = global_info.tot_nb_pcs;
  int pid = state->pid, pc_pid = buffer->pc_pid;
  int s_count, pid_except = state->pid_except, addr = buffer->addr;
  int pc_exp_i = find_pc_index(pid_except, global_info.pcs_pids);
  int pc_index = find_pc_index(pc_pid, global_info.pcs_pids);
  simtime_t now = state->now;
  bool all_s;
  MESI_STATE s = buffer->word_state, expt_s;
  HTM_pc_buffer_t pc_buffer;
  UTL_event_t pc_event;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "pc_except=%2u pc_pid=%2u "
          "addr=0x%08x state=%s\n", now, pid, PRINT_CPU_STATE(CPU_PCRP),
          pid_except, pc_pid, addr, PRINT_MESI_STATE(s));
  __DEBUG_FILE_FLUSH;
#endif

  state->word_states[pc_index] = s;

  state->pc_responses++;

  if (state->pc_responses >= pcs_num) {
    expt_s = state->word_states[pc_exp_i];

    if (expt_s == MESI_STATE_M) {
      // invalidate the others

      for (i = 0; i < pcs_num; i++) {
        pc = global_info.pcs_pids[i];
        if (pc != pid_except) {
          pc_buffer.addr = addr;
          pc_buffer.state = MESI_STATE_I;

          UTL_event_init(&pc_event);

          pc_event.cnt = &pc_buffer;
          pc_event.cnt_size = sizeof (HTM_pc_buffer_t);
          pc_event.event_code = PC_SETS;
          pc_event.pid = pc;

          SCHEDULE_EVENT(pc_event, now + DELTA_TIME);
        }
      }

    } else {
      // if more than 1 different from state I, then put in state S

      s_count = 0;
      for (i = 0; i < pcs_num; i++) {
        if (state->word_states[i] != MESI_STATE_I) {
          s_count++;
        }
      }

      if (s_count > 1) {
        for (i = 0; i < pcs_num; i++) {
          pc = global_info.pcs_pids[i];
          pc_index = find_pc_index(pc, global_info.pcs_pids);
          s = state->word_states[pc_index];

          if (s != MESI_STATE_I) {

            pc_buffer.addr = addr;
            pc_buffer.state = MESI_STATE_S;

            UTL_event_init(&pc_event);

            pc_event.cnt = &pc_buffer;
            pc_event.cnt_size = sizeof (HTM_pc_buffer_t);
            pc_event.event_code = PC_SETS;
            pc_event.pid = pc;

            SCHEDULE_EVENT(pc_event, now + DELTA_TIME);
          }
        }
      }
    }
  }
}

void HTM_cpu_state_mesi_handler(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  int i, addr = buffer->addr, pid_except = buffer->pid_except;
  int pid = state->pid;
  simtime_t now = state->now;
  HTM_pc_buffer_t pc_buffer;
  UTL_event_t pc_event;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "pc_pid=%2u addr=0x%08x \n",
          now, pid, PRINT_CPU_STATE(CPU_MESI), pid_except, addr);
  __DEBUG_FILE_FLUSH;
#endif

  state->pc_responses = 0;
  state->pid_except = pid_except;
  state->addr = addr;

  pc_buffer.addr = addr;
  pc_buffer.callback.pid = global_info.cpu_pid;
  pc_buffer.callback.event_code = CPU_PCRP;

  UTL_event_init(&pc_event);

  pc_event.cnt = &pc_buffer;
  pc_event.cnt_size = sizeof (HTM_pc_buffer_t);
  pc_event.event_code = PC_GETS;

  for (i = 0; i < global_info.tot_nb_pcs; i++) {
    pc_event.pid = global_info.pcs_pids[i];

    SCHEDULE_EVENT(pc_event, now + DELTA_TIME);
  }
}

void HTM_cpu_state_inc_active_txs(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  simtime_t now = state->now;
  int pid = state->pid;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "thr_pid:%i "
          "active_txs (before): %i \n",
          now, pid, PRINT_CPU_STATE(CPU_ICTX),
          buffer->thr_pid, state->active_txs);
  __DEBUG_FILE_FLUSH;
#endif

  // TODO: THIS IS HIDING THE PROBLEM
  if (state->active_txs < global_info.tot_nb_thrs) {
    state->active_txs++;
  }

  if (buffer->do_callback) {
    UTL_event_t thr_event;
    UTL_callback_to_event(&thr_event, &(buffer->callback));
    SCHEDULE_EVENT(thr_event, now + DELTA_TIME);
  }
}

void HTM_cpu_state_dec_active_txs(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  simtime_t now = state->now;
  int pid = state->pid;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "thr_pid:%i "
          "active_txs (before): %i \n",
          now, pid, PRINT_CPU_STATE(CPU_DCTX),
          buffer->thr_pid, state->active_txs);
  __DEBUG_FILE_FLUSH;
#endif

  // TODO: THIS IS HIDING THE PROBLEM
  if (state->active_txs > 0) {
    state->active_txs--;
  }

  if (buffer->do_callback) {
    UTL_event_t thr_event;
    UTL_callback_to_event(&thr_event, &(buffer->callback));
    SCHEDULE_EVENT(thr_event, now + DELTA_TIME);
  }
}

void HTM_cpu_state_get_active_txs(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  simtime_t now = state->now;
  int pid = state->pid;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT " active_txs: %i \n",
          now, pid, PRINT_CPU_STATE(CPU_ACTX), state->active_txs);
  __DEBUG_FILE_FLUSH;
#endif

  if (buffer->do_callback) {
    UTL_event_t thr_event;
    HTM_thr_buffer_t thr_buffer;

    UTL_callback_to_event(&thr_event, &(buffer->callback));

    thr_event.cnt = &thr_buffer;
    thr_event.cnt_size = sizeof (HTM_thr_buffer_t);

    thr_buffer.active_txs = state->active_txs;

    SCHEDULE_EVENT(thr_event, now + DELTA_TIME);
  }
}

void state_pos_inc(mod_state_t state, int cur_budget, bool is_transac) {

  if (cur_budget < 0) {
    // error
    cur_budget = 0;
  }

  if (is_transac) {
    int pos = global_info.budget - cur_budget;
    state[pos] += 1;
  } else {
    state[global_info.budget + 1] += 1;
  }
}

void HTM_cpu_state_broadcast_state(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  simtime_t now = state->now, next_tick = now + DELTA_TIME;
  int i, pid = state->pid, except_pid = buffer->pid_except;
  UTL_event_t thr_event;
  HTM_thr_buffer_t thr_buffer;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "is_transac=%i budget=%i\n",
          now, pid, PRINT_CPU_STATE(CPU_BRST),
          buffer->is_transac, buffer->budget);
  __DEBUG_FILE_FLUSH;
#endif

  if (state->state_replies != 0) {
    return; // loop
  }

  for (i = 0; i < global_info.budget + 2; ++i) {
    state->mod_state[i] = 0;
  }

  state_pos_inc(state->mod_state, buffer->budget, buffer->is_transac);
  state->state_replies = 1;

  for (i = 0; i < global_info.tot_nb_thrs; ++i) {
    if (global_info.thrs_pids[i] != except_pid) {
      UTL_event_init(&thr_event);

      thr_event.cnt = &thr_buffer;
      thr_event.cnt_size = sizeof (HTM_thr_buffer_t);
      thr_event.pid = global_info.thrs_pids[i];
      thr_event.event_code = THR_GETS;

      SCHEDULE_EVENT(thr_event, next_tick);
    }
  }
}

void HTM_cpu_state_update_state(HTM_cpu_state_t * state,
        HTM_cpu_buffer_t * buffer) {

  simtime_t now = state->now;
  int pid = state->pid;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_CPU_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "is_transac=%i budget=%i\n",
          now, pid, PRINT_CPU_STATE(CPU_UPST),
          buffer->is_transac, buffer->budget);
  __DEBUG_FILE_FLUSH;
#endif

  state_pos_inc(state->mod_state, buffer->budget, buffer->is_transac);

  state->state_replies++;
  if (state->state_replies >= global_info.tot_nb_thrs) {
    // all replies collected

    mod_addAcc(now, state->mod_state);
    state->state_replies = 0;
  }
}

int find_pc_index(int pid, int * pids) {
  int i, res = -1, pcs_num = global_info.tot_nb_pcs;

  for (i = 0; i < pcs_num; i++) {
    if (pid == pids[i]) {
      res = i;
      break;
    }
  }

  return res;
}
