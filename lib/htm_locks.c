#include <stdio.h>
#include "htm.h"

void HTM_lock_state_handler(HTM_lock_state_t * state, int pid, SIM_STATE event,
    simtime_t now, HTM_lock_buf_t * buffer) {

  state->pid = pid;
  state->now = now;

  switch (event) {
    case LCK_INIT:
      HTM_lock_state_init(state);
      break;
    case LCK_LOCK:
      HTM_lock_state_lock(state, buffer);
      break;
    case LCK_UNLC:
      HTM_lock_state_unlock(state, buffer);
      break;
    case LCK_UNLA:
      HTM_lock_state_unlock_all(state, buffer);
      break;
    case LCK_CHKL:
      HTM_lock_state_check_locked(state, buffer);
      break;
    case LCK_ADDT:
      HTM_lock_state_add_to_lock(state, buffer);
      break;
    default:
      break;
  }
}

void HTM_lock_state_init(HTM_lock_state_t * state) {

  int pid = state->pid;
  simtime_t now = state->now;
  UTL_event_t event;

#ifdef __DEBUG_FILE__
    fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
            now, pid, PRINT_LOCK_STATE(LCK_INIT));
    __DEBUG_FILE_FLUSH;
#endif

  UTL_event_init(&event);

  event.pid = pid;
  event.event_code = CHK_EXIT;

  SCHEDULE_EVENT(event, now + CHECK_EXIT_DELTA_TIME);

  state->is_free = true;
  state->waiting_queue = COL_LLIST_new();
  state->l_lat = global_info.lat_lock;
  state->u_lat = global_info.lat_unlock;
}

void HTM_lock_state_lock(HTM_lock_state_t * state, HTM_lock_buf_t * buffer) {

  simtime_t now = state->now, l_lat = state->l_lat;
  int pid = state->pid;
  HTM_thr_buffer_t thr_buffer;
  UTL_event_t event;

#ifdef __DEBUG_FILE__
    fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
            now, pid, PRINT_LOCK_STATE(LCK_LOCK));
    __DEBUG_FILE_FLUSH;
#endif

  UTL_event_init(&event);
  UTL_callback_to_event(&event, &(buffer->callback));

  event.cnt = &thr_buffer;
  event.cnt_size = sizeof (HTM_thr_buffer_t);

  if (state->is_free) {
    state->is_free = false;
    thr_buffer.lock_success = true;

    SCHEDULE_EVENT(event, now + l_lat);
  } else {
    COL_LLIST_add(state->waiting_queue, UTL_callback_dump(&(buffer->callback)));
  }
}

void HTM_lock_state_unlock_all(HTM_lock_state_t * state,
    HTM_lock_buf_t * buffer) {

  UTL_event_t lock_event, unlock_event;
  UTL_callback_t * callback;
  HTM_thr_buffer_t thr_buffer;
  simtime_t now = state->now, u_lat = state->u_lat;
  int pid = state->pid;

#ifdef __DEBUG_FILE__
    fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
            now, pid, PRINT_LOCK_STATE(LCK_UNLA));
    __DEBUG_FILE_FLUSH;
#endif

  if (!state->is_free) {
    state->is_free = true;

    while (callback = COL_LLIST_pop(state->waiting_queue)) {
      UTL_callback_to_event(&lock_event, callback);
      
      lock_event.cnt = &thr_buffer;
      lock_event.cnt_size = sizeof(HTM_thr_buffer_t);
      
      thr_buffer.lock_success = false;
      
      SCHEDULE_EVENT(lock_event, now + u_lat);
      free(callback);
    }
  }

  UTL_callback_to_event(&unlock_event, &(buffer->callback));
  unlock_event.cnt = &thr_buffer;
  unlock_event.cnt_size = sizeof (HTM_thr_buffer_t);
  thr_buffer.acc_lat = u_lat;

  SCHEDULE_EVENT(unlock_event, now + u_lat);
}

void HTM_lock_state_unlock(HTM_lock_state_t * state,
        HTM_lock_buf_t * buffer) {

  UTL_event_t lock_event, unlock_event;
  UTL_callback_t * callback;
  HTM_thr_buffer_t thr_buffer;
  simtime_t now = state->now, u_lat = state->u_lat;
  int pid = state->pid;

#ifdef __DEBUG_FILE__
    fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
            now, pid, PRINT_LOCK_STATE(LCK_UNLC));
    __DEBUG_FILE_FLUSH;
#endif

  if (!state->is_free) {
    state->is_free = true;

    if (callback = COL_LLIST_pop(state->waiting_queue)) {
      UTL_callback_to_event(&lock_event, callback);
      
      lock_event.cnt = &thr_buffer;
      lock_event.cnt_size = sizeof(HTM_thr_buffer_t);
      
      thr_buffer.lock_success = false;
      
      SCHEDULE_EVENT(lock_event, now + u_lat);
      free(callback);
      state->is_free = false;
    }
  }

  UTL_callback_to_event(&unlock_event, &(buffer->callback));
  unlock_event.cnt = &thr_buffer;
  unlock_event.cnt_size = sizeof (HTM_thr_buffer_t);
  thr_buffer.acc_lat = u_lat;

  SCHEDULE_EVENT(unlock_event, now + u_lat);
}

void HTM_lock_state_check_locked(HTM_lock_state_t * state,
    HTM_lock_buf_t * buffer) {

  simtime_t now = state->now;
  HTM_thr_buffer_t thr_buffer;
  UTL_event_t event;

  UTL_callback_to_event(&event, &(buffer->callback));

  event.cnt = &thr_buffer;
  event.cnt_size = sizeof (thr_buffer);

  thr_buffer.is_locked = !state->is_free;
  thr_buffer.acc_lat = DELTA_TIME;

  SCHEDULE_EVENT(event, now + DELTA_TIME);
}

void HTM_lock_state_add_to_lock(HTM_lock_state_t * state,
    HTM_lock_buf_t * buffer) {

  simtime_t now = state->now;
  HTM_thr_buffer_t thr_buffer;
  UTL_event_t event;

  UTL_callback_to_event(&event, &(buffer->callback));

  event.cnt = &thr_buffer;
  event.cnt_size = sizeof (thr_buffer);

  thr_buffer.is_locked = !state->is_free;
  thr_buffer.acc_lat = DELTA_TIME;

  COL_LLIST_add(state->waiting_queue, UTL_callback_dump(&(buffer->callback)));

  SCHEDULE_EVENT(event, now + DELTA_TIME);
}
