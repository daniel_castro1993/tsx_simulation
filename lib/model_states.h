#ifndef MODEL_STATES_H
#  define MODEL_STATES_H

#  include "utils.h"

typedef int* mod_state_t;

mod_state_t mod_newState();
mod_state_t cpyState(mod_state_t state);
mod_state_t mod_cpyState(mod_state_t dst, mod_state_t src);

int mod_cmpState(mod_state_t state1, mod_state_t state2);
bool mod_isStateValid(mod_state_t state);

void mod_generateStates(int threads, int budget);
int mod_getIndexState(mod_state_t state);
int mod_setStateProb(mod_state_t state, double prob);
void mod_accReset();
void mod_addAcc(simtime_t now, mod_state_t state);
void mod_addDelta(simtime_t delta, mod_state_t state);

void mod_addDandAbort(simtime_t delta, mod_state_t state);
void mod_addNonDandAbort(simtime_t delta, mod_state_t state);
void mod_addCommit(simtime_t delta, mod_state_t state);
void mod_incEnter(mod_state_t state);
void mod_incContinue(mod_state_t state);
void mod_incExit(mod_state_t state);

void mod_addAvgL(simtime_t avg_L, mod_state_t state);
void mod_incCommit(mod_state_t state);
void mod_incAbort(mod_state_t state);
void mod_probFromAcc();
void mod_printState(FILE * fp, mod_state_t state);
void mod_printAllProbs(FILE * fp);
void mod_printTopProbs(FILE * fp, int topAmount);
void mod_printBotProbs(FILE * fp, int topAmount);
void mod_printUnreachable(FILE * fp, int lineAmount);

bool mod_isInit();
void mod_setInit(bool isInit);

void mod_set_L(int L);
void mod_set_C(double C);
void mod_set_D(int D);
void mod_set_P_read(double P_read);

double mod_compute_lambda(int active_txs);
double mod_compute_R(int active_txs);
double mod_compute_P_R_L(int active_txs, double * P_R);
double mod_compute_R_total();
double mod_compute_P_R_L_total();
double mod_compute_X(double avg_notx_time, double avg_tran_time,
        double avg_serl_time);

typedef struct _mod_states_t {
  int size, threads, budget, L, D;
  double * probs, * acc, * probs_d, * deltas, * avg_L, * sd_L;
  double * d_dang, * d_nonDang, * d_commit;
  double * probs_e1, * probs_e2, * probs_e3;
  double * resp_e1, * resp_e2, * resp_e3;
  int * dang, * nonDang, * nbEnter, * nbExit, * nbContinue;
  int * count_avg_L, * commits, * commit_e3, * aborts;
  double P_read, C;
  simtime_t last_ts;
  mod_state_t * states;
} mod_states_t;

mod_states_t mod_states;

#endif /* MODEL_STATES_H */

