#include "htm.h"
#include "collections.h"

/**
 * 
 * @param state
 * @param process_id
 * @param now
 */
void HTM_sc_state_handler(HTM_sc_state_t * state, int pid, SIM_STATE event,
        simtime_t now, HTM_sc_buffer_t * buffer) {

  if (pid < 0 || pid >= n_prc_tot) {
    printf("ERROR[HTM_sc_state_handler]: invalid pid: %i \n", pid);
  }

  state->now = now;
  state->pid = pid;

  switch (event) {
    case SC_INIT:
      HTM_sc_state_init(state);
      break;
    case SC_MEMR:
      HTM_sc_state_memory_response(state, buffer);
      break;
    case SC_READ:
      HTM_sc_state_read(state, buffer);
      break;
    case SC_WRIT:
      HTM_sc_state_write(state, buffer);
      break;
    default:
      break;
  }
}

void pc_response(simtime_t next_tick, HTM_sc_buffer_t * buffer,
        SIM_STATE sc_event, simtime_t acc_lat) {

  HTM_pc_buffer_t pc_buffer;
  UTL_event_t pc_event;

  pc_buffer.acc_lat = acc_lat;
  pc_buffer.pc_event = buffer->pc_event;
  pc_buffer.sc_event = sc_event;
  pc_buffer.pc_pid = buffer->pc_pid;
  pc_buffer.thr_pid = buffer->thr_pid;
  pc_buffer.callback = buffer->callback;
  pc_buffer.addr = buffer->addr;
  pc_buffer.inst_id = buffer->inst_id;

  UTL_event_init(&pc_event);

  pc_event.cnt = &pc_buffer;
  pc_event.cnt_size = sizeof (HTM_pc_buffer_t);
  pc_event.pid = buffer->pc_pid;
  pc_event.event_code = PC_SCRP;

  if (!buffer->no_callback) {
    SCHEDULE_EVENT(pc_event, next_tick);
  }
}

void mem_response(simtime_t next_tick, HTM_sc_buffer_t * buffer,
        SIM_STATE sc_event, SIM_STATE mem_event_code, simtime_t acc_lat) {

  HTM_mem_buffer_t mem_buffer;
  UTL_event_t mem_event;

  mem_buffer.acc_lat = acc_lat;
  mem_buffer.pc_event = buffer->pc_event;
  mem_buffer.sc_event = sc_event;
  mem_buffer.callback = buffer->callback;
  mem_buffer.pc_pid = buffer->pc_pid;
  mem_buffer.thr_pid = buffer->thr_pid;
  mem_buffer.addr = buffer->addr;
  mem_buffer.inst_id = buffer->inst_id;
  mem_buffer.no_callback = buffer->no_callback;

  UTL_event_init(&mem_event);

  mem_event.pid = global_info.mem_pid;
  mem_event.event_code = mem_event_code;
  mem_event.cnt = &mem_buffer;
  mem_event.cnt_size = sizeof (HTM_mem_buffer_t);

  SCHEDULE_EVENT(mem_event, next_tick);
}

/**
 * 
 * @param state
 * @param now
 */
void HTM_sc_state_init(HTM_sc_state_t * state) {

  simtime_t now = state->now;
  int i, j, pid = state->pid;
  UTL_event_t event;

  UTL_event_init(&event);

  event.pid = pid;
  event.event_code = CHK_EXIT;

  SCHEDULE_EVENT(event, now + CHECK_EXIT_DELTA_TIME);

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_SC_STATE(SC_INIT));
  __DEBUG_FILE_FLUSH;
#endif

  state->r_lat = global_info.lat_sc_r;
  state->w_lat = global_info.lat_sc_w;
  state->lines = new(HTM_sc_line_t, N_LINES_SCACHE);

  for (i = 0; i < N_LINES_SCACHE; i++) {
    for (j = 0; j < N_ASSOC_SCACHE; j++) {
      state->lines[i].words[j].state = MESI_STATE_I;
    }
  }
}

void HTM_sc_state_memory_response(HTM_sc_state_t * state,
        HTM_sc_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x \n",
          now, pid, PRINT_SC_STATE(SC_MEMR), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  switch (buffer->sc_event) {
    case SC_READ:
    case SC_WRIT:
    default:

      pc_response(now + DELTA_TIME, buffer,
              buffer->sc_event, buffer->acc_lat);

      break;
  }
}

void HTM_sc_state_read(HTM_sc_state_t * state, HTM_sc_buffer_t * buffer) {

  simtime_t now = state->now, r_lat = state->r_lat;
  int pid = state->pid;
  int addr, nb_lines, nb_words;
  HTM_sc_line_t * line;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x ",
          now, pid, PRINT_SC_STATE(SC_READ), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_SCACHE;
  nb_words = N_ASSOC_SCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  word->timestamp = now;

  if (word->address == addr && word->state != MESI_STATE_I) {
    // found the word to read

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
    fprintf(__debug_file, " found \n");
    __DEBUG_FILE_FLUSH;
#endif

    pc_response(now + r_lat, buffer, SC_READ, r_lat + buffer->acc_lat);

  } else {

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
    fprintf(__debug_file, " not found \n");
    __DEBUG_FILE_FLUSH;
#endif

    word->address = addr;

    if (word->state == MESI_STATE_M) {
      mem_response(now + r_lat, buffer, SC_READ,
              MEM_WRIT, r_lat + buffer->acc_lat);

    } else {
      word->state = MESI_STATE_E;

      mem_response(now + r_lat, buffer, SC_READ,
              MEM_READ, r_lat + buffer->acc_lat);
    }
  }
}

void HTM_sc_state_write(HTM_sc_state_t * state, HTM_sc_buffer_t * buffer) {

  simtime_t now = state->now, w_lat = state->w_lat;
  int pid = state->pid;
  int addr, nb_lines, nb_words;
  HTM_sc_line_t * line;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x ",
          now, pid, PRINT_SC_STATE(SC_WRIT), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_SCACHE;
  nb_words = N_ASSOC_SCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  word->timestamp = now;

  if (word->address == addr
          && !(word->state & (MESI_STATE_M | MESI_STATE_I))) {
    // found the word to write

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
    fprintf(__debug_file, " found \n");
    __DEBUG_FILE_FLUSH;
#endif

    pc_response(now + w_lat, buffer, SC_WRIT, w_lat + buffer->acc_lat);
  } else {
    // or didn't find or is in MODIFIED state

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
    fprintf(__debug_file, " not found or state M\n");
    __DEBUG_FILE_FLUSH;
#endif

    word->address = addr;

    if (word->state == MESI_STATE_M) {
      mem_response(now + w_lat, buffer, SC_WRIT,
              MEM_WRIT, w_lat + buffer->acc_lat);

    } else {
      word->state = MESI_STATE_M;
      pc_response(now + w_lat, buffer, SC_WRIT, w_lat + buffer->acc_lat);
    }
  }
}

void HTM_sc_state_set_state(HTM_sc_state_t * state,
        HTM_sc_buffer_t * buffer) {

  simtime_t now = state->now;
  int pid = state->pid;
  int addr, nb_lines, nb_words;
  HTM_sc_line_t * line;
  HTM_cache_word_t * word;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_SC_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x \n",
          now, pid, PRINT_SC_STATE(SC_SETS), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  addr = SANITIZE_ADDRESS(buffer->addr);
  nb_lines = N_LINES_SCACHE;
  nb_words = N_ASSOC_SCACHE;

  FIND_CACHE_LINE(line, addr, state->lines, nb_lines);
  CACHE_LINE_FIND_WORD(word, addr, line, nb_words);

  if (word->address == addr && word->state != MESI_STATE_I) {
    word->state = buffer->state;
  }
}


