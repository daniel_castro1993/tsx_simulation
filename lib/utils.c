#include <stdarg.h>
#include <time.h>

#include "collections.h"
#include "utils.h"
#include "float.h"
#include "math.h"

UTL_event_t * UTL_event_new() {
  UTL_event_t * res = new(UTL_event_t, 1);
  set_zero(res, UTL_event_t, 1);
  res->scheduler = UTL_schedule;

  return res;
}

void UTL_event_init(UTL_event_t * event) {
  set_zero(event, UTL_event_t, 1);
  event->scheduler = UTL_schedule;
}

void UTL_event_destroy(UTL_event_t * event, bool free_event) {
  if (event->cnt_destroyer != NULL) {
    event->cnt_destroyer(event->cnt);
  }
  free(event->cnt);
  if (free_event) {
    free(event);
  }
}

UTL_event_t * UTL_event_copy(UTL_event_t * event) {
  UTL_event_t * res = memcpy(new(UTL_event_t, 1), event, sizeof (UTL_event_t));
  memcpy(res->cnt, event->cnt, event->cnt_size);

  return res;
}

UTL_event_t * UTL_callback_to_event(UTL_event_t * dest,
        UTL_callback_t * src) {

  UTL_event_init(dest);

  dest->pid = src->pid;
  dest->ts = src->ts;
  dest->event_code = src->event_code;

  if (dest->pid < 0 || dest->pid >= n_prc_tot) {
    printf("ERROR[UTL_callback_to_event]: invalid pid: %i \n", dest->pid);
  }

  return dest;
}

UTL_callback_t * UTL_callback_dump(UTL_callback_t * src) {
  UTL_callback_t * res = memcpy(new(UTL_callback_t, 1), src,
          sizeof (UTL_callback_t));

  return res;
}

void UTL_schedule(UTL_event_t * event) {
  ScheduleNewEvent(event->pid, event->ts, event->event_code,
          event->cnt, event->cnt_size);
}

void SCHEDULE_EVENT(UTL_event_t _NEXT_EVENT, simtime_t _NEXT_TICK) {
  _NEXT_EVENT.ts = nextafter(_NEXT_TICK, DBL_MAX);
  UTL_schedule(&_NEXT_EVENT);
  if (_NEXT_EVENT.pid < 0 || _NEXT_EVENT.pid >= n_prc_tot) {
    printf("ERROR[SCHEDULE_EVENT]: invalid pid %i \n", _NEXT_EVENT.pid);
  }
  if (_NEXT_EVENT.is_destroy) {
    UTL_event_destroy(&_NEXT_EVENT, false);
  }
}
