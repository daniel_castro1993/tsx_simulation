#include "htm.h"

void HTM_mem_state_handler(HTM_mem_state_t * state, int pid, SIM_STATE event,
        simtime_t now, HTM_mem_buffer_t * buffer) {

  state->now = now;
  state->pid = pid;

  switch (event) {
    case MEM_INIT:
      HTM_mem_state_init(state);
      break;
    case MEM_READ:
      HTM_mem_state_read(state, buffer);
      break;
    case MEM_WRIT:
      HTM_mem_state_write(state, buffer);
      break;
    default:
      break;
  }
}

void sc_response(simtime_t now, SIM_STATE mem_event,
        HTM_mem_buffer_t * buffer, simtime_t mem_lat) {

  HTM_sc_buffer_t sc_buffer;
  UTL_event_t sc_response;

  sc_buffer.acc_lat = mem_lat + buffer->acc_lat;
  sc_buffer.mem_event = mem_event;
  sc_buffer.pc_event = buffer->pc_event;
  sc_buffer.sc_event = buffer->sc_event;
  sc_buffer.pc_pid = buffer->pc_pid;
  sc_buffer.thr_pid = buffer->thr_pid;
  sc_buffer.addr = buffer->addr;
  sc_buffer.inst_id = buffer->inst_id;
  sc_buffer.callback = buffer->callback;

  UTL_event_init(&sc_response);

  sc_response.pid = global_info.sc_pid;
  sc_response.event_code = SC_MEMR;
  sc_response.cnt = &sc_buffer;
  sc_response.cnt_size = sizeof (HTM_sc_buffer_t);

  if (!buffer->no_callback) {
    SCHEDULE_EVENT(sc_response, now + mem_lat);
  }
}

void HTM_mem_state_init(HTM_mem_state_t * state) {

  simtime_t now = state->now;
  int pid = state->pid;
  UTL_event_t event;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_MEM_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "\n",
          now, pid, PRINT_MEM_STATE(MEM_INIT));
  __DEBUG_FILE_FLUSH;
#endif

  state->r_lat = global_info.lat_mem_r;
  state->w_lat = global_info.lat_mem_w;

  UTL_event_init(&event);

  event.pid = pid;
  event.event_code = CHK_EXIT;

  SCHEDULE_EVENT(event, now + CHECK_EXIT_DELTA_TIME);
}

void HTM_mem_state_read(HTM_mem_state_t * state, HTM_mem_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, r_lat = state->r_lat;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_MEM_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x\n",
          now, pid, PRINT_MEM_STATE(MEM_READ), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  sc_response(now, MEM_READ, buffer, r_lat);
}

void HTM_mem_state_write(HTM_mem_state_t * state, HTM_mem_buffer_t * buffer) {

  int pid = state->pid;
  simtime_t now = state->now, w_lat = state->w_lat;

#if defined(__DEBUG_FILE__) && !defined(__DISABLE_MEM_DEB__)
  fprintf(__debug_file, __LOG_TS_PID_EVENT "addr=0x%08x\n",
          now, pid, PRINT_MEM_STATE(MEM_WRIT), buffer->addr);
  __DEBUG_FILE_FLUSH;
#endif

  sc_response(now, MEM_WRIT, buffer, w_lat);
}
