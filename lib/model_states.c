#include <float.h>
#include <stdarg.h>
#include <stdio.h>
#include <sys/time.h>

#include "collections.h"
#include "model_states.h"
#include "htm.h"
#include "stats.h"

bool is_init = false;

int stateSize() {
  return mod_states.budget + 2;
}

mod_state_t mod_newState() {
  register int size = mod_states.budget + 2;
  mod_state_t res = new(int, size);
  set_zero(res, int, size);
  return res;
}

mod_state_t cpyState(mod_state_t state) {
  mod_state_t res = mod_newState();
  memcpy(res, state, sizeof (int) * stateSize());
  return res;
}

mod_state_t mod_cpyState(mod_state_t dst, mod_state_t src) {
  memcpy(dst, src, sizeof (int) * stateSize());
  return dst;
}

void mod_delState(mod_state_t state) {
  free(state);
}

int mod_cmpState(mod_state_t state1, mod_state_t state2) {
  int i, state_size = stateSize();

  for (i = 0; i < state_size; ++i) {
    if (state1[i] != state2[i]) {
      return state1[i] - state2[i];
    }
  }

  return 0;
}

bool mod_isStateValid(mod_state_t state) {
  int i, count = 0;

  for (i = 0; i < stateSize(); ++i) {
    if (state[i] < 0) {
      return false;
    }
    count += state[i];
  }

  return count == global_info.tot_nb_thrs;
}

bool onlyXActs(mod_state_t state) {
  return state[mod_states.budget + 1] == 0 && state[mod_states.budget] == 0;
}

bool someSerl(mod_state_t state) {
  return state[mod_states.budget] != 0;
}

int countNotxs(mod_state_t state) {
  return state[mod_states.budget + 1];
}

int countXActs(mod_state_t state) {
  return mod_states.budget
          - state[mod_states.budget] - state[mod_states.budget + 1];
}

int countSerls(mod_state_t state) {
  return state[mod_states.budget];
}

bool eqState(void * state1, void * state2) {
  return mod_cmpState(state1, state2) == 0;
}

bool checkInStates(COL_llist_t * state_list,
        mod_state_t state, mod_state_t out_state) {

  mod_state_t found = COL_LLIST_find(state_list, eqState, out_state);

  return found == NULL && mod_cmpState(state, out_state) != 0;
}

void generateOutStates(COL_llist_t * state_list, mod_state_t state) {

  mod_state_t store_state;
  int i, j, k, l, budget = mod_states.budget;

  // non-transactions are able to finish
  // and start other non-transactions
  if (state[budget + 1] > 0) {
    store_state = cpyState(state);
    store_state[budget + 1]--;
    store_state[0]++;
    if (checkInStates(state_list, state, store_state)) {
      COL_LLIST_add(state_list, store_state);
    } else {
      mod_delState(store_state);
    }
  }

  // check if there is serialized mode transaction
  if (state[budget] > 0) {
    // or one serialized transaction starts
    // a new transaction or a non-transaction

    // starts a transaction
    store_state = cpyState(state);
    store_state[budget]--;
    store_state[0]++;
    if (checkInStates(state_list, state, store_state)) {
      COL_LLIST_add(state_list, store_state);
    } else {
      mod_delState(store_state);
    }

    // starts a non-transaction
    store_state = cpyState(state);
    store_state[budget]--;
    store_state[budget + 1]++;
    if (checkInStates(state_list, state, store_state)) {
      COL_LLIST_add(state_list, store_state);
    } else {
      mod_delState(store_state);
    }

  } else {
    if (state[budget - 1] > 0) {
      // someone enter serialized mode, shift
      // all transactions right
      store_state = cpyState(state);
      for (i = 0; i < budget; i++) {
        j = budget - i;
        k = j - 1;
        store_state[j] += store_state[k];
        store_state[k] = 0;
      }
      if (checkInStates(state_list, state, store_state)) {
        COL_LLIST_add(state_list, store_state);
      } else {
        mod_delState(store_state);
      }
    }

    for (i = 0; i < budget; i++) {
      if (state[i] == 0) {
        continue;
      }

      // starts other transaction
      store_state = cpyState(state);
      store_state[i]--;
      store_state[0]++;
      if (checkInStates(state_list, state, store_state)) {
        COL_LLIST_add(state_list, store_state);
      } else {
        mod_delState(store_state);
      }

      // starts other non-transaction
      store_state = cpyState(state);
      store_state[i]--;
      store_state[budget + 1]++;
      if (checkInStates(state_list, state, store_state)) {
        COL_LLIST_add(state_list, store_state);
      } else {
        mod_delState(store_state);
      }
    }

    // transations may abort and reduce their budget by 1
    for (i = 0; i < budget - 1; i++) {
      if (state[i] == 0) {
        continue;
      }

      store_state = cpyState(state);
      store_state[i]--;
      store_state[i + 1]++;
      if (checkInStates(state_list, state, store_state)) {
        COL_LLIST_add(state_list, store_state);
      } else {
        mod_delState(store_state);
      }
    }

    // transations may abort due to capacity
    for (i = 0; i < budget - 1; i++) {
      if (state[i] == 0) {
        continue;
      }

      store_state = cpyState(state);
      store_state[i]--;
      store_state[budget]++;
      for (l = 0; l < budget; l++) {
        j = budget - l;
        k = j - 1;
        store_state[j] += store_state[k];
        store_state[k] = 0;
      }
      if (checkInStates(state_list, state, store_state)) {
        COL_LLIST_add(state_list, store_state);
      } else {
        mod_delState(store_state);
      }
    }
  }
}

bool mod_isInit() {
  return is_init;
}

void mod_setInit(bool isInit) {
  is_init = isInit;
}

void mod_generateStates(int threads, int budget) {

  mod_states.threads = threads;
  mod_states.budget = budget;

  COL_llist_t * state_list = COL_LLIST_new();
  mod_state_t state = mod_newState();
  mod_state_t store_state;
  int i, size = 0, last_size;

  state[0] = threads;
  store_state = cpyState(state);
  COL_LLIST_add(state_list, store_state);

  do {
    COL_it_t it;
    last_size = size;
    size = COL_LLIST_size(state_list);

    COL_LLIST_it_new(state_list, &it);

    for (i = 0; i < last_size; ++i) {
      COL_LLIST_it_next(&it);
    }

    for (i = last_size; i < size; ++i) {
      mod_state_t some_state = (mod_state_t) COL_LLIST_it_next(&it);
      generateOutStates(state_list, some_state);
    }
  } while (COL_LLIST_size(state_list) != size);

  mod_states.size = COL_LLIST_size(state_list);
  mod_states.budget = budget;
  mod_states.threads = threads;
  mod_states.states = (int**) COL_LLIST_to_array(state_list);
  mod_states.probs = new_init(double, mod_states.size);
  mod_states.probs_d = new_init(double, mod_states.size);
  mod_states.avg_L = new_init(double, mod_states.size);
  mod_states.sd_L = new_init(double, mod_states.size);
  mod_states.count_avg_L = new_init(int, mod_states.size);
  mod_states.acc = new_init(double, mod_states.size);
  mod_states.deltas = new_init(double, mod_states.size);
  mod_states.d_dang = new_init(double, mod_states.size);
  mod_states.d_nonDang = new_init(double, mod_states.size);
  mod_states.d_commit = new_init(double, mod_states.size);
  mod_states.commits = new_init(int, mod_states.size);
  mod_states.commit_e3 = new_init(int, mod_states.size);
  mod_states.aborts = new_init(int, mod_states.size);
  mod_states.dang = new_init(int, mod_states.size);
  mod_states.nonDang = new_init(int, mod_states.size);
  mod_states.nbEnter = new_init(int, mod_states.size);
  mod_states.nbContinue = new_init(int, mod_states.size);
  mod_states.nbExit = new_init(int, mod_states.size);

  mod_states.probs_e1 = new_init(double, mod_states.size);
  mod_states.probs_e2 = new_init(double, mod_states.size);
  mod_states.probs_e3 = new_init(double, mod_states.size);

  mod_states.resp_e1 = new_init(double, mod_states.size);
  mod_states.resp_e2 = new_init(double, mod_states.size);
  mod_states.resp_e3 = new_init(double, mod_states.size);

  COL_LLIST_destroy(state_list, NULL);
}

int mod_getIndexState(mod_state_t state) {

  mod_state_t * it,
          * begin = mod_states.states,
          * end = begin + mod_states.size;

  for (it = begin; it < end; ++it) {
    if (eqState(*it, state)) {
      return it - begin;
    }
  }

  return -1;
}

int mod_setStateProb(mod_state_t state, double prob) {

  int index = mod_getIndexState(state);

  mod_states.probs[index] = prob;
}

void mod_accReset() {
  int i;

  for (i = 0; i < mod_states.size; ++i) {
    mod_states.acc[i] = 0;
    mod_states.deltas[i] = 0;
    mod_states.d_dang[i] = 0;
    mod_states.d_nonDang[i] = 0;
    mod_states.d_commit[i] = 0;
    mod_states.aborts[i] = 0;
    mod_states.dang[i] = 0;
    mod_states.nonDang[i] = 0;
    mod_states.commit_e3[i] = 0;
    mod_states.commits[i] = 0;
    mod_states.avg_L[i] = 0;
    mod_states.sd_L[i] = 0;
    mod_states.count_avg_L[i] = 0;
    mod_states.nbEnter[i] = 0;
    mod_states.nbContinue[i] = 0;
    mod_states.nbExit[i] = 0;
  }
}

void mod_addAcc(simtime_t now, mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.acc[index] += now - mod_states.last_ts;
    mod_states.last_ts = now;
  }
}

void mod_addDelta(simtime_t delta, mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.deltas[index] += delta;
  }
}

void mod_addAvgL(simtime_t avg_L, mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.count_avg_L[index]++;
    if (mod_states.count_avg_L[index] == 1) {
      mod_states.avg_L[index] = avg_L;
    } else {
      double diff = avg_L - mod_states.avg_L[index];
      double count = mod_states.count_avg_L[index];
      if (count > 1) {
        double r = (count - 2) / (count - 1);
        double pow_sd = mod_states.sd_L[index];
        double pow_diff = pow(diff, 2);
        mod_states.sd_L[index] = r * pow_sd + pow_diff / count;
      }
      mod_states.avg_L[index] += diff / count;
    }
  }
}

void mod_addDandAbort(simtime_t delta, mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.dang[index]++;
    mod_states.d_dang[index] += delta;
  }
}

void mod_addNonDandAbort(simtime_t delta, mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.nonDang[index]++;
    mod_states.d_nonDang[index] += delta;
  }
}

void mod_addCommit(simtime_t delta, mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.commit_e3[index]++;
    mod_states.d_commit[index] += delta;
  }
}

void mod_incEnter(mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.nbEnter[index]++;
  }
}

void mod_incContinue(mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.nbContinue[index]++;
  }
}

void mod_incExit(mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.nbExit[index]++;
  }
}

void mod_incCommit(mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.commits[index] += 1;
  }
}

void mod_incAbort(mod_state_t state) {
  int index = mod_getIndexState(state);

  if (index != -1) {
    mod_states.aborts[index] += 1;
  }
}

void mod_probFromAcc() {
  int i;
  register int size = mod_states.size;
  double total = 0, total_ev = 0;

  for (i = 0; i < size; ++i) {
    total += mod_states.acc[i];
  }

  for (i = 0; i < size; ++i) {
    mod_states.probs[i] = mod_states.acc[i] / total;
  }

  total = 0;
  for (i = 0; i < size; ++i) {
    total += mod_states.deltas[i];
  }

  for (i = 0; i < size; ++i) {
    mod_states.probs_d[i] = mod_states.deltas[i] / total;

    total_ev = mod_states.dang[i];
    total_ev += mod_states.nonDang[i];
    total_ev += mod_states.commit_e3[i];
    mod_states.probs_e1[i] = (double) mod_states.dang[i] / total_ev;
    mod_states.probs_e2[i] = (double) mod_states.nonDang[i] / total_ev;
    mod_states.probs_e3[i] = (double) mod_states.commit_e3[i] / total_ev;

    mod_states.resp_e1[i] = mod_states.d_dang[i]
            / (double) mod_states.dang[i];
    mod_states.resp_e2[i] = mod_states.d_nonDang[i]
            / (double) mod_states.nonDang[i];
    mod_states.resp_e3[i] = mod_states.d_commit[i]
            / (double) mod_states.commit_e3[i];
  }
}

void mod_stateToStr(mod_state_t state, char * str) {

  const char * format = mod_states.threads < 10 ?
          "%i" : mod_states.threads < 100 ? "%2i" : "%3i";
  int i;
  register int size = stateSize();
  int length = 0;

  length = sprintf(str, "[");
  for (i = 0; i < size; ++i) {
    length += sprintf(str + length, format, state[i]);
    if (i != size - 1) {
      length += sprintf(str + length, " ");
    }
  }
  length += sprintf(str + length, "]");
}

void mod_printState(FILE * fp, mod_state_t state) {
  char str[128];
  mod_stateToStr(state, str);
  fprintf(fp, "%s", str);
}

void mod_printAllProbs(FILE * fp) {
  int i;
  register int size = mod_states.size;

  for (i = 0; i < size; ++i) {
    char str[128];
    mod_stateToStr(mod_states.states[i], str);
    fprintf(fp, "%30s, %15.8e\n", str, mod_states.probs[i]);
  }
}

void mod_printTopProbs(FILE * fp, int topAmount) {

  int i, j, k;
  int * topStates = new(int, topAmount);
  register int size = mod_states.size;

  memset(topStates, -1, sizeof (int) * topAmount);

  for (i = 0; i < size; ++i) {
    for (j = 0; j < topAmount; ++j) {
      if (mod_states.probs_d[i] == 0) {
        break;
      }
      if (topStates[j] == -1) {
        topStates[j] = i;
        break;
      }
      // the difference to bottom is the less than operator
      if (mod_states.probs_d[topStates[j]] < mod_states.probs_d[i]) {
        for (k = topAmount - 1; k > j; --k) {
          topStates[k] = topStates[k - 1];
        }
        topStates[j] = i;
        break;
      }
    }
  }

  for (i = 0; i < topAmount; ++i) {
    if (topStates[i] != -1) {
      double a = mod_states.aborts[topStates[i]];
      double c = mod_states.commits[topStates[i]];

      double P_e1 = mod_states.probs_e1[topStates[i]];
      double P_e2 = mod_states.probs_e2[topStates[i]];
      double P_e3 = mod_states.probs_e3[topStates[i]];

      double R_e1 = mod_states.resp_e1[topStates[i]];
      double R_e2 = mod_states.resp_e2[topStates[i]];
      double R_e3 = mod_states.resp_e3[topStates[i]];

      double P_a = a / (a + c);
      double X = c / mod_states.deltas[topStates[i]];

      mod_printState(fp, mod_states.states[topStates[i]]);
      fprintf(fp, ": %8.4f%% (L: %6.3f ? %6.3f, P_a: %7.4f%%, "
              "P_e1: %7.4f%%, P_e2: %7.4f%%, P_e3: %7.4f%%, R_e1: %7.4f, "
              "R_e2: %7.4f, R_e3: %7.4f, X: %7.4f, enter: %6i, "
              "exit: %6i, continue: %6i)\n",
              mod_states.probs_d[topStates[i]] * 100.0,
              mod_states.avg_L[topStates[i]],
              pow(mod_states.sd_L[topStates[i]], 0.5),
              P_a * 100.0, P_e1 * 100.0, P_e2 * 100.0,
              P_e3 * 100.0, R_e1, R_e2, R_e3, X,
              mod_states.nbEnter[topStates[i]],
              mod_states.nbExit[topStates[i]],
              mod_states.nbContinue[topStates[i]]);
    }
  }
}

void mod_printBotProbs(FILE * fp, int botAmount) {

  int i, j, k;
  int * botStates = new(int, botAmount);
  register int size = mod_states.size;

  memset(botStates, -1, sizeof (int) * botAmount);

  for (i = 0; i < size; ++i) {
    for (j = 0; j < botAmount; ++j) {
      if (mod_states.probs_d[i] == 0) {
        break;
      }
      if (botStates[j] == -1) {
        botStates[j] = i;
        break;
      }
      // the difference to top is the greater than operator
      if (mod_states.probs_d[botStates[j]] > mod_states.probs_d[i]) {
        for (k = botAmount - 1; k > j; --k) {
          botStates[k] = botStates[k - 1];
        }
        botStates[j] = i;
        break;
      }
    }
  }
  for (i = 0; i < botAmount; ++i) {
    if (botStates[i] != -1) {
      double a = mod_states.aborts[botStates[i]];
      double c = mod_states.commits[botStates[i]];

      double P_e1 = mod_states.probs_e1[botStates[i]];
      double P_e2 = mod_states.probs_e2[botStates[i]];
      double P_e3 = mod_states.probs_e3[botStates[i]];

      double R_e1 = mod_states.resp_e1[botStates[i]];
      double R_e2 = mod_states.resp_e2[botStates[i]];
      double R_e3 = mod_states.resp_e3[botStates[i]];

      double P_a = a / (a + c);
      double X = c / mod_states.deltas[botStates[i]];

      mod_printState(fp, mod_states.states[botStates[i]]);
      fprintf(fp, ": %8.4f%% (L: %6.3f ? %6.3f, P_a: %7.4f%%, "
              "P_e1: %7.4f%%, P_e2: %7.4f%%, P_e3: %7.4f%%, R_e1: %7.4f, "
              "R_e2: %7.4f, R_e3: %7.4f, X: %7.4f, enter: %6i, "
              "exit: %6i, continue: %6i)\n",
              mod_states.probs_d[botStates[i]] * 100.0,
              mod_states.avg_L[botStates[i]],
              pow(mod_states.sd_L[botStates[i]], 0.5),
              P_a * 100.0, P_e1 * 100.0, P_e2 * 100.0,
              P_e3 * 100.0, R_e1, R_e2, R_e3, X,
              mod_states.nbEnter[botStates[i]],
              mod_states.nbExit[botStates[i]],
              mod_states.nbContinue[botStates[i]]);
    }
  }
}

void mod_printUnreachable(FILE * fp, int lineAmount) {

  int i, j;
  register int size = mod_states.size;

  j = 0;
  for (i = 0; i < size; ++i) {
    if (mod_states.probs_d[i] == 0) {
      mod_printState(fp, mod_states.states[i]);
      fprintf(fp, " ");
      j++;
      if (lineAmount == 0 || (j % lineAmount) == 0) {
        fprintf(fp, "\n");
      }
    }
  }
  fprintf(fp, "\n");
}

double compute_H_i(double lambda, double i) {
  return (1 - SQ(mod_states.P_read)) * lambda * i / mod_states.D;
}

double mod_compute_lambda(int nb_txs) {
  double c_over_l = ((double) mod_states.C / (double) mod_states.L);
  return (double) max(nb_txs - 1, 0) / c_over_l;
}

void mod_set_L(int L) {
  mod_states.L = L;
}

void mod_set_C(double C) {
  mod_states.C = C;
}

void mod_set_D(int D) {
  mod_states.D = D;
}

void mod_set_P_read(double P_read) {
  mod_states.P_read = P_read;
}

double mod_compute_R(int active_txs) {
  double p_r[mod_states.L];
  double res = 0, lambda = mod_compute_lambda(active_txs);
  double c_over_l = ((double) mod_states.C / (double) mod_states.L);
  int i;

  if (lambda == 0) {
    return mod_states.C;
  }

  mod_compute_P_R_L(active_txs, p_r);

  res = p_r[mod_states.L - 1] * mod_states.C;

  for (i = 0; i < mod_states.L - 1; i++) {
    double _i = i;
    double H_i = compute_H_i(lambda, _i + 1.0);
    double to_sum = 0;

    to_sum = p_r[i] * ((_i + 1.0) * c_over_l * (1.0 - exp(-H_i * c_over_l))
            + 1.0 / H_i - exp(-H_i * c_over_l) * (c_over_l + 1.0 / H_i));

    res += to_sum;
  }

  return res;
}

double mod_compute_P_R_L(int active_txs, double * P_R) {
  int i;
  double c_over_l = ((double) mod_states.C / (double) mod_states.L);
  double lambda = mod_compute_lambda(active_txs);
  double p_r[mod_states.L], * vec;

  vec = P_R == NULL ? p_r : P_R;

  vec[0] = 1;

  for (i = 1; i < mod_states.L; i++) {
    double _i = i;
    double H_i = compute_H_i(lambda, _i - 1.0);

    vec[i] = vec[i - 1] * exp(-H_i * c_over_l);
  }

  return vec[mod_states.L - 1];
}

double mod_compute_P_R_L_total() {
  int i;
  double res = 0;

  for (i = 0; i < mod_states.size; ++i) {
    if (mod_states.probs[i] > 0) {

      if (someSerl(mod_states.states[i])) {
        res += 1 * mod_states.probs[i]; // TODO:
      } else {
        res += mod_compute_P_R_L(countXActs(mod_states.states[i]), NULL)
                * mod_states.probs[i];
      }
    }
  }

  return res;
}

double mod_compute_R_total() {
  int i;
  double res = 0;

  for (i = 0; i < mod_states.size; ++i) {
    if (mod_states.probs[i] > 0) {

      if (someSerl(mod_states.states[i])) {
        res += mod_states.C * mod_states.probs[i];
      } else {
        res += mod_compute_R(countXActs(mod_states.states[i]))
                * mod_states.probs[i];
      }
    }
  }
  return res;
}

double mod_compute_X(double avg_notx_time, double avg_tran_time,
        double avg_serl_time) {

  int i;
  double res = 0, nb_xacts, nb_notx;

  for (i = 0; i < mod_states.size; ++i) {
    nb_xacts = countXActs(mod_states.states[i]);
    nb_notx = countNotxs(mod_states.states[i]);
    if (mod_states.probs[i] > 0) {

      if (avg_notx_time > 0) {
        res += mod_states.probs[i] * nb_notx
                / avg_notx_time;
      }
      if (someSerl(mod_states.states[i])) {
        if (avg_serl_time > 0) {
          res += mod_states.probs[i] / avg_serl_time;
        }
      } else {
        if (avg_tran_time > 0) {
          res += mod_states.probs[i]
                  / avg_tran_time * mod_compute_P_R_L(nb_xacts, NULL);
        }
      }
    }
  }
  return res;
}
