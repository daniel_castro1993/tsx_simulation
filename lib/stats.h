#ifndef STATS_H
#  define STATS_H

#  include <math.h>

#  define SQ(X)             ((X)*(X))
#  define CRITICAL_VALUE    1.645 // confidence 90%
#  define CI_ALPHA          0.1   // 100% - 90%

#  define CALC_SUM(_DATA, _SAMPLE_SIZE)                                       \
  ({                                                                          \
    int __i;                                                                  \
    double __value = 0;                                                       \
    for(__i = 0; __i < _SAMPLE_SIZE; __i++) {                                 \
      __value += _DATA[__i];                                                  \
    }                                                                         \
    __value;                                                                  \
  })

#  define CALC_AVG(_DATA, _SAMPLE_SIZE)                                       \
  ({                                                                          \
    CALC_SUM(_DATA, _SAMPLE_SIZE) / (double) _SAMPLE_SIZE;                    \
  })

#  define CALC_SQ_STDDEV(_DATA, _SAMPLE_SIZE, _AVG)                           \
  ({                                                                          \
    int __i;                                                                  \
    double __value = 0, __diff;                                               \
    for(__i = 0; __i < _SAMPLE_SIZE; __i++) {                                 \
      __value += SQ(_DATA[__i] - _AVG);                                       \
    }                                                                         \
    __value / (double) (_SAMPLE_SIZE - 1);                                    \
  })

#  define CALC_CI(_AVG, _STDDEV, _SQRT_SAMPLE_SIZE, _L_VAL, _R_VAL)           \
  ({                                                                          \
    _L_VAL = _AVG - CRITICAL_VALUE * _STDDEV / _SQRT_SAMPLE_SIZE;             \
    _R_VAL = _AVG + CRITICAL_VALUE * _STDDEV / _SQRT_SAMPLE_SIZE;             \
    _R_VAL - _L_VAL;                                                          \
  })                                                                          \
  
#  define IS_WITHIN_CI(_AVG, _CI_WIDTH)                                       \
  ({                                                                          \
    _CI_WIDTH < _AVG * CI_ALPHA;                                              \
  })                                                                          \

#endif /* STATS_H */

