#ifndef WORKLOAD_H
#  define	WORKLOAD_H

#  include "collections.h"
#  include "utils.h"

typedef struct UTL_transaction_t_ {
  int id;
  simtime_t start;
  COL_llist_t* ops;
} UTL_transaction_t;

typedef struct UTL_application_t_ {
  COL_it_t current_transaction;
  COL_llist_t* transactions;
} UTL_application_t;

UTL_application_t* UTL_load_application(const char* file_name);
UTL_transaction_t *UTL_app_get_next_transaction();
void UTL_destroi_application(UTL_transaction_t* app);

#endif	/* WORKLOAD_H */

