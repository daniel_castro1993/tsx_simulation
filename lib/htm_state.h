#ifndef HTM_STATE_H
#  define HTM_STATE_H

#  include "htm.h"

// representes the time between read/write accesses to memory
#  ifndef T_TCB
#    define T_TCB         5
#  endif

#  define NB_TASKS        999999999
#  define STATS_SAMPLE    1000

#  define LAMBDA_ACCS     0.76

#  define STATE_THR_STACK     THR_INIT:                                       \
                              case THR_INTL:                                  \
                              case THR_TXIN:                                  \
                              case THR_TXCK:                                  \
                              case THR_TRAN:                                  \
                              case THR_TXBG:                                  \
                              case THR_TXAB:                                  \
                              case THR_TXCM:                                  \
                              case THR_TXSP:                                  \
                              case THR_NOTX:                                  \
                              case THR_SERL:                                  \
                              case THR_SRSP:                                  \
                              case THR_SRRS:                                  \
                              case THR_TXRS:                                  \
                              case THR_NTRS:                                  \
                              case THR_GETS

#  define PRINT_THR_STATE(state) (state == THR_INIT ? "THR_INIT" :            \
                                  state == THR_INTL ? "THR_INTL" :            \
                                  state == THR_TXIN ? "THR_TXIN" :            \
                                  state == THR_TXCK ? "THR_TXCK" :            \
                                  state == THR_TRAN ? "THR_TRAN" :            \
                                  state == THR_TXBG ? "THR_TXBG" :            \
                                  state == THR_TXAB ? "THR_TXAB" :            \
                                  state == THR_TXCM ? "THR_TXCM" :            \
                                  state == THR_TXSP ? "THR_TXSP" :            \
                                  state == THR_NOTX ? "THR_NOTX" :            \
                                  state == THR_SERL ? "THR_SERL" :            \
                                  state == THR_SRSP ? "THR_SRSP" :            \
                                  state == THR_SRRS ? "THR_SRRS" :            \
                                  state == THR_TXRS ? "THR_TXRS" :            \
                                  state == THR_NTRS ? "THR_NTRS" :            \
                                  state == THR_GETS ? "THR_GETS" : "")

#  define NEW_THR_STATE(_STATE)                                               \
  _STATE = new(HTM_thr_state_t, 1);                                           \
  set_zero(_STATE, HTM_thr_state_t, 1);

#  define NEW_THR_BUFFER(_BUFFER)                                             \
  _BUFFER = new(HTM_thr_buffer_t, 1);                                         \
  set_zero(_BUFFER, HTM_thr_buffer_t, 1);

typedef struct _HTM_thread_statistics_t {
  int stats_pointer[NB_STATS], counter[NB_STATS];

  int * budget_at_commit;
  int * ops_before_abort;
  simtime_t * stat_time[NB_STATS];
} HTM_thr_stats_t;

typedef struct _HTM_thr_state_counter_t {
  simtime_t begin_ts, gen_length, lost_assync;
  int nb_acc_mem_tot, nb_acc_mem, * addrs;
  bool * is_write;
  budget_t current_budget;
} HTM_thr_state_counter_t;

typedef struct _HTM_thr_state_t {
  simtime_t now, last_ts, abort_ts;
  int pid, nb_tasks, current_tx_id, inst_id;
  bool lock_detected, tx_is_running, aborted, check_serial, is_broadcast;

  HTM_thr_state_counter_t counter;
  HTM_thr_stats_t stats;
} HTM_thr_state_t;

typedef struct _HTM_thr_buffer_t {
  int active_txs, addr, pc_op, tx_id, inst_id;
  simtime_t acc_lat;
  bool lock_success, is_locked, aborted, is_broadcast;
  HTM_cache_tx_t * tx;
  ABORT_CODE code;
} HTM_thr_buffer_t;


void HTM_thr_state_handler(HTM_thr_state_t * state, int pid, SIM_STATE event,
        simtime_t now, HTM_thr_buffer_t * buffer);


bool HTM_thr_state_is_end(HTM_thr_state_t * state);

/**
 * 
 * @param state
 * @param process_id
 * @param n_processors
 * @param now
 */
void HTM_thr_state_init(HTM_thr_state_t * state);

void HTM_thr_state_initial(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_reset_stats(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_transaction_init(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_transaction_check(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_transaction(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_transaction_begin(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_transaction_abort(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_transaction_commit(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_transaction_response(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_transaction_spinning(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_serial(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_serial_wait(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_serial_response(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_non_transaction(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_non_transac_response(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_end(HTM_thr_state_t*, HTM_thr_buffer_t*);
void HTM_thr_state_get_state(HTM_thr_state_t*, HTM_thr_buffer_t*);

#endif /* HTM_STATE_H */

